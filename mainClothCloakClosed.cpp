#include <DataDrivenOpt/SkinnedMeshClothUtility.h>
#include <boost/lexical_cast.hpp>
#include <CommonFile/Timing.h>

USE_PRJ_NAMESPACE

int main(int argc, char *argv[])
{
  if(argc < 3) {
    WARNING("Usage: mainClothCloakNarrow bodyFileName startTraj!")
    //e.g. bodyFileName="/datasetStaticFemale/136.data";
    return 0;
  }
  string bodyFileName=argv[1];
  sizeType startTraj=boost::lexical_cast<sizeType>(argv[2]),nrTraj=-1,nrFrm=-1;
  if(argc > 3)
    nrTraj=boost::lexical_cast<sizeType>(argv[3]);
  if(argc > 4)
    nrFrm=boost::lexical_cast<sizeType>(argv[4]);

  disableTiming();
  SkinnedMeshClothUtility body;
  INFOV("Reading %s!",bodyFileName.c_str())
  body.readSkinnedMesh(getSourcePath()+bodyFileName);
  ASSERT(body.generateCloak(0.2f,2.0f,0,30.0f,0.05f,1E2f,1E2f,1.0f))
  if(startTraj == -1) {
    INFOV("You are querying number of trajs, NOT: %ld!",body.nrTraj())
    return 0;
  } else if(startTraj == -2) {
    INFO("You are querying mesh topology!")
    boost::filesystem::ofstream os("clothCloakClosed.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    body.mesh().write(os,dat.get());
    return 0;
  } else {
    ASSERT_MSGV(startTraj >= 0 && startTraj < body.nrTraj(),"Invalid startTraj(%ld), should be in range [%ld,%ld)!",startTraj,0,body.nrTraj())
  }
  INFOV("Generating cloths for traj: %ld!",startTraj)
  body.simulateCloth(startTraj,nrTraj,nrFrm);
  INFO("Outputing to: clothData.data!")
  body.writeClothOnly("clothData.data");
  //body.writeDatasetVTK("clothData.data");
  return 0;
}
