#include "ClothCollision.h"
#include "AlglibToEigen.h"
#include <CommonFile/solvers/alglib/optimization.h>
#include <boost/filesystem/operations.hpp>

USE_PRJ_NAMESPACE

ClothCollision::CollisionHandler::CollisionHandler():_id(0) {}
void ClothCollision::CollisionHandler::handle(boost::shared_ptr<ClothMesh::ClothVertex> V1,boost::shared_ptr<ClothMesh::ClothTriangle> T2,const Vec3d n,const Vec4d& omg,scalarD t)
{
  const ClothMesh::ClothVertex& v0=*V1;
  const ClothMesh::ClothVertex& v1=*(T2->getV0());
  const ClothMesh::ClothVertex& v2=*(T2->getV1());
  const ClothMesh::ClothVertex& v3=*(T2->getV2());

  vector<scalarD> css;
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > tvV;
  tvV.push_back(v0._lastPos);
  tvV.push_back(v1._lastPos);
  tvV.push_back(v2._lastPos);
  tvV.push_back(v3._lastPos);
  tvV.push_back(interp1D(v0._lastPos,v0._pos,t));
  tvV.push_back(interp1D(v1._lastPos,v1._pos,t));
  tvV.push_back(interp1D(v2._lastPos,v2._pos,t));
  tvV.push_back(interp1D(v3._lastPos,v3._pos,t));
  tvV.push_back(tvV[4]+n);
  tvV.push_back(tvV[5]-n);
  tvV.push_back(tvV[6]-n);
  tvV.push_back(tvV[7]-n);

  boost::filesystem::create_directory("./coll");
  ostringstream oss;
  oss << "./coll/frm" << _id << ".vtk";
  VTKWriter<scalarD> os("coll",oss.str(),true);
  os.appendPoints(tvV.begin(),tvV.end());

  vector<Vec3i,Eigen::aligned_allocator<Vec3i> > tvI;
  tvI.push_back(Vec3i::Constant(0));
  tvI.push_back(Vec3i::Constant(4));
  css.push_back(0.0f);
  css.push_back(0.0f);
  os.appendCells(tvI.begin(),tvI.end(),VTKWriter<scalarD>::POINT);

  tvI.clear();
  tvI.push_back(Vec3i(1,2,3));
  tvI.push_back(Vec3i(5,6,7));
  css.push_back(1.0f);
  css.push_back(1.0f);
  os.appendCells(tvI.begin(),tvI.end(),VTKWriter<scalarD>::TRIANGLE);

  tvI.clear();
  tvI.push_back(Vec3i::Constant(0));
  tvI.push_back(Vec3i::Constant(1));
  tvI.push_back(Vec3i::Constant(2));
  tvI.push_back(Vec3i::Constant(3));
  tvI.push_back(Vec3i::Constant(4));
  tvI.push_back(Vec3i::Constant(5));
  tvI.push_back(Vec3i::Constant(6));
  tvI.push_back(Vec3i::Constant(7));
  css.push_back(2.0f);
  css.push_back(2.0f);
  css.push_back(2.0f);
  css.push_back(2.0f);
  css.push_back(2.0f);
  css.push_back(2.0f);
  css.push_back(2.0f);
  css.push_back(2.0f);
  os.appendCells(tvI.begin(),tvI.end(),VTKWriter<scalarD>::POINT);

  tvI.clear();
  tvI.push_back(Vec3i(4,8,0));
  tvI.push_back(Vec3i(5,9,0));
  tvI.push_back(Vec3i(6,10,0));
  tvI.push_back(Vec3i(7,11,0));
  css.push_back(3.0f);
  css.push_back(3.0f);
  css.push_back(3.0f);
  css.push_back(3.0f);
  os.appendCells(tvI.begin(),tvI.end(),VTKWriter<scalarD>::LINE);
  os.appendCustomData("Color",css.begin(),css.end());
  _id++;
}
void ClothCollision::CollisionHandler::handle(boost::shared_ptr<ClothMesh::ClothEdge> E1,boost::shared_ptr<ClothMesh::ClothEdge> E2,const Vec3d n,const Vec4d& omg,scalarD t)
{
  const ClothMesh::ClothVertex& v0=*(E1->_v[0]);
  const ClothMesh::ClothVertex& v1=*(E1->_v[1]);
  const ClothMesh::ClothVertex& v2=*(E2->_v[0]);
  const ClothMesh::ClothVertex& v3=*(E2->_v[1]);

  vector<scalarD> css;
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > tvV;
  tvV.push_back(v0._lastPos);
  tvV.push_back(v1._lastPos);
  tvV.push_back(v2._lastPos);
  tvV.push_back(v3._lastPos);
  tvV.push_back(interp1D(v0._lastPos,v0._pos,t));
  tvV.push_back(interp1D(v1._lastPos,v1._pos,t));
  tvV.push_back(interp1D(v2._lastPos,v2._pos,t));
  tvV.push_back(interp1D(v3._lastPos,v3._pos,t));
  tvV.push_back(tvV[4]+n);
  tvV.push_back(tvV[5]+n);
  tvV.push_back(tvV[6]-n);
  tvV.push_back(tvV[7]-n);

  boost::filesystem::create_directory("./coll");
  ostringstream oss;
  oss << "./coll/frm" << _id << ".vtk";
  VTKWriter<scalarD> os("coll",oss.str(),true);
  os.appendPoints(tvV.begin(),tvV.end());

  vector<Vec3i,Eigen::aligned_allocator<Vec3i> > tvI;
  tvI.push_back(Vec3i(0,1,0));
  tvI.push_back(Vec3i(2,3,0));
  tvI.push_back(Vec3i(4,5,0));
  tvI.push_back(Vec3i(6,7,0));
  css.push_back(0.0f);
  css.push_back(1.0f);
  css.push_back(0.0f);
  css.push_back(1.0f);
  os.appendCells(tvI.begin(),tvI.end(),VTKWriter<scalarD>::LINE);

  tvI.clear();
  tvI.push_back(Vec3i::Constant(0));
  tvI.push_back(Vec3i::Constant(1));
  tvI.push_back(Vec3i::Constant(2));
  tvI.push_back(Vec3i::Constant(3));
  tvI.push_back(Vec3i::Constant(4));
  tvI.push_back(Vec3i::Constant(5));
  tvI.push_back(Vec3i::Constant(6));
  tvI.push_back(Vec3i::Constant(7));
  css.push_back(2.0f);
  css.push_back(2.0f);
  css.push_back(2.0f);
  css.push_back(2.0f);
  css.push_back(2.0f);
  css.push_back(2.0f);
  css.push_back(2.0f);
  css.push_back(2.0f);
  os.appendCells(tvI.begin(),tvI.end(),VTKWriter<scalarD>::POINT);

  tvI.clear();
  tvI.push_back(Vec3i(4,8,0));
  tvI.push_back(Vec3i(5,9,0));
  tvI.push_back(Vec3i(6,10,0));
  tvI.push_back(Vec3i(7,11,0));
  css.push_back(3.0f);
  css.push_back(3.0f);
  css.push_back(3.0f);
  css.push_back(3.0f);
  os.appendCells(tvI.begin(),tvI.end(),VTKWriter<scalarD>::LINE);
  os.appendCustomData("Color",css.begin(),css.end());
  _id++;
}

ClothCollision::NarrowNode::NarrowNode():Serializable(typeid(NarrowNode).name()) {}
bool ClothCollision::NarrowNode::read(istream& is,IOData* dat)
{
  ASSERT_MSG(false,"Not Supported!");
  return false;
}
bool ClothCollision::NarrowNode::write(ostream& os,IOData* dat) const
{
  ASSERT_MSG(false,"Not Supported!");
  return false;
}
void ClothCollision::NarrowNode::buildBVH()
{
  sizeType nrT=(sizeType)_mesh->_tss.size();
  _bvh.resize(nrT);
  for(sizeType i=0; i<nrT; i++) {
    _bvh[i]._nrCell=1;
    _bvh[i]._parent=-1;
    _bvh[i]._cell=i;
    _mesh->_tss[i]->_activeTag=&_active;
  }
  refit(false);
  BVHBuilder<Node<sizeType,BBOX>,3>().buildBVH(_bvh);
  for(sizeType i=nrT; i<(sizeType)_bvh.size(); i++)
    _bvh[i]._cell=-1;
  //refit();
  //writeBVHByLevel(_bvh,boost::shared_ptr<ClothMesh::ClothTriangle>());
}
ClothCollision::BBOX ClothCollision::NarrowNode::refit(bool useLastPos)
{
  _vbb.resize(_mesh->_vss.size());
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)_mesh->_vss.size(); i++) {
    _vbb[i].reset();
    _vbb[i].setUnion(_mesh->_vss[i]->_pos.cast<scalar>());
    if(useLastPos)
      _vbb[i].setUnion(_mesh->_vss[i]->_lastPos.cast<scalar>());
    _vbb[i].enlarged((scalar)_thickness);
  }
  _ebb.resize(_mesh->_ess.size());
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)_mesh->_ess.size(); i++) {
    _ebb[i].reset();
    _ebb[i].setUnion(_vbb[_mesh->_ess[i]->_v[0]->_index]);
    _ebb[i].setUnion(_vbb[_mesh->_ess[i]->_v[1]->_index]);
  }
  for(sizeType i=0; i<(sizeType)_bvh.size(); i++) {
    _bvh[i]._bb.reset();
    if(_bvh[i]._cell >= 0) {
      const ClothMesh::ClothTriangle& t=*(_mesh->_tss[_bvh[i]._cell]);
      _bvh[i]._bb.setUnion(_ebb[t._e[0]->_index]);
      _bvh[i]._bb.setUnion(_ebb[t._e[1]->_index]);
      _bvh[i]._bb.setUnion(_ebb[t._e[2]->_index]);
    } else {
      _bvh[i]._bb.setUnion(_bvh[_bvh[i]._l]._bb);
      _bvh[i]._bb.setUnion(_bvh[_bvh[i]._r]._bb);
    }
  }
  if(!_active.empty()) {
    sizeType nrLeave=((sizeType)_bvh.size()+1)/2;
    ASSERT((sizeType)_active.size() == nrLeave || _active.size() == _bvh.size());

    _active.resize(_bvh.size());
    for(sizeType i=nrLeave; i<(sizeType)_bvh.size(); i++)
      _active[i]=_active[_bvh[i]._l]||_active[_bvh[i]._r];
  }
  if(_bvh.empty())
    return BBOX();
  else return _bvh.back()._bb;
}
bool ClothCollision::NarrowNode::operator<(const NarrowNode& other) const
{
  return _mesh<other._mesh;
}

static FORCE_INLINE void adjustNormalSign(const Vec3d v[4],const Vec4d& omg,Vec3d& n)
{
  if(n.dot(v[0]*omg[0]+v[1]*omg[1]+
           v[2]*omg[2]+v[3]*omg[3]) < 0.0f)
    n*=-1.0f;
}
static FORCE_INLINE void calcCoef(const Vec3d pt[4],const Vec3d v[4],scalarD c[4])
{
  Vec3d x21=pt[1]-pt[0];
  Vec3d x31=pt[2]-pt[0];
  Vec3d x41=pt[3]-pt[0];

  Vec3d v21=v[1]-v[0];
  Vec3d v31=v[2]-v[0];
  Vec3d v41=v[3]-v[0];

  Vec3d x21Xx31=x21.cross(x31);
  Vec3d v21Xv31=v21.cross(v31);
  Vec3d v21Cx31_x21Cv31=v21.cross(x31)+x21.cross(v31);

  c[0]=x21Xx31.dot(x41);
  c[1]=x21Xx31.dot(v41)+v21Cx31_x21Cv31.dot(x41);
  c[2]=v21Xv31.dot(x41)+v21Cx31_x21Cv31.dot(v41);
  c[3]=v21Xv31.dot(v41);
}
static FORCE_INLINE scalarD eval(const scalarD c[4],scalarD s)
{
  scalarD ret=c[3]*s+c[2];
  ret=ret*s+c[1];
  ret=ret*s+c[0];
  return ret;
}
static FORCE_INLINE bool secant(scalarD l,scalarD r,const scalarD c[4],scalarD& s, scalarD tol)
{
  //out of range
  if(l>r)return false;

  //already close to zero
  scalarD el=eval(c,l);
  if(abs(el) < tol) {
    s=l;
    return true;
  }

  //already close to zero
  scalarD er=eval(c,r);
  if(abs(er) < tol) {
    s=r;
    return true;
  }

  //no zero in interval
  if(el*er > 0.0f)
    return false;

  //secant search
  scalarD m,em;
  for(int i=0; i<1000; i++) {
    m=(el*r-er*l)/(el-er);
    em=eval(c,m);
    if(abs(em) < tol || abs(r-l) < ClothCollision::_timeRes) {
      s=m;
      return true;
    }

    if(el*em < 0.0f) {
      r=m;
      er=em;
    } else {
      l=m;
      el=em;
    }
  }
  return false;
}
static FORCE_INLINE int solveCubicAllSecant(scalarD l,scalarD r,const scalarD c[4],scalarD s[4])
{
  scalarD tol=1E-7f*(abs(c[0])+abs(c[1])+abs(c[2])+abs(c[3]));
  //determine the minimum and maximum
  //c[1] + (2*c[2]) * x + (3*c[3]) * x^2
  //sol=(-c[2] (+/-) sqrt(c[2]*c[2]-3*c[3]*c[1]) )/(3*c[3])
  scalarD sol0,sol1;
  scalarD deter=sqrt(c[2]*c[2]-3.0f*c[3]*c[1]);
  if(deter < 0.0f) {
    return secant(l,r,c,s[0],tol) ? 1 : 0;
  } else {
    sol0=(-c[2]-deter)/(3.0f*c[3]);
    sol1=(-c[2]+deter)/(3.0f*c[3]);
    if(sol0>sol1)swap(sol0,sol1);
    //scant search in each segment
    int nrSol=0;
    if(secant(l,
              min<scalarD>(sol0,r),
              c,s[nrSol],tol))nrSol++;
    if(secant(max<scalarD>(l,sol0),
              min<scalarD>(r,sol1),
              c,s[nrSol],tol))nrSol++;
    if(secant(max<scalarD>(l,sol1),
              r,
              c,s[nrSol],tol))nrSol++;
    return nrSol;
  }
}
static FORCE_INLINE bool testVT(const Vec3d& v,const Vec3d& t0,const Vec3d& t1,const Vec3d& t2,Vec4d& omega,Vec3d& n)
{
  Vec3d t13=t0-t2;
  Vec3d t23=t1-t2;
  n=t13.cross(t23);

  scalarD area=n.norm();
  n/=area;

  if(abs((v-t0).dot(n)) > ClothCollision::_thickness)
    return false;
  else {
    scalarD delta=ClothCollision::_thickness/max(sqrt(area),ClothCollision::_rounding);
    Vec3d t43=v-t2;

    Vec2d RHS(t13.dot(t43),t23.dot(t43));
    Mat2d LHS;
    LHS(0,0)=t13.dot(t13);
    LHS(1,0)=LHS(0,1)=t13.dot(t23);
    LHS(1,1)=t23.dot(t23);

    omega.block<2,1>(0,0)=LHS.inverse()*RHS;
    omega[2]=1.0f-omega[0]-omega[1];
    return omega[0] >= -delta &&
           omega[1] >= -delta &&
           omega[2] >= -delta &&
           omega[0] <= 1.0f+delta &&
           omega[1] <= 1.0f+delta &&
           omega[2] <= 1.0f+delta;
  }
}
static FORCE_INLINE bool testEE(const Vec3d& eA0,const Vec3d& eA1,const Vec3d& eB0,const Vec3d& eB1,Vec4d& omega,Vec3d& n)
{
  Vec3d t21=eA1-eA0;
  Vec3d t43=eB1-eB0;

  scalarD delta=ClothCollision::_thickness/max(max(t21.norm(),t43.norm()),ClothCollision::_rounding);
  Vec3d nt21=t21/max(t21.norm(),ClothCollision::_rounding);
  Vec3d nt43=t43/max(t43.norm(),ClothCollision::_rounding);
  n=(nt21).cross(nt43);

  scalarD nNorm=n.norm();
  if(nNorm < ClothCollision::_rounding) {
    /*//ignore parallel edges
    //David Harmon pointed out that parallel edge constraint
    //can be detected by other point triangle test
    Vec3d dA=eA1-eA0;
    dA/=max(dA.norm(),_rounding);

    Vec3d n=eA0-eB0;
    n-=n.dot(dA)*dA;
    if(n.norm() > _thickness)
        return false;

    scalarD IA[2]={eA0.dot(dA),eA1.dot(dA)};
    scalarD IB[2]={eB0.dot(dA),eB1.dot(dA)};

    scalarD SIB[2]={min(IB[0],IB[1]),max(IB[0],IB[1])};
    if(IA[0] >= SIB[0] && IA[0] <= SIB[1]){
        omega[0]=1.0f;
        omega[1]=0.0f;
        omega[2]=(IA[0]-IB[1])/(IB[0]-IB[1]);
        omega[3]=1.0f-omega[2];
        return true;
    }else if(IA[1] >= SIB[0] && IA[1] <= SIB[1]){
        omega[0]=0.0f;
        omega[1]=1.0f;
        omega[2]=(IA[1]-IB[1])/(IB[0]-IB[1]);
        omega[3]=1.0f-omega[2];
        return true;
    }

    scalarD SIA[2]={min(IA[0],IA[1]),max(IA[0],IA[1])};
    if(IB[0] >= SIA[0] && IB[0] <= SIA[1]){
        omega[2]=1.0f;
        omega[3]=0.0f;
        omega[0]=(IB[0]-IA[1])/(IA[0]-IA[1]);
        omega[1]=1.0f-omega[0];
        return true;
    }else if(IB[1] >= SIA[0] && IB[1] <= SIA[1]){
        omega[2]=0.0f;
        omega[3]=1.0f;
        omega[0]=(IB[1]-IA[1])/(IA[0]-IA[1]);
        omega[1]=1.0f-omega[0];
        return true;
    }*/
    return false;
  } else {
    n/=nNorm;
    Vec3d t31=eB0-eA0;

    Vec2d RHS(t21.dot(t31),-t43.dot(t31));
    Mat2d LHS;
    LHS(0,0)=t21.dot(t21);
    LHS(0,1)=-t21.dot(t43);
    LHS(1,0)=-t21.dot(t43);
    LHS(1,1)=t43.dot(t43);
    RHS=LHS.inverse()*RHS;
    if(RHS[0] >= -delta && RHS[1] >= -delta && RHS[0] <= 1.0f+delta && RHS[1] <= 1.0f+delta) {
      t21=eA0+t21*RHS[0];
      t43=eB0+t43*RHS[1];
      if((t21-t43).norm() < ClothCollision::_thickness) {
        omega[0]=1.0f-RHS[0];
        omega[1]=RHS[0];
        omega[2]=1.0f-RHS[1];
        omega[3]=RHS[1];
        return true;
      }
    }
    return false;
  }
}

bool testVT(boost::shared_ptr<ClothMesh::ClothVertex> V1,boost::shared_ptr<ClothMesh::ClothTriangle> T2,ClothCollision::CollisionHandler& handler)
{
  //geometry info
  const ClothMesh::ClothVertex& v0=*(T2->getV0());
  const ClothMesh::ClothVertex& v1=*(T2->getV1());
  const ClothMesh::ClothVertex& v2=*(T2->getV2());
  Vec3d n,pt[4]= {
    V1->_lastPos,
    v0._lastPos,
    v1._lastPos,
    v2._lastPos
  };
  Vec3d v[4]= {
    V1->_pos-V1->_lastPos,
    v0._pos-v0._lastPos,
    v1._pos-v1._lastPos,
    v2._pos-v2._lastPos,
  };

  //CCD test
  scalarD c[4];
  calcCoef(pt,v,c);

  scalarD s[4];
  scalarD cofl=0.0f,cofr=1.0f;
  int nr=solveCubicAllSecant(cofl,cofr,c,s);

  //check result
  Vec3d pos[4];
  Vec4d omega;
  for(int i = 0; i < nr; ++i) {
    for(int j = 0; j < 4; ++j)
      pos[j] = pt[j] + v[j]*s[i];
    if(testVT(pos[0], pos[1], pos[2], pos[3], omega, n)) {
      Vec4d omg(1.0f,-omega[0],-omega[1],-omega[2]);
      adjustNormalSign(v,-omg,n);
      handler.handle(V1,T2,n,omg,s[i]);
      return true;
    }
  }
  return false;
}
bool testEE(boost::shared_ptr<ClothMesh::ClothEdge> E1,boost::shared_ptr<ClothMesh::ClothEdge> E2,ClothCollision::CollisionHandler& handler)
{
  //geometry info
  const ClothMesh::ClothVertex& E10=*(E1->_v[0]);
  const ClothMesh::ClothVertex& E11=*(E1->_v[1]);
  const ClothMesh::ClothVertex& E20=*(E2->_v[0]);
  const ClothMesh::ClothVertex& E21=*(E2->_v[1]);
  Vec3d n,pt[4]= {
    E10._lastPos,
    E11._lastPos,
    E20._lastPos,
    E21._lastPos
  };
  Vec3d v[4]= {
    E10._pos-E10._lastPos,
    E11._pos-E11._lastPos,
    E20._pos-E20._lastPos,
    E21._pos-E21._lastPos
  };

  //CCD test
  scalarD c[4];
  calcCoef(pt,v,c);

  scalarD s[4];
  scalarD cofl = 0.0f, cofr = 1.0f;
  int nr = solveCubicAllSecant(cofl, cofr, c, s);

  //check result
  Vec3d pos[4];
  Vec4d omega;
  for(int i = 0; i < nr; ++i) {
    for(int j = 0; j < 4; ++j)
      pos[j] = pt[j] + v[j]*s[i];
    if(testEE(pos[0],pos[1],pos[2],pos[3],omega,n)) {
      Vec4d omg(omega[0],omega[1],-omega[2],-omega[3]);
      adjustNormalSign(v,-omg,n);
      handler.handle(E1,E2,n,omg,s[i]);
      return true;
    }
  }
  return false;
}
sizeType ClothCollision::nrMesh() const
{
  return ((sizeType)_bvh.size()+1)/2;
}
const ClothMesh& ClothCollision::getMesh(sizeType i) const
{
  return *(_bvh[i]._cell->_mesh);
}
void ClothCollision::updateMesh(boost::shared_ptr<ClothMesh> mesh)
{
  for(sizeType i=0; i<(sizeType)_bvh.size(); i++)
    if(_bvh[i]._cell && _bvh[i]._cell->_mesh == mesh)
      _bvh[i]._cell->buildBVH();
}
void ClothCollision::addMesh(boost::shared_ptr<ClothMesh> mesh)
{
  for(sizeType i=0; i<(sizeType)_bvh.size(); i++)
    if(_bvh[i]._cell && _bvh[i]._cell->_mesh == mesh)
      return;
  for(sizeType i=0; i<(sizeType)_bvh.size();)
    if(_bvh[i]._cell) {
      _bvh[i]._nrCell=1;
      _bvh[i]._parent=-1;
      _bvh[i]._bb=_bvh[i]._cell->refit();
      i++;
    } else {
      _bvh[i]=_bvh.back();
      _bvh.pop_back();
    }
  _bvh.push_back(Node<boost::shared_ptr<NarrowNode>,BBOX>());
  _bvh.back()._nrCell=1;
  _bvh.back()._parent=-1;
  _bvh.back()._cell.reset(new NarrowNode);
  _bvh.back()._cell->_mesh=mesh;
  _bvh.back()._cell->buildBVH();
  _bvh.back()._bb=_bvh.back()._cell->refit();
  //parity check
  for(sizeType i=0; i<(sizeType)_bvh.size(); i++) {
    if(_bvh[i]._cell->_mesh->_vss.empty())
      continue;
    if(i == 0) {
      ASSERT_MSG(_bvh[i]._cell->_mesh->_vss[0]->_type == ClothMesh::CLOTH_MESH,"We only allow one ClothMesh be inserted first!");
    } else {
      ASSERT_MSG(_bvh[i]._cell->_mesh->_vss[0]->_type != ClothMesh::CLOTH_MESH,"We only allow one ClothMesh be inserted first!");
    }
  }
  //rebuild BVH
  BVHBuilder<Node<boost::shared_ptr<NarrowNode>,BBOX>,3>().buildBVH(_bvh);
}
void ClothCollision::delMesh(boost::shared_ptr<ClothMesh> mesh)
{
  while(!_bvh.empty() && !_bvh.back()._cell)
    _bvh.pop_back();
  for(sizeType i=0; i<(sizeType)_bvh.size();)
    if(_bvh[i]._cell->_mesh == mesh) {
      _bvh[i]=_bvh.back();
      _bvh.pop_back();
    } else {
      _bvh[i]._nrCell=1;
      _bvh[i]._parent=-1;
      _bvh[i]._cell->refit();
      i++;
    }
  BVHBuilder<Node<boost::shared_ptr<NarrowNode>,BBOX>,3>().buildBVH(_bvh);
}
void ClothCollision::collide(CollisionHandler& handler,bool useActive)
{
  sizeType nrFound,nrFiltered,nrReal;
  //refit all
  for(sizeType i=0; i<(sizeType)_bvh.size(); i++)
    if(_bvh[i]._cell)
      _bvh[i]._bb=_bvh[i]._cell->refit();
    else {
      _bvh[i]._bb.reset();
      _bvh[i]._bb.setUnion(_bvh[_bvh[i]._l]._bb);
      _bvh[i]._bb.setUnion(_bvh[_bvh[i]._r]._bb);
    }

  //BroadPhase
  _cache.clear();
  BVHQuery<boost::shared_ptr<NarrowNode>,BBOX> q(_bvh,3,boost::shared_ptr<NarrowNode>());
  q.interBodyQuery(q,*this);
  sort(_cache.begin(),_cache.end());
  _cache.erase(unique(_cache.begin(),_cache.end()),_cache.end());

  //NarrowPhase
  _cacheVT.clear();
  _cacheEE.clear();
  for(sizeType i=0; i<(sizeType)_cache.size(); i++) {
    _activeCache=_cache[i];
//#define BRUTE_FORCE
#ifdef BRUTE_FORCE
    vector<boost::shared_ptr<ClothMesh::ClothTriangle> >& tssA=_activeCache._A->_mesh->_tss;
    vector<boost::shared_ptr<ClothMesh::ClothTriangle> >& tssB=_activeCache._B->_mesh->_tss;
    for(sizeType r=0; r<(sizeType)tssA.size(); r++) {
      BBOX bbR;
      bbR.setUnion(tssA[r]->getV0()->_pos.cast<scalar>());
      bbR.setUnion(tssA[r]->getV1()->_pos.cast<scalar>());
      bbR.setUnion(tssA[r]->getV2()->_pos.cast<scalar>());
      bbR.setUnion(tssA[r]->getV0()->_lastPos.cast<scalar>());
      bbR.setUnion(tssA[r]->getV1()->_lastPos.cast<scalar>());
      bbR.setUnion(tssA[r]->getV2()->_lastPos.cast<scalar>());
      for(sizeType c=0; c<(sizeType)tssB.size(); c++) {
        BBOX bbC;
        bbC.setUnion(tssB[c]->getV0()->_pos.cast<scalar>());
        bbC.setUnion(tssB[c]->getV1()->_pos.cast<scalar>());
        bbC.setUnion(tssB[c]->getV2()->_pos.cast<scalar>());
        bbC.setUnion(tssB[c]->getV0()->_lastPos.cast<scalar>());
        bbC.setUnion(tssB[c]->getV1()->_lastPos.cast<scalar>());
        bbC.setUnion(tssB[c]->getV2()->_lastPos.cast<scalar>());
        if(bbR.intersect(bbC))
          onCell(_cache[i]._A->_bvh[r],_cache[i]._B->_bvh[c]);
      }
    }
#else
    BVHQuery<sizeType,BBOX> q1(_cache[i]._A->_bvh,3,-1);
    BVHQuery<sizeType,BBOX> q2(_cache[i]._B->_bvh,3,-1);
    if(useActive && !_cache[i]._A->_active.empty())
      q1._active=&(_cache[i]._A->_active);
    if(useActive && !_cache[i]._B->_active.empty())
      q2._active=&(_cache[i]._B->_active);
    q1.interBodyQuery(q2,*this);
#endif
  }

  //Fine-Grained TV Test
  nrFound=_cacheVT.size();
  sort(_cacheVT.begin(),_cacheVT.end());
  _cacheVT.erase(unique(_cacheVT.begin(),_cacheVT.end()),_cacheVT.end());
  nrFiltered=_cacheVT.size();
  nrReal=0;
  for(sizeType i=0; i<(sizeType)_cacheVT.size(); i++)
    if(testVT(_cacheVT[i]._A,_cacheVT[i]._B,handler))
      nrReal++;
  INFOV("VT Collision: (%d,%d,%d)",nrFound,nrFiltered,nrReal);

  //Fine-Grained EE Test
  nrFound=_cacheEE.size();
  sort(_cacheEE.begin(),_cacheEE.end());
  _cacheEE.erase(unique(_cacheEE.begin(),_cacheEE.end()),_cacheEE.end());
  nrFiltered=_cacheEE.size();
  nrReal=0;
  for(sizeType i=0; i<(sizeType)_cacheEE.size(); i++)
    if(testEE(_cacheEE[i]._A,_cacheEE[i]._B,handler))
      nrReal++;
  INFOV("EE Collision: (%d,%d,%d)",nrFound,nrFiltered,nrReal);
}
void ClothCollision::restartActive()
{
  for(sizeType i=0; i<(sizeType)_bvh.size(); i++)
    if(_bvh[i]._cell) {
      NarrowNode& n=*(_bvh[i]._cell);
      n._active.assign(((sizeType)n._bvh.size()+1)/2,false);
    }
}
void ClothCollision::activate(const ClothMesh::ClothVertex* t)
{
  for(sizeType i=0; i<(sizeType)t->_oneRing.size(); i++) {
    ClothMesh::ClothTriangle& neigh=*(t->_oneRing[i]);
    vector<bool>& tag=*(neigh._activeTag);
    if(!tag.empty())
      tag[neigh._index]=true;
  }
}
void ClothCollision::onCell(const Node<boost::shared_ptr<NarrowNode>,BBOX>& A,const Node<boost::shared_ptr<NarrowNode>,BBOX>& B)
{
  bool rigidA=A._cell->_mesh->_vss[0]->_type&ClothMesh::RIGID_MESH;
  bool rigidB=B._cell->_mesh->_vss[0]->_type&ClothMesh::RIGID_MESH;
  if(!rigidA || !rigidB)
    _cache.push_back(Cache<NarrowNode,NarrowNode>(A._cell,B._cell));
}
static FORCE_INLINE void addColl
(boost::shared_ptr<ClothMesh::ClothEdge> eA,boost::shared_ptr<ClothMesh::ClothEdge> eB,
 vector<ClothCollision::Cache<ClothMesh::ClothEdge,ClothMesh::ClothEdge> >& cacheEE,bool sameMesh)
{
  if(!sameMesh ||
     (eA->_v[0]->_index != eB->_v[0]->_index &&
      eA->_v[0]->_index != eB->_v[1]->_index &&
      eA->_v[1]->_index != eB->_v[0]->_index &&
      eA->_v[1]->_index != eB->_v[1]->_index))
    cacheEE.push_back(ClothCollision::Cache<ClothMesh::ClothEdge,ClothMesh::ClothEdge>(eA,eB));
}
static FORCE_INLINE void addColl
(boost::shared_ptr<ClothMesh::ClothVertex> eA,boost::shared_ptr<ClothMesh::ClothTriangle> eB,
 vector<ClothCollision::Cache<ClothMesh::ClothVertex,ClothMesh::ClothTriangle> >& cacheVT,bool sameMesh)
{
  if(!sameMesh ||
     (eA->_index != eB->getV0()->_index &&
      eA->_index != eB->getV1()->_index &&
      eA->_index != eB->getV2()->_index))
    cacheVT.push_back(ClothCollision::Cache<ClothMesh::ClothVertex,ClothMesh::ClothTriangle>(eA,eB));
}
void ClothCollision::onCell(const Node<sizeType,BBOX>& nA,const Node<sizeType,BBOX>& nB)
{
  const NarrowNode& nodeA=*(_activeCache._A);
  const NarrowNode& nodeB=*(_activeCache._B);
  boost::shared_ptr<ClothMesh::ClothTriangle> tA=nodeA._mesh->_tss[nA._cell];
  boost::shared_ptr<ClothMesh::ClothTriangle> tB=nodeB._mesh->_tss[nB._cell];

  //push EE
  boost::shared_ptr<ClothMesh::ClothEdge> AE[3]= {tA->_e[0],tA->_e[1],tA->_e[2]};
  boost::shared_ptr<ClothMesh::ClothEdge> BE[3]= {tB->_e[0],tB->_e[1],tB->_e[2]};
  for(int i=0; i<3; i++)
    for(int j=0; j<3; j++) {
      //ASSERT(nodeA._mesh->_ess[AE[i]->_index] == AE[i]);
      //ASSERT(nodeB._mesh->_ess[BE[i]->_index] == BE[i]);
      if(AE[i] != BE[j] && nodeA._ebb[AE[i]->_index].intersect(nodeB._ebb[BE[j]->_index]))
        addColl(AE[i],BE[j],_cacheEE,nodeA._mesh == nodeB._mesh);
    }

  //push TB
  boost::shared_ptr<ClothMesh::ClothVertex> AV[3]= {tA->getV0(),tA->getV1(),tA->getV2()};
  boost::shared_ptr<ClothMesh::ClothVertex> BV[3]= {tB->getV0(),tB->getV1(),tB->getV2()};
  for(int i=0; i<3; i++) {
    //ASSERT(nodeA._mesh->_vss[AV[i]->_index] == AV[i]);
    //ASSERT(nodeB._mesh->_vss[BV[i]->_index] == BV[i]);
    if(nodeA._vbb[AV[i]->_index].intersect(nB._bb))addColl(AV[i],tB,_cacheVT,nodeA._mesh == nodeB._mesh);
    if(nodeB._vbb[BV[i]->_index].intersect(nA._bb))addColl(BV[i],tA,_cacheVT,nodeA._mesh == nodeB._mesh);
  }
}
scalarD ClothCollision::_thickness=1E-3f;
scalarD ClothCollision::_rounding=1E-6f;
scalarD ClothCollision::_timeRes=1E-4f;

//impulse handler
struct ImpulseHandler : public ClothCollision::CollisionHandler {
  ImpulseHandler(ClothCollision* coll,scalarD mu,scalarD e,scalarD thickness):_coll(coll),_mu(mu),_e(e),_thickness(thickness) {}
  virtual void handle(boost::shared_ptr<ClothMesh::ClothVertex> V1,boost::shared_ptr<ClothMesh::ClothTriangle> T2,const Vec3d n,const Vec4d& omg,scalarD t) {
    const ClothMesh::ClothVertex* vs[4]= {V1.get(),T2->getV0().get(),T2->getV1().get(),T2->getV2().get(),};
    Vec3d dv=pos(vs[0])*omg[0]+pos(vs[1])*omg[1]+pos(vs[2])*omg[2]+pos(vs[3])*omg[3];
    if(_thickness >= dv.dot(n))
      applyImpulse(vs,Vec4d(m(vs[0]),m(vs[1]),m(vs[2]),m(vs[3])),dv,omg,n);
    _more=true;
  }
  virtual void handle(boost::shared_ptr<ClothMesh::ClothEdge> E1,boost::shared_ptr<ClothMesh::ClothEdge> E2,const Vec3d n,const Vec4d& omg,scalarD t) {
    const ClothMesh::ClothVertex* vs[4]= {E1->_v[0].get(),E1->_v[1].get(),E2->_v[0].get(),E2->_v[1].get(),};
    Vec3d dv=pos(vs[0])*omg[0]+pos(vs[1])*omg[1]+pos(vs[2])*omg[2]+pos(vs[3])*omg[3];
    if(_thickness >= dv.dot(n))
      applyImpulse(vs,Vec4d(m(vs[0]),m(vs[1]),m(vs[2]),m(vs[3])),dv,omg,n);
    _more=true;
  }
  scalarD m(const ClothMesh::ClothVertex* v) const {
    if(v->_type == ClothMesh::CLOTH_MESH)
      return 1.0f;
    else return numeric_limits<scalarD>::infinity();
  }
  Vec3d pos(const ClothMesh::ClothVertex* v) const {
    if(v->_type == ClothMesh::CLOTH_MESH)
      return _posNew.block<3,1>(v->_index*3,0);
    else return v->_pos;
  }
  void applyImpulse(const ClothMesh::ClothVertex* vs[4],const Vec4d& m,const Vec3d& dv,const Vec4d& omg,const Vec3d& n) {
    scalarD coeff=0.0f;
    for(char v=0; v<4; v++)
      coeff+=omg[v]*omg[v]/m[v];

    scalarD vn=_thickness-dv.dot(n);
    Vec3d dvt=dv-vn*n;
    scalarD In=vn/coeff*(1+_e);
    scalarD It=dvt.norm()/coeff;

    Vec3d I;
    if(It < In*_mu)
      I=In*n-dvt/coeff;
    else I=In*n-dvt*In*_mu/dvt.norm();
    for(sizeType v=0; v<4; v++) {
      _coll->activate(vs[v]);
      if(vs[v]->_type == ClothMesh::CLOTH_MESH)
        _posNew.block<3,1>(vs[v]->_index*3,0)+=I*omg[v]/m[v];
    }
  }
  ClothCollision* _coll;
  scalarD _mu,_e,_thickness;
  bool _more;
  Cold _posNew;
};
ClothCollisionSolver::ClothCollisionSolver(boost::shared_ptr<ClothMesh> mesh)
  :_mesh(mesh),_mu(0.75f),_e(0.1f)
{
  _thicknessCCR=1E-3f;
  _thicknessIZ=1E-4f;
  _coll.addMesh(_mesh);
}
const ClothCollision& ClothCollisionSolver::getColl() const
{
  return _coll;
}
ClothCollision& ClothCollisionSolver::getColl()
{
  return _coll;
}
bool ClothCollisionSolver::solve()
{
  //handler for repulsive force
  ImpulseHandler impulser(&_coll,_mu,_e,_thicknessCCR);
  impulser._posNew.resize((sizeType)_mesh->_vss.size()*3);
  impulser._more=false;
  //perform repulsive step
  ClothCollision::_thickness=_thicknessCCR;
  _mesh->assembleC(&(impulser._posNew));
  _coll.collide(impulser,false);
  _mesh->assignC(&(impulser._posNew));
  //apply collision and repulsion
  ClothCollision::_thickness=_thicknessIZ;
  impulser._mu=0.0f;
  impulser._e=0.0f;
  impulser._more=true;
  _coll.restartActive();
  for(sizeType i=0; impulser._more; i++) {
    impulser._more=false;
    _mesh->assembleC(&(impulser._posNew));
    _coll.collide(impulser,i>0);
    _mesh->assignC(&(impulser._posNew));
    if(i > 50)
      return errExit();
  }
  return true;
}
bool ClothCollisionSolver::errExit()
{
  boost::filesystem::ofstream os("./errConfCloth.dat",ios::binary);
  _mesh->write(os);
  WARNING("Collision Response Stuck!")
  return false;
}

//Augmented Lagrangian Solver
class ClothOptimizer
{
public:
  ClothOptimizer()
    :_maxIter(1000000),_maxCGIter((sizeType)sqrt((scalarD)_maxIter)),
     _epsG(1E-6f),_epsF(1E-12f),_epsX(1E-6f),_debug(false) {}
  virtual ~ClothOptimizer() {}
  virtual void prepare(Eigen::Map<const Cold> x) {}
  virtual sizeType nrX() const=0;
  virtual scalarD fVal(Eigen::Map<const Cold> x) const=0;
  virtual void fGrad(Eigen::Map<const Cold> x,Eigen::Map<Cold> grad) const=0;
  virtual sizeType nrC() const=0;
  virtual scalarD cVal(Eigen::Map<const Cold> x,sizeType j,char &sign) const=0;
  virtual void cGrad(Eigen::Map<const Cold> x,sizeType j,scalarD factor,Eigen::Map<Cold> grad) const=0;
  //solve routine
  void setDebug(bool debug) {
    _debug=debug;
  }
  const vector<scalarD>& getLambda() const {
    return _lambda;
  }
  vector<scalarD>& getLambda() {
    return _lambda;
  }
  scalarD getMu() const {
    return _mu;
  }
  void solve(Cold& xInit) {
    alglib::real_1d_array x=toAlglib(xInit);
    _lambda.assign(nrC(),0.0f);
    _mu=1E3f;

    alglib::mincgstate state;
    alglib::mincgreport rep;
    alglib::mincgcreate(x,state);

    sizeType iter=0;
    for(; iter<_maxIter;) {
      sizeType maxIter=min(_maxCGIter,_maxIter-iter);
      alglib::mincgsetcond(state,_epsG,_epsF,_epsX,(alglib::ae_int_t)maxIter);
      if(iter > 0)
        alglib::mincgrestartfrom(state,x);
      alglib::mincgsuggeststep(state,1E-3f*nrX());
      alglib::mincgoptimize(state,valueGrad,NULL,this);
      alglib::mincgresults(state,x,rep);
      updateMultiplier(x,*this);
      //if(_debug)
      //{
      //	INFOV("CG-Iter: %d, Total CG Iter: %d (Code: %d)",rep.iterationscount,iter,rep.terminationtype)
      //	checkKKT(Eigen::Map<const Cold>(x.getcontent(),x.length()));
      //}
      if(rep.iterationscount == 0)break;
      iter+=rep.iterationscount;
    }
    if(_debug) {
      INFOV("Total CG Iter: %d",iter)
      checkKKT(fromAlglib(x));
    }
    xInit=fromAlglib(x);
  }
  scalarD checkKKT(const Cold& x) const {
    scalarD maxCVio=0.0f;
    Cold grad=Cold::Zero(nrX());
    Eigen::Map<Cold> gMap(grad.data(),grad.rows());
    fGrad(Eigen::Map<const Cold>(x.data(),x.size()),gMap);
    for(sizeType i=0; i<nrC(); i++) {
      char sign;
      scalarD cV=cVal(Eigen::Map<const Cold>(x.data(),x.size()),i,sign);
      scalarD cVClamp=clampCons(cV,sign);
      cGrad(Eigen::Map<const Cold>(x.data(),x.size()),i,_lambda[i],gMap);
      maxCVio=max(abs(cVClamp),maxCVio);
    }
    if(_debug) {
      INFOV("Constraint Vio: %f, Opt-Cond-Vio: %f",maxCVio,grad.cwiseAbs().maxCoeff())
    }
    return maxCVio;
  }
protected:
  static scalarD clampCons(scalarD x,char sign) {
    return (sign<0) ? max<scalarD>(x,0.0) :
           (sign>0) ? min<scalarD>(x,0.0) : x;
  }
  static void valueGrad(const alglib::real_1d_array& x,double& value,alglib::real_1d_array& grad,void *ptr) {
    const ClothOptimizer* opt=reinterpret_cast<const ClothOptimizer*>(ptr);
    const vector<scalarD>& lambda=opt->getLambda();
    const scalarD mu=opt->getMu();
    //reset
    Cold xEigen=fromAlglib(x);
    Cold gradEigen=fromAlglib(grad);
    gradEigen.setZero();
    const_cast<ClothOptimizer*>(opt)->prepare(Eigen::Map<const Cold>(xEigen.data(),xEigen.size()));
    value=opt->fVal(Eigen::Map<const Cold>(xEigen.data(),xEigen.size()));
    opt->fGrad(Eigen::Map<const Cold>(xEigen.data(),xEigen.size()),
               Eigen::Map<Cold>(gradEigen.data(),gradEigen.size()));
    for(sizeType i=0; i<opt->nrC(); i++) {
      char sign;
      scalarD cVal=opt->cVal(Eigen::Map<const Cold>(xEigen.data(),xEigen.size()),i,sign);
      scalarD cValClamp=clampCons(cVal+lambda[i]/mu,sign);
      value+=mu*0.5f*cValClamp*cValClamp;
      opt->cGrad(Eigen::Map<const Cold>(xEigen.data(),xEigen.size()),
                 i,mu*cValClamp,
                 Eigen::Map<Cold>(gradEigen.data(),gradEigen.size()));
    }
    grad=toAlglib(gradEigen);
  }
  static void updateMultiplier(const alglib::real_1d_array& x,ClothOptimizer& opt) {
    Cold xEigen=fromAlglib(x);
    vector<scalarD>& lambda=opt.getLambda();
    for(sizeType i=0; i<opt.nrC(); i++) {
      char sign;
      scalarD cVal=opt.cVal(Eigen::Map<const Cold>(xEigen.data(),xEigen.size()),i,sign);
      lambda[i]=clampCons(lambda[i]+opt.getMu()*cVal,sign);
    }
  }
  vector<scalarD> _lambda;
  sizeType _maxIter,_maxCGIter;
  scalarD _epsG,_epsF,_epsX,_mu;
  bool _debug;
};
//non-rigid impact zone handler
struct NRZConstraint {
  NRZConstraint() {}
  NRZConstraint(ClothMesh::ClothVertex* vs[4],Vec3d coef[4],scalarD delta) {
    _vs[0]=vs[0];
    _vs[1]=vs[1];
    _vs[2]=vs[2];
    _vs[3]=vs[3];
    _coef[0]=coef[0];
    _coef[1]=coef[1];
    _coef[2]=coef[2];
    _coef[3]=coef[3];
    _delta=delta;
  }
  ClothMesh::ClothVertex* _vs[4];
  scalarD _delta;
  Vec3d _coef[4];
  Vec4i _vid;
};
struct NRZOptimizer : public ClothOptimizer {
  typedef ClothConstraint::STrips STrips;
  NRZOptimizer(const vector<boost::shared_ptr<ClothEnergy> >& energys,const vector<ClothMesh::ClothVertex*>& vids,const vector<NRZConstraint>& cons)
    :_energys(energys),_vids(vids),_cons(cons) {
    _debug=true;
  }
  virtual sizeType nrX() const {
    return _vids.size()*3;
  }
  virtual void prepare(Eigen::Map<const Cold> x) {
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<(sizeType)_vids.size(); i++)
      _vids[i]->_pos=x.block<3,1>(i*3,0);
  }
  virtual scalarD fVal(Eigen::Map<const Cold> x) const {
    //consistency energy
    scalarD val=(x-_target).squaredNorm()*0.5f;
    //user energy
    for(sizeType i=0; i<(sizeType)_energys.size(); i++)
      val-=_energys[i]->eval();
    return val;
  }
  virtual void fGrad(Eigen::Map<const Cold> x,Eigen::Map<Cold> grad) const {
    //consistency energy
    grad=x-_target;
    //user energy
    Cold gradE=Cold::Zero(grad.rows());
    STrips trips;
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<(sizeType)_energys.size(); i++) {
      _energys[i]->_needHESS=false;
      _energys[i]->eval(gradE,trips,0);
    }
    grad-=gradE;
  }
  virtual sizeType nrC() const {
    return (sizeType)_cons.size();
  }
  virtual scalarD cVal(Eigen::Map<const Cold> x,sizeType j,char &sign) const {
    const NRZConstraint& c=_cons[j];
    scalarD val=c._delta;
    for(sizeType v=0; v<4; v++)
      val+=c._coef[v].dot(c._vs[v]->_pos);
    sign=-1;
    return val;
  }
  virtual void cGrad(Eigen::Map<const Cold> x,sizeType j,scalarD factor,Eigen::Map<Cold> grad) const {
    const NRZConstraint& c=_cons[j];
    for(sizeType v=0; v<4; v++)
      if(c._vid[v] >= 0)
        grad.block<3,1>(c._vid[v]*3,0)+=factor*c._coef[v];
  }
  //data
  Cold _target;
  const vector<boost::shared_ptr<ClothEnergy> >& _energys;
  const vector<ClothMesh::ClothVertex*>& _vids;
  const vector<NRZConstraint>& _cons;
};
//QP Solver, where we solve: Gx=-d, Ax>=b
class ClothQPOptimizer
{
public:
  //The KKT condition is:
  //	Gx+d-A^Tl=0
  //	l>0 || ax>b
  //This solver is based on dual KKT condition:
  //	(AG^{-1}A^T)l=b+AG^{-1}d
  //	l>=0
  //The above problem is built and solved using ALGLIB
  typedef Eigen::SparseMatrix<scalarD,0,sizeType> SMAT;
  ClothQPOptimizer():_maxIter(100000),_epsG(1E-6f),_epsF(1E-12f),_epsX(1E-6f),_debug(false) {}
  void solve(Cold& x) {
    //build dual system
    SMAT AT=_A.transpose();
    Cold dL=-(_b+_A*_invG.solve(_d));
    Matd GL=_A*_invG.solve(AT);

    //solve using ALGLIB
    alglib::real_1d_array dLAlglib,LAlglib,bndl,bndu;
    alglib::real_2d_array GLAlglib;
    dLAlglib.setlength(GL.rows());
    GLAlglib.setlength(GL.rows(),GL.rows());
    for(sizeType r=0; r<GL.rows(); r++) {
      dLAlglib(r)=dL[r];
      for(sizeType c=0; c<GL.rows(); c++)
        GLAlglib(r,c)=GL(r,c);
    }

    LAlglib.setlength(_A.rows());
    bndl.setlength(_A.rows());
    bndu.setlength(_A.rows());
    for(sizeType r=0; r<_A.rows(); r++) {
      bndl(r)=0.0f;
      bndu(r)=numeric_limits<scalarD>::max();
      LAlglib(r)=0.0f;
    }

    alglib::minqpstate state;
    alglib::minqpreport rep;
    alglib::minqpcreate(GL.rows(),state);
    alglib::minqpsetalgoquickqp(state,_epsG,_epsF,_epsX,_maxIter,true);
    alglib::minqpsetlinearterm(state,dLAlglib);
    alglib::minqpsetquadraticterm(state,GLAlglib);
    alglib::minqpsetbc(state,bndl,bndu);
    alglib::minqpsetstartingpoint(state,LAlglib);
    alglib::minqpoptimize(state);
    alglib::minqpresults(state,LAlglib,rep);

    //recover primal variable
    Cold LMap=fromAlglib(LAlglib);
    x=_invG.solve(AT*LMap-_d);
    if(_debug)
      checkKKT(Eigen::Map<Cold>(LMap.data(),LMap.size()),x);
  }
  void checkKKT(const Eigen::Map<Cold>& lambda,const Cold& x) const {
    Cold cons=_A*x-_b;
    scalarD maxCVio=0.0f,maxDVio=0.0f;
    for(sizeType i=0; i<lambda.rows(); i++) {
      maxCVio=min<scalarD>(maxCVio,fabs(min(lambda[i],cons[i])));
      maxDVio=max<scalarD>(maxDVio,fabs(lambda[i]*cons[i]));
    }
    INFOV("QP Solved with LNorm: %f, maxCVio %f, maxDVio %f!",lambda.norm(),maxCVio,maxDVio);
  }
protected:
  //data
  Eigen::SimplicialCholesky<SMAT> _invG;
  SMAT _G,_A;
  Cold _d,_b;
  //param
  sizeType _maxIter;
  scalarD _epsG,_epsF,_epsX,_mu;
  bool _debug;
};
class NRZQPOptimizer : public ClothQPOptimizer
{
public:
  typedef ClothConstraint::STrips STrips;
  NRZQPOptimizer(const vector<boost::shared_ptr<ClothEnergy> >& energys,const vector<ClothMesh::ClothVertex*>& vids,const vector<NRZConstraint>& cons)
    :_energys(energys),_vids(vids),_cons(cons) {
    _debug=true;
  }
  void update() {
    //initialize from target position
    sizeType nrV=(sizeType)_vids.size();
    for(sizeType i=0; i<nrV; i++)
      _vids[i]->_pos=_target.block<3,1>(i*3,0);

    //update _G/_d
    _G.resize(nrV*3,nrV*3);
    _d.setZero(nrV*3);
    STrips trips;
    //OMP_PARALLEL_FOR_
    for(sizeType i=0; i<(sizeType)_energys.size(); i++) {
      _energys[i]->_needHESS=true;
      _energys[i]->eval(_d,trips,0);
    }
    _G.setFromTriplets(trips.begin(),trips.end());
    _G*=-1.0f;
    _d*=-1.0f;
    for(sizeType r=0; r<_G.rows(); r++)
      _G.coeffRef(r,r)+=1.0f;
    _invG.compute(_G);

    //udpate _A/_b
    sizeType nrC=(sizeType)_cons.size();
    _A.resize(nrC,nrV*3);
    _b.setZero(nrC);
    trips.clear();
    for(sizeType i=0; i<nrC; i++) {
      const NRZConstraint& c=_cons[i];
      _b[i]=c._delta;
      for(sizeType v=0; v<4; v++) {
        _b[i]+=c._coef[v].dot(c._vs[v]->_pos);
        if(c._vid[v] >= 0)
          addBlock(trips,i,c._vid[v]*3,-c._coef[v].transpose());
      }
    }
    _A.setFromTriplets(trips.begin(),trips.end());
  }
  //data
  Cold _target;
  const vector<boost::shared_ptr<ClothEnergy> >& _energys;
  const vector<ClothMesh::ClothVertex*>& _vids;
  const vector<NRZConstraint>& _cons;
};
//non-rigid impact zone handler
struct NRZHandler : public ClothCollision::CollisionHandler {
  typedef boost::unordered_map<ClothMesh::ClothVertex*,sizeType> VMAP;
  typedef VMAP::const_iterator CVITER;
  NRZHandler(vector<boost::shared_ptr<ClothEnergy> >& energys,ClothCollision& coll,const Cold& target,scalarD thickness)
    :_energys(energys),_coll(coll),_target(target),_thickness(thickness) {
    if(!_energys.empty()) {
      //this requires coupled optimizaiton, include all vertices
      const ClothMesh& mesh=_coll.getMesh(0);
      for(sizeType v=0; v<(sizeType)mesh._vss.size(); v++)
        _vids[mesh._vss[v].get()]=v;
    }
  }
  void handle(boost::shared_ptr<ClothMesh::ClothVertex> V1,boost::shared_ptr<ClothMesh::ClothTriangle> T2,const Vec3d n,const Vec4d& omg,scalarD t) {
    ClothMesh::ClothVertex* vs[4]= {V1.get(),T2->getV0().get(),T2->getV1().get(),T2->getV2().get(),};
    Vec3d coef[4]= {-omg[0]*n,-omg[1]*n,-omg[2]*n,-omg[3]*n,};

    NRZConstraint cons(vs,coef,_thickness);
    _cons.push_back(cons);
    for(sizeType v=0; v<4; v++)
      _coll.activate(vs[v]);
    _more=true;
  }
  void handle(boost::shared_ptr<ClothMesh::ClothEdge> E1,boost::shared_ptr<ClothMesh::ClothEdge> E2,const Vec3d n,const Vec4d& omg,scalarD t) {
    ClothMesh::ClothVertex* vs[4]= {E1->_v[0].get(),E1->_v[1].get(),E2->_v[0].get(),E2->_v[1].get(),};
    Vec3d coef[4]= {-omg[0]*n,-omg[1]*n,-omg[2]*n,-omg[3]*n,};
    NRZConstraint cons(vs,coef,_thickness);
    _cons.push_back(cons);
    for(sizeType v=0; v<4; v++)
      _coll.activate(vs[v]);
    _more=true;
  }
  const vector<NRZConstraint>& getCons() const {
    return _cons;
  }
  bool solve() {
    if(!_more)
      return false;

    //reorder
    sizeType vid=(sizeType)_vids.size();
    for(sizeType i=0; i<(sizeType)_cons.size(); i++)
      for(sizeType v=0; v<4; v++)
        if(_cons[i]._vs[v]->_type == ClothMesh::CLOTH_MESH) {
          CVITER iter=_vids.find(_cons[i]._vs[v]);
          if(iter == _vids.end()) {
            _cons[i]._vid[v]=vid;
            _vids[_cons[i]._vs[v]]=vid++;
          } else _cons[i]._vid[v]=iter->second;
        } else _cons[i]._vid[v]=-1;

    //optimize zone position
    optimize();
    return true;
  }
  virtual void optimize() {
    sizeType vid=(sizeType)_vids.size();
    _invVids.resize(vid);
    NRZOptimizer opt(_energys,_invVids,_cons);
    opt._target.resize(vid*3);
    for(CVITER beg=_vids.begin(),end=_vids.end(); beg!=end; beg++) {
      _invVids[beg->second]=beg->first;
      opt._target.block<3,1>(beg->second*3,0)=_target.block<3,1>(beg->first->_index*3,0);
      //we start from lastPos, that should be feasible
    }
    Cold xNew=opt._target;
    opt.solve(xNew);
    for(sizeType i=0; i<vid; i++)
      const_cast<ClothMesh::ClothVertex*>(_invVids[i])->_pos=xNew.block<3,1>(i*3,0);
  }
  bool _more;
protected:
  vector<boost::shared_ptr<ClothEnergy> >& _energys;
  ClothCollision& _coll;
  const Cold& _target;
  scalarD _thickness;
  //optimization data
  vector<ClothMesh::ClothVertex*> _invVids;
  vector<NRZConstraint> _cons;
  VMAP _vids;
};
struct NRZQPHandler : public NRZHandler {
  NRZQPHandler(vector<boost::shared_ptr<ClothEnergy> >& energys,ClothCollision& coll,const Cold& target,scalarD thickness)
    :NRZHandler(energys,coll,target,thickness),_opt(energys,_invVids,_cons) {}
  virtual void optimize() {
    sizeType vid=(sizeType)_vids.size();
    _invVids.resize(vid);
    _opt._target.resize(vid*3);
    for(CVITER beg=_vids.begin(),end=_vids.end(); beg!=end; beg++) {
      _invVids[beg->second]=beg->first;
      _opt._target.block<3,1>(beg->second*3,0)=_target.block<3,1>(beg->first->_index*3,0);
      //we start from lastPos, that should be feasible
    }

    Cold xNew;
    _opt.update();
    _opt.solve(xNew);
    for(sizeType i=0; i<vid; i++)
      const_cast<ClothMesh::ClothVertex*>(_invVids[i])->_pos+=xNew.block<3,1>(i*3,0);
  }
private:
  NRZQPOptimizer _opt;
};
ClothCollisionNRZSolver::ClothCollisionNRZSolver(boost::shared_ptr<ClothMesh> mesh):ClothCollisionSolver(mesh) {}
bool ClothCollisionNRZSolver::solve()
{
  Cold target((sizeType)_mesh->_vss.size()*3);
  _mesh->assembleC(&target);
  ClothCollision::_thickness=_thicknessCCR;
  boost::shared_ptr<NRZHandler> NRZSolver;
  //if(_energys.empty())
  //	NRZSolver.reset(new NRZHandler(_energys,_coll,target,_thicknessCCR));
  //else NRZSolver.reset(new NRZQPHandler(_energys,_coll,target,_thicknessCCR));
  NRZSolver.reset(new NRZQPHandler(_energys,_coll,target,_thicknessCCR));
  _coll.restartActive();
  for(sizeType i=0; true; i++) {
    NRZSolver->_more=false;
    _coll.collide(*NRZSolver,_energys.empty() && i>0);
    if(NRZSolver->_more) {
      if(i > 50)
        return errExit();
      NRZSolver->solve();
    } else break;
  }
  return true;
}
