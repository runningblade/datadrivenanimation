#include "SkinnedMeshUtility.h"
#include <CommonFile/RotationUtil.h>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

USE_PRJ_NAMESPACE

//feature
#define HAS_JOINT (!filter || filter->find(i) != filter->end())
struct LimbDirection {
  static Cold calc(SkinnedMeshUtility& body,const Cold& theta,const set<sizeType>* filter) {
    MatX3d joints;
    body.getBody().getMesh(theta,&joints,NULL,NULL,NULL,NULL,true);
    //count
    sizeType nrM=0;
    for(sizeType i=1; i<body.getBody().nrJoint(); i++)
      if(HAS_JOINT)
        nrM+=3;
    //fill
    Cold ret=Cold::Zero(nrM);
    for(sizeType i=1,k=0; i<body.getBody().nrJoint(); i++)
      if(HAS_JOINT) {
        ret.segment<3>(k)=(joints.row(i)-joints.row(body.getBody().kintree()(0,i))).transpose();
        k+=3;
      }
    return ret;
  }
  static void write(SkinnedMeshUtility& body,const Cold& f,const string& path,ObjMesh* mesh,const set<sizeType>* filter) {
    vector<scalar> css;
    map<sizeType,sizeType> vMap;
    vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
    vector<Vec3i,Eigen::aligned_allocator<Vec3i> > iss;
    vss.push_back(Vec3::Zero());
    css.push_back(0);
    vMap[0]=0;
    for(sizeType i=1,k=1; i<body.getBody().nrJoint(); i++)
      if(HAS_JOINT)
        vMap[i]=k++;
    for(sizeType i=1,k=0; i<body.getBody().nrJoint(); i++)
      if(HAS_JOINT) {
        Vec3 delta=f.segment<3>(k).cast<scalar>();
        sizeType p=body.getBody().kintree()(0,i);
        ASSERT_MSGV(vMap.find(p) != vMap.end(),"Filter not coherent at joint: %ld, parent: %ld!",i,p)
        vss.push_back(vss[vMap.find(p)->second]+delta);
        iss.push_back(Vec3i((sizeType)vss.size()-1,vMap.find(p)->second,-1));
        css.push_back(i);
        k+=3;
      }
    boost::shared_ptr<VTKWriter<scalar> > os;
    if(!path.empty()) {
      os.reset(new VTKWriter<scalar>("LimbDirection",path,true));
      os->appendPoints(vss.begin(),vss.end());
      os->appendCells(iss.begin(),iss.end(),VTKWriter<scalar>::LINE);
      os->appendCustomPointData("id",css.begin(),css.end());
    }
  }
};
struct NormalizedLimbDirection {
  static Cold calc(SkinnedMeshUtility& body,const Cold& theta,const set<sizeType>* filter) {
    MatX3d joints;
    body.getBody().getMesh(theta,&joints,NULL,NULL,NULL,NULL,true);
    //count
    sizeType nrM=0;
    for(sizeType i=1; i<body.getBody().nrJoint(); i++)
      if(HAS_JOINT)
        nrM+=2;
    //fill
    Cold ret=Cold::Zero(nrM);
    for(sizeType i=1,k=0; i<body.getBody().nrJoint(); i++)
      if(HAS_JOINT) {
        Vec3d d=(joints.row(i)-joints.row(body.getBody().kintree()(0,i))).transpose();
        ret[k+0]=atan2(d[1],d[0]);
        ret[k+1]=atan2(d[2],d.segment<2>(0).norm());
        k+=2;
      }
    return ret;
  }
  static void write(SkinnedMeshUtility& body,const Cold& f,const string& path,ObjMesh* mesh,const set<sizeType>* filter) {
    MatX3d joints;
    vector<scalar> css;
    map<sizeType,sizeType> vMap;
    vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
    vector<Vec3i,Eigen::aligned_allocator<Vec3i> > iss;
    body.getBody().getMesh(body.restPose(),&joints,NULL,NULL,NULL,NULL,true);
    vss.push_back(Vec3::Zero());
    css.push_back(0);
    vMap[0]=0;
    for(sizeType i=1,k=1; i<body.getBody().nrJoint(); i++)
      if(HAS_JOINT)
        vMap[i]=k++;
    for(sizeType i=1,k=0; i<body.getBody().nrJoint(); i++)
      if(HAS_JOINT) {
        Vec3 delta(cos(f[k])*cos(f[k+1]),sin(f[k])*cos(f[k+1]),sin(f[k+1]));
        sizeType p=body.getBody().kintree()(0,i);
        delta*=(joints.row(i)-joints.row(p)).norm();
        ASSERT_MSGV(vMap.find(p) != vMap.end(),"Filter not coherent at joint: %ld, parent: %ld!",i,p)
        vss.push_back(vss[vMap.find(p)->second]+delta);
        iss.push_back(Vec3i((sizeType)vss.size()-1,vMap.find(p)->second,-1));
        css.push_back(i);
        k+=2;
      }
    boost::shared_ptr<VTKWriter<scalar> > os;
    if(!path.empty()) {
      os.reset(new VTKWriter<scalar>("NormalizedLimbDirection",path,true));
      os->appendPoints(vss.begin(),vss.end());
      os->appendCells(iss.begin(),iss.end(),VTKWriter<scalar>::LINE);
      os->appendCustomPointData("id",css.begin(),css.end());
    }
  }
};
struct LimbRotation {
  static Cold calc(SkinnedMeshUtility& body,const Cold& theta,const set<sizeType>* filter) {
    ArticulatedBody& b=body.getBody().getBody();
    BodyOpT<const Body> op(b.nrBody());
    b.transform(b.root(),op,(void*)&theta);
    //count
    sizeType nrM=0;
    for(sizeType i=0; i<body.getBody().nrJoint(); i++) {
      const Body& B=b.findBody(body.getBody().names()[i]);
      if(B.hasMesh() && HAS_JOINT)
        nrM+=3;
    }
    //fill
    Cold ret=Cold::Zero(nrM);
    for(sizeType i=0,k=0; i<body.getBody().nrJoint(); i++) {
      const Body& B=b.findBody(body.getBody().names()[i]);
      if(B.hasMesh() && HAS_JOINT) {
        Mat3 R=op.getTrans(B.id()).block<3,3>(0,0);
        scalar scale=sqrt((R.transpose()*R).diagonal().sum()/3);
        ret.segment<3>(k)=invExpW<scalarD>((R/scale).cast<scalarD>());
        k+=3;
      }
    }
    return ret;
  }
  static void write(SkinnedMeshUtility& body,const Cold& f,const string& path,ObjMesh* mesh,const set<sizeType>* filter) {
    Cold theta=body.restPose();
    vector<Mat3d,Eigen::aligned_allocator<Mat3d> > rots(body.getBody().nrJoint(),Mat3d::Identity());
    const ArticulatedBody& b=body.getBody().getBody();
    for(sizeType i=0,k=0; i<body.getBody().nrJoint(); i++) {
      const Body& B=b.findBody(body.getBody().names()[i]);
      if(B.hasMesh() && HAS_JOINT) {
        rots[i]=expWGradV<scalarD>(f.segment<3>(k),NULL,NULL,NULL,NULL);
        sizeType p=body.getBody().kintree()(0,i);
        if(p >= 0 && p < body.getBody().nrJoint())
          theta.segment<3>(i*3)=invExpW<scalarD>(rots[p].transpose()*rots[i]);
        else theta.segment<3>(i*3)=invExpW<scalarD>(rots[i]);
        k+=3;
      }
    }
    ObjMesh m;
    BodyOpT<const Body> op(b.nrBody());
    b.transform(b.root(),op,(void*)&theta);
    boost::shared_ptr<VTKWriter<scalar> > os;
    if(!path.empty())
      os.reset(new VTKWriter<scalar>("LimbRotation",path,true));
    for(sizeType i=0,k=0; i<body.getBody().nrJoint(); i++) {
      const Body& B=b.findBody(body.getBody().names()[i]);
      if(B.hasMesh() && HAS_JOINT) {
        B.meshRef()->setT(op.getTrans(B.id()));
        B.meshRef()->getMesh(m);
        if(!path.empty())
          m.writeVTK(*os);
        if(mesh)
          mesh->addMesh(m,"joint"+boost::lexical_cast<string>(i));
        k+=3;
      }
    }
  }
};
struct Theta {
  static Cold calc(SkinnedMeshUtility& body,const Cold& theta,const set<sizeType>* filter) {
    ArticulatedBody& b=body.getBody().getBody();
    BodyOpT<const Body> op(b.nrBody());
    b.transform(b.root(),op,(void*)&theta);
    //count
    sizeType nrM=0;
    for(sizeType i=0; i<body.getBody().nrJoint(); i++) {
      const Body& B=b.findBody(body.getBody().names()[i]);
      if(B.hasMesh() && HAS_JOINT)
        nrM+=3;
    }
    //fill
    Cold ret=Cold::Zero(nrM);
    for(sizeType i=0,k=0; i<body.getBody().nrJoint(); i++) {
      const Body& B=b.findBody(body.getBody().names()[i]);
      if(B.hasMesh() && HAS_JOINT) {
        ret.segment<3>(k)=theta.segment<3>(i);
        k+=3;
      }
    }
    return ret;
  }
  static void write(SkinnedMeshUtility& body,const Cold& f,const string& path,ObjMesh* mesh,const set<sizeType>* filter) {
    Cold theta=body.restPose();
    const ArticulatedBody& b=body.getBody().getBody();
    for(sizeType i=0,k=0; i<body.getBody().nrJoint(); i++) {
      const Body& B=b.findBody(body.getBody().names()[i]);
      if(B.hasMesh() && HAS_JOINT) {
        theta.segment<3>(i*3)=f.segment<3>(k);
        k+=3;
      }
    }
    ObjMesh m;
    BodyOpT<const Body> op(b.nrBody());
    b.transform(b.root(),op,(void*)&theta);
    boost::shared_ptr<VTKWriter<scalar> > os;
    if(!path.empty())
      os.reset(new VTKWriter<scalar>("Theta",path,true));
    for(sizeType i=0,k=0; i<body.getBody().nrJoint(); i++) {
      const Body& B=b.findBody(body.getBody().names()[i]);
      if(B.hasMesh() && HAS_JOINT) {
        B.meshRef()->setT(op.getTrans(B.id()));
        B.meshRef()->getMesh(m);
        if(!path.empty())
          m.writeVTK(*os);
        if(mesh)
          mesh->addMesh(m,"joint"+boost::lexical_cast<string>(i));
        k+=3;
      }
    }
  }
};
sizeType SkinnedMeshUtility::getFeatureType() const
{
  return _feat;
}
void SkinnedMeshUtility::pruneFootAndHand(set<sizeType>& filter)
{
  filter.erase(7);
  filter.erase(8);
  filter.erase(10);
  filter.erase(11);

  filter.erase(20);
  filter.erase(21);
  filter.erase(22);
  filter.erase(23);
}
SkinnedMeshUtility::FEATURE_TYPE SkinnedMeshUtility::parseFeatureFromString(string name,set<sizeType>* filter)
{
  SkinnedMeshUtility::FEATURE_TYPE type=SAME_FEATURE;
  if(beginsWith(name,"LIMB_DIRECTION"))
    type=LIMB_DIRECTION;
  else if(beginsWith(name,"NORMALIZED_LIMB_DIRECTION"))
    type=NORMALIZED_LIMB_DIRECTION;
  else if(beginsWith(name,"LIMB_ROTATION"))
    type=LIMB_ROTATION;
  else if(beginsWith(name,"THETA"))
    type=THETA;
  else {
    ASSERT_MSGV(false,"Unknown feature type: %s!",name.c_str())
  }
  string typeStr=parseFeatureToString((FEATURE_TYPE)type);
  if(name.length() > typeStr.length()+1)
    name=name.substr(typeStr.length()+1);
  else name="";
  if(filter) {
    filter->clear();
    boost::char_separator<char> sep("_");
    boost::tokenizer<boost::char_separator<char> > tok(name,sep);
    ASSERT_MSG(tok.begin() != tok.end(),"Incorrect feature type!")
    boost::tokenizer<boost::char_separator<char> >::iterator it=tok.begin();
    while(it != tok.end()) {
      try {
        sizeType id=boost::lexical_cast<sizeType>(*it);
        filter->insert(id);
      } catch(...) {
        break;
      }
      it++;
    }
  }
  return type;
}
string SkinnedMeshUtility::parseFeatureToString(FEATURE_TYPE feat,const set<sizeType>* filter)
{
  string type;
  if(feat == LIMB_DIRECTION)
    type="LIMB_DIRECTION";
  else if(feat == NORMALIZED_LIMB_DIRECTION)
    type="NORMALIZED_LIMB_DIRECTION";
  else if(feat == LIMB_ROTATION)
    type="LIMB_ROTATION";
  else if(feat == THETA)
    type="THETA";
  else {
    ASSERT_MSGV(false,"Unknown feature type: %ld!",feat)
  }
  if(filter) {
    for(set<sizeType>::const_iterator beg=filter->begin(),end=filter->end(); beg!=end; beg++)
      type+="_"+boost::lexical_cast<string>(*beg);
  }
  return type;
}
Cold SkinnedMeshUtility::getFeature(const Cold& theta,FEATURE_TYPE feat,const set<sizeType>* filter)
{
  if(feat == LIMB_DIRECTION)
    return LimbDirection::calc(*this,theta,filter);
  else if(feat == NORMALIZED_LIMB_DIRECTION)
    return NormalizedLimbDirection::calc(*this,theta,filter);
  else if(feat == LIMB_ROTATION)
    return LimbRotation::calc(*this,theta,filter);
  else if(feat == THETA)
    return Theta::calc(*this,theta,filter);
  else {
    ASSERT_MSGV(false,"Unknown feature type: %ld!",feat)
    return Cold::Zero(0);
  }
}
void SkinnedMeshUtility::writeFeatureVTK(const Cold& f,const string& path,FEATURE_TYPE feat,const set<sizeType>* filter)
{
  if(feat == LIMB_DIRECTION)
    return LimbDirection::write(*this,f,path,NULL,filter);
  else if(feat == NORMALIZED_LIMB_DIRECTION)
    return NormalizedLimbDirection::write(*this,f,path,NULL,filter);
  else if(feat == LIMB_ROTATION)
    return LimbRotation::write(*this,f,path,NULL,filter);
  else if(feat == THETA)
    return Theta::write(*this,f,path,NULL,filter);
}
void SkinnedMeshUtility::writeFeatureObj(const Cold& f,ObjMesh& mesh,FEATURE_TYPE feat,const set<sizeType>* filter)
{
  if(feat == LIMB_DIRECTION)
    return LimbDirection::write(*this,f,"",&mesh,filter);
  else if(feat == NORMALIZED_LIMB_DIRECTION)
    return NormalizedLimbDirection::write(*this,f,"",&mesh,filter);
  else if(feat == LIMB_ROTATION)
    return LimbRotation::write(*this,f,"",&mesh,filter);
  else if(feat == THETA)
    return Theta::write(*this,f,"",&mesh,filter);
}
void SkinnedMeshUtility::writeFeatureVTK(const Datum& f,const string& path,FEATURE_TYPE feat,const set<sizeType>* filter)
{
  writeFeatureVTK(datumToFeature(f),path,feat,filter);
}
void SkinnedMeshUtility::writeFeatureObj(const Datum& f,ObjMesh& mesh,FEATURE_TYPE feat,const set<sizeType>* filter)
{
  writeFeatureObj(datumToFeature(f),mesh,feat,filter);
}
Cold SkinnedMeshUtility::datumToFeature(const Datum& f)
{
  Cold fVec;
  fVec.resize(f.float_data_size());
  for(sizeType i=0; i<fVec.size(); i++)
    fVec[i]=f.float_data(i);
  return fVec;
}
