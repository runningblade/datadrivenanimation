#ifndef SKINNED_MESH_CLOTH_UTILITY_H
#define SKINNED_MESH_CLOTH_UTILITY_H

#include "SkinnedMeshUtility.h"
#include "ClothEnergies.h"

PRJ_BEGIN

struct VTKRenderHandle;
class SkinnedMeshClothUtility : public SkinnedMeshUtility
{
public:
  enum PATTERN_TYPE {
    DRESS,
    CLOAK,
    NR_PATTERN,
  };
  struct Iterator {
    Iterator(SkinnedMeshClothUtility& util);
    sizeType index() const;
    const Cold& cloth() const;
    const Cold& feature() const;
    const Cold& theta() const;
    void operator++();
    bool end() const;
  private:
    SkinnedMeshClothUtility& _util;
    sizeType _index;
  };
  struct JointConstraint {
    boost::shared_ptr<ClothConstraint> _cons;
    sizeType _jid;
    Vec3d _rel;
  };
  struct RenderMaterial {
    Vec3 _ambient;
    Vec3 _diffuse;
    Vec3 _specular;
    scalar _spower;
    sizeType _nrSubd;
  };
  struct RenderLight {
    Vec3 _position;
    Vec3 _target;
    scalar _angle;
    Vec3 _diffuse;
    Vec3 _ambient;
    Vec3 _specular;
  };
  struct RenderCamera {
    Vec3 _position;
    Vec3 _target;
  };
  struct RenderProperty {
    RenderProperty();
    RenderProperty(const string& path);
    RenderMaterial _matCloth,_matBody;
    vector<RenderLight> _lights;
    vector<RenderCamera> _cameras;
    sizeType _width,_height;
    Vec3 _bkgColor;
    string _bkgImage;
  };
  struct ClusterNode : public Serializable {
    ClusterNode():Serializable(typeid(ClusterNode).name()) {}
    ClusterNode(sizeType idFrm,sizeType l,sizeType r,scalar dist)
      :Serializable(typeid(ClusterNode).name()),_idFrm(idFrm),_l(l),_r(r),_dist(dist) {}
    virtual bool read(istream& is) override {
      readBinaryData(_idFrm,is);
      readBinaryData(_l,is);
      readBinaryData(_r,is);
      readBinaryData(_dist,is);
      return is.good();
    }
    virtual bool write(ostream& os) const override {
      writeBinaryData(_idFrm,os);
      writeBinaryData(_l,os);
      writeBinaryData(_r,os);
      writeBinaryData(_dist,os);
      return os.good();
    }
    sizeType _idFrm,_l,_r;
    scalar _dist;
  };
  struct Cluster : public Serializable {
    Cluster():Serializable(typeid(Cluster).name()) {}
    virtual bool read(istream& is) override {
      readVector(_nodes,is);
      return is.good();
    }
    virtual bool write(ostream& os) const override {
      writeVector(_nodes,os);
      return os.good();
    }
    bool read(const string& path) {
      boost::filesystem::ifstream is(path);
      return read(is);
    }
    bool write(const string& path) const {
      boost::filesystem::ofstream os(path);
      return write(os);
    }
    vector<ClusterNode> _nodes;
  };
  SkinnedMeshClothUtility();
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  bool readClothOnly(const string& path);
  bool writeClothOnly(const string& path);
  bool readSkinnedMeshCloth(const string& path);
  bool writeSkinnedMeshCloth(const string& path) const;
  void writeClothVTK(const string& path,const Cold* X=NULL) const;
  //collision filter (call this before calling generate***)
  void enableCollisionSet();
  void disableCollisionSet();
  void addCollisionJoint(sizeType id);
  //generate cloth template
  void generateSolver(const ObjMesh& obj,const map<sizeType,Vec3>& cons,sizeType jid,sizeType collSubd,scalar mu,scalar lambda,scalar bend,const Vec3& initExpandXZ=Vec3(1,1,1));
  bool solveClothForPose();
  bool solveClothForPose(sizeType i);
  bool solveClothForPose(const Cold& theta);
  void enableCollision();
  void disableCollision();
  void simulateCloth(sizeType startTraj,sizeType nrTraj=0,sizeType nrFrm=0);
  virtual void writeDatasetVTK(const string& path) override;
  sizeType nrClothFrm() const;
  const ClothMesh& mesh() const;
  ClothMesh& mesh();
  //hierarchical clustering
  sizeType findTrajOff(sizeType fid) const;
  Cluster cluster(const string& clothName,FEATURE_TYPE feat,sizeType MAX_CLUSTER_CAPACITY);
  //different stitch patterns of cloth
  bool generateCloak(scalar height,scalar length,scalar sepLength,scalar sepAngle,scalar res,scalar mu,scalar lambda,scalar bend);
  bool generateDress(scalar height,scalar length,scalar looseAngle,scalar res,scalar mu,scalar lambda,scalar bend);
  //convert cloth
  void convertClothCloak(Cold& vss,vector<Datum>& datums,bool back) const;
  void convertClothDress(Cold& vss,vector<Datum>& datums,bool back) const;
  void convertCloth(Cold& vss,vector<Datum>& datums,PATTERN_TYPE pattern,bool back) const;
  void convertClothToCaffe(const Cold& vss,const Cold& theta,vector<Datum>& datums,PATTERN_TYPE pattern) const;
  void convertClothFromCaffe(Cold& vss,const vector<Datum>& datums,PATTERN_TYPE pattern) const;
  //convert body
  string convertBodyCloak(const Cold& theta,Datum& datum,FEATURE_TYPE feat,set<sizeType>* filterOut);
  string convertBodyDress(const Cold& theta,Datum& datum,FEATURE_TYPE feat,set<sizeType>* filterOut);
  string convertBodyToCaffe(const Cold& theta,Datum& datum,FEATURE_TYPE feat,PATTERN_TYPE pattern,set<sizeType>* filterOut);
  static SkinnedMeshClothUtility::PATTERN_TYPE parsePatternFromString(const string& path);
  static void drawClothVTK(const Datum& dat,const string& path);
  static Cold unifyRoot(const Cold& theta);
  static void debugUnifyRoot();
  //render cloth
  void drawClothImage(struct VTKRenderHandle& ret,const RenderProperty& property,const Cold* vssCloth,const Datum* datum,FEATURE_TYPE feat,const set<sizeType>* filter,string& outputColor);
  void drawClothImage(struct VTKRenderHandle& ret,const RenderProperty& property,const vector<Datum>& datums,FEATURE_TYPE feat,const set<sizeType>* filter,PATTERN_TYPE pattern,string& outputColor);
  void drawClothImage(const RenderProperty& property,const Cold* vssCloth,const Datum* datum,FEATURE_TYPE feat,const set<sizeType>* filter,string& outputColor);
  void drawClothImage(const RenderProperty& property,const vector<Datum>& datums,FEATURE_TYPE feat,const set<sizeType>* filter,PATTERN_TYPE pattern,string& outputColor);
  void drawClothImageFile(const RenderProperty& property,const Cold* vssCloth,const Datum* datum,FEATURE_TYPE feat,const set<sizeType>* filter,const string& outputColorFile);
  void drawClothImageFile(const RenderProperty& property,const vector<Datum>& datums,FEATURE_TYPE feat,const set<sizeType>* filter,PATTERN_TYPE pattern,const string& outputColorFile);
protected:
  vector<Cold,Eigen::aligned_allocator<Cold> > _cloths;
  //solver
  boost::shared_ptr<set<sizeType> > _collisionSet;
  boost::shared_ptr<ArticulatedBodyEnergy> _collE;
  boost::shared_ptr<ClothEnergySolver> _sol;
  boost::shared_ptr<ClothMesh> _mesh;
  vector<boost::shared_ptr<ClothMesh> > _jointMeshes;
  vector<JointConstraint> _jointCons;
  scalar _avgElemSz;
  Cold _last;
  //vtk handle
  boost::shared_ptr<VTKRenderHandle> _vtkHandle;
};

PRJ_END

#endif
