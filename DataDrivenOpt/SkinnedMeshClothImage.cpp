#include "SkinnedMeshClothUtility.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/lexical_cast.hpp>
#ifdef HAS_VTK_SUPPORT
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkCellArray.h>
#include <vtkTriangle.h>
#include <vtkLight.h>
#include <vtkLightActor.h>
#include <vtkImageActor.h>
#include <vtkImageResample.h>
#include <vtkProperty.h>
#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkWindowToImageFilter.h>
#include <vtkImageShiftScale.h>
#include <vtkGraphicsFactory.h>
#include <vtkImagingFactory.h>
#include <vtkJPEGReader.h>
#include <vtkPNGReader.h>
#include <vtkBMPReader.h>
#include <vtkJPEGWriter.h>
#include <vtkBMPWriter.h>
#include <vtkUnsignedCharArray.h>
#endif

PRJ_BEGIN

#ifdef HAS_VTK_SUPPORT
template <typename MESH>
vtkSmartPointer<vtkPolyData> meshToPoly(const MESH& mesh)
{
  vtkSmartPointer<vtkPoints> points=vtkSmartPointer<vtkPoints>::New();
  for(sizeType i=0; i<(sizeType)mesh.getV().size(); i++)
    points->InsertNextPoint(mesh.getV()[i][0],mesh.getV()[i][1],mesh.getV()[i][2]);
  vtkSmartPointer<vtkCellArray> tris=vtkSmartPointer<vtkCellArray>::New();
  for(sizeType i=0; i<(sizeType)mesh.getI().size(); i++) {
    vtkSmartPointer<vtkTriangle> t=vtkSmartPointer<vtkTriangle>::New();
    t->GetPointIds()->SetId(0,mesh.getI()[i][0]);
    t->GetPointIds()->SetId(1,mesh.getI()[i][1]);
    t->GetPointIds()->SetId(2,mesh.getI()[i][2]);
    tris->InsertNextCell(t);
  }
  vtkSmartPointer<vtkPolyData> polyData=vtkSmartPointer<vtkPolyData>::New();
  polyData->SetPoints(points);
  polyData->SetPolys(tris);
  vtkSmartPointer<vtkDoubleArray> normalData=vtkSmartPointer<vtkDoubleArray>::New();
  normalData->SetNumberOfComponents(3);
  normalData->SetNumberOfTuples(mesh.getV().size());
  for(sizeType i=0; i<(sizeType)mesh.getV().size(); i++) {
    double N[3]= {mesh.getN()[i][0],mesh.getN()[i][1],mesh.getN()[i][2]};
    normalData->SetTuple(i,N);
  }
  polyData->GetPointData()->SetNormals(normalData);
  return polyData;
}
template <typename MESH>
void updateMeshToPoly(vtkPolyData& data,const MESH& mesh)
{
  vtkSmartPointer<vtkPoints> points=vtkSmartPointer<vtkPoints>::New();
  for(sizeType i=0; i<(sizeType)mesh.getV().size(); i++)
    points->InsertNextPoint(mesh.getV()[i][0],mesh.getV()[i][1],mesh.getV()[i][2]);
  vtkSmartPointer<vtkCellArray> tris=vtkSmartPointer<vtkCellArray>::New();
  for(sizeType i=0; i<(sizeType)mesh.getI().size(); i++) {
    vtkSmartPointer<vtkTriangle> t=vtkSmartPointer<vtkTriangle>::New();
    t->GetPointIds()->SetId(0,mesh.getI()[i][0]);
    t->GetPointIds()->SetId(1,mesh.getI()[i][1]);
    t->GetPointIds()->SetId(2,mesh.getI()[i][2]);
    tris->InsertNextCell(t);
  }
  data.SetPoints(points);
  data.SetPolys(tris);
  vtkSmartPointer<vtkDoubleArray> normalData=vtkSmartPointer<vtkDoubleArray>::New();
  normalData->SetNumberOfComponents(3);
  normalData->SetNumberOfTuples(mesh.getV().size());
  for(sizeType i=0; i<(sizeType)mesh.getV().size(); i++) {
    double N[3]= {mesh.getN()[i][0],mesh.getN()[i][1],mesh.getN()[i][2]};
    normalData->SetTuple(i,N);
  }
  data.GetPointData()->SetNormals(normalData);
  data.Modified();
  data.Update();
}
vtkSmartPointer<vtkImageData> readImage(const string& path)
{
  string ext=boost::filesystem::path(path).extension().string();
  if(ext == ".jpg") {
    vtkSmartPointer<vtkJPEGReader> jpegReader=vtkSmartPointer<vtkJPEGReader>::New();
    jpegReader->SetFileName(path.c_str());
    jpegReader->Update();
    return jpegReader->GetOutput();
  } else if(ext == ".png") {
    vtkSmartPointer<vtkPNGReader> pngReader=vtkSmartPointer<vtkPNGReader>::New();
    pngReader->SetFileName(path.c_str());
    pngReader->Update();
    return pngReader->GetOutput();
  } else if(ext == ".bmp") {
    vtkSmartPointer<vtkBMPReader> pngReader=vtkSmartPointer<vtkBMPReader>::New();
    pngReader->SetFileName(path.c_str());
    pngReader->Update();
    return pngReader->GetOutput();
  } else {
    ASSERT_MSG(false,"Unknown image extension!")
    return NULL;
  }
}
void setMaterial(vtkProperty* prop,const SkinnedMeshClothUtility::RenderMaterial& mat)
{
  prop->SetAmbientColor(mat._ambient[0],mat._ambient[1],mat._ambient[2]);
  prop->SetDiffuseColor(mat._diffuse[0],mat._diffuse[1],mat._diffuse[2]);
  prop->SetSpecularColor(mat._specular[0],mat._specular[1],mat._specular[2]);
  prop->SetSpecularPower(mat._spower);
}
struct VTKRenderHandle {
  vtkSmartPointer<vtkPolyData> _body;
  vtkSmartPointer<vtkPolyData> _cloth;
  vtkSmartPointer<vtkJPEGWriter> _writer;
  vtkSmartPointer<vtkRenderWindow> _renderWindow;
};
#else
struct VTKRenderHandle {};
#endif
void readVec(const boost::property_tree::ptree& pt,const string& name,Vec3& v)
{
  v[0]=pt.get<scalar>(name+"X");
  v[1]=pt.get<scalar>(name+"Y");
  v[2]=pt.get<scalar>(name+"Z");
}
void readVec(const boost::property_tree::ptree& pt,const string& name,Vec3& v,const Vec3& def)
{
  v[0]=pt.get<scalar>(name+"X",def[0]);
  v[1]=pt.get<scalar>(name+"Y",def[1]);
  v[2]=pt.get<scalar>(name+"Z",def[2]);
}
void readMaterial(const boost::property_tree::ptree& pt,SkinnedMeshClothUtility::RenderMaterial& m)
{
  readVec(pt,"ambient",m._ambient,Vec3(0,0,0));
  readVec(pt,"diffuse",m._diffuse,Vec3(1,1,1));
  readVec(pt,"specular",m._specular,Vec3(1,1,1));
  m._spower=pt.get<scalar>("spower",4);
  m._nrSubd=pt.get<sizeType>("nrSubd",0);
}
void readLight(const boost::property_tree::ptree& pt,SkinnedMeshClothUtility::RenderLight& l)
{
  readVec(pt,"position",l._position);
  readVec(pt,"target",l._target);
  readVec(pt,"ambient",l._ambient,Vec3(0,0,0));
  readVec(pt,"diffuse",l._diffuse,Vec3(1,1,1));
  readVec(pt,"specular",l._specular,Vec3(1,1,1));
  l._angle=pt.get<scalar>("angle",30);
}
void readCamera(const boost::property_tree::ptree& pt,SkinnedMeshClothUtility::RenderCamera& c)
{
  readVec(pt,"position",c._position);
  readVec(pt,"target",c._target);
}
SkinnedMeshClothUtility::RenderProperty::RenderProperty() {}
SkinnedMeshClothUtility::RenderProperty::RenderProperty(const string& path)
{
  boost::property_tree::ptree pt;
  readPtreeAscii(pt,path);
  readMaterial(pt.get_child("matCloth"),_matCloth);
  readMaterial(pt.get_child("matBody"),_matBody);
  for(sizeType i=0;; i++)
    if(pt.get_child_optional("light"+boost::lexical_cast<string>(i))) {
      _lights.push_back(RenderLight());
      readLight(pt.get_child("light"+boost::lexical_cast<string>(i)),_lights.back());
    } else break;
  for(sizeType i=0;; i++)
    if(pt.get_child_optional("camera"+boost::lexical_cast<string>(i))) {
      _cameras.push_back(RenderCamera());
      readCamera(pt.get_child("camera"+boost::lexical_cast<string>(i)),_cameras.back());
    } else break;
  readVec(pt,"bkgColor",_bkgColor,Vec3(0,0,0));
  _bkgImage=pt.get<string>("bkgImage","");
  _width=pt.get<sizeType>("width",256);
  _height=pt.get<sizeType>("height",256);
}
//interface
void SkinnedMeshClothUtility::drawClothImage(VTKRenderHandle& ret,const RenderProperty& property,const Cold* vssCloth,const Datum* datum,FEATURE_TYPE feat,const set<sizeType>* filter,string& outputColor)
{
#ifndef HAS_VTK_SUPPORT
  FUNCTION_NOT_IMPLEMENTED
#else
  if(ret._renderWindow.GetPointer() == NULL) {
    // Setup offscreen rendering
    vtkSmartPointer<vtkGraphicsFactory> graphics_factory=vtkSmartPointer<vtkGraphicsFactory>::New();
    graphics_factory->SetOffScreenOnlyMode(1);
    graphics_factory->SetUseMesaClasses(1);
    vtkSmartPointer<vtkImagingFactory> imaging_factory=vtkSmartPointer<vtkImagingFactory>::New();
    imaging_factory->SetUseMesaClasses(1);
    //create renderer
    sizeType nrC=(sizeType)property._cameras.size();
    vector<vtkSmartPointer<vtkRenderer> > renderers;
    ret._renderWindow=vtkSmartPointer<vtkRenderWindow>::New();
    ret._renderWindow->SetSize((int)(property._width*nrC),(int)property._height);
    ret._renderWindow->SetOffScreenRendering(1);
    for(sizeType i=0; i<nrC; i++) {
      const Vec3& pos=property._cameras[i]._position;
      const Vec3& tar=property._cameras[i]._target;
      vtkSmartPointer<vtkRenderer> renderer=vtkSmartPointer<vtkRenderer>::New();
      renderer->GetActiveCamera()->SetPosition(pos[0],pos[1],pos[2]);
      renderer->GetActiveCamera()->SetFocalPoint(tar[0],tar[1],tar[2]);
      renderer->GetActiveCamera()->SetViewUp(0,1,0);
      renderer->SetViewport((scalar)i/(scalar)nrC,0,(scalar)(i+1)/(scalar)nrC,1);
      renderer->SetBackground(property._bkgColor[0],property._bkgColor[1],property._bkgColor[2]);
      renderer->RemoveAllLights();
      ret._renderWindow->AddRenderer(renderer);
      renderers.push_back(renderer);
    }
    //create cloth
    if(vssCloth) {
      ObjMeshD mesh;
      _mesh->assignC(vssCloth);
      _mesh->convertC2Obj(mesh);
      mesh.subdivide(property._matCloth._nrSubd);
      ret._cloth=meshToPoly(mesh);
      vtkSmartPointer<vtkPolyDataMapper> mapper=vtkSmartPointer<vtkPolyDataMapper>::New();
      vtkSmartPointer<vtkActor> actor=vtkSmartPointer<vtkActor>::New();
      mapper->SetInput(ret._cloth);
      setMaterial(actor->GetProperty(),property._matCloth);
      actor->SetMapper(mapper);
      for(sizeType i=0; i<nrC; i++)
        renderers[i]->AddActor(actor);
    }
    //create body
    if(datum) {
      ObjMesh mesh;
      writeFeatureObj(*datum,mesh,feat,filter);
      mesh.smooth();
      mesh.subdivide(property._matBody._nrSubd);
      ret._body=meshToPoly(mesh);
      vtkSmartPointer<vtkPolyDataMapper> mapper=vtkSmartPointer<vtkPolyDataMapper>::New();
      vtkSmartPointer<vtkActor> actor=vtkSmartPointer<vtkActor>::New();
      mapper->SetInput(ret._body);
      setMaterial(actor->GetProperty(),property._matBody);
      actor->SetMapper(mapper);
      for(sizeType i=0; i<nrC; i++)
        renderers[i]->AddActor(actor);
    }
    //create light
    for(sizeType i=0; i<(sizeType)property._lights.size(); i++) {
      const RenderLight& l=property._lights[i];
      vtkSmartPointer<vtkLight> light=vtkSmartPointer<vtkLight>::New();
      light->SetLightTypeToSceneLight();
      light->SetPosition(l._position[0],l._position[1],l._position[2]);
      light->SetPositional(true);
      light->SetConeAngle(l._angle);
      light->SetFocalPoint(l._target[0],l._target[1],l._target[2]);
      light->SetAmbientColor(l._ambient[0],l._ambient[1],l._ambient[2]);
      light->SetDiffuseColor(l._diffuse[0],l._diffuse[1],l._diffuse[2]);
      light->SetSpecularColor(l._specular[0],l._specular[1],l._specular[2]);
      for(sizeType i=0; i<nrC; i++)
        renderers[i]->AddLight(light);
    }
    //setup camera
    ret._renderWindow->Render();
    {
      vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter=vtkSmartPointer<vtkWindowToImageFilter>::New();
      windowToImageFilter->SetInput(ret._renderWindow);
      windowToImageFilter->Update();
      ret._writer=vtkSmartPointer<vtkJPEGWriter>::New();
      ret._writer->SetInput(windowToImageFilter->GetOutput());
      ret._writer->SetWriteToMemory(true);
      ret._writer->Write();
      vtkUnsignedCharArray* res=ret._writer->GetResult();
      outputColor.clear();
      outputColor.append(res->GetPointer(0),res->GetPointer(0)+res->GetDataSize());
    }
  } else {
    //cloth
    if(vssCloth) {
      ObjMeshD mesh;
      _mesh->assignC(vssCloth);
      _mesh->convertC2Obj(mesh);
      mesh.subdivide(property._matCloth._nrSubd);
      updateMeshToPoly(*(ret._cloth),mesh);
    }
    //body
    if(datum) {
      ObjMesh mesh;
      writeFeatureObj(*datum,mesh,feat,filter);
      mesh.smooth();
      mesh.subdivide(property._matBody._nrSubd);
      updateMeshToPoly(*(ret._body),mesh);
    }
    //render
    ret._renderWindow->Render();
    {
      vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter=vtkSmartPointer<vtkWindowToImageFilter>::New();
      windowToImageFilter->SetInput(ret._renderWindow);
      windowToImageFilter->Update();
      ret._writer=vtkSmartPointer<vtkJPEGWriter>::New();
      ret._writer->SetInput(windowToImageFilter->GetOutput());
      ret._writer->SetWriteToMemory(true);
      ret._writer->Write();
      vtkUnsignedCharArray* res=ret._writer->GetResult();
      outputColor.clear();
      outputColor.append(res->GetPointer(0),res->GetPointer(0)+res->GetDataSize());
    }
  }
#endif
}
void SkinnedMeshClothUtility::drawClothImage(VTKRenderHandle& ret,const RenderProperty& property,const vector<Datum>& datums,FEATURE_TYPE feat,const set<sizeType>* filter,PATTERN_TYPE pattern,string& outputColor)
{
  Cold vss;
  convertClothFromCaffe(vss,datums,pattern);
  drawClothImage(ret,property,&vss,&(datums.back()),feat,filter,outputColor);
}
void SkinnedMeshClothUtility::drawClothImage(const RenderProperty& property,const Cold* vssCloth,const Datum* datum,FEATURE_TYPE feat,const set<sizeType>* filter,string& outputColor)
{
  if(!_vtkHandle)
    _vtkHandle.reset(new VTKRenderHandle);
  drawClothImage(*_vtkHandle,property,vssCloth,datum,feat,filter,outputColor);
}
void SkinnedMeshClothUtility::drawClothImage(const RenderProperty& property,const vector<Datum>& datums,FEATURE_TYPE feat,const set<sizeType>* filter,PATTERN_TYPE pattern,string& outputColor)
{
  if(!_vtkHandle)
    _vtkHandle.reset(new VTKRenderHandle);
  drawClothImage(*_vtkHandle,property,datums,feat,filter,pattern,outputColor);
}
void SkinnedMeshClothUtility::drawClothImageFile(const RenderProperty& property,const Cold* vssCloth,const Datum* datum,FEATURE_TYPE feat,const set<sizeType>* filter,const string& outputColorFile)
{
  string outputColor;
  drawClothImage(property,vssCloth,datum,feat,filter,outputColor);
  boost::filesystem::ofstream ofstream(outputColorFile,ios::binary);
  ofstream.write(&outputColor[0],outputColor.size());
}
void SkinnedMeshClothUtility::drawClothImageFile(const RenderProperty& property,const vector<Datum>& datums,FEATURE_TYPE feat,const set<sizeType>* filter,PATTERN_TYPE pattern,const string& outputColorFile)
{
  string outputColor;
  drawClothImage(property,datums,feat,filter,pattern,outputColor);
  boost::filesystem::ofstream ofstream(outputColorFile,ios::binary);
  ofstream.write(&outputColor[0],outputColor.size());
}

PRJ_END
