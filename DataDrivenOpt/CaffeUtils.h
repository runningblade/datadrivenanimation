#ifndef CAFFE_UTILS_H
#define CAFFE_UTILS_H

#include "SkinnedMeshClothUtility.h"
#ifndef CUDA_SUPPORT
#define CPU_ONLY
#endif
#include <caffe.hpp>
#include <caffe/util/db.hpp>
#include <caffe/data_transformer.hpp>
#include "caffe/util/signal_handler.h"

PRJ_BEGIN

void reopenDB(db::DB& leveldb,const string& path);
struct DatumFPZ;
class DatasetKey
{
public:
  DatasetKey();
  DatasetKey(string key);
  virtual ~DatasetKey();
  virtual string str() const;
  virtual string strPrefix() const;
  virtual bool operator<(const DatasetKey& other) const;
  static bool compressDatums(const vector<Datum>& datums,const vector<string>& images,string& key,string& value);
  bool decompressDatumsFPZ(vector<DatumFPZ>& datums,vector<string>& images,string value);
  bool decompressDatums(vector<Datum>& datums,vector<string>& images,string value);
  static void debugCompression(sizeType N);
  static void debugFindRemove(const string& dbType);
  static size_t compressDatum(const Datum& dat,string& str);
  static size_t decompressDatumFPZ(DatumFPZ& dat,string& str);
  static size_t decompressDatum(Datum& dat,string& str);
  //data
  string _idDataset,_featStr;
  int _idTraj,_idFrm;
  //datum specs
  vector<Vec3i,Eigen::aligned_allocator<Vec3i> > _datumSpecs;
  vector<pair<string,sizeType> > _imageSpecs;
};

PRJ_END

#endif
