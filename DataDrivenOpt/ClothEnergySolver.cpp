#include "ClothEnergies.h"
#include "ClothCollision.h"
#include "ClothArticulatedEnergies.h"
#include "LMInterface.h"
#include "LMUmfpack.h"
#include "LMCholmod.h"
#include <CommonFile/solvers/Minimizer.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

//helper
class ClothObjective : public Objective<scalarD>
{
public:
  typedef FixedSparseMatrix<scalarD,Kernel<scalarD> > FSMat;
  ClothObjective(vector<boost::shared_ptr<ClothEnergy> >& energys,boost::shared_ptr<ClothMesh>& mesh,bool NMesh)
    :_energys(energys),_mesh(mesh),_NMesh(NMesh) {
    for(vector<boost::shared_ptr<ClothEnergy> >::iterator beg=_energys.begin(),end=_energys.end(); beg!=end; beg++)
      if(boost::dynamic_pointer_cast<ArticulatedBodyEnergy>(*beg)) {
        _collA=boost::dynamic_pointer_cast<ArticulatedBodyEnergy>(*beg);
        _energys.erase(beg);
      }
  }
  virtual int operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient) {
    Vec X=x;
    scalarD E=0;
    if(_cons && _cons0)
      X=*_cons*x+*_cons0;
    if(_NMesh)
      _mesh->assignN(&X);
    else _mesh->assignC(&X);
    //assemble
    _tmp.clear();
    _fgradP.assign(Vec::Zero(inputsAll()));
    //compute
    OMP_PARALLEL_FOR_I(OMP_ADD(E))
    for(sizeType i=0; i<(sizeType)_energys.size(); i++) {
      _energys[i]->_needHESS=false;
      _energys[i]->eval(_fgradP.getMatrixI(),_tmp,0);
      E+=_energys[i]->eval();
    }
    //articulated collision
    if(_collA) {
      _collA->_needHESS=false;
      _collA->eval(_fgradP,_tmp,0);
      E+=_collA->eval();
    }
    //negate
    FX=-E;
    DFDX=-_fgradP.getMatrix();
    //constraint
    if(_cons && _cons0)
      DFDX=_cons->transpose()*DFDX;
    return 0;
  }
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable) {
    fvec.setZero(0);
    if(fjac)
      fjac->clear();
    return 0;
  }
  virtual scalarD operator()(const Vec& x,Vec* fgrad,SMat* fhess) {
    Vec X=x;
    scalarD E=0;
    if(_cons && _cons0)
      X=*_cons*x+*_cons0;
    if(_NMesh)
      _mesh->assignN(&X);
    else _mesh->assignC(&X);
    //assemble
    _tmp.clear();
    if(fgrad)
      _fgradP.assign(Vec::Zero(inputsAll()));
    //compute
    OMP_PARALLEL_FOR_I(OMP_ADD(E))
    for(sizeType i=0; i<(sizeType)_energys.size(); i++) {
      _energys[i]->_needHESS=fhess != NULL;
      if(fgrad)
        _energys[i]->eval(_fgradP.getMatrixI(),_tmp,0);
      E+=_energys[i]->eval();
    }
    //articulated collision
    if(_collA) {
      _collA->_needHESS=fhess != NULL;
      _collA->eval(_fgradP,_tmp,0);
      E+=_collA->eval();
    }
    //negate
    E*=-1;
    if(fgrad)
      *fgrad=-_fgradP.getMatrix();
    if(fhess) {
      fhess->resize(inputsAll(),inputsAll());
      fhess->setFromTriplets(_tmp.begin(),_tmp.end());
      *fhess*=-1;
    }
    //constraint
    if(_cons && _cons0) {
      if(fgrad)
        *fgrad=_cons->transpose()**fgrad;
      if(fhess)
        *fhess=_cons->transpose()*(*fhess**_cons);
    }
    return E;
  }
  virtual bool analyzeCons(const STrips& cons,const Vec& cons0,sizeType maxIslandSize=3) {
    FSMat consF;
    consF.resize(cons0.size(),inputs());
    consF.buildFromTripletsDepulicate(cons.getVector(),0);
    return analyzeCons(consF,cons0,maxIslandSize);
  }
  virtual bool analyzeCons(const FSMat& cons,const Vec& cons0,sizeType maxIslandSize=3) {
    _cons=NULL;
    _cons0=NULL;
    Matd basis;
    Vec basis0;
    STrips trips;
    set<sizeType> variables;
    map<sizeType,set<sizeType> > islands;
    //cluster variables
    for(sizeType k=0; k<cons.rows(); k++) {
      sizeType var=-1;
      for(ConstSMIterator<scalarD> beg=cons.begin(k),end=cons.end(k); beg!=end; ++beg) {
        if(islands.find(beg.col()) != islands.end())
          var=beg.col();
        variables.insert(beg.col());
      }
      if(var == -1)
        var=cons.begin(k).col();
      for(ConstSMIterator<scalarD> beg=cons.begin(k),end=cons.end(k); beg!=end; ++beg)
        islands[var].insert(beg.col());
    }
    //check if island size is within allowed range
    for(map<sizeType,set<sizeType> >::const_iterator beg=islands.begin(),end=islands.end(); beg!=end; beg++)
      if((sizeType)beg->second.size() > maxIslandSize)
        return false;
    //build constraint matrix
    sizeType off=0,nrVar=inputs();
    for(sizeType i=0; i<nrVar; i++)
      if(variables.find(i) == variables.end())
        trips.push_back(STrip(i,off++,1));
    _cons0.reset(new Vec(Vec::Zero(nrVar)));
    for(map<sizeType,set<sizeType> >::const_iterator beg=islands.begin(),end=islands.end(); beg!=end; beg++) {
      orthogonalBasis(beg->second,cons,cons0,basis,basis0);
      addIndexBlock(trips,beg->second,off,basis);
      setIndexBlock(*_cons0,beg->second,basis0);
      off+=basis.cols();
    }
    _cons.reset(new SMat(nrVar,off));
    _cons->setFromTriplets(trips.begin(),trips.end());
    _invCTC.compute(_cons->transpose()**_cons);
    return true;
  }
  virtual Cold assemble(const Cold& c) const {
    if(!_cons)
      return c;
    ASSERT(c.size() == _cons->rows())
    return _invCTC.solve(_cons->transpose()*(c-*_cons0));
  }
  virtual Cold assign(const Cold& c) const {
    if(!_cons)
      return c;
    ASSERT(c.size() == _cons->cols())
    return *_cons*c+*_cons0;
  }
  virtual int inputsAll() const {
    if(_NMesh)
      return (int)_mesh->_ess.size()*3;
    else return (int)_mesh->_vss.size()*3;
  }
  virtual int inputs() const {
    if(_cons)
      return _cons->cols();
    return inputsAll();
  }
  virtual int values() const {
    return 0;
  }
private:
  static void orthogonalBasis(const set<sizeType>& vars,const FSMat& cons,const Vec& cons0,Matd& basis,Vec& basis0) {
    sizeType off=0,nrEqn=0;
    map<sizeType,sizeType> varMaps;
    for(set<sizeType>::const_iterator beg=vars.begin(),end=vars.end(); beg!=end; beg++)
      varMaps[*beg]=off++;
    //count how many equations related
    for(sizeType k=0; k<cons.rows(); k++)
      if(varMaps.find(cons.begin(k).col()) != varMaps.end())
        nrEqn++;
    //build eqn matrix
    off=0;
    Matd c=Matd::Zero(nrEqn,vars.size());
    Vec c0=Vec::Zero(nrEqn);
    for(sizeType k=0; k<cons.rows(); k++) {
      if(varMaps.find(cons.begin(k).col()) == varMaps.end())
        continue;
      for(ConstSMIterator<scalarD> beg=cons.begin(k),end=cons.end(k); beg!=end; ++beg)
        c(off,varMaps[beg.col()])=*beg;
      c0[off++]=cons0[cons.begin(k).row()];
    }
    Eigen::FullPivLU<Matd> invCICIT=(c*c.transpose()).fullPivLu();
    //compute orthogonal matrix using random projection method
    basis.setZero(vars.size(),vars.size()-nrEqn);
    basis0.setZero(0);
    off=0;
    while(off < basis.cols() || basis0.size() == 0) {
      Vec test=Vec::Random(basis.rows());
      if(basis0.size() == 0)
        basis0=test-c.transpose()*invCICIT.solve(c*test+c0);
      if(off < basis.cols()) {
        basis.col(off)=test-c.transpose()*invCICIT.solve(c*test);
        for(sizeType j=0; j<off; j++)
          basis.col(off)-=basis.col(off).dot(basis.col(j))*basis.col(j);
        if(basis.col(off).norm() > 1E-3f)
          basis.col(off++).normalize();
      }
    }
  }
  static void addIndexBlock(STrips& trips,const set<sizeType>& rowId,sizeType offCol,const Matd& basis) {
    sizeType off=0;
    for(set<sizeType>::const_iterator beg=rowId.begin(),end=rowId.end(); beg!=end; beg++,off++)
      for(sizeType c=0; c<basis.cols(); c++)
        trips.push_back(STrip(*beg,offCol+c,basis(off,c)));
  }
  static void setIndexBlock(Vec& cons0,const set<sizeType>& rowId,const Vec& basis0) {
    sizeType off=0;
    ASSERT((sizeType)rowId.size() == basis0.size())
    for(set<sizeType>::const_iterator beg=rowId.begin(),end=rowId.end(); beg!=end; beg++)
      cons0[*beg]=basis0[off++];
  }
  //data
  ParallelMatrix<Vec> _fgradP;
  vector<boost::shared_ptr<ClothEnergy> > _energys;
  boost::shared_ptr<ArticulatedBodyEnergy> _collA;
  Eigen::SimplicialCholesky<SMat> _invCTC;
  boost::shared_ptr<ClothMesh>& _mesh;
  boost::shared_ptr<SMat> _cons;
  boost::shared_ptr<Vec> _cons0;
  bool _NMesh;
};
void optimizeClothLM(ClothEnergySolver::Vec& XNew,scalar eps,scalar epsF,ClothObjective& obj,Callback<scalarD,Kernel<scalarD> >& cb,
                     const ClothEnergySolver::Vec& cvec,const ClothEnergySolver::STrips& cjac,bool SPD)
{
  LMInterface sol;
  sol.setTolF(epsF);
  sol.setTolX(0);
  sol.setTolG(eps);
  sol.setTauMax(1E9f);
  sol.setTau(1E-6f);
  if(SPD) {
#ifdef CHOLMOD_SUPPORT
    sol.useInterface(boost::shared_ptr<LMInterface::LMDenseInterface>(new CholmodWrapper()));
#endif
  } else {
    ClothEnergySolver::SMat tmpCJac;
    tmpCJac.resize(cvec.size(),XNew.size());
    tmpCJac.setFromTriplets(cjac.begin(),cjac.end());
    sol.setCI(tmpCJac,cvec,Coli::Zero(cvec.size()));
#ifdef UMFPACK_SUPPORT
    sol.useInterface(boost::shared_ptr<LMInterface::LMDenseInterface>(new CholmodWrapper()));
#endif
  }
  XNew=obj.assemble(XNew);
  //sol.debugGradient(obj,XNew+Vec::Random(XNew.size())*XNew.cwiseAbs().maxCoeff(),Cold::Ones(XNew.size()));
  sol.solveSparse(XNew,obj,cb);
  XNew=obj.assign(XNew);
}
void optimizeClothLBFGS(ClothEnergySolver::Vec& XNew,scalar eps,scalar epsF,ClothObjective& obj,Callback<scalarD,Kernel<scalarD> >& cb,
                        const ClothEnergySolver::Vec& cvec,const ClothEnergySolver::STrips& cjac,bool SPD)
{
  scalarD fx;
  LBFGSMinimizer<scalarD> sol;
  sol.nrCorrect()=100;
  sol.delta()=epsF;
  sol.epsilon()=eps;
  XNew=obj.assemble(XNew);
  sol.minimize(XNew,fx,obj,cb);
  ASSERT_MSG(SPD,"LBFGS solver cannot handle constraints!")
  //LMInterface().debugGradient(obj,XNew,Cold::Ones(XNew.size()));
  XNew=obj.assign(XNew);
}
//ClothEnergySolver
ClothEnergySolver::ClothEnergySolver(boost::shared_ptr<ClothMesh> mesh,bool NMesh)
  :ClothConstraintSolver(mesh,NMesh),_ignoreC(false),_searchD(false)
{
  addEnergyKinetic();
}
void ClothEnergySolver::addEnergyKinetic()
{
  if(_NMesh) {
    for(sizeType i=0; i<(sizeType)_mesh->_ess.size(); i++) {
      boost::shared_ptr<ClothEnergy> E(new ClothKineticEnergy<ClothMesh::ClothEdge>(_mesh->_ess[i]));
      _energys.push_back(E);
    }
  } else {
    for(sizeType i=0; i<(sizeType)_mesh->_vss.size(); i++) {
      boost::shared_ptr<ClothEnergy> E(new ClothKineticEnergy<ClothMesh::ClothVertex>(_mesh->_vss[i]));
      _energys.push_back(E);
    }
  }
}
void ClothEnergySolver::addEnergyRegularize(scalarD coef)
{
  if(_NMesh) {
    for(sizeType i=0; i<(sizeType)_mesh->_ess.size(); i++) {
      boost::shared_ptr<ClothEnergy> E(new ClothConstraintEnergy<ClothMesh::ClothEdge>(_mesh->_ess[i],Vec3d::Zero(),coef));
      _energys.push_back(E);
    }
  } else {
    for(sizeType i=0; i<(sizeType)_mesh->_vss.size(); i++) {
      boost::shared_ptr<ClothEnergy> E(new ClothConstraintEnergy<ClothMesh::ClothVertex>(_mesh->_vss[i],Vec3d::Zero(),coef));
      _energys.push_back(E);
    }
  }
}
void ClothEnergySolver::addEnergyHookean(scalarD K,bool linear,bool SPD,bool QUAD,bool rest)
{
  if(_NMesh) {
    for(sizeType i=0; i<(sizeType)_mesh->_tss.size(); i++) {
      ClothMesh::ClothTriangle& t=*(_mesh->_tss[i]);
      for(sizeType j=0; j<3; j++) {
        sizeType jn=(j+1)%3;
        boost::shared_ptr<ClothEnergy> E;
        if(!linear)E.reset(new ClothHookeanEnergy<ClothMesh::ClothEdge>(t._e[j],t._e[jn],(t._e[j]->_pos0-t._e[jn]->_pos0).norm(),K,SPD,QUAD));
        else if(rest)E.reset(new ClothRestLinearHookeanEnergy<ClothMesh::ClothEdge>(t._e[j],t._e[jn],(t._e[j]->_pos0-t._e[jn]->_pos0).norm(),K));
        else E.reset(new ClothLinearHookeanEnergy<ClothMesh::ClothEdge>(t._e[j],t._e[jn],(t._e[j]->_pos0-t._e[jn]->_pos0).norm(),K));
        _energys.push_back(E);
      }
    }
  } else {
    for(sizeType i=0; i<(sizeType)_mesh->_ess.size(); i++) {
      ClothMesh::ClothEdge& e=*(_mesh->_ess[i]);
      boost::shared_ptr<ClothEnergy> E;
      if(!linear)E.reset(new ClothHookeanEnergy<ClothMesh::ClothVertex>(e._v[0],e._v[1],(e._v[0]->_pos0-e._v[1]->_pos0).norm(),K,SPD,QUAD));
      else if(rest)E.reset(new ClothRestLinearHookeanEnergy<ClothMesh::ClothVertex>(e._v[0],e._v[1],(e._v[0]->_pos0-e._v[1]->_pos0).norm(),K));
      else E.reset(new ClothLinearHookeanEnergy<ClothMesh::ClothVertex>(e._v[0],e._v[1],(e._v[0]->_pos0-e._v[1]->_pos0).norm(),K));
      _energys.push_back(E);
    }
  }
}
void ClothEnergySolver::addEnergyStretch(scalarD mu,scalarD lambda)
{
  for(sizeType i=0; i<(sizeType)_mesh->_tss.size(); i++) {
    boost::shared_ptr<ClothEnergy> E;
    E.reset(new ClothStretchEnergy(_mesh->_tss[i],mu,lambda));
    _energys.push_back(E);
  }
}
void ClothEnergySolver::addEnergyQuadraticBend(scalarD K)
{
  if(_NMesh) {
    for(sizeType i=0; i<(sizeType)_mesh->_ess.size(); i++) {
      ClothMesh::ClothEdge& e=*(_mesh->_ess[i]);
      if(!e._t[0] || !e._t[1])continue;
      boost::shared_ptr<ClothEnergy> E;
      E.reset(new ClothQuadraticBendEnergy(_mesh->_ess[i],K,true));
      _energys.push_back(E);
    }
  } else {
    for(sizeType i=0; i<(sizeType)_mesh->_ess.size(); i++) {
      ClothMesh::ClothEdge& e=*(_mesh->_ess[i]);
      if(!e._t[0] || !e._t[1])continue;
      boost::shared_ptr<ClothEnergy> E;
      E.reset(new ClothQuadraticBendEnergy(_mesh->_ess[i],K,false));
      _energys.push_back(E);
    }
  }
}
void ClothEnergySolver::addEnergyBend(scalarD K)
{
  for(sizeType i=0; i<(sizeType)_mesh->_ess.size(); i++) {
    ClothMesh::ClothEdge& e=*(_mesh->_ess[i]);
    if(!e._t[0] || !e._t[1])continue;
    boost::shared_ptr<ClothEnergy> E;
    E.reset(new ClothBendEnergy(_mesh->_ess[i],K));
    _energys.push_back(E);
  }
}
void ClothEnergySolver::addEnergyARAP(scalarD K)
{
  map<sizeType,set<boost::shared_ptr<ClothMesh::ClothEdge> > > oneRing;
  for(sizeType i=0; i<(sizeType)_mesh->_ess.size(); i++) {
    oneRing[_mesh->_ess[i]->_v[0]->_index].insert(_mesh->_ess[i]);
    oneRing[_mesh->_ess[i]->_v[1]->_index].insert(_mesh->_ess[i]);
  }
  for(sizeType i=0; i<(sizeType)_mesh->_vss.size(); i++) {
    boost::shared_ptr<ClothEnergy> E;
    E.reset(new ClothARAPEnergy(_mesh->_vss[i],oneRing[i],K));
    _energys.push_back(E);
  }
}
void ClothEnergySolver::addEnergyMass(const Vec3d& G)
{
  if(_NMesh) {
    for(sizeType i=0; i<(sizeType)_mesh->_ess.size(); i++) {
      boost::shared_ptr<ClothEnergy> E;
      E.reset(new ClothFixedForceEnergy<ClothMesh::ClothEdge>(_mesh->_ess[i],G*_mesh->_ess[i]->_mass));
      _energys.push_back(E);
    }
  } else {
    for(sizeType i=0; i<(sizeType)_mesh->_vss.size(); i++) {
      boost::shared_ptr<ClothEnergy> E;
      E.reset(new ClothFixedForceEnergy<ClothMesh::ClothVertex>(_mesh->_vss[i],G*_mesh->_vss[i]->_mass));
      _energys.push_back(E);
    }
  }
}
void ClothEnergySolver::addEnergyFixClosestPoint(scalarD K,const Vec3d& pt,bool pull,bool pos0)
{
  sizeType id;
  Vec3d currPos;
  findClosestPoint(pt,id,currPos,pos0);
  boost::shared_ptr<ClothEnergy> e;
  if(_NMesh)e.reset(new ClothConstraintEnergy<ClothMesh::ClothEdge>(_mesh->_ess[id],pull ? pt : currPos,K));
  else e.reset(new ClothConstraintEnergy<ClothMesh::ClothVertex>(_mesh->_vss[id],pull ? pt : currPos,K));
  _energys.push_back(e);
}
void ClothEnergySolver::addEnergyFixClosestPointLine(scalarD K,const Vec3d& pt,Vec3d& D,bool line,bool pos0)
{
  sizeType id;
  Vec3d currPos;
  findClosestPoint(pt,id,currPos,pos0);
  boost::shared_ptr<ClothEnergy> e;
  if(_NMesh)e.reset(new ClothConstraintEnergy<ClothMesh::ClothEdge>(_mesh->_ess[id],currPos,D,K,line));
  else e.reset(new ClothConstraintEnergy<ClothMesh::ClothVertex>(_mesh->_vss[id],currPos,D,K,line));
  _energys.push_back(e);
}
boost::shared_ptr<ArticulatedBodyEnergy> ClothEnergySolver::addEnergyArticulatedBody(ArticulatedBody& body,const set<sizeType>* collisionSet,sizeType nrSubd)
{
  boost::shared_ptr<ArticulatedBodyEnergy> e;
  if(nrSubd <= 0)
    e.reset(new ArticulatedBodyEnergy(_mesh,body,collisionSet));
  else e.reset(new ArticulatedBodyEnergySubd(_mesh,body,collisionSet,nrSubd));
  addEnergy(e);
  return e;
}
template <typename T>
void ClothEnergySolver::removeEnergy()
{
  for(vector<boost::shared_ptr<ClothEnergy> >::iterator beg=_energys.begin(),end=_energys.end(); beg!=end; beg++)
    if(boost::dynamic_pointer_cast<T>(*beg))
      _energys.erase(beg);
}
void ClothEnergySolver::removeEnergy(boost::shared_ptr<ClothEnergy> e)
{
  vector<boost::shared_ptr<ClothEnergy> >::iterator it=find(_energys.begin(),_energys.end(),e);
  if(it != _energys.end())
    _energys.erase(it);
}
void ClothEnergySolver::addEnergy(boost::shared_ptr<ClothEnergy> e)
{
  vector<boost::shared_ptr<ClothEnergy> >::const_iterator it=find(_energys.begin(),_energys.end(),e);
  if(e && it == _energys.end())
    _energys.push_back(e);
}
void ClothEnergySolver::writeVTK(const string& str) const
{
  VTKWriter<scalarD> osE("Energys",str,true);
  for(sizeType i=0; i<(sizeType)_energys.size(); i++)
    _energys[i]->writeVTK(osE);
}
bool ClothEnergySolver::solve(scalarD dt,STEP_MODE mode,bool useCB,SOLVER_MODE smode)
{
  _mesh->saveLast();
  //get state variable
  Vec X,V,M,XOld;
  if(_NMesh)_mesh->assembleN(&X,&V,&M);
  else _mesh->assembleC(&X,&V,&M);
  _mesh->assembleC(&XOld);
  //newton iteration
  Vec XNew=X;
  //initialize LHS element of mass matrix
  for(sizeType i=0; i<(sizeType)_energys.size(); i++) {
    if(boost::dynamic_pointer_cast<ClothKineticEnergy<ClothMesh::ClothVertex> >(_energys[i]))
      boost::dynamic_pointer_cast<ClothKineticEnergy<ClothMesh::ClothVertex> >(_energys[i])->initialize(dt,mode);
    if(boost::dynamic_pointer_cast<ClothKineticEnergy<ClothMesh::ClothEdge> >(_energys[i]))
      boost::dynamic_pointer_cast<ClothKineticEnergy<ClothMesh::ClothEdge> >(_energys[i])->initialize(dt,mode);
  }
  //setup constraint
  if(_ignoreC)_nrC=0;
  else ClothConstraintSolver::assemble();
  //update
  Vec cvec;
  STrips cjac;
  bool SPD=false;
  ClothObjective obj(_energys,_mesh,_NMesh);
  if(!_ignoreC) {
    cvec.resize(_nrC);
    ClothConstraintSolver::operator()(Cold::Zero(X.size()),cvec,cjac);
    SPD=obj.analyzeCons(cjac,cvec);
  }
  OmpSettings::getOmpSettingsNonConst().setNrThreads(omp_get_num_procs());
  NoCallback<scalarD,Kernel<scalarD> > cbNo;
  Callback<scalarD,Kernel<scalarD> > cb;
  if(smode == LM) {
    optimizeClothLM(XNew,_eps,_epsF,obj,
                    useCB ? cb : cbNo,
                    cvec,cjac,SPD);
  } else if(smode == LBFGS) {
    optimizeClothLBFGS(XNew,_eps,_epsF,obj,
                       useCB ? cb : cbNo,
                       cvec,cjac,SPD);
  } else if(smode == HYBRID) {
    optimizeClothLBFGS(XNew,_eps*10,_epsF,obj,
                       useCB ? cb : cbNo,
                       cvec,cjac,SPD);
    optimizeClothLM(XNew,_eps,_epsF,obj,
                    useCB ? cb : cbNo,
                    cvec,cjac,SPD);
  } else {
    ASSERT_MSG(false,"Unknown ClothEnergySolver mode!")
  }
  //solve constraint
  if(_solC)
    _solC->solve(XNew);
  for(sizeType i=0; i<(sizeType)_energys.size(); i++) {
    ClothKineticEnergy<ClothMesh::ClothVertex>* ptrA=dynamic_cast<ClothKineticEnergy<ClothMesh::ClothVertex>*>(_energys[i].get());
    ClothKineticEnergy<ClothMesh::ClothEdge>* ptrB=dynamic_cast<ClothKineticEnergy<ClothMesh::ClothEdge>*>(_energys[i].get());
    if(ptrA)ptrA->finalize(dt,XNew);
    if(ptrB)ptrB->finalize(dt,XNew);
  }
  return adjustX(dt);
}
void ClothEnergySolver::solve(const string& path,scalarD t,scalarD dt,STEP_MODE mode,bool useCB,SOLVER_MODE smode)
{
  sizeType k=0;
  recreate(path);
  for(scalar ct=0; ct<t; ct+=dt) {
    INFOV("Cloth simulator at time: %f!",ct)
    if(!solve(dt,mode,useCB,smode))
      break;
    _mesh->writeVTKC(path+"/frm"+boost::lexical_cast<string>(k++)+".vtk");
  }
}
int ClothEnergySolver::operator()(const Vec& x,scalarD& FX,Vec& DFDX,scalarD step,bool wantGradient)
{
  if(_NMesh)_mesh->assignN(&x);
  else _mesh->assignC(&x);

  FX=0.0f;
  DFDX.resize(x.size());
  STrips HESS;
  for(sizeType i=0; i<(sizeType)_energys.size(); i++) {
    _energys[i]->_needHESS=false;
    FX-=_energys[i]->eval();
    _energys[i]->eval(DFDX,HESS,0);
  }
  DFDX*=-1.0f;
  return 0;
}
void ClothEnergySolver::setIgnoreC(bool ignoreC)
{
  _ignoreC=ignoreC;
}
void ClothEnergySolver::setSearchD(bool searchD)
{
  _searchD=searchD;
}
bool ClothEnergySolver::adjustX(scalarD dt)
{
  bool succ=true;
  //solve collision
  if(_NMesh) {
    Cold NOld;
    _mesh->assembleN(&NOld);
    _mesh->convertN2C();
    if(_solColl)
      succ=_solColl->solve();
    _mesh->convertC2N();
    for(sizeType i=0; i<(sizeType)_mesh->_ess.size(); i++) {
      ClothMesh::ClothEdge& e=*(_mesh->_ess[i]);
      e._vel+=(e._pos-NOld.block<3,1>(i*3,0))/dt;
    }
  } else {
    Cold COld;
    _mesh->assembleC(&COld);
    if(_solColl)
      succ=_solColl->solve();
    for(sizeType i=0; i<(sizeType)_mesh->_vss.size(); i++) {
      ClothMesh::ClothVertex& v=*(_mesh->_vss[i]);
      v._vel+=(v._pos-COld.block<3,1>(i*3,0))/dt;
    }
  }
  return succ;
}
