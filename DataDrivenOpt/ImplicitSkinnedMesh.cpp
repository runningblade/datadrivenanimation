#include "SkinnedMesh.h"
#include "LMInterface.h"
#include "ImplicitFunctionMeshing.h"
#include <CommonFile/CollisionDetection.h>
#include <CommonFile/PoissonDiskSampling.h>
#include <CommonFile/ParallelPoissonDiskSampling.h>
#include <CommonFile/geom/StaticGeomCell.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

//helper
scalar dist(const Vec3& pos,const ParticleSetN& pset,sizeType* index=NULL)
{
  sizeType minIndex=-1;
  scalar dist,minDist=ScalarUtil<scalar>::scalar_max;
  for(sizeType i=0; i<pset.size(); i++) {
    dist=(pset[i]._pos-pos).norm();
    if(dist < minDist) {
      minDist=dist;
      minIndex=i;
    }
  }
  if(index)
    *index=minIndex;
  return minDist;
}
ParticleN<scalar> addClosurePoint(sizeType f,sizeType t,const MatX3d& joints,const ParticleSetN& pset,scalar extrude)
{
  Vec3 vf=joints.row(f).cast<scalar>();
  Vec3 vt=joints.row(t).cast<scalar>();

  ParticleN<scalar> p;
  p._pos=vf;
  p._normal=(vt-vf).normalized();
  p._pos+=p._normal*dist(vf,pset)*extrude;
  return p;
}
ParticleN<scalar> addClosurePoint(sizeType f,const Vec3& dir,const MatX3d& joints,const ParticleSetN& pset,scalar extrude)
{
  Vec3 vf=joints.row(f).cast<scalar>();

  ParticleN<scalar> p;
  p._pos=vf;
  p._normal=dir;
  p._pos+=dir*dist(vf,pset)*extrude;
  return p;
}
sizeType findOther(sizeType v0,sizeType v1,sizeType t,const ObjMesh::EdgeMap& ess,const ObjMesh& obj,pair<int,int>& e)
{
  e=make_pair(obj.getI()[t][v0],obj.getI()[t][v1]);
  if(e.first>e.second)swap(e.first,e.second);
  ObjMesh::Edge E=ess._ess.find(e)->second;
  if(E._tris[0] == t)
    return E._tris[1];
  else {
    ASSERT(E._tris[1] == t)
    return E._tris[0];
  }
}
Vec3 getClosureDir(sizeType f,sizeType t,const MatX3d& joints)
{
  Vec3 vf=joints.row(f).cast<scalar>();
  Vec3 vt=joints.row(t).cast<scalar>();
  return (vt-vf).normalized();
}
void transformPSetInv(const Mat4& T,ParticleSetN& pset)
{
  for(sizeType i=0; i<pset.size(); i++) {
    pset[i]._pos=transformHomoInv<scalar>(T,pset[i]._pos);
    pset[i]._normal=T.block<3,3>(0,0).inverse()*pset[i]._normal;
    pset[i]._normal.normalize();
  }
}
void transformPSet(const Mat4& T,ParticleSetN& pset)
{
  for(sizeType i=0; i<pset.size(); i++) {
    pset[i]._pos=transformHomo<scalar>(T,pset[i]._pos);
    pset[i]._normal=T.block<3,3>(0,0)*pset[i]._normal;
    pset[i]._normal.normalize();
  }
}
void reflectObj(ObjMesh& obj,sizeType symAxis)
{
  for(sizeType i=0; i<(sizeType)obj.getV().size(); i++)
    obj.getV()[i][symAxis]*=-1;
  for(sizeType i=0; i<(sizeType)obj.getI().size(); i++)
    swap(obj.getI()[i][1],obj.getI()[i][2]);
  obj.smooth();
}
class SymmetryObjective : public Objective<scalarD>
{
public:
  SymmetryObjective(SkinnedMeshUsingArticulatedBody& skinnedMesh,const Coli& symJointMapV,const Vec& theta,sizeType symAxis,scalarD reg=1E-1f)
    :_skinnedMesh(skinnedMesh),_theta(theta),_reg(reg) {
    ObjMesh obj;
    STrips trips;
    sizeType nrV=0;
    _skinnedMesh.getMesh(Cold::Zero(_skinnedMesh.nrTheta()),NULL,NULL,NULL,NULL,&obj,true);
    for(sizeType i=0; i<symJointMapV.size(); i++)
      if(symJointMapV[i] == i)
        trips.push_back(STrip(nrV++,i*3+symAxis,1));
      else if(symJointMapV[i] < i)
        for(sizeType d=0; d<3; d++) {
          trips.push_back(STrip(nrV,i*3+d,1));
          trips.push_back(STrip(nrV++,symJointMapV[i]*3+d,d==symAxis?1:-1));
        }
    _symMap.resize(nrV,(sizeType)obj.getV().size()*3);
    _symMap.setFromTriplets(trips.begin(),trips.end());
  }
  virtual int operator()(const Vec& x,Vec& fvec,DMat* fjac,bool modifiable) override {
    //joints
    Vec theta=_theta;
    theta.segment(0,inputs())=x;
    //skinnedMesh
    Matd tmp;
    MatX3d meshv;
    vector<MatX3d,Eigen::aligned_allocator<MatX3d> > dmeshv;
    _skinnedMesh.getMesh(theta,NULL,NULL,&meshv,fjac ? &dmeshv : NULL,NULL,true);
    //assemble
    tmp=meshv.transpose();
    fvec=_symMap*Eigen::Map<Vec>(tmp.data(),tmp.size());
    if(fjac) {
      fjac->setZero(values(),inputs());
      for(sizeType j=0; j<inputs(); j++) {
        tmp=dmeshv[j].transpose();
        fjac->col(j)=_symMap*Eigen::Map<Vec>(tmp.data(),tmp.size());
      }
    }
    return 0;
  }
  virtual scalarD operator()(const Vec& x,Vec* fgrad,DMat* fhess) override {
    sizeType nrV=_skinnedMesh.nrTheta()-7;
    if(fgrad) {
      fgrad->setZero(inputs());
      fgrad->segment(3,nrV)=(x.segment(3,nrV)-_theta.segment(3,nrV))*_reg;
    }
    if(fhess) {
      fhess->setIdentity(inputs(),inputs());
      fhess->diagonal().segment(3,nrV).setOnes();
    }
    return (x.segment(3,nrV)-_theta.segment(3,nrV)).squaredNorm()*_reg/2;
  }
  int inputs() const override {
    return _skinnedMesh.nrTheta()-1;
  }
  int values() const override {
    return _symMap.rows();
  }
  //data
  SkinnedMeshUsingArticulatedBody& _skinnedMesh;
  SMat _symMap;
  Vec _theta;
  scalarD _reg;
};
//code to generate implicit mesh
Coli SkinnedMeshUsingArticulatedBody::detectSymmetry(pair<int,int> edge)
{
  ObjMesh obj;
  ObjMesh::EdgeMap ess;
  getMesh(Cold::Zero(nrTheta()),NULL,NULL,NULL,NULL,&obj,true);
  obj.buildEdge(ess);
  if(edge.first>edge.second)
    swap(edge.first,edge.second);
  pair<int,int> e,tris=make_pair(ess._ess[edge]._tris[0],ess._ess[edge]._tris[1]);
  set<pair<int,int>,ObjMesh::EdgeMap::LSS> visitedEdge;
  std::stack<pair<int,int> > stack;
  stack.push(tris);
  Coli ret=Coli::Constant(_weights.rows(),-1);
  ret[edge.first]=edge.first;
  ret[edge.second]=edge.second;
  while(!stack.empty()) {
    tris=stack.top();
    stack.pop();
    //match
    Vec3i t0m(-1,-1,-1);
    Vec3i t1m(-1,-1,-1);
    Vec3i t0=obj.getI()[tris.first];
    Vec3i t1=obj.getI()[tris.second];
    Vec3i rt0m(ret[t0[0]],ret[t0[1]],ret[t0[2]]);
    Vec3i rt1m(ret[t1[0]],ret[t1[1]],ret[t1[2]]);
    sizeType m0=-1,c0=0,m1=-1,c1=0;
    for(sizeType d=0; d<3; d++) {
      if(rt0m[d] >= 0) {
        for(sizeType d2=0; d2<3; d2++)
          if(t1[d2] == rt0m[d]) {
            ASSERT(rt1m[d2] == t0[d])
            t1m[d2]=d;
            t0m[d]=d2;
          }
        c0++;
      } else m0=d;
      if(rt1m[d] >= 0)
        c1++;
      else m1=d;
    }
    ASSERT(c0 == c1 && c0 >= 2 && c1 >= 2)
    if(c0 == 2 && c1 == 2) {
      ret[t0[m0]]=t1[m1];
      ret[t1[m1]]=t0[m0];
      t0m[m0]=m1;
      t1m[m1]=m0;
    }
    //propagate
    for(sizeType d=0; d<3; d++) {
      sizeType t0=findOther(d,(d+1)%3,tris.first,ess,obj,e);
      if(visitedEdge.find(e) != visitedEdge.end())
        continue;
      visitedEdge.insert(e);
      sizeType t1=findOther(t0m[d],t0m[(d+1)%3],tris.second,ess,obj,e);
      if(visitedEdge.find(e) != visitedEdge.end())
        continue;
      visitedEdge.insert(e);
      stack.push(make_pair(t0,t1));
    }
  }
  for(sizeType i=0; i<ret.size(); i++) {
    ASSERT(ret[i] >= 0)
  }
  return ret;
}
void SkinnedMeshUsingArticulatedBody::optimizeSymmetry(const Coli& symJointMapV,Cold& theta,sizeType symAxis,const string& debug)
{
  ObjMesh mesh;
  if(!debug.empty()) {
    recreate(debug);
    getMesh(theta,NULL,NULL,NULL,NULL,&mesh,true);
    mesh.writeVTK(debug+"/originalMesh.vtk",true);
    //write symmetric pair
    vector<Vec3i,Eigen::aligned_allocator<Vec3i> > lss,pss;
    for(sizeType i=0; i<(sizeType)symJointMapV.size(); i++)
      if(symJointMapV[i] == i)
        pss.push_back(Vec3i(i,i,i));
      else lss.push_back(Vec3i(i,symJointMapV[i],-1));
    VTKWriter<scalar> os("symmetry",debug+"/symmetry.vtk",true);
    os.appendPoints(mesh.getV().begin(),mesh.getV().end());
    os.appendCells(lss.begin(),lss.end(),VTKWriter<scalar>::LINE);
    os.appendCells(pss.begin(),pss.end(),VTKWriter<scalar>::POINT);
  }
  LMInterface sol;
  SymmetryObjective obj(*this,symJointMapV,theta,symAxis);
  //fix torso joints
  Callback<scalarD,Kernel<scalarD> > cb;
  NoCallback<scalarD,Kernel<scalarD> > noCb;
  sol.setTolF(0);
  sol.setTolG(1E-5f);
  Cold theta0=theta.segment(0,obj.inputs());
  //sol.debugGradient(obj,theta0+Cold::Random(obj.inputs())*0.1f);
  sol.solveDense(theta0,obj,!debug.empty() ? cb : noCb);
  theta.segment(0,obj.inputs())=theta0;
  if(!debug.empty()) {
    getMesh(theta,NULL,NULL,NULL,NULL,&mesh,true);
    mesh.writeVTK(debug+"/symmetricMesh.vtk",true);
  }
}
void SkinnedMeshUsingArticulatedBody::generateImplicitRigidBody(const Coli& jointMap,const Cold& theta,const Coli* symJointMap,sizeType symAxis,const set<sizeType>* exclude,scalar extrude,sizeType nrSubdivide,const string& debug)
{
#define EPS_FAIR 1E-4f
#define FAIR_COEF 1.0f
#define POISSON_RADIUS_COEF 0.5f
  MatX3d joints;
  scalar radius;
  ObjMesh obj,objI;
  BodyOpT<const Body> op(_body.nrBody());
  //segment
  getMesh(theta,&joints,NULL,NULL,NULL,&obj,true);
  _body.transform(_body.root(),op,(void*)&theta);
  radius=avgElementSize(obj);
  if(!debug.empty()) {
    recreate(debug);
    writeJointVTK(joints,debug+"/joints.vtk");
    obj.write(debug+"/mesh.obj");
  }
  INFO("Segmenting body!")
  MeshParts parts=segmentByBone(jointMap,joints,obj,exclude,debug);
  //sampling
  parts._samples.resize(nrJoint());
  ParallelPoissonDiskSampling sampler(3);
  sampler.getRadius()=radius*POISSON_RADIUS_COEF;
  sampler.getDensity()=30;
  for(sizeType j=0; j<nrJoint(); j++) {
    objI=obj;
    objI.getI().clear();
    const set<sizeType>& iss=parts._indices[j];
    for(set<sizeType>::const_iterator beg=iss.begin(),end=iss.end(); beg!=end; beg++)
      objI.getI().push_back(obj.getI()[*beg]);
    if(iss.empty())
      continue;
    sampler.sample(objI);
    if(!debug.empty())
      sampler.getPSet().writeVTK(debug+"/samplePart"+boost::lexical_cast<string>(j)+".vtk");
    parts._samples[j]=sampler.getPSet();
    //make symmetry
    if(symJointMap && symAxis >= 0) {
      detectPartSymmetry(j,parts,symJointMap,symAxis);
      if(!debug.empty())
        parts._samples[j].writeVTK(debug+"/sampleSymmetricPart"+boost::lexical_cast<string>(j)+".vtk");
    }
    //adjust, add closure constraint
    adjustSamples(parts._joints[j],joints,parts._samples[j],extrude);
    if(!debug.empty())
      parts._samples[j].writeVTK(debug+"/sampleClampedPart"+boost::lexical_cast<string>(j)+".vtk");
  }
  //generate body
  radius/=theta[nrTheta()-1];
  for(sizeType j=0; j<nrJoint(); j++) {
    //transform back to rest shape
    transformPSetInv(op.getTrans(j),parts._samples[j]);
    if(parts._samples[j].size() > 3) {
      INFOV("Generating body: %ld, radius: %f!",j,radius)
      if(symJointMap && symAxis >= 0 && (*symJointMap)[j] >= 0 && (*symJointMap)[j] < j) {
        _body.findBodyRef(_names[(*symJointMap)[j]]).mesh().getMesh(objI,true);
        reflectObj(objI,symAxis);
      } else {
        HRBFImplicitFunc f(parts._samples[j]);
        objI=implicitFuncToObj(f,radius,0.5f);
        objI.subdivide(nrSubdivide);
        implicitFuncFairObj(f,objI,radius*EPS_FAIR,0.5f,FAIR_COEF,!debug.empty());
      }
      boost::shared_ptr<ObjMeshGeomCell> geom(new ObjMeshGeomCell(Mat4::Identity(),objI,0,false));
      _body.findBodyRef(_names[j]).setMesh(geom);
      if(!debug.empty())
        objI.writeVTK(debug+"/bodyMesh"+boost::lexical_cast<string>(j)+".vtk",true);
    }
  }
  if(!debug.empty())
    _body.writeFrameVTK(debug+"/bodyMesh.vtk",&theta);
}
SkinnedMeshUsingArticulatedBody::MeshParts SkinnedMeshUsingArticulatedBody::segmentByBone(const Coli& jointMap,const MatX3d& joints,const ObjMesh& obj,const set<sizeType>* exclude,const string& debug) const
{
  MeshParts parts;
  //merge indices
  parts._indices.resize(nrJoint());
  for(sizeType i=0; i<(sizeType)obj.getI().size(); i++) {
    sizeType minIndex=-1;
    scalarD dist,minDist=ScalarUtil<scalarD>::scalar_max;
    Vec3d cp,b,ctr=obj.getTC(i).cast<scalarD>();
    for(sizeType j=0,parent; j<nrJoint(); j++) {
      parent=_kintree(0,j);
      if(parent >= 0 && parent < nrJoint()) {
        LineSegTpl<scalarD> l(joints.row(j),joints.row(parent));
        l.calcPointDist(ctr,dist,cp,b);
        if(dist < minDist) {
          minIndex=parent;
          minDist=dist;
        }
      }
    }
    parts._indices[minIndex].insert(i);
  }
  //exclude mesh parts
  if(exclude)
    for(set<sizeType>::const_iterator beg=exclude->begin(),end=exclude->end(); beg!=end; beg++)
      parts._indices[*beg].clear();
  //merge joints
  parts._joints.resize(nrJoint());
  for(sizeType j=0,parent; j<nrJoint(); j++) {
    parent=_kintree(0,j);
    parts._joints[j].insert(j);
    if(parent >= 0 && parent < nrJoint() && jointMap[j] < 0) {
      //merge indices
      set<sizeType>& partIndexP=parts._indices[parent];
      set<sizeType>& partIndex=parts._indices[j];
      partIndexP.insert(partIndex.begin(),partIndex.end());
      partIndex.clear();
      //merge joints
      set<sizeType>& partJointP=parts._joints[parent];
      set<sizeType>& partJoint=parts._joints[j];
      partJointP.insert(partJoint.begin(),partJoint.end());
      partJoint.clear();
    }
  }
  if(!debug.empty()) {
    vector<scalar> css(obj.getI().size(),-1);
    for(sizeType j=0; j<nrJoint(); j++) {
      const set<sizeType>& iss=parts._indices[j];
      for(set<sizeType>::const_iterator beg=iss.begin(),end=iss.end(); beg!=end; beg++)
        css[*beg]=(scalar)j;
    }
    obj.writeVTK(debug+"/mesh.vtk",true,false,false,NULL,&css);
  }
  return parts;
}
void SkinnedMeshUsingArticulatedBody::adjustSamples(set<sizeType> partJoints,const MatX3d& joints,ParticleSetN& pset,scalar extrude) const
{
#define CONVEXITY_THRES 0.9f
  //insert all direction children
  sizeType parent;
  vector<Plane> planes;
  set<sizeType> directChildren;
  for(sizeType j=0; j<nrJoint(); j++) {
    parent=_kintree(0,j);
    if(partJoints.find(parent) != partJoints.end())
      directChildren.insert(j);
  }
  partJoints.insert(directChildren.begin(),directChildren.end());
  //find all the ends
  ParticleSetN newPSet;
  for(set<sizeType>::const_iterator beg=partJoints.begin(),end=partJoints.end(); beg!=end; beg++) {
    //check if this is a valid joint
    bool valid=false;
    parent=_kintree(0,*beg);  //check parent
    if(parent >= 0 && parent < nrJoint() && partJoints.find(parent) == partJoints.end())
      valid=true;
    for(sizeType jj=0; jj<nrJoint(); jj++) {  //check children
      parent=_kintree(0,jj);
      if(parent == *beg && partJoints.find(jj) == partJoints.end())
        valid=true;
    }
    if(!valid)
      continue;
    //add closure point
    Vec3 dir=Vec3::Zero();
    parent=_kintree(0,*beg);  //check parent
    if(parent >= 0 && parent < nrJoint() && partJoints.find(parent) != partJoints.end())
      dir-=getClosureDir(*beg,parent,joints);
    for(sizeType jj=0; jj<nrJoint(); jj++) {  //check children
      parent=_kintree(0,jj);
      if(parent == *beg && partJoints.find(jj) != partJoints.end())
        dir-=getClosureDir(*beg,jj,joints);
    }
    dir.normalize();
    newPSet.addParticle(addClosurePoint(*beg,dir,joints,pset,extrude));
    planes.push_back(Plane(joints.row(*beg).cast<scalar>(),dir));
  }
  //convexity check
  vector<scalar> confidence(pset.size(),0);
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<pset.size(); i++) {
    for(sizeType j=0; j<pset.size(); j++)
      if(j!=i && pset[i]._normal.dot(pset[j]._pos-pset[i]._pos) < 0)
        confidence[i]++;
    confidence[i]/=(scalar)pset.size();
  }
  sizeType sz=0;
  for(sizeType i=0; i<pset.size(); i++)
    if(confidence[i] > CONVEXITY_THRES)
      pset[sz++]=pset[i];
  pset.resize(sz);
  //plane check
  sz=0;
  for(sizeType i=0; i<pset.size(); i++) {
    bool valid=true;
    for(sizeType p=0; p<(sizeType)planes.size(); p++)
      if(planes[p].side(pset[i]._pos) > 0)
        valid=false;
    if(valid)
      pset[sz++]=pset[i];
  }
  pset.resize(sz);
  //append
  pset.append(newPSet);
#undef CONVEXITY_THRES
}
void SkinnedMeshUsingArticulatedBody::detectPartSymmetry(sizeType i,MeshParts& parts,const Coli* symJointMap,sizeType symAxis) const
{
  ParticleSetN& pset=parts._samples[i];
  if(pset.size() > 0) {
    if((*symJointMap)[i] == -1) {
      sizeType sz=0;
      for(sizeType i=0; i<pset.size(); i++)
        if(pset[i]._pos[symAxis] < 0)
          pset[sz++]=pset[i];
      pset.resize(sz);
      for(sizeType i=0; i<sz; i++) {
        ParticleN<scalar> p=pset[i];
        p._pos[symAxis]*=-1;
        p._normal[symAxis]*=-1;
        pset.addParticle(p);
      }
    } else if((*symJointMap)[i] < i) {
      ASSERT(i == (*symJointMap)[(*symJointMap)[i]])
      ParticleSetN psetOther=parts._samples[(*symJointMap)[i]];
      pset=psetOther;
      for(sizeType i=0; i<pset.size(); i++) {
        pset[i]._pos[symAxis]*=-1;
        pset[i]._normal[symAxis]*=-1;
      }
    }
  }
}
scalar SkinnedMeshUsingArticulatedBody::avgElementSize(const ObjMesh& obj) const
{
  scalar area=0;
  for(sizeType i=0; i<(sizeType)obj.getI().size(); i++)
    area+=obj.getArea(i);
  return sqrt(area/(scalar)obj.getI().size());
}
