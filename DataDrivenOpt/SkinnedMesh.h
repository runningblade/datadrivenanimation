#ifndef SKINNED_MESH_H
#define SKINNED_MESH_H

#include "ArticulatedBody.h"
#include "HRBFImplicitFunc.h"

PRJ_BEGIN

class SkinnedMesh : public Serializable
{
public:
  SkinnedMesh();
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual void getJoint(const Cold& theta,const MatX3d& J,
                        vector<Mat4d,Eigen::aligned_allocator<Mat4d> >& tss,
                        vector<vector<Mat4d,Eigen::aligned_allocator<Mat4d> > >* dtss,
                        MatX3d* joints,Matd djoints[3])=0;
  virtual void getMesh(const Cold& theta,MatX3d* joints,
                       Matd djoints[3],MatX3d* meshv,
                       vector<MatX3d,Eigen::aligned_allocator<MatX3d> >* dmeshv,
                       ObjMesh* mesh,bool adjustPose);
  virtual void writeJointVTK(const MatX3d& J,const string& path) const;
  virtual void readPkl(const string& path);
  virtual void readBeta(const string& path);
  virtual sizeType nrTheta() const;
  virtual sizeType nrBeta() const;
  virtual sizeType nrJoint() const;
  virtual const Mat2Xi& kintree() const;
  //debug
  virtual void debugJoint(const string& path,scalar delta,sizeType nr);
  virtual void debugDJoint(bool adjustPose);
protected:
  string _bsStyle;
  string _bsType;
  Mat2Xi _kintree;
  MatX3d _vTemplate;
  MatX3i _f;
  Matd _weights;
  Matd _J;
  Matd _JRegressor;
  vector<MatX3d,Eigen::aligned_allocator<MatX3d> > _shapeDirs;
  vector<MatX3d,Eigen::aligned_allocator<MatX3d> > _poseDirs;
  Cold _beta;
};
class SkinnedMeshUsingArticulatedBody : public SkinnedMesh
{
public:
  struct MeshParts {
    vector<set<sizeType> > _indices;
    vector<set<sizeType> > _joints;
    vector<ParticleSetN> _samples;
  };
  friend class ICPObjective;
  SkinnedMeshUsingArticulatedBody();
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual void getJoint(const Cold& theta,const MatX3d& J,
                        vector<Mat4d,Eigen::aligned_allocator<Mat4d> >& tss,
                        vector<vector<Mat4d,Eigen::aligned_allocator<Mat4d> > >* dtss,
                        MatX3d* joints,Matd djoints[3]) override;
  virtual void readPkl(const string& path);
  virtual sizeType nrTheta() const;
  virtual const vector<string>& names() const;
  //debug
  virtual void debugJoint(const string& path,scalar delta,sizeType nr);
  //articulated body utility
  virtual void writeFrameSeqVTK(const string& path,sizeType begId=0,sizeType step=1);
  virtual void writeFrameVTK(const string& path,const Cold* dof=NULL);
  virtual void registerASF(const ArticulatedBody& other,Coli& jointMap,Cold& theta,Coli* symJointMap,sizeType symAxis,const string& checkWrite);
  virtual void registerAMC(const ArticulatedBody& other,const Coli& jointMap,const Cold& theta,const string& checkWrite);
  virtual const ArticulatedBody& getBody() const;
  virtual ArticulatedBody& getBody();
  //convert to implicit skinned mesh
  Coli detectSymmetry(pair<int,int> edge);
  void optimizeSymmetry(const Coli& symJointMapV,Cold& theta,sizeType symAxis,const string& debug);
  void generateImplicitRigidBody(const Coli& jointMap,const Cold& theta,const Coli* symJointMap,sizeType symAxis,const set<sizeType>* exclude,scalar extrude,sizeType nrSubdivide,const string& debug);
  MeshParts segmentByBone(const Coli& jointMap,const MatX3d& joints,const ObjMesh& obj,const set<sizeType>* exclude,const string& debug) const;
  void adjustSamples(set<sizeType> partJoints,const MatX3d& joints,ParticleSetN& pset,scalar extrude) const;
  void detectPartSymmetry(sizeType i,MeshParts& parts,const Coli* symJointMap,sizeType symAxis) const;
  scalar avgElementSize(const ObjMesh& obj) const;
protected:
  vector<string> _names;
  ArticulatedBody _body;
};

PRJ_END

#endif
