#ifndef DT_ENTRY_H
#define DT_ENTRY_H

#include <CommonFile/MathBasic.h>

PRJ_BEGIN

//sparse matrix
class DTEntry
{
public:
  DTEntry();
  DTEntry(const Mat4& Dq,const Mat4& DqDt,sizeType col);
  virtual ~DTEntry() {}
  bool operator<(const DTEntry& other) const;
  bool operator==(const DTEntry& other) const;
  static void merge(const vector<DTEntry>& I,const vector<DTEntry>& J,vector<DTEntry>& out);
  static void merge(vector<pair<sizeType,Mat4> >& J,vector<pair<sizeType,Mat4> >& out);
  Mat4 _Dq,_DqDt;
  sizeType _col;
  Vec6 _J,_DJDt;
  //compute on request
  Mat6 _DMgDq;
  vector<pair<sizeType,Mat4> > _DDTDDq,_crossTerm;
};
class DTTransfer
{
public:
  void computeJ(const Mat6& M);
  void computeDMgDq(const Mat6& M);
  vector<DTEntry> _entry;
  Mat6 _Mg,_cwMg;
  Mat4 _T,_DTDt;
  Vec6 _vel;
};

PRJ_END

#endif
