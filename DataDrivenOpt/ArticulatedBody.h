#ifndef ARTICULATED_BODY_H
#define ARTICULATED_BODY_H

#include "Utils.h"
#include "DTEntry.h"
#include <CommonFile/geom/StaticGeom.h>
#include <boost/unordered_map.hpp>

PRJ_BEGIN

enum DOF_TYPE {TX,TY,TZ,RX,RY,RZ,S};
class DOF : public Serializable
{
  friend class Body;
public:
  DOF();
  DOF(DOF_TYPE type,sizeType off);
  virtual bool read(istream& is, IOData* dat);
  virtual bool write(ostream& os, IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  void setUnit(scalar length, scalar angle);
  string typeStr() const;
  char _type;
  sizeType _off;
  Vec2 _lmt;
};
class Body : public Serializable
{
  friend class SkinnedMeshUsingArticulatedBody;
  friend class ArticulatedBody;
  friend class BodyOpBuildM;
public:
  class ForceContact
  {
  public:
    ForceContact();
    ForceContact(const Vec3& r,const Vec3& f);
    ForceContact(const Vec3& r,const Vec3& x0,const Vec3& n);
    Vec3 _r;	//in reference space
    Vec3 _f;	//in global space
    Vec3 _x0,_n;	//contact point/normal with depth
    bool _isForce;
    const Body* _body;
  };
  Body(bool rodriguez=false);
  virtual ~Body();
  virtual bool read(istream& is, IOData* dat);
  virtual bool write(ostream& os, IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  bool readASF(string& buf,istream& is, scalar length, scalar angle,sizeType& dof);
  Mat4d buildMat(const Cold& dat,Matd* ddat=NULL,const Mat4d& T0=Mat4d::Identity()) const;
  //getter
  const Mat6& M() const;
  const Vec3& dir() const;
  const Vec3& COM() const;
  const Mat4& toParent() const;
  const vector<DOF>& dof() const;
  sizeType id() const;
  const string& name() const;
  bool hasParent() const;
  const Body& parent() const;
  bool hasMesh() const;
  const StaticGeomCell& mesh() const;
  boost::shared_ptr<StaticGeomCell> meshRef() const;
  void setMesh(boost::shared_ptr<StaticGeomCell> geom);
private:
  static void addChild(boost::shared_ptr<Body> parent,boost::shared_ptr<Body> child);
  static void buildMap(boost::unordered_map<string, boost::shared_ptr<Body> >& BodyMap,boost::shared_ptr<Body> body);
  //param
  Mat6 _M;
  Vec3 _dir,_COM;
  Mat4 _toParent;
  vector<DOF> _dof;
  boost::shared_ptr<StaticGeomCell> _geom;
  //misc
  sizeType _id;
  string _name;
  boost::shared_ptr<Body> _child,_next;
  boost::weak_ptr<Body> _parent;
  bool _rodriguez;  //or Euler-angle
};
template <typename Body>
class BodyOp
{
public:
  BodyOp(sizeType nrBody) {}
  virtual void operator()(Body& b,void* data)=0;
  virtual ~BodyOp() {}
};
template <typename Body>
class BodyOpT : public BodyOp<Body>
{
public:
  BodyOpT(sizeType nrBody,sizeType DT=0)
    :BodyOp<Body>(nrBody) {
    _T.setZero(4,4*nrBody);
    if(DT > 0)
      _DT.setZero(4*nrBody,4*DT);
  }
  virtual ~BodyOpT() {}
  virtual void operator()(Body& b,void* data) {
    Eigen::Map<Mat4d> T=getTransRef(b.id());
    T.setIdentity();
    if(b.hasParent())
      T=getTransRef(b.parent().id());
    T*=b.toParent().template cast<scalarD>();
    if(data) {
      Mat4d TVar=b.buildMat(*(Cold*)data,_DT.size() > 0 ? &_DT : NULL,T);
      if(_DT.size() > 0 && b.hasParent()) {
        Mat4d TVarToCurr=b.toParent().template cast<scalarD>()*TVar;
        for(sizeType c=0; c<_DT.cols(); c+=4)
          _DT.block<4,4>(b.id()*4,c)+=_DT.block<4,4>(b.parent().id()*4,c)*TVarToCurr;
      }
      T*=TVar;
    }
  }
  Mat4 getTrans(sizeType id) const {
    return _T.block<4,4>(0,id*4).cast<scalar>();
  }
  Mat4d getTransD(sizeType id) const {
    return _T.block<4,4>(0,id*4);
  }
  const Mat4Xd& getTrans() const {
    return _T;
  }
  Mat4Xd getDTransDir(const Cold& data) const {
    Mat4Xd ret=Mat4Xd::Zero(_T.rows(),_T.cols());
    for(sizeType r=0; r<_DT.rows(); r+=4)
      for(sizeType c=0; c<_DT.cols(); c+=4)
        ret.block<4,4>(0,r)+=_DT.block<4,4>(r,c)*data[c/4];
    return ret;
  }
  const Matd& getDTrans() const {
    return _DT;
  }
  const Mat4 getDTrans(sizeType r,sizeType c) const {
    return _DT.block<4,4>(r*4,c*4).cast<scalar>();
  }
  const Mat4d getDTransD(sizeType r,sizeType c) const {
    return _DT.block<4,4>(r*4,c*4);
  }
  BBox<scalar> getBB() const {
    BBox<scalar> bb;
    for(sizeType i=0; i<_T.cols(); i+=4)
      bb.setUnion(_T.block<3,1>(0,i+3).cast<scalar>());
    return bb;
  }
  BBox<scalarD> getBBD() const {
    BBox<scalarD> bb;
    for(sizeType i=0; i<_T.cols(); i+=4)
      bb.setUnion(_T.block<3,1>(0,i+3));
    return bb;
  }
private:
  Eigen::Map<Mat4d> getTransRef(sizeType id) {
    return Eigen::Map<Mat4d>(_T.data()+id*16);
  }
  Mat4Xd _T;
  Matd _DT;
};
class ArticulatedBody : public Serializable
{
  friend class SkinnedMeshUsingArticulatedBody;
public:
  typedef boost::unordered_map<string, boost::shared_ptr<Body> > BodyMap;
  ArticulatedBody();
  ArticulatedBody(const BodyMap& bodies);
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  bool readFrm(istream& is,IOData* dat);
  bool writeFrm(ostream& os,IOData* dat) const;
  bool readASF(istream& is,const ObjMesh& joint);
  bool readAMC(istream& is);
  void setFrm(const Matd& frm);
  void clearFrm();
  void randomShuffle();
  Body& root();
  const Body& root() const;
  const Matd& frm() const;
  sizeType nrFrm() const;
  sizeType nrDof() const;
  sizeType nrBody() const;
  sizeType nrValidBody() const;
  const Body& findBody(sizeType id) const;
  const Body& findBody(const string& name) const;
  Body& findBodyRef(sizeType id);
  Body& findBodyRef(const string& name);
  void writeFrameSeqVTK(const string& path,sizeType begId=0,sizeType step=1,const string& refName="") const;
  void writeFrameVTK(const string& path,const Cold* dof=NULL,const string& refName="") const;
  void writeJointsSeqVTK(const string& path,sizeType begId=0,sizeType step=1) const;
  void writeJointsVTK(const string& path,const Cold* dof=NULL) const;
  void writeBodyConfiguration() const;
  void transform(Body& b,BodyOp<Body>& op,void* dof);
  void transform(const Body& b,BodyOp<const Body>& op,void* dof) const;
  void debugDJoint();
  void buildBodyM(const ObjMesh& joint);
private:
  void buildToParent(Body& b);
  BodyMap  _bodyMap;
  Matd _frm;
};

PRJ_END

#endif
