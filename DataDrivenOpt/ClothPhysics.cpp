#include "ClothPhysics.h"
#include "LMInterface.h"
#include "LMCholmod.h"
#include <CommonFile/CollisionDetection.h>
#include <CommonFile/solvers/Minimizer.h>

USE_PRJ_NAMESPACE

ClothConstraintSolver::ClothConstraintSolver(boost::shared_ptr<ClothMesh> mesh,bool NMesh)
  :_mesh(mesh),_NMesh(NMesh),_eps(1E-5f),_epsF(_eps),_nrIter(100) {}
void ClothConstraintSolver::addConstraintArea()
{
  if(_NMesh) {
    for(sizeType i=0; i<(sizeType)_mesh->_tss.size(); i++) {
      const ClothMesh::ClothTriangle& t=*(_mesh->_tss[i]);
      boost::shared_ptr<ClothAreaConstraint<ClothMesh::ClothEdge> > c
      (new ClothAreaConstraint<ClothMesh::ClothEdge>(t._e[0],t._e[1],t._e[2]));
      _cons.push_back(c);
    }
  } else {
    for(sizeType i=0; i<(sizeType)_mesh->_vss.size(); i++) {
      const ClothMesh::ClothTriangle& t=*(_mesh->_tss[i]);
      boost::shared_ptr<ClothAreaConstraint<ClothMesh::ClothVertex> > c
      (new ClothAreaConstraint<ClothMesh::ClothVertex>(t.getV0(),t.getV1(),t.getV2()));
      _cons.push_back(c);
    }
  }
}
void ClothConstraintSolver::addConstraintInextensible()
{
  if(_NMesh) {
    for(sizeType i=0; i<(sizeType)_mesh->_tss.size(); i++) {
      const ClothMesh::ClothTriangle& t=*(_mesh->_tss[i]);
      addSingleInextensibleN(t._e[0],t._e[1]);
      addSingleInextensibleN(t._e[1],t._e[2]);
      addSingleInextensibleN(t._e[2],t._e[0]);
    }
  } else {
    for(sizeType i=0; i<(sizeType)_mesh->_ess.size(); i++) {
      const ClothMesh::ClothEdge& e=*(_mesh->_ess[i]);
      const scalarD len=(e._v[0]->_pos0-e._v[1]->_pos0).norm();
      boost::shared_ptr<ClothConstraint> c(new ClothInextensibleConstraint<ClothMesh::ClothVertex>(e._v[0],e._v[1],len));
      _cons.push_back(c);
    }
  }
}
void ClothConstraintSolver::addSingleInextensibleN(boost::shared_ptr<ClothMesh::ClothEdge> e0,boost::shared_ptr<ClothMesh::ClothEdge> e1)
{
  const scalarD len=(e0->_pos0-e1->_pos0).norm();
  boost::shared_ptr<ClothConstraint> c(new ClothInextensibleConstraint<ClothMesh::ClothEdge>(e0,e1,len));
  _cons.push_back(c);
}
void ClothConstraintSolver::addConstraintBoundary()
{
  if(_NMesh) {
    std::vector<std::vector<boost::shared_ptr<ClothMesh::ClothTriangle> > > boundary;
    _mesh->findBoundary(&boundary,NULL);
    for(sizeType i=0; i<(sizeType)boundary.size(); i++) {
      for(sizeType j=1; j<(sizeType)boundary[i].size(); j++) {
        boost::shared_ptr<ClothConstraint> c
        (new ClothBoundaryConstraint(boundary[i][j-1],boundary[i][j],_mesh->_vss[i]));
        _cons.push_back(c);
      }
    }
  }
}
void ClothConstraintSolver::findClosestPoint(const Vec3d& pt,sizeType& id,Vec3d& currPos,bool pos0) const
{
  scalarD dist=ScalarUtil<scalarD>::scalar_max;
  id=-1;
  Vec X;
  if(_NMesh)_mesh->assembleN(&X,NULL,NULL,pos0);
  else _mesh->assembleC(&X,NULL,NULL,pos0);
  for(sizeType i=0; i<(sizeType)X.size(); i+=3) {
    scalarD d=(X.block<3,1>(i,0)-pt).norm();
    if(d < dist) {
      id=i/3;
      dist=d;
      currPos=X.block<3,1>(i,0);
    }
  }
}
void ClothConstraintSolver::fixPointAbove(const Vec4d& plane,bool pos0)
{
  Vec X;
  if(_NMesh)_mesh->assembleN(&X,NULL,NULL,pos0);
  else _mesh->assembleC(&X,NULL,NULL,pos0);
  for(sizeType i=0; i<(sizeType)X.size(); i+=3) {
    Vec3d pt=X.block<3,1>(i,0);
    if(pt.dot(plane.block<3,1>(0,0))+plane[3] > 0.0f) {
      boost::shared_ptr<ClothConstraint> c;
      if(_NMesh)c.reset(new ClothFixedConstraint<ClothMesh::ClothEdge>(_mesh->_ess[i/3],pt));
      else c.reset(new ClothFixedConstraint<ClothMesh::ClothVertex>(_mesh->_vss[i/3],pt));
      _cons.push_back(c);
    }
  }
}
boost::shared_ptr<ClothConstraint> ClothConstraintSolver::fixClosestPoint(const Vec3d& pt,bool pull,bool pos0)
{
  sizeType id;
  Vec3d currPos;
  findClosestPoint(pt,id,currPos,pos0);
  boost::shared_ptr<ClothConstraint> c;
  if(_NMesh)c.reset(new ClothFixedConstraint<ClothMesh::ClothEdge>(_mesh->_ess[id],pull ? pt : currPos));
  else c.reset(new ClothFixedConstraint<ClothMesh::ClothVertex>(_mesh->_vss[id],pull ? pt : currPos));
  _cons.push_back(c);
  return c;
}
boost::shared_ptr<ClothConstraint> ClothConstraintSolver::fixClosestPointLine(const Vec3d& pt,const Vec3d& dir,bool pos0)
{
  sizeType id;
  Vec3d currPos;
  findClosestPoint(pt,id,currPos,pos0);
  boost::shared_ptr<ClothConstraint> c;
  if(_NMesh)c.reset(new ClothLineConstraint<ClothMesh::ClothEdge>(_mesh->_ess[id],currPos,dir));
  else c.reset(new ClothLineConstraint<ClothMesh::ClothVertex>(_mesh->_vss[id],currPos,dir));
  _cons.push_back(c);
  return c;
}
boost::shared_ptr<ClothConstraint> ClothConstraintSolver::fixClosestPointPlane(const Vec3d& pt,const Vec3d& n,bool pos0)
{
  sizeType id;
  Vec3d currPos;
  findClosestPoint(pt,id,currPos,pos0);
  boost::shared_ptr<ClothConstraint> c;
  if(_NMesh)c.reset(new ClothPlaneConstraint<ClothMesh::ClothEdge>(_mesh->_ess[id],currPos,n));
  else c.reset(new ClothPlaneConstraint<ClothMesh::ClothVertex>(_mesh->_vss[id],currPos,n));
  _cons.push_back(c);
  return c;
}
void ClothConstraintSolver::writeVTK(const std::string& str) const
{
  VTKWriter<scalarD> os("Constraints",str,true);
  for(sizeType i=0; i<(sizeType)_cons.size(); i++)
    _cons[i]->writeVTK(os);
}
void ClothConstraintSolver::assemble()
{
  sizeType index=0;
  for(sizeType i=0; i<(sizeType)_cons.size(); i++) {
    _cons[i]->_index=index;
    index+=_cons[i]->nr();
  }
  _nrC=index;
}
void ClothConstraintSolver::solve(Vec& X)
{
  _X0=X;
  if(_NMesh)_mesh->assignN(&X);
  else _mesh->assignC(&X);

  Vec C,RHS,D;
  scalarD delta,regCoef=1E-8f;
  STrips JC;
  Eigen::SparseMatrix<scalarD,0,sizeType> J,JJT,Reg;
  boost::shared_ptr<LMInterface::LMDenseInterface> sol;
#ifdef CHOLMOD_SUPPORT
  sol.reset(new CholmodWrapper());
#else
  sol=LMInterface::getInterface(true);
#endif
  assemble();
  J.resize((int)_nrC,(int)X.size());
  Reg.resize((int)_nrC,(int)_nrC);
  for(int i=0; i<(int)_nrC; i++)
    Reg.coeffRef(i,i)=regCoef;
  for(sizeType iter=0; iter<_nrIter; iter++) {
    operator()(X,C,JC);
    delta=Kernel<scalarD>::absMax(C);
    INFOV("CMaxErr: %f",delta)
    if(iter > 3 && delta < _eps)
      break;

    J.setFromTriplets(JC.begin(),JC.end());
    JJT=J*J.transpose()+Reg;
    sol->recompute(JJT,0,false);

    RHS=C+J*(_X0-X);
    D=_X0-X-J.transpose()*sol->solve(RHS);
    search(X,D);
  }

  if(_NMesh)_mesh->assignN(&X);
  else _mesh->assignC(&X);
}
void ClothConstraintSolver::search(Vec& X,Vec& D)
{
  LineSearcherBackTracking<scalarD> searcher;
  Vec XTmp=X,DFDX;
  scalarD f,s=1.0f;
  operator()(X,f,DFDX,1,true);
  searcher(X,f,DFDX,D,s,XTmp,XTmp,XTmp,*this,LINESEARCH_MORETHUENTE);
}
int ClothConstraintSolver::operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient)
{
  if(_NMesh)_mesh->assignN(&x);
  else _mesh->assignC(&x);

  Vec C;
  STrips JC;
  operator()(x,C,JC);

  Eigen::SparseMatrix<scalarD,0,sizeType> J;
  J.resize((int)_nrC,(int)x.size());
  J.setFromTriplets(JC.begin(),JC.end());

  FX=C.squaredNorm()*0.5f;
  DFDX=J.transpose()*C;
  return 0;
}
int ClothConstraintSolver::operator()(const Vec& x,Vec& cvec,STrips& cjac)
{
  if(_NMesh)_mesh->assignN(&x);
  else _mesh->assignC(&x);
  cvec.resize(_nrC);
  cjac.clear();
  for(sizeType i=0; i<(sizeType)_cons.size(); i++)
    _cons[i]->eval(cvec,cjac,_cons[i]->_index);
  return 0;
}
int ClothConstraintSolver::inputs() const
{
  if(_NMesh)return (int)_mesh->_ess.size()*3;
  else return (int)_mesh->_vss.size()*3;
}
int ClothConstraintSolver::constraints() const
{
  return (int)_nrC;
}
void ClothConstraintSolver::setNrIter(sizeType iter)
{
  _nrIter=iter;
}
sizeType ClothConstraintSolver::nrIter() const
{
  return _nrIter;
}
scalarD ClothConstraintSolver::epsF() const
{
  return _epsF;
}
scalarD& ClothConstraintSolver::epsF()
{
  return _epsF;
}
scalarD ClothConstraintSolver::eps() const
{
  return _eps;
}
scalarD& ClothConstraintSolver::eps()
{
  return _eps;
}
bool ClothConstraintSolver::isNMesh() const
{
  return _NMesh;
}

//ClothFixedConstraint
template <typename T>
ClothFixedConstraint<T>::ClothFixedConstraint(boost::shared_ptr<T> v,const Vec3d& pos):_v(v),_pos(pos) {}
template <typename T>
void ClothFixedConstraint<T>::writeVTK(VTKWriter<scalarD>& os) const
{
  std::vector<Vec3d,Eigen::aligned_allocator<Vec3d> > vss;
  vss.push_back(_v->_pos);
  vss.push_back(_pos);
  os.setRelativeIndex();
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalarD>::IteratorIndex<Vec3i>(0,2,0),
                 VTKWriter<scalarD>::IteratorIndex<Vec3i>(1,2,0),
                 VTKWriter<scalarD>::LINE,true);
}
template <typename T>
sizeType ClothFixedConstraint<T>::nr() const
{
  return 3;
}
template <typename T>
void ClothFixedConstraint<T>::getRelatedVertex(std::vector<sizeType>& vs) const
{
  vs.push_back(_v->_index);
}
template <typename T>
void ClothFixedConstraint<T>::eval(Vec& c,STrips& grad,sizeType off) const
{
  Vec3d dv=_v->_pos-_pos;
  c.block<3,1>(off,0)=dv;
  addIK(grad,off,_v->_index*3,1.0f,3);
}
template <typename T>
Eigen::Matrix<scalarD,-1,3> ClothFixedConstraint<T>::getBasis() const
{
  Eigen::Matrix<scalarD,-1,3> ret;
  ret.resize(3,3);
  ret.row(0)=Vec3d::Unit(0);
  ret.row(1)=Vec3d::Unit(1);
  ret.row(2)=Vec3d::Unit(2);
  return ret;
}
template <typename T>
void ClothFixedConstraint<T>::project(Vec3d& pos,Vec3d& f) const
{
  pos=_pos;
  f.setZero();
}
//ClothLineConstraint
template <typename T>
ClothLineConstraint<T>::ClothLineConstraint(boost::shared_ptr<T> v,const Vec3d& pos,const Vec3d& dir):_v(v),_pos(pos),_dir(dir)
{
  _dir.normalize();
  _dir1=ClothMesh::perp(_dir);
  _dir2=_dir.cross(_dir1);
}
template <typename T>
void ClothLineConstraint<T>::writeVTK(VTKWriter<scalarD>& os) const
{
  std::vector<Vec3d,Eigen::aligned_allocator<Vec3d> > vss;
  vss.push_back(_v->_pos);
  vss.push_back(_pos-_dir);
  vss.push_back(_v->_pos);
  vss.push_back(_pos+_dir);
  os.setRelativeIndex();
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalarD>::IteratorIndex<Vec3i>(0,2,0),
                 VTKWriter<scalarD>::IteratorIndex<Vec3i>(2,2,0),
                 VTKWriter<scalarD>::LINE,true);
}
template <typename T>
sizeType ClothLineConstraint<T>::nr() const
{
  return 2;
}
template <typename T>
void ClothLineConstraint<T>::getRelatedVertex(std::vector<sizeType>& vs) const
{
  vs.push_back(_v->_index);
}
template <typename T>
void ClothLineConstraint<T>::eval(Vec& c,STrips& grad,sizeType off) const
{
  Vec3d dv=_v->_pos-_pos;
  Vec2d dvP(dv.dot(_dir1),dv.dot(_dir2));
  c.block<2,1>(off,0)=dvP;
  addBlock(grad,off+0,_v->_index*3,_dir1.transpose());
  addBlock(grad,off+1,_v->_index*3,_dir2.transpose());
}
template <typename T>
Eigen::Matrix<scalarD,-1,3> ClothLineConstraint<T>::getBasis() const
{
  Eigen::Matrix<scalarD,-1,3> ret;
  ret.resize(2,3);
  ret.row(0)=_dir1;
  ret.row(1)=_dir2;
  return ret;
}
template <typename T>
void ClothLineConstraint<T>::project(Vec3d& pos,Vec3d& f) const
{
  pos=_dir.dot(pos-_pos)*_dir+_pos;
  f=_dir.dot(f)*_dir;
}
//ClothPlaneConstraint
template <typename T>
ClothPlaneConstraint<T>::ClothPlaneConstraint(boost::shared_ptr<T> v,const Vec3d& pos,const Vec3d& dir):ClothLineConstraint<T>(v,pos,dir) {}
template <typename T>
sizeType ClothPlaneConstraint<T>::nr() const
{
  return 1;
}
template <typename T>
void ClothPlaneConstraint<T>::eval(Vec& c,STrips& grad,sizeType off) const
{
  Vec3d dv=_v->_pos-_pos;
  c[off]=dv.dot(_dir);
  addBlock(grad,off,_v->_index*3,_dir.transpose());
}
template <typename T>
Eigen::Matrix<scalarD,-1,3> ClothPlaneConstraint<T>::getBasis() const
{
  Eigen::Matrix<scalarD,-1,3> ret;
  ret.resize(1,3);
  ret.row(0)=_dir;
  return ret;
}
template <typename T>
void ClothPlaneConstraint<T>::project(Vec3d& pos,Vec3d& f) const
{
  pos-=_dir.dot(pos-_pos)*_dir;
  f-=_dir.dot(f)*_dir;
}
//ClothInextensibleConstraint
template <typename T>
ClothInextensibleConstraint<T>::ClothInextensibleConstraint(boost::shared_ptr<T> v0,boost::shared_ptr<T> v1,scalarD rest)
  :_v0(v0),_v1(v1),_rest(rest),_useSqr(true) {}
template <typename T>
void ClothInextensibleConstraint<T>::writeVTK(VTKWriter<scalarD>& os) const
{
  std::vector<Vec3d,Eigen::aligned_allocator<Vec3d> > vss;
  vss.push_back(_v0->_pos);
  vss.push_back(_v1->_pos);
  os.setRelativeIndex();
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalarD>::IteratorIndex<Vec3i>(0,0,1),
                 VTKWriter<scalarD>::IteratorIndex<Vec3i>(1,0,1),
                 VTKWriter<scalarD>::LINE,true);
}
template <typename T>
bool ClothInextensibleConstraint<T>::linear() const
{
  return false;
}
template <typename T>
sizeType ClothInextensibleConstraint<T>::nr() const
{
  return 1;
}
template <typename T>
void ClothInextensibleConstraint<T>::getRelatedVertex(std::vector<sizeType>& vs) const
{
  vs.push_back(_v0->_index);
  vs.push_back(_v1->_index);
}
template <typename T>
void ClothInextensibleConstraint<T>::eval(Vec& c,STrips& grad,sizeType off) const
{
  Vec3d x10;
  if(_useSqr) {
    c[off]=(_v0->_pos-_v1->_pos).squaredNorm()-_rest*_rest;
    x10=2.0f*(_v0->_pos-_v1->_pos);
  } else {
    c[off]=(_v0->_pos-_v1->_pos).norm()-_rest;
    x10=_v0->_pos-_v1->_pos;
    scalarD normSafe=std::max(x10.norm(),ScalarUtil<scalarD>::scalar_eps);
    x10/=normSafe;
  }
  addBlock(grad,off,_v0->_index*3, x10.transpose());
  addBlock(grad,off,_v1->_index*3,-x10.transpose());
}
//ClothBoundary Constraint
ClothBoundaryConstraint::ClothBoundaryConstraint
(boost::shared_ptr<ClothMesh::ClothTriangle> t0,
 boost::shared_ptr<ClothMesh::ClothTriangle> t1,
 boost::shared_ptr<ClothMesh::ClothVertex> v):_t0(t0),_t1(t1)
{
  _coef=TriangleTpl<scalarD>(t0->getV0()->_pos,t0->getV1()->_pos,t0->getV2()->_pos).area()+
        TriangleTpl<scalarD>(t1->getV0()->_pos,t1->getV1()->_pos,t1->getV2()->_pos).area();
  sizeType currPos=0;
  boost::shared_ptr<ClothMesh::ClothTriangle> t[2]= {t0,t1};
  for(sizeType tid=0; tid<2; tid++) {
    for(sizeType i=0; i<3; i++)
      if(t[tid]->_e[i]->_v[0] == v || t[tid]->_e[i]->_v[1] == v)
        _v[currPos++]=t[tid]->_e[i];
    for(sizeType i=0; i<3; i++)
      if(t[tid]->_e[i]->_v[0] != v && t[tid]->_e[i]->_v[1] != v)
        _v[currPos++]=t[tid]->_e[i];
    ASSERT(currPos == 3 || currPos == 6)
  }
  ASSERT(currPos == 6)
}
void ClothBoundaryConstraint::writeVTK(VTKWriter<scalarD>& os) const
{
  TriangleTpl<scalarD> t0(_t0->getV0()->_pos,_t0->getV1()->_pos,_t0->getV2()->_pos);
  TriangleTpl<scalarD> t1(_t1->getV0()->_pos,_t1->getV1()->_pos,_t1->getV2()->_pos);

  scalarD delta=sqrt(t0.area()+t1.area())*0.1f;
  std::vector<Vec3d,Eigen::aligned_allocator<Vec3d> > vss;
  vss.push_back(t0.masscenter());
  vss.push_back(t1.masscenter());
  vss.push_back((t0.masscenter()+t1.masscenter())*0.5f+
                (t0.normal()+t1.normal()).normalized()*delta);

  os.setRelativeIndex();
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells( VTKWriter<scalarD>::IteratorIndex<Vec3i>(0,0,1),
                  VTKWriter<scalarD>::IteratorIndex<Vec3i>(1,0,1),
                  VTKWriter<scalarD>::QUADRATIC_LINE,true);
}
sizeType ClothBoundaryConstraint::nr() const
{
  return 3;
}
void ClothBoundaryConstraint::getRelatedVertex(std::vector<sizeType>& vs) const
{
  for(sizeType i=0; i<6; i++)
    vs.push_back(_v[i]->_index);
}
void ClothBoundaryConstraint::eval(Vec& c,STrips& grad,sizeType off) const
{
  Vec3d dvC=_v[0]->_pos+_v[1]->_pos-_v[2]->_pos-
            _v[3]->_pos-_v[4]->_pos+_v[5]->_pos;
  c.block<3,1>(off,0)=dvC;
  addIK(grad,off,_v[0]->_index*3,+1.0f,3);
  addIK(grad,off,_v[1]->_index*3,+1.0f,3);
  addIK(grad,off,_v[2]->_index*3,-1.0f,3);
  addIK(grad,off,_v[3]->_index*3,-1.0f,3);
  addIK(grad,off,_v[4]->_index*3,-1.0f,3);
  addIK(grad,off,_v[5]->_index*3,+1.0f,3);
}
//ClothAreaConstraint
template <typename T>
ClothAreaConstraint<T>::ClothAreaConstraint(boost::shared_ptr<T> v0,boost::shared_ptr<T> v1,boost::shared_ptr<T> v2)
  :_v0(v0),_v1(v1),_v2(v2)
{
  const Vec2d p0=v0->_pos0.template block<2,1>(0,0);
  const Vec2d p1=v1->_pos0.template block<2,1>(0,0);
  const Vec2d p2=v2->_pos0.template block<2,1>(0,0);
  Vec2d d0=p1-p0;
  Vec2d d1=p2-p0;
  _area=d0[0]*d1[1]-d0[1]*d1[0];
}
template <typename T>
void ClothAreaConstraint<T>::writeVTK(VTKWriter<scalarD>& os) const
{
  std::vector<Vec3d,Eigen::aligned_allocator<Vec3d> > vss;
  vss.push_back(_v0->_pos);
  vss.push_back(_v1->_pos);
  vss.push_back(_v2->_pos);
  vss.push_back(_v0->_pos);
  os.setRelativeIndex();
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalarD>::IteratorIndex<Vec3i>(0,0,1),
                 VTKWriter<scalarD>::IteratorIndex<Vec3i>(3,0,1),
                 VTKWriter<scalarD>::LINE,true);
}
template <typename T>
bool ClothAreaConstraint<T>::linear() const
{
  return false;
}
template <typename T>
sizeType ClothAreaConstraint<T>::nr() const
{
  return 1;
}
template <typename T>
void ClothAreaConstraint<T>::getRelatedVertex(std::vector<sizeType>& vs) const
{
  vs.push_back(_v0->_index);
  vs.push_back(_v1->_index);
  vs.push_back(_v2->_index);
}
template <typename T>
void ClothAreaConstraint<T>::eval(Vec& c,STrips& grad,sizeType off) const
{
  const Vec2d p0=_v0->_pos.template block<2,1>(0,0);
  const Vec2d p1=_v1->_pos.template block<2,1>(0,0);
  const Vec2d p2=_v2->_pos.template block<2,1>(0,0);
  Vec2d d0=p1-p0;
  Vec2d d1=p2-p0;
  c[off]=d0[0]*d1[1]-d0[1]*d1[0]-_area;
  grad.push_back(Eigen::Triplet<scalarD,sizeType>(off,_v0->_index*3+0,p1[1]-p2[1]));
  grad.push_back(Eigen::Triplet<scalarD,sizeType>(off,_v0->_index*3+1,p2[0]-p1[0]));
  grad.push_back(Eigen::Triplet<scalarD,sizeType>(off,_v1->_index*3+0,p2[1]-p0[1]));
  grad.push_back(Eigen::Triplet<scalarD,sizeType>(off,_v1->_index*3+1,p0[0]-p2[0]));
  grad.push_back(Eigen::Triplet<scalarD,sizeType>(off,_v2->_index*3+0,p0[1]-p1[1]));
  grad.push_back(Eigen::Triplet<scalarD,sizeType>(off,_v2->_index*3+1,p1[0]-p0[0]));
}
