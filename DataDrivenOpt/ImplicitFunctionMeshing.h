#ifndef IMPLICIT_FUNCTION_MESHING_H
#define IMPLICIT_FUNCTION_MESHING_H

#include <CommonFile/ImplicitFuncInterface.h>
#include <CommonFile/ObjMesh.h>

PRJ_BEGIN

//normal mesher
ObjMesh implicitFuncToObj(const ImplicitFunc<scalar>& f,scalar radius,scalar level);
void implicitFuncFairObj(const ImplicitFunc<scalar>& f,ObjMesh& mesh,scalar eps,scalar level,scalar coef,bool debug);
void implicitFuncToVTK(const ImplicitFunc<scalar>& f,const string& path,scalar radius,scalar level);

PRJ_END

#endif
