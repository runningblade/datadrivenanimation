#include "SkinnedMeshClothUtility.h"
#include "ClothMesh.h"
#include <CommonFile/MakeMesh.h>
#include <CommonFile/RotationUtil.h>

USE_PRJ_NAMESPACE

//parameter: Dress
#define JID_DRESS 0
#define SUBD_DRESS 2
#define OVERSAMPLE_DRESS 1
//parameter: Cloak
#define JID_CLOAK 12
#define SUBD_CLOAK 2
#define OVERSAMPLE_CLOAK 1
//parameter: assign
#define ASSIGN_VERTEX(ID) \
if(back)  \
  vss[id*3+c]=datums[ID].float_data(k);  \
else ptr->Add(vss[id*3+c]);
//helper
vector<vector<boost::shared_ptr<ClothMesh::ClothVertex> > > clothGridIndices;
Vec3 findVertex(const vector<Vec3,Eigen::aligned_allocator<Vec3> >& vss,scalar len)
{
  scalar currLen=0;
  sizeType i=0;
  while(true) {
    Vec3 a=vss[i%vss.size()];
    Vec3 b=vss[(i+1)%vss.size()];
    scalar newLen=(b-a).norm()+currLen;
    if(newLen > len)
      return interp1D(a,b,(newLen-currLen)/(b-a).norm());
    else currLen=newLen;
    i++;
  }
  return Vec3::Zero();
}
sizeType findVertexIndex(const vector<Vec3,Eigen::aligned_allocator<Vec3> >& vss,const Vec3& v)
{
  for(sizeType i=0; i<(sizeType)vss.size(); i++)
    if((vss[i]-v).norm() < 1E-9f)
      return i;
  return -1;
}
//SkinnedMeshClothUtility
bool SkinnedMeshClothUtility::generateCloak(scalar height,scalar length,scalar sepLength,scalar sepAngle,scalar res,scalar mu,scalar lambda,scalar bend)
{
  //rest pose
  Cold theta0=restPose();
  BodyOpT<const Body> op(_body.getBody().nrBody());
  _body.getBody().transform(_body.getBody().root(),op,(void*)&theta0);
  //get mesh
  ObjMesh obj;
  const Body& b=_body.getBody().findBody(_body.names()[JID_CLOAK]);
  b.meshRef()->setT(op.getTrans(b.id()));
  b.meshRef()->getMesh(obj,false);
  //find ray intersections
  BBox<scalar> bb=obj.getBB();
  _avgElemSz=_body.avgElementSize(obj);
  vector<Vec3,Eigen::aligned_allocator<Vec3> > interp;
  for(scalar i=0; i<360; i+=1) {
    scalar ang=i*M_PI/180.0f;
    Vec3 ctr=(bb._minC+bb._maxC)/2;
    Vec3 outer=ctr+Vec3(cos(ang),0,sin(ang))*bb.getExtent().norm();
    ctr[1]=outer[1]=interp1D(bb._minC[1],bb._maxC[1],height);
    scalar s=b.mesh().rayQuery(outer,ctr-outer);
    if(s >= 1)
      return false;
    s-=_avgElemSz/(ctr-outer).norm();
    interp.push_back(outer+(ctr-outer)*s);
  }
  //generate mesh
  scalar ringLength=0;
  set<sizeType> removeVerts;
  vector<pair<Vec3,Vec3> > consVec;
  for(sizeType i=0; i<(sizeType)interp.size(); i++)
    ringLength+=(interp[i]-interp[(i+1)%interp.size()]).norm();
  scalar radius=ringLength/(M_PI*2);
  sizeType nrY=ceil(length/res);
  sizeType nrX=ceil((length/2+radius)*(M_PI*2)/res);
  MakeMesh::makeCylinder3D(obj,1,1,nrX,nrY,false);
  //find which edge should be removed to make an opening
  set<sizeType> removeIndices;
  for(sizeType x=0; x<nrX; x++) {
    Vec3& v=obj.getV()[x];
    scalar cosTheta=v[2]/sqrt(v[0]*v[0]+v[2]*v[2]);
    if(cosTheta > cos(sepAngle*M_PI/180.0f))
      removeIndices.insert(x);
  }
  //fillin vertices
  Vec3 ctr=Vec3::Zero();
  for(sizeType i=0; i<(sizeType)interp.size(); i++)
    ctr+=interp[i];
  ctr/=(scalar)interp.size();
  for(sizeType y=0,k=0; k<(sizeType)obj.getV().size(); y++)
    for(sizeType x=0; x<nrX; x++,k++) {
      Vec3& v=obj.getV()[k];
      v[1]=interp[0][1];
      scalar sinTheta=v[0]/sqrt(v[0]*v[0]+v[2]*v[2]);
      scalar cosTheta=v[2]/sqrt(v[0]*v[0]+v[2]*v[2]);
      v[0]=ctr[0]+sinTheta*(radius+length*(scalar)y/(scalar)nrY);
      v[2]=ctr[2]+cosTheta*(radius+length*(scalar)y/(scalar)nrY);
      if(removeIndices.find(x) != removeIndices.end())  //check if this edge should be removed
        if((scalar)y/(scalar)nrY > (length-sepLength)/length) //check if this edge in this location should be removed
          removeVerts.insert(k);
      if(y == 0)
        consVec.push_back(make_pair(v,findVertex(interp,ringLength*(scalar)x/(scalar)nrX)));
    }
  //remove triangles
  removeIndices.clear();
  for(sizeType i=0; i<(sizeType)obj.getI().size(); i++)
    if(removeVerts.find(obj.getI()[i][0]) != removeVerts.end() ||
       removeVerts.find(obj.getI()[i][1]) != removeVerts.end() ||
       removeVerts.find(obj.getI()[i][2]) != removeVerts.end()) {
      removeIndices.insert(i/2*2);
      removeIndices.insert(i/2*2+1);
    }
  sizeType nrI=0;
  for(sizeType i=0; i<(sizeType)obj.getI().size(); i++)
    if(removeIndices.find(i) == removeIndices.end())
      obj.getI()[nrI++]=obj.getI()[i];
  obj.getI().resize(nrI);
  obj.smooth();
  obj.makeUnique();
  obj.smooth();
  //rebuild constraint
  map<sizeType,Vec3> cons;
  for(sizeType i=0; i<(sizeType)consVec.size(); i++) {
    sizeType vid=findVertexIndex(obj.getV(),consVec[i].first);
    if(vid >= 0)
      cons[vid]=consVec[i].second;
  }
  //generate cloth simulator
  enableCollisionSet();
  addCollisionJoint(0);
  addCollisionJoint(1);
  addCollisionJoint(2);
  addCollisionJoint(4);
  addCollisionJoint(5);
  //addCollisionJoint(7);
  //addCollisionJoint(8);
  //addCollisionJoint(10);
  //addCollisionJoint(11);
  addCollisionJoint(3);
  addCollisionJoint(6);
  addCollisionJoint(9);
  addCollisionJoint(12);
  addCollisionJoint(15);
  addCollisionJoint(13);
  addCollisionJoint(14);
  addCollisionJoint(16);
  addCollisionJoint(17);
  addCollisionJoint(18);
  addCollisionJoint(19);
  //addCollisionJoint(20);
  //addCollisionJoint(21);
  //addCollisionJoint(22);
  //addCollisionJoint(23);
  generateSolver(obj,cons,JID_CLOAK,SUBD_CLOAK,mu,lambda,bend);
  disableCollisionSet();
  //_body.getBody().writeFrameVTK("body.vtk",&theta0);
  //obj.writeVTK("cloth.vtk",true);
  return true;
}
bool SkinnedMeshClothUtility::generateDress(scalar height,scalar length,scalar looseAngle,scalar res,scalar mu,scalar lambda,scalar bend)
{
  //rest pose
  Cold theta0=restPose();
  BodyOpT<const Body> op(_body.getBody().nrBody());
  _body.getBody().transform(_body.getBody().root(),op,(void*)&theta0);
  //get mesh
  ObjMesh obj;
  const Body& b=_body.getBody().findBody(_body.names()[JID_DRESS]);
  b.meshRef()->setT(op.getTrans(b.id()));
  b.meshRef()->getMesh(obj,false);
  //find ray intersections
  BBox<scalar> bb=obj.getBB();
  _avgElemSz=_body.avgElementSize(obj);
  vector<Vec3,Eigen::aligned_allocator<Vec3> > interp;
  for(scalar i=0; i<360; i+=1) {
    scalar ang=i*M_PI/180.0f;
    Vec3 ctr=(bb._minC+bb._maxC)/2;
    Vec3 outer=ctr+Vec3(cos(ang),0,sin(ang))*bb.getExtent().norm();
    ctr[1]=outer[1]=interp1D(bb._minC[1],bb._maxC[1],height);
    scalar s=b.mesh().rayQuery(outer,ctr-outer);
    if(s >= 1)
      return false;
    s-=_avgElemSz/(ctr-outer).norm();
    interp.push_back(outer+(ctr-outer)*s);
  }
  //generate mesh
  scalar ringLength=0;
  map<sizeType,Vec3> cons;
  for(sizeType i=0; i<(sizeType)interp.size(); i++)
    ringLength+=(interp[i]-interp[(i+1)%interp.size()]).norm();
  sizeType nrY=ceil(length/res);
  sizeType nrX=ceil(ringLength/res);
  MakeMesh::makeCylinder3D(obj,ringLength/2/M_PI,length/2,nrX,nrY/2,false);
  obj.getPos()=(bb._minC+bb._maxC)/2;
  obj.getPos()[1]=0;
  obj.applyTrans();
  for(sizeType y=0,k=0; k<(sizeType)obj.getV().size(); y++)
    for(sizeType x=0; x<nrX; x++,k++) {
      Vec3& v=obj.getV()[k];
      v[1]+=(interp[0][1]-length/2);
      scalar len=interp[0][1]-v[1];
      scalar ang=looseAngle*M_PI/180.0f;
      scalar sinPhi=v[0]/sqrt(v[0]*v[0]+v[2]*v[2]);
      scalar cosPhi=v[2]/sqrt(v[0]*v[0]+v[2]*v[2]);
      v[0]+=len*sin(ang)*sinPhi;
      v[2]+=len*sin(ang)*cosPhi;
      v[1]=interp[0][1]-len*cos(ang);
      if(k>=(sizeType)obj.getV().size()-nrX)
        cons[k]=findVertex(interp,ringLength*(scalar)x/(scalar)nrX);
    }
  obj.smooth();
  //generate cloth simulator
  enableCollisionSet();
  addCollisionJoint(0);
  addCollisionJoint(1);
  addCollisionJoint(2);
  addCollisionJoint(4);
  addCollisionJoint(5);
  //addCollisionJoint(7);
  //addCollisionJoint(8);
  //addCollisionJoint(10);
  //addCollisionJoint(11);
  generateSolver(obj,cons,JID_DRESS,SUBD_DRESS,mu,lambda,bend);
  disableCollisionSet();
  //_body.getBody().writeFrameVTK("body.vtk",&theta0);
  //obj.writeVTK("cloth.vtk",true);
  return true;
}
//convert cloth
void SkinnedMeshClothUtility::convertClothCloak(Cold& vss,vector<Datum>& datums,bool back) const
{
  if(back)
    vss.resize((sizeType)_mesh->_vss.size()*3);
  else datums.clear();
  if(clothGridIndices.empty()) {
    vector<vector<boost::shared_ptr<ClothMesh::ClothVertex> > > boundaryVL;
    _mesh->findBoundary(NULL,&boundaryVL);
    ASSERT_MSG(boundaryVL.size() == 2,"ClothDress mesh must have two boundaries!")
    _mesh->findGridIndex(clothGridIndices,boundaryVL[0]);
  }
  sizeType w=(int)clothGridIndices[0].size(),w2=0;
  sizeType h=(int)clothGridIndices.size(),h1=-1;
  sizeType wMin=w,wMax=0;
  for(sizeType i=0; i<h; i++)
    for(sizeType j=0; j<w; j++)
      if(h1 < 0 && !clothGridIndices[i][j])
        h1=i;
  if(h1 > 0) {
    google::protobuf::RepeatedField<float>* ptr=NULL;
    sizeType wExt=(int)(w*OVERSAMPLE_DRESS);
    //part1: top
    {
      Datum dat;
      dat.set_channels(3);
      dat.set_height(h1);
      dat.set_width(wExt); //we oversample the cloth
      ptr=dat.mutable_float_data();
      for(sizeType c=0,k=0; c<3; c++)
        for(sizeType i=0; i<h1; i++)
          for(sizeType j=0; j<wExt; j++,k++) {
            sizeType id=clothGridIndices[i][j%w]->_index;
            ASSIGN_VERTEX(0)
          }
      if(!back)
        datums.push_back(dat);
    }
    //part2: body
    {
      for(sizeType j=0; j<w; j++)
        if(!clothGridIndices[h1][j] && clothGridIndices[h1][(j+1)%w])
          for(wMin=(j+1)%w,wMax=wMin; clothGridIndices[h1][wMax]; wMax=(wMax+1)%w)
            w2++;
      Datum dat;
      dat.set_channels(3);
      dat.set_height(h);
      dat.set_width(w2);
      ptr=dat.mutable_float_data();
      for(sizeType c=0,k=0; c<3; c++)
        for(sizeType i=0; i<h; i++)
          for(sizeType j=wMin; j!=wMax; j=(j+1)%w,k++) {
            sizeType id=clothGridIndices[i][j]->_index;
            ASSIGN_VERTEX(1)
          }
      if(!back)
        datums.push_back(dat);
    }
  } else {
    //single part
    convertClothDress(vss,datums,back);
  }
}
void SkinnedMeshClothUtility::convertClothDress(Cold& vss,vector<Datum>& datums,bool back) const
{
  if(back)
    vss.resize((sizeType)_mesh->_vss.size()*3);
  else datums.clear();
  if(clothGridIndices.empty()) {
    vector<vector<boost::shared_ptr<ClothMesh::ClothVertex> > > boundaryVL;
    _mesh->findBoundary(NULL,&boundaryVL);
    ASSERT_MSG(boundaryVL.size() == 2,"ClothDress mesh must have two boundaries!")
    _mesh->findGridIndex(clothGridIndices,boundaryVL[0]);
  }
  sizeType w=(int)clothGridIndices[0].size();
  sizeType h=(int)clothGridIndices.size();
  sizeType wExt=(int)(w*OVERSAMPLE_DRESS);
  Datum dat;
  dat.set_channels(3);
  dat.set_height(h);
  dat.set_width(wExt); //we oversample the cloth
  google::protobuf::RepeatedField<float>* ptr=dat.mutable_float_data();
  for(sizeType c=0,k=0; c<3; c++)
    for(sizeType i=0; i<h; i++)
      for(sizeType j=0; j<wExt; j++,k++) {
        sizeType id=clothGridIndices[i][j%w]->_index;
        ASSIGN_VERTEX(0)
      }
  if(!back)
    datums.push_back(dat);
}
void SkinnedMeshClothUtility::convertCloth(Cold& vss,vector<Datum>& datums,PATTERN_TYPE pattern,bool back) const
{
  if(pattern == DRESS)
    convertClothDress(vss,datums,back);
  else if(pattern == CLOAK)
    convertClothCloak(vss,datums,back);
  else {
    ASSERT_MSG(false,"Unknown pattern type!")
  }
}
void SkinnedMeshClothUtility::convertClothToCaffe(const Cold& vss,const Cold& theta,vector<Datum>& datums,PATTERN_TYPE pattern) const
{
  const Body& b=_body.getBody().findBody(_body.names()[0]);
  BodyOpT<const Body> op(_body.getBody().nrBody());
  _body.getBody().transform(_body.getBody().root(),op,(void*)&theta);

  Cold tRoot=unifyRoot(theta);
  BodyOpT<const Body> opRoot(_body.getBody().nrBody());
  _body.getBody().transform(_body.getBody().root(),opRoot,(void*)&tRoot);

  Cold vssRoot=vss;
  Mat4d TRoot=opRoot.getTransD(b.id())*op.getTransD(b.id()).inverse();
  for(sizeType i=0; i<vss.size(); i+=3)
    vssRoot.segment<3>(i)=transformHomo<scalarD>(TRoot,vss.segment<3>(i));
  convertCloth(const_cast<Cold&>(vssRoot),datums,pattern,false);
}
void SkinnedMeshClothUtility::convertClothFromCaffe(Cold& vss,const vector<Datum>& datums,PATTERN_TYPE pattern) const
{
  convertCloth(vss,const_cast<vector<Datum>&>(datums),pattern,true);
}
//convert body
string SkinnedMeshClothUtility::convertBodyCloak(const Cold& theta,Datum& datum,FEATURE_TYPE feat,set<sizeType>* filterOut)
{
  set<sizeType> filter;
  filter.insert(0);
  filter.insert(3);
  filter.insert(6);
  filter.insert(9);
  filter.insert(12);
  filter.insert(15);
  filter.insert(13);
  filter.insert(14);
  filter.insert(16);
  filter.insert(17);
  filter.insert(18);
  filter.insert(19);
  filter.insert(20);
  filter.insert(21);
  if(filterOut)
    *filterOut=filter;

  Cold f=getFeature(theta,feat,&filter);
  datum.set_channels(1);
  datum.set_height(1);
  datum.set_width(f.size());
  google::protobuf::RepeatedField<float>* ptr=datum.mutable_float_data();
  for(sizeType i=0; i<f.size(); i++)
    ptr->Add(f[i]);
  return parseFeatureToString(feat,&filter);
}
string SkinnedMeshClothUtility::convertBodyDress(const Cold& theta,Datum& datum,FEATURE_TYPE feat,set<sizeType>* filterOut)
{
  set<sizeType> filter;
  filter.insert(0);
  filter.insert(1);
  filter.insert(2);
  filter.insert(4);
  filter.insert(5);
  filter.insert(7);
  filter.insert(8);
  if(filterOut)
    *filterOut=filter;

  Cold f=getFeature(theta,feat,&filter);
  datum.set_channels(1);
  datum.set_height(1);
  datum.set_width(f.size());
  google::protobuf::RepeatedField<float>* ptr=datum.mutable_float_data();
  for(sizeType i=0; i<f.size(); i++)
    ptr->Add(f[i]);
  return parseFeatureToString(feat,&filter);
}
string SkinnedMeshClothUtility::convertBodyToCaffe(const Cold& theta,Datum& datum,FEATURE_TYPE feat,PATTERN_TYPE pattern,set<sizeType>* filterOut)
{
  Cold tRoot=unifyRoot(theta);
  if(pattern == DRESS)
    return convertBodyDress(tRoot,datum,feat,filterOut);
  else if(pattern == CLOAK)
    return convertBodyCloak(tRoot,datum,feat,filterOut);
  else {
    ASSERT_MSG(false,"Unknown pattern type!")
    return "";
  }
}
SkinnedMeshClothUtility::PATTERN_TYPE SkinnedMeshClothUtility::parsePatternFromString(const string& path)
{
  SkinnedMeshClothUtility::PATTERN_TYPE pattern=SkinnedMeshClothUtility::NR_PATTERN;
  if(path.find("Dress") != string::npos || path.find("dress") != string::npos)
    pattern=SkinnedMeshClothUtility::DRESS;
  else if(path.find("Cloak") != string::npos || path.find("cloak") != string::npos)
    pattern=SkinnedMeshClothUtility::CLOAK;
  else {
    ASSERT_MSG(false,"Cannot find pattern from path!")
  }
  return pattern;
}
void SkinnedMeshClothUtility::drawClothVTK(const Datum& dat,const string& path)
{
#define GI(I,J) (I)*w+(J)
  ASSERT(dat.channels() == 3)
  int w=dat.width(),h=dat.height();
  ASSERT(dat.float_data_size() == 3*w*h)
  vector<Vec3,Eigen::aligned_allocator<Vec3> > vss(w*h);
  vector<Vec4,Eigen::aligned_allocator<Vec4> > css(w*h,Vec4(0,0,0,1));
  vector<Vec3i,Eigen::aligned_allocator<Vec3i> > iss;
  for(sizeType c=0,k=0; c<3; c++)
    for(sizeType i=0; i<h; i++)
      for(sizeType j=0; j<w; j++,k++) {
        vss[GI(i,j)][c]=dat.float_data(k);
        css[GI(i,j)][c]=c==0 ? (scalar)i/(scalar)(h-1) :
                        c==1 ? (scalar)j/(scalar)(w-1) : 0;
      }
  for(sizeType i=0; i<h-1; i++)
    for(sizeType j=0; j<w-1; j++) {
      iss.push_back(Vec3i(GI(i,j),GI(i+1,j),GI(i+1,j+1)));
      iss.push_back(Vec3i(GI(i,j),GI(i+1,j+1),GI(i,j+1)));
    }
  VTKWriter<scalar> os("cloth",path,true);
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,0,0),
                 VTKWriter<scalar>::IteratorIndex<Vec3i>((sizeType)vss.size(),0,0),
                 VTKWriter<scalar>::POINT);
  os.appendCells(iss.begin(),iss.end(),VTKWriter<scalar>::TRIANGLE);
  os.appendCustomPointColorData("texcoord",css.begin(),css.end());
#undef GI
}
Cold SkinnedMeshClothUtility::unifyRoot(const Cold& theta)
{
  Mat3d RRoot=expWGradV<scalarD>(theta.segment<3>(0),NULL,NULL,NULL,NULL);
  if(abs((RRoot*Vec3d::UnitZ()).dot(Vec3d::UnitY())) > 0.999f) {
    Vec3d xTo=RRoot*Vec3d::UnitX(); //project using X-Axis
    xTo[1]=0;
    xTo.normalize();
    Mat3d trans=ScalarUtil<scalarD>::ScalarQuat::FromTwoVectors(xTo,Vec3d::UnitX()).toRotationMatrix();
    RRoot=(trans*RRoot).eval();
  } else {
    Vec3d xTo=RRoot*Vec3d::UnitZ(); //project using Z-Axis
    xTo[1]=0;
    xTo.normalize();
    Mat3d trans=ScalarUtil<scalarD>::ScalarQuat::FromTwoVectors(xTo,Vec3d::UnitZ()).toRotationMatrix();
    RRoot=(trans*RRoot).eval();
  }
  Cold tRoot=theta;
  tRoot.segment<3>(tRoot.size()-4).setZero();
  tRoot.segment<3>(0)=invExpW<scalarD>(RRoot);
  return tRoot;
}
void SkinnedMeshClothUtility::debugUnifyRoot()
{
#define NR_TEST 100
  for(sizeType t=0; t<NR_TEST; t++) {
    Cold theta=Cold::Random(20)*M_PI;
    Cold tRoot=unifyRoot(theta);
    Mat3d R=expWGradV<scalarD>(tRoot.segment<3>(0),NULL,NULL,NULL,NULL);
    Vec3d newX=R*Vec3d::UnitX();
    Vec3d newZ=R*Vec3d::UnitZ();
    INFOV("Transform: X->(%f,%f,%f), Z->(%f,%f,%f)",newX[0],newX[1],newX[2],newZ[0],newZ[1],newZ[2])
  }
  exit(-1);
#undef NR_TEST
}
