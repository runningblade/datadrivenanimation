#include "ClothEnergies.h"
#include "ClothCollision.h"
#include <CommonFile/CollisionDetection.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

//ClothKineticEnergy
template <typename T>
ClothKineticEnergy<T>::ClothKineticEnergy(boost::shared_ptr<T> v):_v(v) {}
template <typename T>
void ClothKineticEnergy<T>::initialize(scalarD dt,STEP_MODE mode)
{
  if(!_lastV) {
    _lastV.reset(new Vec3d);
    *_lastV=_v->_vel;
  }
  if(!_lastX) {
    _lastX.reset(new Vec3d);
    *_lastX=_v->_pos;
  }
  _vel=_v->_vel;
  _pos=_v->_pos;
  switch(mode) {
  case  NEWTON_FIRST_ORDER:
    _b=-_v->_pos/dt-_vel;
    _coef=1.0f/dt;
    break;
  case NEWTON_SECOND_ORDER:
    _b=-2.0f/dt*_pos+0.5f/dt**_lastX
       -4.0f/3.0f*_vel+1.0f/3.0f**_lastV;
    _coef=1.5f/dt;
    break;
  case QUASISTATIC:
    _b.setZero();
    _coef=1.0f/dt;
    break;
  default:
    ASSERT(false);
  }
  _mode=mode;
}
template <typename T>
void ClothKineticEnergy<T>::finalize(scalarD dt,const Vec& XNew)
{
  Vec3d VNewI=Vec3d::Zero();
  Vec3d XNewI=XNew.block<3,1>(_v->_index*3,0);
  switch(_mode) {
  case NEWTON_FIRST_ORDER:
    VNewI=(XNewI-_pos)/dt;
    break;
  case NEWTON_SECOND_ORDER:
    VNewI=(1.5f*XNewI-2.0f*_pos+0.5f**_lastX)/dt;
    break;
  case QUASISTATIC:
    VNewI.setZero();
    break;
  }
  _v->_vel=VNewI;
  _v->_pos=XNewI;
  *_lastV=_vel;
  *_lastX=_pos;
}
template <typename T>
void ClothKineticEnergy<T>::getRelatedVertex(std::vector<sizeType>& vs) const
{
  vs.push_back(_v->_index);
}
template <typename T>
scalarD ClothKineticEnergy<T>::eval() const
{
  if(_mode != QUASISTATIC)
    return -0.5f*_v->_mass*(_v->_pos*_coef+_b).squaredNorm();
  else return 0.0f;
}
template <typename T>
void ClothKineticEnergy<T>::eval(Vec& c,STrips& grad,sizeType off) const
{
  if(_mode != QUASISTATIC) {
    if(_needHESS)
      addIK(grad,_v->_index*3,_v->_index*3,-_v->_mass*_coef*_coef,3);
    c.block<3,1>(_v->_index*3,0)-=_coef*_v->_mass*(_coef*_v->_pos+_b);
  }
}
//ClothFixedForceEnergy
template <typename T>
ClothFixedForceEnergy<T>::ClothFixedForceEnergy(boost::shared_ptr<T> v,const Vec3d& force):_v(v),_force(force) {}
template <typename T>
void ClothFixedForceEnergy<T>::writeVTK(VTKWriter<scalarD>& os) const
{
  std::vector<Vec3d,Eigen::aligned_allocator<Vec3d> > vss;
  vss.push_back(_v->_pos);
  vss.push_back(_v->_pos+_force.normalized());
  os.setRelativeIndex();
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalarD>::IteratorIndex<Vec3i>(0,0,1),
                 VTKWriter<scalarD>::IteratorIndex<Vec3i>(1,0,1),
                 VTKWriter<scalarD>::LINE,true);
}
template <typename T>
void ClothFixedForceEnergy<T>::getRelatedVertex(std::vector<sizeType>& vs) const
{
  vs.push_back(_v->_index);
}
template <typename T>
scalarD ClothFixedForceEnergy<T>::eval() const
{
  return _force.dot(_v->_pos);
}
template <typename T>
void ClothFixedForceEnergy<T>::eval(Vec& c,STrips& grad,sizeType off) const
{
  c.block<3,1>(_v->_index*3,0)+=_force;
}
//ClothFixedNormalEnergy
template <typename T>
ClothFixedNormalEnergy<T>::ClothFixedNormalEnergy(boost::shared_ptr<ClothMesh::ClothTriangle> t,const Vec3d& axis,scalarD stiff)
  :_axis(axis),_stiff(stiff)
{
  _v[0]=t->getElem<T>(0);
  _v[1]=t->getElem<T>(1);
  _v[2]=t->getElem<T>(2);
  evalN(_v[0]->_pos,_v[1]->_pos,_v[2]->_pos,_n0);
}
template <typename T>
void ClothFixedNormalEnergy<T>::initialize(scalarD dt,STEP_MODE mode)
{
  scalarD axisLen=_axis.norm();
  Vec3d axisN=_axis/axisLen;

  _n0-=_n0.dot(axisN)*axisN;
  _n0.normalize();

  _n0+=axisN.cross(_n0)*axisLen*dt;
  _n0.normalize();
  //INFOV("Draging to normal: (%f,%f,%f).",_n0[0],_n0[1],_n0[2]);
}
template <typename T>
void ClothFixedNormalEnergy<T>::writeVTK(VTKWriter<scalarD>& os) const
{
  Vec3d N;
  evalN(_v[0]->_pos,_v[1]->_pos,_v[2]->_pos,N);
  std::vector<Vec3d,Eigen::aligned_allocator<Vec3d> > vss;
  vss.push_back((_v[0]->_pos+_v[1]->_pos+_v[2]->_pos)/3.0f);
  vss.push_back(vss[0]+N);
  os.setRelativeIndex();
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalarD>::IteratorIndex<Vec3i>(0,0,1),
                 VTKWriter<scalarD>::IteratorIndex<Vec3i>(1,0,1),
                 VTKWriter<scalarD>::LINE,true);
}
template <typename T>
void ClothFixedNormalEnergy<T>::getRelatedVertex(std::vector<sizeType>& vs) const
{
  vs.push_back(_v[0]->_index);
  vs.push_back(_v[1]->_index);
  vs.push_back(_v[2]->_index);
}
template <typename T>
scalarD ClothFixedNormalEnergy<T>::eval() const
{
  Vec3d N;
  evalN(_v[0]->_pos,_v[1]->_pos,_v[2]->_pos,N);
  return -(0.5f*_stiff)*(N-_n0).squaredNorm();
}
template <typename T>
void ClothFixedNormalEnergy<T>::eval(Vec& c,STrips& grad,sizeType off) const
{
  Vec3d N;
  Eigen::Matrix<scalarD,3,9> G;
  evalN(_v[0]->_pos,_v[1]->_pos,_v[2]->_pos,N,&G);
  Eigen::Matrix<scalarD,9,1> GTN=G.transpose()*(N-_n0)*_stiff;
  Eigen::Matrix<scalarD,9,9> GTG=G.transpose()*G*_stiff;
  for(sizeType rr=0; rr<3; rr++) {
    c.block<3,1>(_v[rr]->_index*3,0)-=GTN.template block<3,1>(rr*3,0);
    for(sizeType cc=0; cc<3; cc++)
      addBlock(grad,_v[rr]->_index*3,_v[cc]->_index*3,-GTG.template block<3,3>(rr*3,cc*3));
  }
}
template <typename T>
void ClothFixedNormalEnergy<T>::evalN(const Vec3d& x0,const Vec3d& x1,const Vec3d& x2,Vec3d& n,Eigen::Matrix<scalarD,3,9>* G)
{
  //input
  //Vec3d x0;
  //Vec3d x1;
  //Vec3d x2;

  //temp
  scalarD tt1;
  scalarD tt2;
  scalarD tt3;
  scalarD tt4;
  scalarD tt5;
  scalarD tt6;
  scalarD tt7;
  scalarD tt8;
  scalarD tt9;
  scalarD tt10;
  scalarD tt11;
  scalarD tt12;
  scalarD tt13;
  scalarD tt14;
  scalarD tt15;
  scalarD tt16;
  scalarD tt17;
  scalarD tt18;
  scalarD tt19;
  scalarD tt20;
  scalarD tt21;
  scalarD tt22;
  scalarD tt23;
  scalarD tt24;
  scalarD tt25;
  scalarD tt26;
  scalarD tt27;
  scalarD tt28;
  scalarD tt29;
  scalarD tt30;
  scalarD tt31;
  scalarD tt32;
  scalarD tt33;
  scalarD tt34;
  scalarD tt35;
  scalarD tt36;
  scalarD tt37;
  scalarD tt38;
  scalarD tt39;
  scalarD tt40;
  scalarD tt41;
  scalarD tt42;

  tt1=-x0[1];
  tt2=x2[1]+tt1;
  tt3=-x0[2];
  tt4=x1[2]+tt3;
  tt5=x1[1]+tt1;
  tt6=x2[2]+tt3;
  tt7=tt5*tt6-tt2*tt4;
  tt8=-x0[0];
  tt9=x2[0]+tt8;
  tt10=x1[0]+tt8;
  tt11=tt10*tt2-tt9*tt5;
  tt12=tt9*tt4-tt10*tt6;
  tt13=sqrt(pow(tt7,2)+pow(tt12,2)+pow(tt11,2));
  tt14=1/tt13;
  tt15=-x2[1];
  tt16=tt15+x1[1];
  tt17=-x1[2];
  tt18=x2[2]+tt17;
  tt19=2*tt18*tt12+2*tt16*tt11;
  tt20=1/pow(tt13,3);
  tt21=-x1[0];
  tt22=x2[0]+tt21;
  tt23=-x2[2];
  tt24=tt23+x1[2];
  tt25=2*tt24*tt7+2*tt22*tt11;
  tt26=-x2[0];
  tt27=tt26+x1[0];
  tt28=-x1[1];
  tt29=x2[1]+tt28;
  tt30=2*tt29*tt7+2*tt27*tt12;
  tt31=tt23+x0[2];
  tt32=2*tt31*tt12+2*tt2*tt11;
  tt33=tt26+x0[0];
  tt34=2*tt6*tt7+2*tt33*tt11;
  tt35=tt15+x0[1];
  tt36=2*tt35*tt7+2*tt9*tt12;
  tt37=tt28+x0[1];
  tt38=2*tt4*tt12+2*tt37*tt11;
  tt39=tt17+x0[2];
  tt40=2*tt39*tt7+2*tt10*tt11;
  tt41=tt21+x0[0];
  tt42=2*tt5*tt7+2*tt41*tt12;
  n[0]=tt7*tt14;
  n[1]=tt12*tt14;
  n[2]=tt11*tt14;
  if(G) {
    Eigen::Matrix<scalarD,3,9>& grad=*G;
    grad(0,0)=-tt7*tt19*tt20/2.0;
    grad(0,1)=tt24*tt14-tt7*tt25*tt20/2.0;
    grad(0,2)=tt29*tt14-tt7*tt30*tt20/2.0;
    grad(0,3)=-tt7*tt32*tt20/2.0;
    grad(0,4)=tt6*tt14-tt7*tt34*tt20/2.0;
    grad(0,5)=tt35*tt14-tt7*tt36*tt20/2.0;
    grad(0,6)=-tt7*tt38*tt20/2.0;
    grad(0,7)=tt39*tt14-tt7*tt40*tt20/2.0;
    grad(0,8)=tt5*tt14-tt7*tt42*tt20/2.0;
    grad(1,0)=tt18*tt14-tt12*tt19*tt20/2.0;
    grad(1,1)=-tt12*tt25*tt20/2.0;
    grad(1,2)=tt27*tt14-tt12*tt30*tt20/2.0;
    grad(1,3)=tt31*tt14-tt12*tt32*tt20/2.0;
    grad(1,4)=-tt12*tt34*tt20/2.0;
    grad(1,5)=tt9*tt14-tt12*tt36*tt20/2.0;
    grad(1,6)=tt4*tt14-tt12*tt38*tt20/2.0;
    grad(1,7)=-tt12*tt40*tt20/2.0;
    grad(1,8)=tt41*tt14-tt12*tt42*tt20/2.0;
    grad(2,0)=tt16*tt14-tt11*tt19*tt20/2.0;
    grad(2,1)=tt22*tt14-tt11*tt25*tt20/2.0;
    grad(2,2)=-tt11*tt30*tt20/2.0;
    grad(2,3)=tt2*tt14-tt11*tt32*tt20/2.0;
    grad(2,4)=tt33*tt14-tt11*tt34*tt20/2.0;
    grad(2,5)=-tt11*tt36*tt20/2.0;
    grad(2,6)=tt37*tt14-tt11*tt38*tt20/2.0;
    grad(2,7)=tt10*tt14-tt11*tt40*tt20/2.0;
    grad(2,8)=-tt11*tt42*tt20/2.0;
  }
}
//ClothHookeanEnergy
template <typename T>
ClothHookeanEnergy<T>::ClothHookeanEnergy(boost::shared_ptr<T> v0,boost::shared_ptr<T> v1,scalarD rest,scalarD K,bool SPD,bool QUAD)
  :_v0(v0),_v1(v1),_rest(rest),_K(K),_SPD(SPD),_QUAD(QUAD)
{
  if(_v0->_index > _v1->_index)
    std::swap(_v0,_v1);
}
template <typename T>
void ClothHookeanEnergy<T>::writeVTK(VTKWriter<scalarD>& os) const
{
  std::vector<Vec3d,Eigen::aligned_allocator<Vec3d> > vss;
  vss.push_back(_v0->_pos);
  vss.push_back(_v1->_pos);
  os.setRelativeIndex();
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalarD>::IteratorIndex<Vec3i>(0,0,1),
                 VTKWriter<scalarD>::IteratorIndex<Vec3i>(1,0,1),
                 VTKWriter<scalarD>::LINE,true);
}
template <typename T>
bool ClothHookeanEnergy<T>::linear() const
{
  return false;
}
template <typename T>
void ClothHookeanEnergy<T>::getRelatedVertex(std::vector<sizeType>& vs) const
{
  vs.push_back(_v0->_index);
  vs.push_back(_v1->_index);
}
template <typename T>
bool ClothHookeanEnergy<T>::contributeToDamping() const
{
  return true;
}
template <typename T>
scalarD ClothHookeanEnergy<T>::eval() const
{
  Vec3d x10=_v0->_pos-_v1->_pos;
  if(_QUAD) {
    scalarD normSqr=x10.squaredNorm();
    scalarD delta=normSqr-_rest*_rest;
    return -0.5f*_K*delta*delta;
  } else {
    scalarD normSafe=std::max(x10.norm(),ScalarUtil<scalarD>::scalar_eps);
    return -0.5f*_K*(normSafe-_rest)*(normSafe-_rest);
  }
}
template <typename T>
void ClothHookeanEnergy<T>::eval(Vec& c,STrips& grad,sizeType off) const
{
  const_cast<ClothHookeanEnergy&>(*this).prepare();
  c.block<3,1>(_v0->_index*3,0)-=_force;
  c.block<3,1>(_v1->_index*3,0)+=_force;
  if(_needHESS) {
    addBlock(grad,_v0->_index*3,_v0->_index*3,-_JA);
    addBlock(grad,_v0->_index*3,_v1->_index*3,_JA);
    addBlock(grad,_v1->_index*3,_v0->_index*3,_JA);
    addBlock(grad,_v1->_index*3,_v1->_index*3,-_JA);
  }
}
template <typename T>
void ClothHookeanEnergy<T>::prepare()
{
  Vec3d x10=_v0->_pos-_v1->_pos;
  if(_QUAD) {
    scalarD normSqr=x10.squaredNorm();
    scalarD delta=normSqr-_rest*_rest;
    _force=x10*(2.0f*delta*_K);
    _JA=Mat3d::Identity()*(2.0f*delta*_K);
    _JA+=x10*x10.transpose()*(4.0f*_K);
  } else {
    scalarD normSafe=std::max(x10.norm(),ScalarUtil<scalarD>::scalar_eps);
    x10/=normSafe;
    _force=x10*(normSafe-_rest)*_K;
    if(!_SPD || normSafe > _rest) {
      scalarD coef=_K*(normSafe-_rest)/normSafe;
      _JA=Mat3d::Identity()*coef;
      _JA+=x10*x10.transpose()*(_K-coef);
    } else {
      _JA=x10*x10.transpose()*_K;
    }
  }
}
template <typename T>
scalarD ClothHookeanEnergy<T>::strain() const
{
  return (_v0->_pos-_v1->_pos).norm()/_rest;
}
//ClothLinearHookeanEnergy
template <typename T>
ClothLinearHookeanEnergy<T>::ClothLinearHookeanEnergy(boost::shared_ptr<T> v0,boost::shared_ptr<T> v1,scalarD rest,scalarD K)
  :ClothHookeanEnergy<T>(v0,v1,rest,K,false,false) {}
template <typename T>
bool ClothLinearHookeanEnergy<T>::linear() const
{
  return true;
}
template <typename T>
scalarD ClothLinearHookeanEnergy<T>::eval() const
{
  Vec3d dist=_v0->_pos-_v1->_pos;
  scalarD normSafe=std::max(dist.norm(),ScalarUtil<scalarD>::scalar_eps);
  dist*=_rest/normSafe;
  return -0.5f*_K*(_v0->_pos-_v1->_pos-dist).squaredNorm();
}
template <typename T>
void ClothLinearHookeanEnergy<T>::eval(Vec& c,STrips& grad,sizeType off) const
{
  Vec3d dist=_v0->_pos-_v1->_pos;
  scalarD normSafe=std::max(dist.norm(),ScalarUtil<scalarD>::scalar_eps);
  dist*=_rest/normSafe;

  Vec3d force=_K*(_v0->_pos-_v1->_pos-dist);
  c.template block<3,1>(_v0->_index*3,0)-=force;
  c.template block<3,1>(_v1->_index*3,0)+=force;

  if(_needHESS) {
    addIK(grad,_v0->_index*3,_v0->_index*3,-_K,3);
    addIK(grad,_v0->_index*3,_v1->_index*3, _K,3);
    addIK(grad,_v1->_index*3,_v0->_index*3, _K,3);
    addIK(grad,_v1->_index*3,_v1->_index*3,-_K,3);
  }
}
//ClothRestLinearHookeanEnergy
template <typename T>
ClothRestLinearHookeanEnergy<T>::ClothRestLinearHookeanEnergy(boost::shared_ptr<T> v0,boost::shared_ptr<T> v1,scalarD rest,scalarD K)
  :ClothHookeanEnergy<T>(v0,v1,rest,K,false,false)
{
  _dist=_v0->_pos0-_v1->_pos0;
  scalarD normSafe=std::max(_dist.norm(),ScalarUtil<scalarD>::scalar_eps);
  _dist*=_rest/normSafe;
}
template <typename T>
bool ClothRestLinearHookeanEnergy<T>::linear() const
{
  return true;
}
template <typename T>
scalarD ClothRestLinearHookeanEnergy<T>::eval() const
{
  return -0.5f*_K*(_v0->_pos-_v1->_pos-_dist).squaredNorm();
}
template <typename T>
void ClothRestLinearHookeanEnergy<T>::eval(Vec& c,STrips& grad,sizeType off) const
{
  Vec3d force=_K*(_v0->_pos-_v1->_pos-_dist);
  c.template block<3,1>(_v0->_index*3,0)-=force;
  c.template block<3,1>(_v1->_index*3,0)+=force;

  if(_needHESS) {
    addIK(grad,_v0->_index*3,_v0->_index*3,-_K,3);
    addIK(grad,_v0->_index*3,_v1->_index*3, _K,3);
    addIK(grad,_v1->_index*3,_v0->_index*3, _K,3);
    addIK(grad,_v1->_index*3,_v1->_index*3,-_K,3);
  }
}
//ClothConstraintEnergy
template <typename T>
ClothConstraintEnergy<T>::ClothConstraintEnergy(boost::shared_ptr<T> v,const Vec3d& pos,scalarD K)
  :_v(v),_pos(pos)
{
  _H=-Mat3d::Identity()*K;
}
template <typename T>
ClothConstraintEnergy<T>::ClothConstraintEnergy(boost::shared_ptr<T> v,const Vec3d& pos,const Vec3d& dir,scalarD K,bool line)
  :_v(v),_pos(pos)
{
  if(line) {
    Eigen::Matrix<scalarD,3,2> B;
    B.col(0)=ClothMesh::perp(dir.normalized());
    B.col(1)=dir.cross(B.col(0));
    _H=-K*B*B.transpose();
  } else {
    _H=-K*dir.normalized()*dir.normalized().transpose();
  }
}
template <typename T>
bool ClothConstraintEnergy<T>::linear() const
{
  return true;
}
template <typename T>
void ClothConstraintEnergy<T>::getRelatedVertex(std::vector<sizeType>& vs) const
{
  vs.push_back(_v->_index);
}
template <typename T>
scalarD ClothConstraintEnergy<T>::eval() const
{
  Vec3d D=_v->_pos-_pos;
  return 0.5f*(D.transpose()*_H*D)(0,0);
}
template <typename T>
void ClothConstraintEnergy<T>::eval(Vec& c,STrips& grad,sizeType off) const
{
  c.block<3,1>(_v->_index*3,0)+=_H*(_v->_pos-_pos);
  if(_needHESS)
    addBlock(grad,_v->_index*3,_v->_index*3,_H);
}
//ClothARAPEnergy
ClothARAPEnergy::ClothARAPEnergy(boost::shared_ptr<ClothMesh::ClothVertex> vertex,set<boost::shared_ptr<ClothMesh::ClothEdge> > oneRing,scalarD K)
{
  scalarD ang;
  boost::shared_ptr<ClothMesh::ClothEdge> eOut;
  boost::shared_ptr<ClothMesh::ClothVertex> vOut;
  _vertex=vertex;
  _oneRing.resize(oneRing.size());
  _v0.resize(oneRing.size());
  _weight.assign(oneRing.size(),0.0f);
  sizeType i=0;
  for(set<boost::shared_ptr<ClothMesh::ClothEdge> >::iterator beg=oneRing.begin(),end=oneRing.end(); beg!=end; beg++,i++) {
    boost::shared_ptr<ClothMesh::ClothEdge> e=*beg;
    //add oneRing vertex
    if(e->_v[0] == vertex)
      _oneRing[i]=e->_v[1];
    else _oneRing[i]=e->_v[0];
    //add v0
    _v0[i]=vertex->_pos0-_oneRing[i]->_pos0;
    //add weight
    if(e->_t[0]) {
      ClothQuadraticBendEnergy::findOtherEdge(e->_t[0],vertex,e,eOut,vOut);
      ang=getAngle3D<scalarD>(_vertex->_pos0-vOut->_pos0,_oneRing[i]->_pos0-vOut->_pos0);
      _weight[i]+=1.0f/tan(ang);
    }
    if(e->_t[1]) {
      ClothQuadraticBendEnergy::findOtherEdge(e->_t[1],vertex,e,eOut,vOut);
      ang=getAngle3D<scalarD>(_vertex->_pos0-vOut->_pos0,_oneRing[i]->_pos0-vOut->_pos0);
      _weight[i]+=1.0f/tan(ang);
    }
    _weight[i]*=0.5f*K;
  }
  ASSERT(_weight.size() <= 8);	//otherwise multigrid cannot be applied
}
scalarD ClothARAPEnergy::eval() const
{
  Mat3d R=rotation();
  scalarD ret=0.0f;
  for(sizeType i=0; i<(sizeType)_oneRing.size(); i++)
    ret=-0.5f*_weight[i]*(_vertex->_pos-_oneRing[i]->_pos-R*_v0[i]).squaredNorm();
  return ret;
}
void ClothARAPEnergy::getRelatedVertex(std::vector<sizeType>& vs) const
{
  vs.push_back(_vertex->_index);
  for(sizeType i=0; i<(sizeType)_oneRing.size(); i++)
    vs.push_back(_oneRing[i]->_index);
}
void ClothARAPEnergy::eval(Vec& c,STrips& grad,sizeType off) const
{
  Mat3d R=rotation();
  Vec3d force;
  for(sizeType i=0; i<(sizeType)_oneRing.size(); i++) {
    force=-_weight[i]*(_vertex->_pos-_oneRing[i]->_pos-R*_v0[i]);
    c.block<3,1>(_vertex->_index*3,0)+=force;
    c.block<3,1>(_oneRing[i]->_index*3,0)-=force;
    if(_needHESS) {
      addIK(grad,_vertex->_index*3    ,_vertex->_index*3    ,-_weight[i],3);
      addIK(grad,_vertex->_index*3    ,_oneRing[i]->_index*3, _weight[i],3);
      addIK(grad,_oneRing[i]->_index*3,_vertex->_index*3    , _weight[i],3);
      addIK(grad,_oneRing[i]->_index*3,_oneRing[i]->_index*3,-_weight[i],3);
    }
  }
}
Mat3d ClothARAPEnergy::rotation() const
{
  Mat3d M=Mat3d::Zero();
  Vec3d E;
  for(sizeType i=0; i<(sizeType)_oneRing.size(); i++) {
    E=_vertex->_pos-_oneRing[i]->_pos;
    M+=_weight[i]*_v0[i]*E.transpose();
  }
  Eigen::JacobiSVD<Mat3d> sol(M,Eigen::ComputeFullU|Eigen::ComputeFullV);
  return sol.matrixV()*sol.matrixU().transpose();
}
//ClothStretchEnergy
ClothStretchEnergy::ClothStretchEnergy(boost::shared_ptr<ClothMesh::ClothTriangle> t,scalarD mu,scalarD lambda)
  :_mu(mu),_lambda(lambda)
{
  _v[0]=t->getV0();
  _v[1]=t->getV1();
  _v[2]=t->getV2();

  Vec3d n=(_v[1]->_pos0-_v[0]->_pos0).cross(_v[2]->_pos0-_v[0]->_pos0);
  _A=n.norm()*0.5f;
  n.normalize();
  Vec3d t0=(_v[1]->_pos0-_v[0]->_pos0).cross(n);
  Vec3d t1=(_v[2]->_pos0-_v[1]->_pos0).cross(n);
  Vec3d t2=(_v[0]->_pos0-_v[2]->_pos0).cross(n);

  scalarD coef=1.0f/(8.0f*_A*_A);
  _m0=(t1*t2.transpose()+t2*t1.transpose());
  _m0*=coef;
  _m1=(t0*t2.transpose()+t2*t0.transpose());
  _m1*=coef;
  _m2=(t0*t1.transpose()+t1*t0.transpose());
  _m2*=coef;
  _m=-_m0*t0.squaredNorm()-_m1*t1.squaredNorm()-_m2*t2.squaredNorm();
}
void ClothStretchEnergy::getRelatedVertex(std::vector<sizeType>& vs) const
{
  vs.push_back(_v[0]->_index);
  vs.push_back(_v[1]->_index);
  vs.push_back(_v[2]->_index);
}
scalarD ClothStretchEnergy::evalStretchEnergy(const Vec3d& v0,const Vec3d& v1,const Vec3d& v2,const Mat3d& m0,const Mat3d& m1,const Mat3d& m2,const Mat3d& m) const
{
  //temp
  scalarD tt1;
  scalarD tt2;
  scalarD tt3;
  scalarD tt4;
  scalarD tt5;
  scalarD tt6;

  tt1=pow((v1[2]-v0[2]),2)+pow((v1[1]-v0[1]),2)+pow((v1[0]-v0[0]),2);
  tt2=pow((v0[2]-v2[2]),2)+pow((v0[1]-v2[1]),2)+pow((v0[0]-v2[0]),2);
  tt3=pow((v2[2]-v1[2]),2)+pow((v2[1]-v1[1]),2)+pow((v2[0]-v1[0]),2);
  tt4=pow((m1(0,0)*tt3+m2(0,0)*tt2+m0(0,0)*tt1+m(0,0)),2);
  tt5=pow((m1(1,1)*tt3+m2(1,1)*tt2+m0(1,1)*tt1+m(1,1)),2);
  tt6=pow((tt2*m2(2,2)+tt3*m1(2,2)+tt1*m0(2,2)+m(2,2)),2);
  return -(tt6+pow((tt2*m2(2,1)+tt3*m1(2,1)+tt1*m0(2,1)+m(2,1)),2)+pow((tt2*m2(2,0)+tt3*m1(2,0)+tt1*m0(2,0)+m(2,0)),2)+pow((m1(1,2)*tt3+m2(1,2)*tt2+m0(1,2)*tt1+m(1,2)),2)+tt5+pow((m1(1,0)*tt3+m2(1,0)*tt2+m0(1,0)*tt1+m(1,0)),2)+pow((m1(0,2)*tt3+m2(0,2)*tt2+m0(0,2)*tt1+m(0,2)),2)+pow((m1(0,1)*tt3+m2(0,1)*tt2+m0(0,1)*tt1+m(0,1)),2)+tt4)*_mu-0.5*(tt6+tt5+tt4)*_lambda;
}
void ClothStretchEnergy::evalStretchForce(const Vec3d& v0,const Vec3d& v1,const Vec3d& v2,const Mat3d& m0,const Mat3d& m1,const Mat3d& m2,const Mat3d& m,Vec3d& f0,Vec3d& f1,Vec3d& f2,Matd* HESS) const
{
  //temp
  scalarD tt1;
  scalarD tt2;
  scalarD tt3;
  scalarD tt4;
  scalarD tt5;
  scalarD tt6;
  scalarD tt7;
  scalarD tt8;
  scalarD tt9;
  scalarD tt10;
  scalarD tt11;
  scalarD tt12;
  scalarD tt13;
  scalarD tt14;
  scalarD tt15;
  scalarD tt16;
  scalarD tt17;
  scalarD tt18;
  scalarD tt19;
  scalarD tt20;
  scalarD tt21;
  scalarD tt22;
  scalarD tt23;
  scalarD tt24;
  scalarD tt25;
  scalarD tt26;
  scalarD tt27;
  scalarD tt28;
  scalarD tt29;
  scalarD tt30;
  scalarD tt31;
  scalarD tt32;
  scalarD tt33;
  scalarD tt34;
  scalarD tt35;
  scalarD tt36;
  scalarD tt37;
  scalarD tt38;
  scalarD tt39;
  scalarD tt40;
  scalarD tt41;
  scalarD tt42;
  scalarD tt43;
  scalarD tt44;
  scalarD tt45;
  scalarD tt46;
  scalarD tt47;
  scalarD tt48;
  scalarD tt49;
  scalarD tt50;
  scalarD tt51;
  scalarD tt52;
  scalarD tt53;
  scalarD tt54;
  scalarD tt55;
  scalarD tt56;
  scalarD tt57;
  scalarD tt58;
  scalarD tt59;
  scalarD tt60;
  scalarD tt61;
  scalarD tt62;
  scalarD tt63;
  scalarD tt64;
  scalarD tt65;
  scalarD tt66;
  scalarD tt67;
  scalarD tt68;
  scalarD tt69;
  scalarD tt70;
  scalarD tt71;
  scalarD tt72;
  scalarD tt73;
  scalarD tt74;
  scalarD tt75;
  scalarD tt76;
  scalarD tt77;
  scalarD tt78;
  scalarD tt79;
  scalarD tt80;
  scalarD tt81;
  scalarD tt82;
  scalarD tt83;
  scalarD tt84;
  scalarD tt85;
  scalarD tt86;
  scalarD tt87;
  scalarD tt88;
  scalarD tt89;
  scalarD tt90;
  scalarD tt91;
  scalarD tt92;
  scalarD tt93;
  scalarD tt94;
  scalarD tt95;
  scalarD tt96;
  scalarD tt97;
  scalarD tt98;
  scalarD tt99;
  scalarD tt100;
  scalarD tt101;
  scalarD tt102;
  scalarD tt103;
  scalarD tt104;
  scalarD tt105;
  scalarD tt106;
  scalarD tt107;
  scalarD tt108;
  scalarD tt109;
  scalarD tt110;
  scalarD tt111;
  scalarD tt112;
  scalarD tt113;
  scalarD tt114;
  scalarD tt115;
  scalarD tt116;
  scalarD tt117;
  scalarD tt118;
  scalarD tt119;
  scalarD tt120;
  scalarD tt121;
  scalarD tt122;
  scalarD tt123;
  scalarD tt124;
  scalarD tt125;
  scalarD tt126;
  scalarD tt127;
  scalarD tt128;
  scalarD tt129;
  scalarD tt130;
  scalarD tt131;
  scalarD tt132;
  scalarD tt133;
  scalarD tt134;
  scalarD tt135;
  scalarD tt136;
  scalarD tt137;
  scalarD tt138;
  scalarD tt139;
  scalarD tt140;
  scalarD tt141;
  scalarD tt142;
  scalarD tt143;
  scalarD tt144;
  scalarD tt145;
  scalarD tt146;
  scalarD tt147;
  scalarD tt148;
  scalarD tt149;
  scalarD tt150;
  scalarD tt151;
  scalarD tt152;
  scalarD tt153;
  scalarD tt154;
  scalarD tt155;
  scalarD tt156;
  scalarD tt157;
  scalarD tt158;
  scalarD tt159;
  scalarD tt160;
  scalarD tt161;
  scalarD tt162;
  scalarD tt163;
  scalarD tt164;
  scalarD tt165;
  scalarD tt166;
  scalarD tt167;
  scalarD tt168;
  scalarD tt169;
  scalarD tt170;
  scalarD tt171;
  scalarD tt172;
  scalarD tt173;
  scalarD tt174;
  scalarD tt175;
  scalarD tt176;
  scalarD tt177;
  scalarD tt178;
  scalarD tt179;
  scalarD tt180;
  scalarD tt181;
  scalarD tt182;
  scalarD tt183;
  scalarD tt184;
  scalarD tt185;
  scalarD tt186;
  scalarD tt187;
  scalarD tt188;
  scalarD tt189;
  scalarD tt190;
  scalarD tt191;
  scalarD tt192;
  scalarD tt193;
  scalarD tt194;
  scalarD tt195;
  scalarD tt196;
  scalarD tt197;
  scalarD tt198;
  scalarD tt199;
  scalarD tt200;
  scalarD tt201;
  scalarD tt202;
  scalarD tt203;
  scalarD tt204;
  scalarD tt205;
  scalarD tt206;
  scalarD tt207;
  scalarD tt208;
  scalarD tt209;
  scalarD tt210;
  scalarD tt211;
  scalarD tt212;
  scalarD tt213;
  scalarD tt214;
  scalarD tt215;
  scalarD tt216;
  scalarD tt217;
  scalarD tt218;
  scalarD tt219;
  scalarD tt220;
  scalarD tt221;
  scalarD tt222;
  scalarD tt223;
  scalarD tt224;
  scalarD tt225;
  scalarD tt226;
  scalarD tt227;
  scalarD tt228;
  scalarD tt229;
  scalarD tt230;
  scalarD tt231;
  scalarD tt232;
  scalarD tt233;
  scalarD tt234;
  scalarD tt235;
  scalarD tt236;
  scalarD tt237;
  scalarD tt238;
  scalarD tt239;
  scalarD tt240;
  scalarD tt241;
  scalarD tt242;
  scalarD tt243;
  scalarD tt244;
  scalarD tt245;
  scalarD tt246;
  scalarD tt247;
  scalarD tt248;
  scalarD tt249;
  scalarD tt250;
  scalarD tt251;
  scalarD tt252;
  scalarD tt253;
  scalarD tt254;
  scalarD tt255;
  scalarD tt256;
  scalarD tt257;
  scalarD tt258;
  scalarD tt259;
  scalarD tt260;
  scalarD tt261;
  scalarD tt262;
  scalarD tt263;
  scalarD tt264;
  scalarD tt265;
  scalarD tt266;
  scalarD tt267;
  scalarD tt268;
  scalarD tt269;
  scalarD tt270;
  scalarD tt271;
  scalarD tt272;
  scalarD tt273;
  scalarD tt274;
  scalarD tt275;
  scalarD tt276;
  scalarD tt277;
  scalarD tt278;
  scalarD tt279;
  scalarD tt280;
  scalarD tt281;
  scalarD tt282;
  scalarD tt283;
  scalarD tt284;
  scalarD tt285;
  scalarD tt286;
  scalarD tt287;
  scalarD tt288;
  scalarD tt289;
  scalarD tt290;
  scalarD tt291;
  scalarD tt292;
  scalarD tt293;
  scalarD tt294;
  scalarD tt295;
  scalarD tt296;
  scalarD tt297;
  scalarD tt298;
  scalarD tt299;
  scalarD tt300;
  scalarD tt301;
  scalarD tt302;
  scalarD tt303;
  scalarD tt304;
  scalarD tt305;
  scalarD tt306;
  scalarD tt307;
  scalarD tt308;
  scalarD tt309;
  scalarD tt310;
  scalarD tt311;
  scalarD tt312;
  scalarD tt313;
  scalarD tt314;
  scalarD tt315;
  scalarD tt316;
  scalarD tt317;
  scalarD tt318;
  scalarD tt319;
  scalarD tt320;
  scalarD tt321;
  scalarD tt322;
  scalarD tt323;
  scalarD tt324;
  scalarD tt325;
  scalarD tt326;
  scalarD tt327;
  scalarD tt328;
  scalarD tt329;
  scalarD tt330;
  scalarD tt331;
  scalarD tt332;
  scalarD tt333;
  scalarD tt334;
  scalarD tt335;
  scalarD tt336;
  scalarD tt337;
  scalarD tt338;
  scalarD tt339;
  scalarD tt340;
  scalarD tt341;
  scalarD tt342;
  scalarD tt343;
  scalarD tt344;
  scalarD tt345;
  scalarD tt346;
  scalarD tt347;
  scalarD tt348;
  scalarD tt349;
  scalarD tt350;
  scalarD tt351;
  scalarD tt352;
  scalarD tt353;
  scalarD tt354;
  scalarD tt355;
  scalarD tt356;
  scalarD tt357;
  scalarD tt358;
  scalarD tt359;
  scalarD tt360;
  scalarD tt361;
  scalarD tt362;
  scalarD tt363;
  scalarD tt364;
  scalarD tt365;
  scalarD tt366;
  scalarD tt367;
  scalarD tt368;
  scalarD tt369;
  scalarD tt370;
  scalarD tt371;
  scalarD tt372;
  scalarD tt373;
  scalarD tt374;
  scalarD tt375;
  scalarD tt376;
  scalarD tt377;
  scalarD tt378;
  scalarD tt379;
  scalarD tt380;
  scalarD tt381;

  tt1=v1[0]-v0[0];
  tt2=v0[0]-v2[0];
  tt3=2*tt2*m2(0,0)-2*tt1*m0(0,0);
  tt4=v1[1]-v0[1];
  tt5=v1[2]-v0[2];
  tt6=pow(tt5,2)+pow(tt4,2)+pow(tt1,2);
  tt7=v0[1]-v2[1];
  tt8=v0[2]-v2[2];
  tt9=pow(tt8,2)+pow(tt7,2)+pow(tt2,2);
  tt10=v2[0]-v1[0];
  tt11=v2[1]-v1[1];
  tt12=v2[2]-v1[2];
  tt13=pow(tt12,2)+pow(tt11,2)+pow(tt10,2);
  tt14=m1(0,0)*tt13+m2(0,0)*tt9+m0(0,0)*tt6+m(0,0);
  tt15=2*tt3*tt14;
  tt16=2*tt2*m2(1,1)-2*tt1*m0(1,1);
  tt17=m1(1,1)*tt13+m2(1,1)*tt9+m0(1,1)*tt6+m(1,1);
  tt18=2*tt16*tt17;
  tt19=2*tt2*m2(2,2)-2*tt1*m0(2,2);
  tt20=tt9*m2(2,2)+tt13*m1(2,2)+tt6*m0(2,2)+m(2,2);
  tt21=2*tt19*tt20;
  tt22=2*tt2*m2(0,1)-2*tt1*m0(0,1);
  tt23=m1(0,1)*tt13+m2(0,1)*tt9+m0(0,1)*tt6+m(0,1);
  tt24=2*tt2*m2(0,2)-2*tt1*m0(0,2);
  tt25=m1(0,2)*tt13+m2(0,2)*tt9+m0(0,2)*tt6+m(0,2);
  tt26=2*tt2*m2(1,0)-2*tt1*m0(1,0);
  tt27=m1(1,0)*tt13+m2(1,0)*tt9+m0(1,0)*tt6+m(1,0);
  tt28=2*tt2*m2(1,2)-2*tt1*m0(1,2);
  tt29=m1(1,2)*tt13+m2(1,2)*tt9+m0(1,2)*tt6+m(1,2);
  tt30=2*tt2*m2(2,0)-2*tt1*m0(2,0);
  tt31=tt9*m2(2,0)+tt13*m1(2,0)+tt6*m0(2,0)+m(2,0);
  tt32=2*tt2*m2(2,1)-2*tt1*m0(2,1);
  tt33=tt9*m2(2,1)+tt13*m1(2,1)+tt6*m0(2,1)+m(2,1);
  tt34=2*m2(0,0)*tt7-2*m0(0,0)*tt4;
  tt35=2*tt34*tt14;
  tt36=2*tt7*m2(1,1)-2*tt4*m0(1,1);
  tt37=2*tt36*tt17;
  tt38=2*tt7*m2(2,2)-2*tt4*m0(2,2);
  tt39=2*tt38*tt20;
  tt40=2*m2(0,1)*tt7-2*m0(0,1)*tt4;
  tt41=2*m2(0,2)*tt7-2*m0(0,2)*tt4;
  tt42=2*tt7*m2(1,0)-2*tt4*m0(1,0);
  tt43=2*tt7*m2(1,2)-2*tt4*m0(1,2);
  tt44=2*tt7*m2(2,0)-2*tt4*m0(2,0);
  tt45=2*tt7*m2(2,1)-2*tt4*m0(2,1);
  tt46=2*m2(0,0)*tt8-2*m0(0,0)*tt5;
  tt47=2*tt46*tt14;
  tt48=2*m2(1,1)*tt8-2*m0(1,1)*tt5;
  tt49=2*tt48*tt17;
  tt50=2*tt8*m2(2,2)-2*tt5*m0(2,2);
  tt51=2*tt20*tt50;
  tt52=2*m2(0,1)*tt8-2*m0(0,1)*tt5;
  tt53=2*m2(0,2)*tt8-2*m0(0,2)*tt5;
  tt54=2*m2(1,0)*tt8-2*m0(1,0)*tt5;
  tt55=2*m2(1,2)*tt8-2*m0(1,2)*tt5;
  tt56=2*tt8*m2(2,0)-2*tt5*m0(2,0);
  tt57=2*tt8*m2(2,1)-2*tt5*m0(2,1);
  tt58=2*tt1*m0(0,0)-2*tt10*m1(0,0);
  tt59=2*tt58*tt14;
  tt60=2*tt1*m0(1,1)-2*tt10*m1(1,1);
  tt61=2*tt60*tt17;
  tt62=2*tt1*m0(2,2)-2*tt10*m1(2,2);
  tt63=2*tt62*tt20;
  tt64=2*tt1*m0(0,1)-2*tt10*m1(0,1);
  tt65=2*tt1*m0(0,2)-2*tt10*m1(0,2);
  tt66=2*tt1*m0(1,0)-2*tt10*m1(1,0);
  tt67=2*tt1*m0(1,2)-2*tt10*m1(1,2);
  tt68=2*tt1*m0(2,0)-2*tt10*m1(2,0);
  tt69=2*tt1*m0(2,1)-2*tt10*m1(2,1);
  tt70=2*m0(0,0)*tt4-2*m1(0,0)*tt11;
  tt71=2*tt70*tt14;
  tt72=2*tt4*m0(1,1)-2*tt11*m1(1,1);
  tt73=2*tt72*tt17;
  tt74=2*tt4*m0(2,2)-2*tt11*m1(2,2);
  tt75=2*tt74*tt20;
  tt76=2*m0(0,1)*tt4-2*m1(0,1)*tt11;
  tt77=2*m0(0,2)*tt4-2*m1(0,2)*tt11;
  tt78=2*tt4*m0(1,0)-2*tt11*m1(1,0);
  tt79=2*tt4*m0(1,2)-2*tt11*m1(1,2);
  tt80=2*tt4*m0(2,0)-2*tt11*m1(2,0);
  tt81=2*tt4*m0(2,1)-2*tt11*m1(2,1);
  tt82=2*m0(0,0)*tt5-2*m1(0,0)*tt12;
  tt83=2*tt82*tt14;
  tt84=2*m0(1,1)*tt5-2*m1(1,1)*tt12;
  tt85=2*tt84*tt17;
  tt86=2*tt5*m0(2,2)-2*tt12*m1(2,2);
  tt87=2*tt86*tt20;
  tt88=2*m0(0,1)*tt5-2*m1(0,1)*tt12;
  tt89=2*m0(0,2)*tt5-2*m1(0,2)*tt12;
  tt90=2*m0(1,0)*tt5-2*m1(1,0)*tt12;
  tt91=2*m0(1,2)*tt5-2*m1(1,2)*tt12;
  tt92=2*tt5*m0(2,0)-2*tt12*m1(2,0);
  tt93=2*tt5*m0(2,1)-2*tt12*m1(2,1);
  tt94=2*tt10*m1(0,0)-2*tt2*m2(0,0);
  tt95=2*tt94*tt14;
  tt96=2*tt10*m1(1,1)-2*tt2*m2(1,1);
  tt97=2*tt96*tt17;
  tt98=2*tt10*m1(2,2)-2*tt2*m2(2,2);
  tt99=2*tt98*tt20;
  tt100=2*tt10*m1(0,1)-2*tt2*m2(0,1);
  tt101=2*tt10*m1(0,2)-2*tt2*m2(0,2);
  tt102=2*tt10*m1(1,0)-2*tt2*m2(1,0);
  tt103=2*tt10*m1(1,2)-2*tt2*m2(1,2);
  tt104=2*tt10*m1(2,0)-2*tt2*m2(2,0);
  tt105=2*tt10*m1(2,1)-2*tt2*m2(2,1);
  tt106=2*m1(0,0)*tt11-2*m2(0,0)*tt7;
  tt107=2*tt106*tt14;
  tt108=2*tt11*m1(1,1)-2*tt7*m2(1,1);
  tt109=2*tt108*tt17;
  tt110=2*tt11*m1(2,2)-2*tt7*m2(2,2);
  tt111=2*tt110*tt20;
  tt112=2*m1(0,1)*tt11-2*m2(0,1)*tt7;
  tt113=2*m1(0,2)*tt11-2*m2(0,2)*tt7;
  tt114=2*tt11*m1(1,0)-2*tt7*m2(1,0);
  tt115=2*tt11*m1(1,2)-2*tt7*m2(1,2);
  tt116=2*tt11*m1(2,0)-2*tt7*m2(2,0);
  tt117=2*tt11*m1(2,1)-2*tt7*m2(2,1);
  tt118=2*m1(0,0)*tt12-2*m2(0,0)*tt8;
  tt119=2*tt118*tt14;
  tt120=2*m1(1,1)*tt12-2*m2(1,1)*tt8;
  tt121=2*tt120*tt17;
  tt122=2*tt12*m1(2,2)-2*tt8*m2(2,2);
  tt123=2*tt20*tt122;
  tt124=2*m1(0,1)*tt12-2*m2(0,1)*tt8;
  tt125=2*m1(0,2)*tt12-2*m2(0,2)*tt8;
  tt126=2*m1(1,0)*tt12-2*m2(1,0)*tt8;
  tt127=2*m1(1,2)*tt12-2*m2(1,2)*tt8;
  tt128=2*tt12*m1(2,0)-2*tt8*m2(2,0);
  tt129=2*tt12*m1(2,1)-2*tt8*m2(2,1);
  f0[0]=-(tt21+2*tt32*tt33+2*tt30*tt31+2*tt28*tt29+tt18+2*tt26*tt27+2*tt24*tt25+2*tt22*tt23+tt15)*_mu-0.5*(tt21+tt18+tt15)*_lambda;
  f0[1]=-(tt39+2*tt45*tt33+2*tt44*tt31+2*tt43*tt29+tt37+2*tt42*tt27+2*tt41*tt25+2*tt40*tt23+tt35)*_mu-0.5*(tt39+tt37+tt35)*_lambda;
  f0[2]=-(tt51+2*tt33*tt57+2*tt31*tt56+2*tt55*tt29+tt49+2*tt54*tt27+2*tt53*tt25+2*tt52*tt23+tt47)*_mu-0.5*(tt51+tt49+tt47)*_lambda;
  f1[0]=-(tt63+2*tt69*tt33+2*tt68*tt31+2*tt67*tt29+tt61+2*tt66*tt27+2*tt65*tt25+2*tt64*tt23+tt59)*_mu-0.5*(tt63+tt61+tt59)*_lambda;
  f1[1]=-(tt75+2*tt81*tt33+2*tt80*tt31+2*tt79*tt29+tt73+2*tt78*tt27+2*tt77*tt25+2*tt76*tt23+tt71)*_mu-0.5*(tt75+tt73+tt71)*_lambda;
  f1[2]=-(tt87+2*tt93*tt33+2*tt92*tt31+2*tt91*tt29+tt85+2*tt90*tt27+2*tt89*tt25+2*tt88*tt23+tt83)*_mu-0.5*(tt87+tt85+tt83)*_lambda;
  f2[0]=-(tt99+2*tt105*tt33+2*tt104*tt31+2*tt103*tt29+tt97+2*tt102*tt27+2*tt101*tt25+2*tt100*tt23+tt95)*_mu-0.5*(tt99+tt97+tt95)*_lambda;
  f2[1]=-(tt111+2*tt117*tt33+2*tt116*tt31+2*tt115*tt29+tt109+2*tt114*tt27+2*tt113*tt25+2*tt112*tt23+tt107)*_mu-0.5*(tt111+tt109+tt107)*_lambda;
  f2[2]=-(tt123+2*tt33*tt129+2*tt31*tt128+2*tt127*tt29+tt121+2*tt126*tt27+2*tt125*tt25+2*tt124*tt23+tt119)*_mu-0.5*(tt123+tt121+tt119)*_lambda;

  if(!HESS)
    return;
  tt130=2*pow(tt3,2);
  tt131=2*pow(tt16,2);
  tt132=2*m0(0,0);
  tt133=2*m2(0,0);
  tt134=2*(tt133+tt132)*tt14;
  tt135=2*m0(1,1);
  tt136=2*m2(1,1);
  tt137=2*(tt136+tt135)*tt17;
  tt138=2*m0(2,2);
  tt139=2*m2(2,2);
  tt140=2*(tt139+tt138)*tt20;
  tt141=2*pow(tt19,2);
  tt142=2*m0(0,1);
  tt143=2*m2(0,1);
  tt144=2*(tt143+tt142)*tt23;
  tt145=2*m0(0,2);
  tt146=2*m2(0,2);
  tt147=2*(tt146+tt145)*tt25;
  tt148=2*m0(1,0);
  tt149=2*m2(1,0);
  tt150=2*(tt149+tt148)*tt27;
  tt151=2*m0(1,2);
  tt152=2*m2(1,2);
  tt153=2*(tt152+tt151)*tt29;
  tt154=2*m0(2,0);
  tt155=2*m2(2,0);
  tt156=2*(tt155+tt154)*tt31;
  tt157=2*m0(2,1);
  tt158=2*m2(2,1);
  tt159=2*(tt158+tt157)*tt33;
  tt160=2*tt3*tt34;
  tt161=2*tt16*tt36;
  tt162=2*tt19*tt38;
  tt163=-(tt162+2*tt32*tt45+2*tt30*tt44+2*tt28*tt43+tt161+2*tt26*tt42+2*tt24*tt41+2*tt22*tt40+tt160)*_mu-0.5*(tt162+tt161+tt160)*_lambda;
  tt164=2*tt3*tt46;
  tt165=2*tt16*tt48;
  tt166=2*tt19*tt50;
  tt167=-(tt166+2*tt32*tt57+2*tt30*tt56+2*tt28*tt55+tt165+2*tt26*tt54+2*tt24*tt53+2*tt22*tt52+tt164)*_mu-0.5*(tt166+tt165+tt164)*_lambda;
  tt168=2*tt58*tt3;
  tt169=2*tt60*tt16;
  tt170=-4*m0(0,0)*tt14;
  tt171=-4*m0(1,1)*tt17;
  tt172=2*tt62*tt19;
  tt173=-4*m0(2,2)*tt20;
  tt174=-4*m0(0,1)*tt23;
  tt175=-4*m0(0,2)*tt25;
  tt176=-4*m0(1,0)*tt27;
  tt177=-4*m0(1,2)*tt29;
  tt178=-4*m0(2,0)*tt31;
  tt179=-4*m0(2,1)*tt33;
  tt180=-(tt173+tt172+tt179+2*tt69*tt32+tt178+2*tt68*tt30+tt177+tt171+tt176+tt175+tt174+tt170+2*tt67*tt28+tt169+2*tt66*tt26+2*tt65*tt24+2*tt64*tt22+tt168)*_mu-0.5*(tt173+tt172+tt171+tt170+tt169+tt168)*_lambda;
  tt181=2*tt3*tt70;
  tt182=2*tt72*tt16;
  tt183=2*tt74*tt19;
  tt184=-(tt183+2*tt81*tt32+2*tt80*tt30+2*tt79*tt28+tt182+2*tt78*tt26+2*tt24*tt77+2*tt22*tt76+tt181)*_mu-0.5*(tt183+tt182+tt181)*_lambda;
  tt185=2*tt3*tt82;
  tt186=2*tt16*tt84;
  tt187=2*tt86*tt19;
  tt188=-(tt187+2*tt93*tt32+2*tt92*tt30+2*tt28*tt91+tt186+2*tt26*tt90+2*tt24*tt89+2*tt22*tt88+tt185)*_mu-0.5*(tt187+tt186+tt185)*_lambda;
  tt189=2*tt94*tt3;
  tt190=2*tt96*tt16;
  tt191=-4*m2(0,0)*tt14;
  tt192=-4*m2(1,1)*tt17;
  tt193=2*tt98*tt19;
  tt194=-4*m2(2,2)*tt20;
  tt195=-4*m2(0,1)*tt23;
  tt196=-4*m2(0,2)*tt25;
  tt197=-4*m2(1,0)*tt27;
  tt198=-4*m2(1,2)*tt29;
  tt199=-4*m2(2,0)*tt31;
  tt200=-4*m2(2,1)*tt33;
  tt201=-(tt194+tt193+tt200+2*tt105*tt32+tt199+2*tt104*tt30+tt198+tt192+tt197+tt196+tt195+tt191+2*tt103*tt28+tt190+2*tt102*tt26+2*tt101*tt24+2*tt100*tt22+tt189)*_mu-0.5*(tt194+tt193+tt192+tt191+tt190+tt189)*_lambda;
  tt202=2*tt3*tt106;
  tt203=2*tt16*tt108;
  tt204=2*tt19*tt110;
  tt205=-(tt204+2*tt32*tt117+2*tt30*tt116+2*tt28*tt115+tt203+2*tt26*tt114+2*tt24*tt113+2*tt22*tt112+tt202)*_mu-0.5*(tt204+tt203+tt202)*_lambda;
  tt206=2*tt3*tt118;
  tt207=2*tt16*tt120;
  tt208=2*tt19*tt122;
  tt209=-(tt208+2*tt32*tt129+2*tt30*tt128+2*tt28*tt127+tt207+2*tt26*tt126+2*tt24*tt125+2*tt22*tt124+tt206)*_mu-0.5*(tt208+tt207+tt206)*_lambda;
  tt210=2*pow(tt34,2);
  tt211=2*pow(tt36,2);
  tt212=2*pow(tt38,2);
  tt213=2*tt34*tt46;
  tt214=2*tt36*tt48;
  tt215=2*tt38*tt50;
  tt216=-(tt215+2*tt45*tt57+2*tt44*tt56+2*tt43*tt55+tt214+2*tt42*tt54+2*tt41*tt53+2*tt40*tt52+tt213)*_mu-0.5*(tt215+tt214+tt213)*_lambda;
  tt217=2*tt58*tt34;
  tt218=2*tt60*tt36;
  tt219=2*tt62*tt38;
  tt220=-(tt219+2*tt69*tt45+2*tt68*tt44+2*tt67*tt43+tt218+2*tt66*tt42+2*tt65*tt41+2*tt64*tt40+tt217)*_mu-0.5*(tt219+tt218+tt217)*_lambda;
  tt221=2*tt34*tt70;
  tt222=2*tt72*tt36;
  tt223=2*tt74*tt38;
  tt224=-(tt173+tt223+tt179+2*tt81*tt45+tt178+2*tt80*tt44+tt177+tt171+tt176+tt175+tt174+tt170+2*tt79*tt43+tt222+2*tt78*tt42+2*tt41*tt77+2*tt40*tt76+tt221)*_mu-0.5*(tt173+tt223+tt171+tt170+tt222+tt221)*_lambda;
  tt225=2*tt34*tt82;
  tt226=2*tt36*tt84;
  tt227=2*tt86*tt38;
  tt228=-(tt227+2*tt93*tt45+2*tt92*tt44+2*tt43*tt91+tt226+2*tt42*tt90+2*tt41*tt89+2*tt40*tt88+tt225)*_mu-0.5*(tt227+tt226+tt225)*_lambda;
  tt229=2*tt94*tt34;
  tt230=2*tt96*tt36;
  tt231=2*tt98*tt38;
  tt232=-(tt231+2*tt105*tt45+2*tt104*tt44+2*tt103*tt43+tt230+2*tt102*tt42+2*tt101*tt41+2*tt100*tt40+tt229)*_mu-0.5*(tt231+tt230+tt229)*_lambda;
  tt233=2*tt34*tt106;
  tt234=2*tt108*tt36;
  tt235=2*tt110*tt38;
  tt236=-(tt194+tt235+tt200+2*tt117*tt45+tt199+2*tt116*tt44+tt198+tt192+tt197+tt196+tt195+tt191+2*tt115*tt43+tt234+2*tt114*tt42+2*tt41*tt113+2*tt40*tt112+tt233)*_mu-0.5*(tt194+tt235+tt192+tt191+tt234+tt233)*_lambda;
  tt237=2*tt34*tt118;
  tt238=2*tt36*tt120;
  tt239=2*tt38*tt122;
  tt240=-(tt239+2*tt45*tt129+2*tt44*tt128+2*tt43*tt127+tt238+2*tt42*tt126+2*tt41*tt125+2*tt40*tt124+tt237)*_mu-0.5*(tt239+tt238+tt237)*_lambda;
  tt241=2*pow(tt46,2);
  tt242=2*pow(tt48,2);
  tt243=2*pow(tt50,2);
  tt244=2*tt58*tt46;
  tt245=2*tt60*tt48;
  tt246=2*tt62*tt50;
  tt247=-(tt246+2*tt69*tt57+2*tt68*tt56+2*tt67*tt55+tt245+2*tt66*tt54+2*tt65*tt53+2*tt64*tt52+tt244)*_mu-0.5*(tt246+tt245+tt244)*_lambda;
  tt248=2*tt70*tt46;
  tt249=2*tt72*tt48;
  tt250=2*tt74*tt50;
  tt251=-(tt250+2*tt81*tt57+2*tt80*tt56+2*tt79*tt55+tt249+2*tt78*tt54+2*tt77*tt53+2*tt76*tt52+tt248)*_mu-0.5*(tt250+tt249+tt248)*_lambda;
  tt252=2*tt46*tt82;
  tt253=2*tt48*tt84;
  tt254=2*tt86*tt50;
  tt255=-(tt254+tt173+2*tt93*tt57+tt179+2*tt92*tt56+tt178+tt177+tt171+tt176+tt175+tt174+tt170+2*tt55*tt91+tt253+2*tt54*tt90+2*tt53*tt89+2*tt52*tt88+tt252)*_mu-0.5*(tt254+tt173+tt171+tt170+tt253+tt252)*_lambda;
  tt256=2*tt94*tt46;
  tt257=2*tt96*tt48;
  tt258=2*tt98*tt50;
  tt259=-(tt258+2*tt105*tt57+2*tt104*tt56+2*tt103*tt55+tt257+2*tt102*tt54+2*tt101*tt53+2*tt100*tt52+tt256)*_mu-0.5*(tt258+tt257+tt256)*_lambda;
  tt260=2*tt106*tt46;
  tt261=2*tt108*tt48;
  tt262=2*tt110*tt50;
  tt263=-(tt262+2*tt117*tt57+2*tt116*tt56+2*tt115*tt55+tt261+2*tt114*tt54+2*tt113*tt53+2*tt112*tt52+tt260)*_mu-0.5*(tt262+tt261+tt260)*_lambda;
  tt264=2*tt46*tt118;
  tt265=2*tt48*tt120;
  tt266=2*tt122*tt50;
  tt267=-(tt266+tt194+2*tt129*tt57+tt200+2*tt128*tt56+tt199+tt198+tt192+tt197+tt196+tt195+tt191+2*tt55*tt127+tt265+2*tt54*tt126+2*tt53*tt125+2*tt52*tt124+tt264)*_mu-0.5*(tt266+tt194+tt192+tt191+tt265+tt264)*_lambda;
  tt268=2*pow(tt58,2);
  tt269=2*pow(tt60,2);
  tt270=2*m1(0,0);
  tt271=2*(tt270+tt132)*tt14;
  tt272=2*m1(1,1);
  tt273=2*(tt272+tt135)*tt17;
  tt274=2*pow(tt62,2);
  tt275=2*m1(2,2);
  tt276=2*(tt275+tt138)*tt20;
  tt277=2*m1(0,1);
  tt278=2*(tt277+tt142)*tt23;
  tt279=2*m1(0,2);
  tt280=2*(tt279+tt145)*tt25;
  tt281=2*m1(1,0);
  tt282=2*(tt281+tt148)*tt27;
  tt283=2*m1(1,2);
  tt284=2*(tt283+tt151)*tt29;
  tt285=2*m1(2,0);
  tt286=2*(tt285+tt154)*tt31;
  tt287=2*m1(2,1);
  tt288=2*(tt287+tt157)*tt33;
  tt289=2*tt58*tt70;
  tt290=2*tt60*tt72;
  tt291=2*tt62*tt74;
  tt292=-(tt291+2*tt69*tt81+2*tt68*tt80+2*tt67*tt79+tt290+2*tt66*tt78+2*tt65*tt77+2*tt64*tt76+tt289)*_mu-0.5*(tt291+tt290+tt289)*_lambda;
  tt293=2*tt58*tt82;
  tt294=2*tt60*tt84;
  tt295=2*tt62*tt86;
  tt296=-(tt295+2*tt69*tt93+2*tt68*tt92+2*tt67*tt91+tt294+2*tt66*tt90+2*tt65*tt89+2*tt64*tt88+tt293)*_mu-0.5*(tt295+tt294+tt293)*_lambda;
  tt297=2*tt58*tt94;
  tt298=2*tt60*tt96;
  tt299=-4*m1(0,0)*tt14;
  tt300=-4*m1(1,1)*tt17;
  tt301=2*tt62*tt98;
  tt302=-4*m1(2,2)*tt20;
  tt303=-4*m1(0,1)*tt23;
  tt304=-4*m1(0,2)*tt25;
  tt305=-4*m1(1,0)*tt27;
  tt306=-4*m1(1,2)*tt29;
  tt307=-4*m1(2,0)*tt31;
  tt308=-4*m1(2,1)*tt33;
  tt309=-(tt302+tt301+tt308+2*tt69*tt105+tt307+2*tt68*tt104+tt306+tt300+tt305+tt304+tt303+tt299+2*tt67*tt103+tt298+2*tt66*tt102+2*tt65*tt101+2*tt64*tt100+tt297)*_mu-0.5*(tt302+tt301+tt300+tt299+tt298+tt297)*_lambda;
  tt310=2*tt58*tt106;
  tt311=2*tt60*tt108;
  tt312=2*tt62*tt110;
  tt313=-(tt312+2*tt69*tt117+2*tt68*tt116+2*tt67*tt115+tt311+2*tt66*tt114+2*tt65*tt113+2*tt64*tt112+tt310)*_mu-0.5*(tt312+tt311+tt310)*_lambda;
  tt314=2*tt58*tt118;
  tt315=2*tt60*tt120;
  tt316=2*tt62*tt122;
  tt317=-(tt316+2*tt69*tt129+2*tt68*tt128+2*tt67*tt127+tt315+2*tt66*tt126+2*tt65*tt125+2*tt64*tt124+tt314)*_mu-0.5*(tt316+tt315+tt314)*_lambda;
  tt318=2*pow(tt70,2);
  tt319=2*pow(tt72,2);
  tt320=2*pow(tt74,2);
  tt321=2*tt70*tt82;
  tt322=2*tt72*tt84;
  tt323=2*tt74*tt86;
  tt324=-(tt323+2*tt81*tt93+2*tt80*tt92+2*tt79*tt91+tt322+2*tt78*tt90+2*tt77*tt89+2*tt76*tt88+tt321)*_mu-0.5*(tt323+tt322+tt321)*_lambda;
  tt325=2*tt94*tt70;
  tt326=2*tt72*tt96;
  tt327=2*tt74*tt98;
  tt328=-(tt327+2*tt81*tt105+2*tt80*tt104+2*tt79*tt103+tt326+2*tt78*tt102+2*tt101*tt77+2*tt100*tt76+tt325)*_mu-0.5*(tt327+tt326+tt325)*_lambda;
  tt329=2*tt70*tt106;
  tt330=2*tt72*tt108;
  tt331=2*tt74*tt110;
  tt332=-(tt302+tt331+tt308+2*tt81*tt117+tt307+2*tt80*tt116+tt306+tt300+tt305+tt304+tt303+tt299+2*tt79*tt115+tt330+2*tt78*tt114+2*tt77*tt113+2*tt76*tt112+tt329)*_mu-0.5*(tt302+tt331+tt300+tt299+tt330+tt329)*_lambda;
  tt333=2*tt70*tt118;
  tt334=2*tt72*tt120;
  tt335=2*tt74*tt122;
  tt336=-(tt335+2*tt81*tt129+2*tt80*tt128+2*tt79*tt127+tt334+2*tt78*tt126+2*tt77*tt125+2*tt76*tt124+tt333)*_mu-0.5*(tt335+tt334+tt333)*_lambda;
  tt337=2*pow(tt82,2);
  tt338=2*pow(tt84,2);
  tt339=2*pow(tt86,2);
  tt340=2*tt94*tt82;
  tt341=2*tt96*tt84;
  tt342=2*tt86*tt98;
  tt343=-(tt342+2*tt93*tt105+2*tt92*tt104+2*tt103*tt91+tt341+2*tt102*tt90+2*tt101*tt89+2*tt100*tt88+tt340)*_mu-0.5*(tt342+tt341+tt340)*_lambda;
  tt344=2*tt106*tt82;
  tt345=2*tt108*tt84;
  tt346=2*tt86*tt110;
  tt347=-(tt346+2*tt93*tt117+2*tt92*tt116+2*tt115*tt91+tt345+2*tt114*tt90+2*tt113*tt89+2*tt112*tt88+tt344)*_mu-0.5*(tt346+tt345+tt344)*_lambda;
  tt348=2*tt82*tt118;
  tt349=2*tt84*tt120;
  tt350=2*tt86*tt122;
  tt351=-(tt350+tt302+2*tt93*tt129+tt308+2*tt92*tt128+tt307+tt306+tt300+tt305+tt304+tt303+tt299+2*tt91*tt127+tt349+2*tt90*tt126+2*tt89*tt125+2*tt88*tt124+tt348)*_mu-0.5*(tt350+tt302+tt300+tt299+tt349+tt348)*_lambda;
  tt352=2*pow(tt94,2);
  tt353=2*pow(tt96,2);
  tt354=2*(tt133+tt270)*tt14;
  tt355=2*(tt136+tt272)*tt17;
  tt356=2*(tt139+tt275)*tt20;
  tt357=2*pow(tt98,2);
  tt358=2*(tt143+tt277)*tt23;
  tt359=2*(tt146+tt279)*tt25;
  tt360=2*(tt149+tt281)*tt27;
  tt361=2*(tt152+tt283)*tt29;
  tt362=2*(tt155+tt285)*tt31;
  tt363=2*(tt158+tt287)*tt33;
  tt364=2*tt94*tt106;
  tt365=2*tt96*tt108;
  tt366=2*tt98*tt110;
  tt367=-(tt366+2*tt105*tt117+2*tt104*tt116+2*tt103*tt115+tt365+2*tt102*tt114+2*tt101*tt113+2*tt100*tt112+tt364)*_mu-0.5*(tt366+tt365+tt364)*_lambda;
  tt368=2*tt94*tt118;
  tt369=2*tt96*tt120;
  tt370=2*tt98*tt122;
  tt371=-(tt370+2*tt105*tt129+2*tt104*tt128+2*tt103*tt127+tt369+2*tt102*tt126+2*tt101*tt125+2*tt100*tt124+tt368)*_mu-0.5*(tt370+tt369+tt368)*_lambda;
  tt372=2*pow(tt106,2);
  tt373=2*pow(tt108,2);
  tt374=2*pow(tt110,2);
  tt375=2*tt106*tt118;
  tt376=2*tt108*tt120;
  tt377=2*tt110*tt122;
  tt378=-(tt377+2*tt117*tt129+2*tt116*tt128+2*tt115*tt127+tt376+2*tt114*tt126+2*tt113*tt125+2*tt112*tt124+tt375)*_mu-0.5*(tt377+tt376+tt375)*_lambda;
  tt379=2*pow(tt118,2);
  tt380=2*pow(tt120,2);
  tt381=2*pow(tt122,2);
  (*HESS)(0,0)=-(tt141+tt140+2*pow(tt32,2)+tt159+2*pow(tt30,2)+tt156+tt153+tt137+tt150+tt147+tt144+tt134+2*pow(tt28,2)+tt131+2*pow(tt26,2)+2*pow(tt24,2)+2*pow(tt22,2)+tt130)*_mu-0.5*(tt141+tt140+tt137+tt134+tt131+tt130)*_lambda;
  (*HESS)(0,1)=tt163;
  (*HESS)(0,2)=tt167;
  (*HESS)(0,3)=tt180;
  (*HESS)(0,4)=tt184;
  (*HESS)(0,5)=tt188;
  (*HESS)(0,6)=tt201;
  (*HESS)(0,7)=tt205;
  (*HESS)(0,8)=tt209;
  (*HESS)(1,0)=tt163;
  (*HESS)(1,1)=-(tt212+tt140+2*pow(tt45,2)+tt159+2*pow(tt44,2)+tt156+tt153+tt137+tt150+tt147+tt144+tt134+2*pow(tt43,2)+tt211+2*pow(tt42,2)+2*pow(tt41,2)+2*pow(tt40,2)+tt210)*_mu-0.5*(tt212+tt140+tt137+tt134+tt211+tt210)*_lambda;
  (*HESS)(1,2)=tt216;
  (*HESS)(1,3)=tt220;
  (*HESS)(1,4)=tt224;
  (*HESS)(1,5)=tt228;
  (*HESS)(1,6)=tt232;
  (*HESS)(1,7)=tt236;
  (*HESS)(1,8)=tt240;
  (*HESS)(2,0)=tt167;
  (*HESS)(2,1)=tt216;
  (*HESS)(2,2)=-(tt243+tt140+2*pow(tt57,2)+tt159+2*pow(tt56,2)+tt156+tt153+tt137+tt150+tt147+tt144+tt134+2*pow(tt55,2)+tt242+2*pow(tt54,2)+2*pow(tt53,2)+2*pow(tt52,2)+tt241)*_mu-0.5*(tt243+tt140+tt137+tt134+tt242+tt241)*_lambda;
  (*HESS)(2,3)=tt247;
  (*HESS)(2,4)=tt251;
  (*HESS)(2,5)=tt255;
  (*HESS)(2,6)=tt259;
  (*HESS)(2,7)=tt263;
  (*HESS)(2,8)=tt267;
  (*HESS)(3,0)=tt180;
  (*HESS)(3,1)=tt220;
  (*HESS)(3,2)=tt247;
  (*HESS)(3,3)=-(tt276+tt274+tt288+2*pow(tt69,2)+tt286+2*pow(tt68,2)+tt284+tt273+tt282+tt280+tt278+tt271+2*pow(tt67,2)+tt269+2*pow(tt66,2)+2*pow(tt65,2)+2*pow(tt64,2)+tt268)*_mu-0.5*(tt276+tt274+tt273+tt271+tt269+tt268)*_lambda;
  (*HESS)(3,4)=tt292;
  (*HESS)(3,5)=tt296;
  (*HESS)(3,6)=tt309;
  (*HESS)(3,7)=tt313;
  (*HESS)(3,8)=tt317;
  (*HESS)(4,0)=tt184;
  (*HESS)(4,1)=tt224;
  (*HESS)(4,2)=tt251;
  (*HESS)(4,3)=tt292;
  (*HESS)(4,4)=-(tt276+tt320+tt288+2*pow(tt81,2)+tt286+2*pow(tt80,2)+tt284+tt273+tt282+tt280+tt278+tt271+2*pow(tt79,2)+tt319+2*pow(tt78,2)+2*pow(tt77,2)+2*pow(tt76,2)+tt318)*_mu-0.5*(tt276+tt320+tt273+tt271+tt319+tt318)*_lambda;
  (*HESS)(4,5)=tt324;
  (*HESS)(4,6)=tt328;
  (*HESS)(4,7)=tt332;
  (*HESS)(4,8)=tt336;
  (*HESS)(5,0)=tt188;
  (*HESS)(5,1)=tt228;
  (*HESS)(5,2)=tt255;
  (*HESS)(5,3)=tt296;
  (*HESS)(5,4)=tt324;
  (*HESS)(5,5)=-(tt276+tt339+tt288+2*pow(tt93,2)+tt286+2*pow(tt92,2)+2*pow(tt91,2)+tt338+2*pow(tt90,2)+2*pow(tt89,2)+2*pow(tt88,2)+tt337+tt284+tt273+tt282+tt280+tt278+tt271)*_mu-0.5*(tt276+tt339+tt338+tt337+tt273+tt271)*_lambda;
  (*HESS)(5,6)=tt343;
  (*HESS)(5,7)=tt347;
  (*HESS)(5,8)=tt351;
  (*HESS)(6,0)=tt201;
  (*HESS)(6,1)=tt232;
  (*HESS)(6,2)=tt259;
  (*HESS)(6,3)=tt309;
  (*HESS)(6,4)=tt328;
  (*HESS)(6,5)=tt343;
  (*HESS)(6,6)=-(tt357+tt356+2*pow(tt105,2)+tt363+2*pow(tt104,2)+tt362+tt361+tt355+tt360+tt359+tt358+tt354+2*pow(tt103,2)+tt353+2*pow(tt102,2)+2*pow(tt101,2)+2*pow(tt100,2)+tt352)*_mu-0.5*(tt357+tt356+tt355+tt354+tt353+tt352)*_lambda;
  (*HESS)(6,7)=tt367;
  (*HESS)(6,8)=tt371;
  (*HESS)(7,0)=tt205;
  (*HESS)(7,1)=tt236;
  (*HESS)(7,2)=tt263;
  (*HESS)(7,3)=tt313;
  (*HESS)(7,4)=tt332;
  (*HESS)(7,5)=tt347;
  (*HESS)(7,6)=tt367;
  (*HESS)(7,7)=-(tt374+tt356+2*pow(tt117,2)+tt363+2*pow(tt116,2)+tt362+tt361+tt355+tt360+tt359+tt358+tt354+2*pow(tt115,2)+tt373+2*pow(tt114,2)+2*pow(tt113,2)+2*pow(tt112,2)+tt372)*_mu-0.5*(tt374+tt356+tt355+tt354+tt373+tt372)*_lambda;
  (*HESS)(7,8)=tt378;
  (*HESS)(8,0)=tt209;
  (*HESS)(8,1)=tt240;
  (*HESS)(8,2)=tt267;
  (*HESS)(8,3)=tt317;
  (*HESS)(8,4)=tt336;
  (*HESS)(8,5)=tt351;
  (*HESS)(8,6)=tt371;
  (*HESS)(8,7)=tt378;
  (*HESS)(8,8)=-(tt381+tt356+2*pow(tt129,2)+tt363+2*pow(tt128,2)+tt362+2*pow(tt127,2)+tt380+2*pow(tt126,2)+2*pow(tt125,2)+2*pow(tt124,2)+tt379+tt361+tt355+tt360+tt359+tt358+tt354)*_mu-0.5*(tt381+tt356+tt380+tt379+tt355+tt354)*_lambda;
}
scalarD ClothStretchEnergy::eval() const
{
  return evalStretchEnergy(_v[0]->_pos,_v[1]->_pos,_v[2]->_pos,_m0,_m1,_m2,_m)*_A;
}
void ClothStretchEnergy::eval(Vec& c,STrips& grad,sizeType off) const
{
  Vec3d f[3];
  Matd HESS;
  if(_needHESS)
    HESS.resize(9,9);
  evalStretchForce(_v[0]->_pos,_v[1]->_pos,_v[2]->_pos,_m0,_m1,_m2,_m,f[0],f[1],f[2],_needHESS ? &HESS : NULL);
  c.block<3,1>(_v[0]->_index*3,0)+=f[0]*_A;
  c.block<3,1>(_v[1]->_index*3,0)+=f[1]*_A;
  c.block<3,1>(_v[2]->_index*3,0)+=f[2]*_A;
  if(_needHESS)
    for(sizeType i=0; i<3; i++)
      for(sizeType j=0; j<3; j++)
        addBlock(grad,_v[i]->_index*3,_v[j]->_index*3,HESS.block<3,3>(i*3,j*3)*_A);
}
//ClothQuadraticBendEnergy
ClothQuadraticBendEnergy::ClothQuadraticBendEnergy(boost::shared_ptr<ClothMesh::ClothEdge> e,scalarD K,bool NMesh)
{
  _NMesh=NMesh;

  //assign edge and vertex
  ASSERT(e->_t[0] && e->_t[1])
  _e[0]=e;
  _v[0]=e->_v[0];
  _v[1]=e->_v[1];
  findOtherEdge(e->_t[0],_v[0],e,_e[1],_v[2]);
  findOtherEdge(e->_t[1],_v[0],e,_e[2],_v[3]);
  findOtherEdge(e->_t[0],_v[1],e,_e[3],_v[2]);
  findOtherEdge(e->_t[1],_v[1],e,_e[4],_v[3]);

  //build edge based formulation
  Eigen::Matrix<scalarD,5,1> L;
  L.setZero();
  L[1]=-coef(_v[1],_v[2],_v[0]);
  L[2]=-coef(_v[1],_v[3],_v[0]);
  L[3]=-coef(_v[0],_v[2],_v[1]);
  L[4]=-coef(_v[0],_v[3],_v[1]);
  L[0]=-L.sum();
  _QE=K*L*L.transpose()/e->_mass;

  //build also the vertex based formulation
  if(!_NMesh) {
    Eigen::Matrix<scalarD,5,4> V2E;
    V2E.setZero();
    V2E(0,0)=0.5f;
    V2E(0,1)=0.5f;
    V2E(1,0)=0.5f;
    V2E(1,2)=0.5f;
    V2E(2,0)=0.5f;
    V2E(2,3)=0.5f;
    V2E(3,1)=0.5f;
    V2E(3,2)=0.5f;
    V2E(4,1)=0.5f;
    V2E(4,3)=0.5f;
    _QV=V2E.transpose()*_QE*V2E;

    //debug code
    //Eigen::Matrix<scalarD,4,1> V;
    //V[0]=coef(_v[0],_v[2],_v[1])+coef(_v[0],_v[3],_v[1]);
    //V[1]=coef(_v[1],_v[2],_v[0])+coef(_v[1],_v[3],_v[0]);
    //V[2]=-coef(_v[1],_v[2],_v[0])-coef(_v[0],_v[2],_v[1]);
    //V[3]=-coef(_v[1],_v[3],_v[0])-coef(_v[0],_v[3],_v[1]);
    //Eigen::Matrix<scalarD,4,4> QVDebug=K*V*V.transpose()/e->_mass;
    //It's expected that: QVDebug=_QV*4
  }
}
void ClothQuadraticBendEnergy::writeVTK(VTKWriter<scalarD>& os) const
{
  TriangleTpl<scalarD>
  t0(_e[0]->_t[0]->getV0()->_pos,
     _e[0]->_t[0]->getV1()->_pos,
     _e[0]->_t[0]->getV2()->_pos);
  TriangleTpl<scalarD>
  t1(_e[0]->_t[1]->getV0()->_pos,
     _e[0]->_t[1]->getV1()->_pos,
     _e[0]->_t[1]->getV2()->_pos);

  scalarD delta=sqrt(t0.area()+t1.area())*0.1f;
  std::vector<Vec3d,Eigen::aligned_allocator<Vec3d> > vss;
  vss.push_back(t0.masscenter());
  vss.push_back(t1.masscenter());
  vss.push_back((t0.masscenter()+t1.masscenter())*0.5f+
                (t0.normal()+t1.normal()).normalized()*delta);

  os.setRelativeIndex();
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells( VTKWriter<scalarD>::IteratorIndex<Vec3i>(0,0,1),
                  VTKWriter<scalarD>::IteratorIndex<Vec3i>(1,0,1),
                  VTKWriter<scalarD>::QUADRATIC_LINE,true);
}
void ClothQuadraticBendEnergy::findOtherEdge
(boost::shared_ptr<ClothMesh::ClothTriangle> t,
 boost::shared_ptr<ClothMesh::ClothVertex> v,
 boost::shared_ptr<ClothMesh::ClothEdge> e,
 boost::shared_ptr<ClothMesh::ClothEdge>& eOut,
 boost::shared_ptr<ClothMesh::ClothVertex>& vOut)
{
  for(int i=0; i<3; i++)
    if(t->_e[i] != e) {
      if(t->_e[i]->_v[0] == v) {
        eOut=t->_e[i];
        vOut=t->_e[i]->_v[1];
        break;
      } else if(t->_e[i]->_v[1] == v) {
        eOut=t->_e[i];
        vOut=t->_e[i]->_v[0];
        break;
      }
    }
}
scalarD ClothQuadraticBendEnergy::coef
(boost::shared_ptr<ClothMesh::ClothVertex> v0,
 boost::shared_ptr<ClothMesh::ClothVertex> v1,
 boost::shared_ptr<ClothMesh::ClothVertex> vb) const
{
  //ASSERT(tan(getAngle3D<scalarD>(v0->_pos0-vb->_pos0,v1->_pos0-vb->_pos0)) > 0.0f);
  return 1.0f/tan(getAngle3D<scalarD>(v0->_pos0-vb->_pos0,v1->_pos0-vb->_pos0));
}
void ClothQuadraticBendEnergy::getRelatedVertex(std::vector<sizeType>& vs) const
{
  if(_NMesh) {
    for(sizeType i=0; i<5; i++)
      vs.push_back(_e[i]->_index);
  } else {
    for(sizeType i=0; i<4; i++)
      vs.push_back(_v[i]->_index);
  }
}
scalarD ClothQuadraticBendEnergy::eval() const
{
  scalarD val=0.0f;
  if(_NMesh) {
    for(sizeType i=0; i<5; i++)
      for(sizeType j=0; j<5; j++)
        val-=0.5f*_QE(i,j)*_e[j]->_pos.dot(_e[i]->_pos);
  } else {
    for(sizeType i=0; i<4; i++)
      for(sizeType j=0; j<4; j++)
        val-=0.5f*_QV(i,j)*_v[j]->_pos.dot(_v[i]->_pos);
  }
  return val;
}
void ClothQuadraticBendEnergy::eval(Vec& c,STrips& grad,sizeType off) const
{
  if(_NMesh) {
    for(sizeType i=0; i<5; i++) {
      Vec3d f=Vec3d::Zero();
      for(sizeType j=0; j<5; j++) {
        if(_needHESS)
          addIK(grad,_e[i]->_index*3,_e[j]->_index*3,-_QE(i,j),3);
        f-=_QE(i,j)*_e[j]->_pos;
      }
      c.block<3,1>(_e[i]->_index*3,0)+=f;
    }
  } else {
    for(sizeType i=0; i<4; i++) {
      Vec3d f=Vec3d::Zero();
      for(sizeType j=0; j<4; j++) {
        if(_needHESS)
          addIK(grad,_v[i]->_index*3,_v[j]->_index*3,-_QV(i,j),3);
        f-=_QV(i,j)*_v[j]->_pos;
      }
      c.block<3,1>(_v[i]->_index*3,0)+=f;
    }
  }
}
//ClothBendEnergy
ClothBendEnergy::ClothBendEnergy(boost::shared_ptr<ClothMesh::ClothEdge> e,scalarD K):ClothQuadraticBendEnergy(e,K,false)
{
  _A=e->_mass*K;
}
scalarD ClothBendEnergy::evalBendEnergy(const Vec3d& v0,const Vec3d& v1,const Vec3d& v2,const Vec3d& v3) const
{
  //temp
  scalarD tt1;
  scalarD tt2;
  scalarD tt3;
  scalarD tt4;
  scalarD tt5;
  scalarD tt6;
  scalarD tt7;
  scalarD tt8;
  scalarD tt9;
  scalarD tt10;
  scalarD tt11;
  scalarD tt12;
  scalarD tt13;
  scalarD tt14;
  scalarD tt15;
  scalarD tt16;
  scalarD tt17;
  scalarD tt18;

  tt1=-v0[0];
  tt2=v1[0]+tt1;
  tt3=-v0[1];
  tt4=v1[1]+tt3;
  tt5=-v0[2];
  tt6=v1[2]+tt5;
  tt7=v3[0]+tt1;
  tt8=v3[1]+tt3;
  tt9=tt2*tt8-tt7*tt4;
  tt10=v2[0]+tt1;
  tt11=v2[2]+tt5;
  tt12=tt2*tt11-tt10*tt6;
  tt13=v2[1]+tt3;
  tt14=tt10*tt4-tt2*tt13;
  tt15=v3[2]+tt5;
  tt16=tt7*tt6-tt2*tt15;
  tt17=tt13*tt6-tt4*tt11;
  tt18=tt4*tt15-tt8*tt6;
  return -0.5*pow(atan2((tt6*(tt17*tt16-tt12*tt18)+tt4*(tt14*tt18-tt9*tt17)+tt2*(tt9*tt12-tt14*tt16))/sqrt(pow(tt6,2)+pow(tt4,2)+pow(tt2,2)),tt17*tt18+tt12*tt16+tt14*tt9),2);
}
void ClothBendEnergy::evalBendForce(const Vec3d& v0,const Vec3d& v1,const Vec3d& v2,const Vec3d& v3,Vec3d& f0,Vec3d& f1,Vec3d& f2,Vec3d& f3,scalarD& theta) const
{
  //temp
  scalarD tt1;
  scalarD tt2;
  scalarD tt3;
  scalarD tt4;
  scalarD tt5;
  scalarD tt6;
  scalarD tt7;
  scalarD tt8;
  scalarD tt9;
  scalarD tt10;
  scalarD tt11;
  scalarD tt12;
  scalarD tt13;
  scalarD tt14;
  scalarD tt15;
  scalarD tt16;
  scalarD tt17;
  scalarD tt18;
  scalarD tt19;
  scalarD tt20;
  scalarD tt21;
  scalarD tt22;
  scalarD tt23;
  scalarD tt24;
  scalarD tt25;
  scalarD tt26;
  scalarD tt27;
  scalarD tt28;
  scalarD tt29;
  scalarD tt30;
  scalarD tt31;
  scalarD tt32;
  scalarD tt33;
  scalarD tt34;
  scalarD tt35;
  scalarD tt36;
  scalarD tt37;
  scalarD tt38;
  scalarD tt39;
  scalarD tt40;
  scalarD tt41;
  scalarD tt42;
  scalarD tt43;
  scalarD tt44;
  scalarD tt45;
  scalarD tt46;
  scalarD tt47;
  scalarD tt48;
  scalarD tt49;
  scalarD tt50;
  scalarD tt51;
  scalarD tt52;
  scalarD tt53;
  scalarD tt54;
  scalarD tt55;
  scalarD tt56;
  scalarD tt57;
  scalarD tt58;
  scalarD tt59;
  scalarD tt60;
  scalarD tt61;
  scalarD tt62;
  scalarD tt63;
  scalarD tt64;

  tt1=-v0[0];
  tt2=v1[0]+tt1;
  tt3=pow(tt2,2);
  tt4=-v0[1];
  tt5=v1[1]+tt4;
  tt6=pow(tt5,2);
  tt7=-v0[2];
  tt8=v1[2]+tt7;
  tt9=pow(tt8,2);
  tt10=tt9+tt6+tt3;
  tt11=sqrt(tt10);
  tt12=1/tt11;
  tt13=v2[0]+tt1;
  tt14=v2[1]+tt4;
  tt15=tt13*tt5-tt2*tt14;
  tt16=-v3[1];
  tt17=tt16+v1[1];
  tt18=-v1[1];
  tt19=v2[1]+tt18;
  tt20=v3[0]+tt1;
  tt21=v3[1]+tt4;
  tt22=tt2*tt21-tt20*tt5;
  tt23=v2[2]+tt7;
  tt24=tt2*tt23-tt13*tt8;
  tt25=-v1[2];
  tt26=v3[2]+tt25;
  tt27=-v2[2];
  tt28=tt27+v1[2];
  tt29=v3[2]+tt7;
  tt30=tt20*tt8-tt2*tt29;
  tt31=tt22*tt24;
  tt32=-tt15*tt30;
  tt33=tt14*tt8-tt5*tt23;
  tt34=-tt22*tt33;
  tt35=tt5*tt29-tt21*tt8;
  tt36=tt15*tt35;
  tt37=tt33*tt30;
  tt38=-tt24*tt35;
  tt39=tt8*(tt38+tt37)+tt5*(tt36+tt34)+tt2*(tt32+tt31);
  tt40=tt33*tt35+tt24*tt30+tt15*tt22;
  tt41=1/(pow(tt39,2)/tt10+pow(tt40,2));
  tt42=1/pow(tt11,3);
  tt43=-v1[0];
  tt44=v3[0]+tt43;
  tt45=-v2[0];
  tt46=tt45+v1[0];
  tt47=-v3[2];
  tt48=tt47+v1[2];
  tt49=v2[2]+tt25;
  tt50=-v3[0];
  tt51=tt50+v1[0];
  tt52=v3[1]+tt18;
  tt53=v2[0]+tt43;
  tt54=-v2[1];
  tt55=tt54+v1[1];
  tt56=tt54+v0[1];
  tt57=tt47+v0[2];
  tt58=tt50+v0[0];
  tt59=tt27+v0[2];
  tt60=tt16+v0[1];
  tt61=tt45+v0[0];
  tt62=tt25+v0[2];
  tt63=tt43+v0[0];
  tt64=tt18+v0[1];
  f0[0]=tt40*(tt2*tt42*tt39+tt12*(tt8*(tt33*tt26-tt28*tt35)+tt5*(tt19*tt35-tt17*tt33)+tt2*(-tt19*tt30-tt15*tt26+tt17*tt24+tt22*tt28)+tt15*tt30-tt22*tt24))*tt41-tt12*(tt28*tt30+tt24*tt26+tt19*tt22+tt15*tt17)*tt39*tt41;
  f0[1]=tt40*(tt5*tt42*tt39+tt12*(tt5*(tt46*tt35+tt15*tt48-tt44*tt33-tt22*tt49)+tt8*(tt49*tt30-tt24*tt48)+tt2*(tt44*tt24-tt46*tt30)-tt15*tt35+tt22*tt33))*tt41-tt12*(tt49*tt35+tt33*tt48+tt46*tt22+tt44*tt15)*tt39*tt41;
  f0[2]=tt40*(tt8*tt42*tt39+tt12*(tt8*(-tt53*tt35+tt55*tt30+tt51*tt33-tt52*tt24)+tt24*tt35-tt33*tt30+tt5*(tt15*tt52-tt55*tt22)+tt2*(tt53*tt22-tt51*tt15)))*tt41-tt12*(tt55*tt35+tt53*tt30+tt52*tt33+tt51*tt24)*tt39*tt41;
  f1[0]=tt40*(tt12*(tt8*(tt33*tt57-tt23*tt35)+tt5*(tt56*tt35-tt21*tt33)+tt2*(-tt56*tt30-tt15*tt57+tt21*tt24+tt22*tt23)+tt32+tt31)-tt2*tt42*tt39)*tt41-tt12*(tt23*tt30+tt24*tt57+tt56*tt22+tt15*tt21)*tt39*tt41;
  f1[1]=tt40*(tt12*(tt5*(tt13*tt35+tt15*tt29-tt58*tt33-tt22*tt59)+tt8*(tt59*tt30-tt24*tt29)+tt2*(tt58*tt24-tt13*tt30)+tt36+tt34)-tt5*tt42*tt39)*tt41-tt12*(tt59*tt35+tt33*tt29+tt13*tt22+tt58*tt15)*tt39*tt41;
  f1[2]=tt40*(tt12*(tt8*(-tt61*tt35+tt14*tt30+tt20*tt33-tt60*tt24)+tt38+tt37+tt5*(tt15*tt60-tt14*tt22)+tt2*(tt61*tt22-tt20*tt15))-tt8*tt42*tt39)*tt41-tt12*(tt14*tt35+tt61*tt30+tt60*tt33+tt20*tt24)*tt39*tt41;
  f2[0]=tt12*tt40*(tt2*(tt22*tt62-tt5*tt30)-tt62*tt8*tt35+tt6*tt35)*tt41-tt12*(tt62*tt30+tt5*tt22)*tt39*tt41;
  f2[1]=tt12*tt40*(tt5*(tt63*tt35-tt22*tt8)+tt9*tt30-tt63*tt2*tt30)*tt41-tt12*(tt8*tt35+tt63*tt22)*tt39*tt41;
  f2[2]=tt12*tt40*(tt8*(tt64*tt30-tt2*tt35)-tt64*tt5*tt22+tt3*tt22)*tt41-tt12*(tt64*tt35+tt2*tt30)*tt39*tt41;
  f3[0]=tt12*(tt2*(tt64*tt24-tt15*tt8)+tt9*tt33-tt64*tt5*tt33)*tt40*tt41-tt12*(tt8*tt24+tt64*tt15)*tt39*tt41;
  f3[1]=tt12*(tt5*(tt15*tt62-tt2*tt33)-tt62*tt8*tt24+tt3*tt24)*tt40*tt41-tt12*(tt62*tt33+tt2*tt15)*tt39*tt41;
  f3[2]=tt12*(tt8*(tt63*tt33-tt5*tt24)+tt6*tt15-tt63*tt2*tt15)*tt40*tt41-tt12*(tt5*tt33+tt63*tt24)*tt39*tt41;
  theta=atan2(tt12*tt39,tt40);
}
scalarD ClothBendEnergy::eval() const
{
  return evalBendEnergy(_v[0]->_pos,_v[1]->_pos,_v[2]->_pos,_v[3]->_pos)*_A;
}
void ClothBendEnergy::eval(Vec& c,STrips& grad,sizeType off) const
{
  const_cast<ClothBendEnergy&>(*this).prepare();
  c.block<3,1>(_v[0]->_index*3,0)+=_f[0]*_A*-_theta;
  c.block<3,1>(_v[1]->_index*3,0)+=_f[1]*_A*-_theta;
  c.block<3,1>(_v[2]->_index*3,0)+=_f[2]*_A*-_theta;
  c.block<3,1>(_v[3]->_index*3,0)+=_f[3]*_A*-_theta;
  if(_needHESS)
    for(sizeType i=0; i<4; i++)
      for(sizeType j=0; j<4; j++)
        addBlock(grad,_v[i]->_index*3,_v[j]->_index*3,-_f[i]*_f[j].transpose()*_A);
}
void ClothBendEnergy::prepare()
{
  evalBendForce(_v[0]->_pos,_v[1]->_pos,_v[2]->_pos,_v[3]->_pos,_f[0],_f[1],_f[2],_f[3],_theta);
}
//instance
template class ClothKineticEnergy<ClothMesh::ClothVertex>;
template class ClothFixedForceEnergy<ClothMesh::ClothVertex>;
template class ClothFixedNormalEnergy<ClothMesh::ClothVertex>;
template class ClothHookeanEnergy<ClothMesh::ClothVertex>;
template class ClothLinearHookeanEnergy<ClothMesh::ClothVertex>;
template class ClothRestLinearHookeanEnergy<ClothMesh::ClothVertex>;
template class ClothConstraintEnergy<ClothMesh::ClothVertex>;

template class ClothKineticEnergy<ClothMesh::ClothEdge>;
template class ClothFixedForceEnergy<ClothMesh::ClothEdge>;
template class ClothFixedNormalEnergy<ClothMesh::ClothEdge>;
template class ClothHookeanEnergy<ClothMesh::ClothEdge>;
template class ClothLinearHookeanEnergy<ClothMesh::ClothEdge>;
template class ClothRestLinearHookeanEnergy<ClothMesh::ClothEdge>;
template class ClothConstraintEnergy<ClothMesh::ClothEdge>;
