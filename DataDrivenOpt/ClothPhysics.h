#ifndef CLOTH_PHYSICS_H
#define CLOTH_PHYSICS_H

#include "ClothMesh.h"
#include "Utils.h"
#include <CommonFile/solvers/Objective.h>

PRJ_BEGIN

//constraint solver
class ClothConstraint
{
public:
  typedef ClothMesh::Vec Vec;
  typedef ClothMesh::STrip STrip;
  typedef ClothMesh::STrips STrips;
  template <typename T>
  void debugConstraint(std::vector<boost::shared_ptr<T> >& vs) const {
    scalarD delta=1E-7f;
    scalarD f=0.0f;
    Vec c,c2,J;
    c.resize(nr());
    c2.resize(nr());
    J.resize(vs.size()*3);
    J.setZero();

    Eigen::SparseMatrix<scalarD,0,sizeType> gradMat;
    STrips grad,grad0;
    c.setZero();
    eval(c,grad,0);
    gradMat.resize((int)nr(),(int)vs.size()*3);
    gradMat.setFromTriplets(grad.begin(),grad.end());
    for(int i=0; i<gradMat.outerSize(); i++)
      for(Eigen::SparseMatrix<scalarD,0,sizeType>::InnerIterator it(gradMat,i); it; ++it) {
        sizeType vid=it.col()/3;
        sizeType off=it.col()-vid*3;

        Vec3d tmp=vs[vid]->_pos;
        vs[vid]->_pos[off]+=delta;
        c2.setZero();
        eval(c2,grad0,0);
        scalarD gradN=(c2[it.row()]-c[it.row()])/delta;
        INFOV("Err: %f %f (%f)",it.value(),gradN,abs(it.value()-gradN))
        vs[vid]->_pos=tmp;
      }
  }
  virtual void writeVTK(VTKWriter<scalarD>& os) const {}
  virtual bool linear() const {
    return true;
  }
  virtual sizeType nr() const=0;
  virtual void getRelatedVertex(std::vector<sizeType>& vs) const=0;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const {}
  //For PCG solver, not every constraint has this interface
  virtual Eigen::Matrix<scalarD,-1,3> getBasis() const {
    ASSERT_MSG(false,"Not Supported!")return Eigen::Matrix<scalarD,-1,3>();
  }
  //For Multigrid solver, not every constraint has this interface
  virtual void project(Vec3d& pos,Vec3d& f) const {
    ASSERT_MSG(false,"Project Not Supported!")
  }
  sizeType _index;
};
class ClothEnergy : public ClothConstraint
{
public:
  typedef Eigen::Matrix<scalarD,9,1> Vec9d;
  template <typename T>
  void debugConstraintEval(std::vector<boost::shared_ptr<T> >& vs) const {
    Vec f;
    scalarD delta=1E-8f;
    scalarD E=eval(),dN;
    STrips grad;
    f.resize(vs.size()*3);
    f.setZero();
    eval(f,grad,0);

    std::vector<sizeType> rvs;
    getRelatedVertex(rvs);
    for(sizeType i=0; i<(sizeType)rvs.size(); i++)
      for(sizeType j=0; j<3; j++) {
        Vec3d tmp=vs[rvs[i]]->_pos;
        vs[rvs[i]]->_pos[j]+=delta;
        dN=(eval()-E)/delta;
        INFOV("Err: %f %f (%f)",f[rvs[i]*3+j],dN,f[rvs[i]*3+j]-dN);
        vs[rvs[i]]->_pos=tmp;
      }
  }
  //We reuse _index to store the number of DOF
  virtual sizeType nr() const {
    return _index;
  }
  virtual void getRelatedVertex(std::vector<sizeType>& vs) const {}
  virtual bool contributeToDamping() const {
    return false;
  }
  //For ClothEnergy, off is not used
  virtual scalarD eval() const=0;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const {}
  bool _needHESS;
};
class ClothConstraintSolver : public Objective<scalarD>
{
public:
  ClothConstraintSolver(boost::shared_ptr<ClothMesh> mesh,bool NMesh);
  virtual ~ClothConstraintSolver() {}
  void addConstraintArea();
  void addConstraintInextensible();
  void addSingleInextensibleN(boost::shared_ptr<ClothMesh::ClothEdge> e0,boost::shared_ptr<ClothMesh::ClothEdge> e1);
  void addConstraintBoundary();
  void findClosestPoint(const Vec3d& pt,sizeType& id,Vec3d& currPos,bool pos0=false) const;
  void fixPointAbove(const Vec4d& plane,bool pos0=false);
  boost::shared_ptr<ClothConstraint> fixClosestPoint(const Vec3d& pt,bool pull=false,bool pos0=false);
  boost::shared_ptr<ClothConstraint> fixClosestPointLine(const Vec3d& pt,const Vec3d& dir,bool pos0=false);
  boost::shared_ptr<ClothConstraint> fixClosestPointPlane(const Vec3d& pt,const Vec3d& n,bool pos0=false);
  void writeVTK(const std::string& str) const;
  void assemble();
  virtual void solve(Vec& X);
  void search(Vec& X,Vec& D);
  //objective wrapper
  int operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient) override;
  int operator()(const Vec& x,Vec& cvec,STrips& cjac);
  int inputs() const;
  int constraints() const;
  void setNrIter(sizeType iter);
  sizeType nrIter() const;
  scalarD epsF() const;
  scalarD& epsF();
  scalarD eps() const;
  scalarD& eps();
  bool isNMesh() const;
protected:
  //data
  std::vector<boost::shared_ptr<ClothConstraint> > _cons;
  //param
  boost::shared_ptr<ClothMesh> _mesh;
  bool _NMesh;
  //temporary info
  sizeType _nrC;
  scalarD _eps,_epsF;
  sizeType _nrIter;
  Vec _X0;
};

//build-in constraints
template <typename T>
class ClothFixedConstraint : public ClothConstraint
{
public:
  using typename ClothConstraint::Vec;
  using typename ClothConstraint::STrip;
  using typename ClothConstraint::STrips;
  ClothFixedConstraint(boost::shared_ptr<T> v,const Vec3d& pos);
  virtual void writeVTK(VTKWriter<scalarD>& os) const;
  virtual sizeType nr() const;
  virtual void getRelatedVertex(std::vector<sizeType>& vs) const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  virtual Eigen::Matrix<scalarD,-1,3> getBasis() const;
  virtual void project(Vec3d& pos,Vec3d& f) const;
  boost::shared_ptr<T> _v;
  Vec3d _pos;
};
template <typename T>
class ClothLineConstraint : public ClothConstraint
{
public:
  using typename ClothConstraint::Vec;
  using typename ClothConstraint::STrip;
  using typename ClothConstraint::STrips;
  ClothLineConstraint(boost::shared_ptr<T> v,const Vec3d& pos,const Vec3d& dir);
  virtual void writeVTK(VTKWriter<scalarD>& os) const;
  virtual sizeType nr() const;
  virtual void getRelatedVertex(std::vector<sizeType>& vs) const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  virtual Eigen::Matrix<scalarD,-1,3> getBasis() const;
  virtual void project(Vec3d& pos,Vec3d& f) const;
  boost::shared_ptr<T> _v;
  Vec3d _pos,_dir,_dir1,_dir2;
};
template <typename T>
class ClothPlaneConstraint : public ClothLineConstraint<T>
{
public:
  using typename ClothLineConstraint<T>::Vec;
  using typename ClothLineConstraint<T>::STrip;
  using typename ClothLineConstraint<T>::STrips;
  using ClothLineConstraint<T>::_v;
  using ClothLineConstraint<T>::_pos;
  using ClothLineConstraint<T>::_dir;
  ClothPlaneConstraint(boost::shared_ptr<T> v,const Vec3d& pos,const Vec3d& dir);
  virtual sizeType nr() const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  virtual Eigen::Matrix<scalarD,-1,3> getBasis() const;
  virtual void project(Vec3d& pos,Vec3d& f) const ;
};
template <typename T>
class ClothInextensibleConstraint : public ClothConstraint
{
public:
  ClothInextensibleConstraint(boost::shared_ptr<T> v0,boost::shared_ptr<T> v1,scalarD rest);
  virtual void writeVTK(VTKWriter<scalarD>& os) const;
  virtual bool linear() const;
  virtual sizeType nr() const;
  virtual void getRelatedVertex(std::vector<sizeType>& vs) const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  boost::shared_ptr<T> _v0;
  boost::shared_ptr<T> _v1;
  scalarD _rest;
  bool _useSqr;
};
class ClothBoundaryConstraint : public ClothConstraint
{
public:
  ClothBoundaryConstraint
  (boost::shared_ptr<ClothMesh::ClothTriangle> t0,
   boost::shared_ptr<ClothMesh::ClothTriangle> t1,
   boost::shared_ptr<ClothMesh::ClothVertex> v);
  virtual void writeVTK(VTKWriter<scalarD>& os) const;
  virtual sizeType nr() const;
  virtual void getRelatedVertex(std::vector<sizeType>& vs) const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  boost::shared_ptr<ClothMesh::ClothEdge> _v[6];
  boost::shared_ptr<ClothMesh::ClothTriangle> _t0;
  boost::shared_ptr<ClothMesh::ClothTriangle> _t1;
  scalarD _coef;
};
template <typename T>
class ClothAreaConstraint : public ClothConstraint
{
public:
  ClothAreaConstraint(boost::shared_ptr<T> v0,boost::shared_ptr<T> v1,boost::shared_ptr<T> v2);
  virtual void writeVTK(VTKWriter<scalarD>& os) const;
  virtual bool linear() const;
  virtual sizeType nr() const;
  virtual void getRelatedVertex(std::vector<sizeType>& vs) const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  boost::shared_ptr<T> _v0;
  boost::shared_ptr<T> _v1;
  boost::shared_ptr<T> _v2;
  scalarD _area;
};

PRJ_END

#endif
