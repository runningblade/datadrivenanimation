#include "SkinnedMeshUtility.h"
#include <CommonFile/RotationUtil.h>
#include <CommonFile/geom/BVHBuilder.h>
#include <CommonFile/solvers/Objective.h>
#include <CommonFile/CollisionDetection.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

struct Cache {
  Cache() {}
  Cache(boost::shared_ptr<StaticGeomCell> c1,boost::shared_ptr<StaticGeomCell> c2) {
    if(sizeType(c1->getUserData<void>()) > sizeType(c2->getUserData<void>()))
      swap(c1,c2);
    _c1=c1;
    _c2=c2;
  }
  bool operator<(const Cache& other) const {
    if(_c1 < other._c1)
      return true;
    else if(_c1 == other._c1)
      return _c2 < other._c2;
    else return false;
  }
  bool operator==(const Cache& other) const {
    return _c1 == other._c1 && _c2 == other._c2;
  }
  boost::shared_ptr<StaticGeomCell> _c1,_c2;
};
struct PointCallback {
  PointCallback(boost::shared_ptr<StaticGeomCell> cellOther,
                boost::shared_ptr<StaticGeomCell> cell,
                vector<SkinnedMeshUtility::Collision>& colls)
    :_cellOther(cellOther),_cell(cell),_colls(colls) {
    Mat4 T=_cell->getT();
    Mat4 TOther=_cellOther->getT();
    Mat4 fromOther=T.inverse()*TOther;
    _obb=OBBTpl<scalar,3>(fromOther.block<3,3>(0,0),fromOther.block<3,1>(0,3),_cellOther->getBB(true));
  }
  void updateDist(const Node<sizeType>& node) {
    if(!_obb.intersect(node._bb))
      return;
    SkinnedMeshUtility::Collision coll;
    coll._bodyA=(sizeType)_cell->getUserData<void>();
    coll._bodyB=(sizeType)_cellOther->getUserData<void>();
    coll._posA=transformHomo<scalar>(_cell->getT(),_cell->vss()[node._cell]);
    if(_cellOther->closest(coll._posA,coll._nA,&(coll._normalA)))
      _colls.push_back(coll);
  }
  bool validNode(const Node<sizeType>& node) {
    return _obb.intersect(node._bb);
  }
  boost::shared_ptr<StaticGeomCell> _cellOther,_cell;
  vector<SkinnedMeshUtility::Collision>& _colls;
  OBBTpl<scalar,3> _obb;
};
Cold interpTheta(const Cold& theta0,const Cold& theta1,const Coli& var,scalar val)
{
  Cold ret=theta0;
  for(sizeType i=0; i<var.size(); i++)
    if(var[i] == 1)
      ret[i]=interp1D<scalarD>(theta0[i],theta1[i],val);
  return ret;
}
void SkinnedMeshUtility::CollisionFilter::clear()
{
  _joints.clear();
}
void SkinnedMeshUtility::CollisionFilter::setMutual(bool mutual)
{
  _isMutual=mutual;
}
void SkinnedMeshUtility::CollisionFilter::insertJoint(sizeType j)
{
  _joints.insert(j);
}
bool SkinnedMeshUtility::CollisionFilter::isValid(sizeType j1,sizeType j2) const
{
  if(_isMutual) {
    if(_joints.find(j1) != _joints.end() &&
       _joints.find(j2) != _joints.end())
      return true;
  } else {
    if(_joints.find(j1) != _joints.end() ||
       _joints.find(j2) != _joints.end())
      return true;
  }
  return false;
}
//SkinnedMeshUtility
SkinnedMeshUtility::SkinnedMeshUtility():Serializable(typeid(SkinnedMeshUtility).name()) {}
bool SkinnedMeshUtility::read(istream& is,IOData* dat)
{
  _body.read(is,dat);
  readBinaryData(_feat,is);
  readVector(_time,is);
  readVector(_trajOff,is);
  readVector(_pathOff,is);
  readVector(_frms,is);
  readVector(_feats,is);
  return is.good();
}
bool SkinnedMeshUtility::write(ostream& os,IOData* dat) const
{
  _body.write(os,dat);
  writeBinaryData(_feat,os);
  writeVector(_time,os);
  writeVector(_trajOff,os);
  writeVector(_pathOff,os);
  writeVector(_frms,os);
  writeVector(_feats,os);
  return os.good();
}
boost::shared_ptr<Serializable> SkinnedMeshUtility::copy() const
{
  return boost::shared_ptr<Serializable>(new SkinnedMeshUtility);
}
const SkinnedMeshUsingArticulatedBody& SkinnedMeshUtility::getBody() const
{
  return _body;
}
SkinnedMeshUsingArticulatedBody& SkinnedMeshUtility::getBody()
{
  return _body;
}
bool SkinnedMeshUtility::readSkinnedMesh(const string& path)
{
  boost::filesystem::ifstream is(path,ios::binary);
  boost::shared_ptr<IOData> dat=getIOData();
  return SkinnedMeshUtility::read(is,dat.get());
}
bool SkinnedMeshUtility::writeSkinnedMesh(const string& path) const
{
  boost::filesystem::ofstream os(path,ios::binary);
  boost::shared_ptr<IOData> dat=getIOData();
  return SkinnedMeshUtility::write(os,dat.get());
}
void SkinnedMeshUtility::writeBodyVTK(const string& path,const Cold* theta)
{
  Cold theta0=restPose();
  if(theta)
    theta0=*theta;
  _body.getBody().writeFrameVTK(path,&theta0);
}
sizeType SkinnedMeshUtility::nrTraj() const
{
  return (sizeType)_trajOff.size();
}
sizeType SkinnedMeshUtility::nrFrm() const
{
  return (sizeType)_frms.size();
}
Matd SkinnedMeshUtility::getFrm() const
{
  Matd ret=Matd::Zero(_frms[0].size(),(sizeType)_frms.size());
  for(sizeType i=0; i<(sizeType)_frms.size(); i++)
    ret.col(i)=_frms[i];
  return ret;
}
Cold SkinnedMeshUtility::getFrm(sizeType i) const
{
  return _frms[i];
}
bool SkinnedMeshUtility::isBodyUpright(sizeType i) const
{
  const Cold& frm=_frms[i];
  Vec3d Y=expWGradV<scalarD>(frm.segment<3>(0),NULL,NULL,NULL,NULL)*Vec3d::UnitY();
  return Y[1] > 0;
}
bool SkinnedMeshUtility::isTrajBodyUpright(sizeType i) const
{
  sizeType beg=_trajOff[i];
  sizeType end=i+1 < (sizeType)_trajOff.size() ? _trajOff[i+1] : _frms.size();
  for(sizeType i=beg; i<end; i++)
    if(!isBodyUpright(i))
      return false;
  return true;
}
const vector<sizeType>& SkinnedMeshUtility::getTrajOff() const
{
  return _trajOff;
}
//collision
void SkinnedMeshUtility::excludeCollision(const vector<sizeType>& parts)
{
  for(sizeType i=0; i<(sizeType)parts.size(); i++)
    for(sizeType j=i; j<(sizeType)parts.size(); j++) {
      _exclusive[parts[i]].insert(parts[j]);
      _exclusive[parts[j]].insert(parts[i]);
    }
}
void SkinnedMeshUtility::buildCollision()
{
  _geom=StaticGeom(3);
  _bvhvs.resize(_body.nrJoint());
  ArticulatedBody& body=_body.getBody();
  for(sizeType i=0; i<_body.nrJoint(); i++) {
    const Body& b=body.findBody(_body.names()[i]);
    if(b.hasMesh()) {
      b.meshRef()->setUserData<void>((void*)i);
      boost::shared_ptr<StaticGeomCell> geom=b.meshRef();
      _geom.addGeomCell(geom);
      //build bvhv
      BVHV& bvhv=_bvhvs[i];
      const vector<Vec3,Eigen::aligned_allocator<Vec3> >& vss=geom->vss();
      bvhv.assign(vss.size(),Node<sizeType>());
      for(sizeType i=0; i<(sizeType)vss.size(); i++) {
        bvhv[i]._cell=i;
        bvhv[i]._nrCell=1;
        bvhv[i]._bb.setUnion(vss[i]);
      }
      BVHBuilder<Node<sizeType>,3> builder;
      builder.buildBVH(bvhv);
      for(sizeType i=(sizeType)vss.size(); i<(sizeType)bvhv.size(); i++)
        bvhv[i]._cell=-1;
    }
  }
  _geom.assemble();
  //exclude
  _exclusive.assign(_body.nrJoint(),set<sizeType>());
  for(sizeType i=0,parent; i<_body.nrJoint(); i++) {
    parent=_body.kintree()(0,i);
    if(parent >= 0 && parent < _body.nrJoint()) {
      _exclusive[parent].insert(i);
      _exclusive[i].insert(parent);
    }
  }
  //two-ring exclusive
  vector<set<sizeType> > twoRing=_exclusive;
  for(sizeType i=0,parent; i<_body.nrJoint(); i++) {
    parent=_body.kintree()(0,i);
    if(parent >= 0 && parent < _body.nrJoint()) {
      twoRing[parent].insert(_exclusive[i].begin(),_exclusive[i].end());
      twoRing[i].insert(_exclusive[parent].begin(),_exclusive[parent].end());
    }
  }
  _exclusive=twoRing;
  //HARD CODE: exclude collision between main body parts
  vector<sizeType> mainBody;
  mainBody.push_back(0);
  mainBody.push_back(3);
  mainBody.push_back(9);
  excludeCollision(mainBody);
}
void SkinnedMeshUtility::debugCollision(const string& path,scalar eps)
{
  recreate(path);
  Collisions colls;
  for(sizeType i=0; i<(sizeType)_frms.size(); i++) {
    findCollision(_frms[i],colls,NULL);
    writeBodyVTK(path+"/frm"+boost::lexical_cast<string>(i)+".vtk",&_frms[i]);
    writeCollision(path+"/coll"+boost::lexical_cast<string>(i)+".vtk",colls);
    //resolve
    _frms[i]=resolveCollision(_frms[i],eps);
    _body.getBody().writeFrameVTK(path+"/resolvedFrm"+boost::lexical_cast<string>(i)+".vtk",&(_frms[i]));
  }
}
bool SkinnedMeshUtility::findCollision(const Cold& theta,Collisions& colls,const CollisionFilter* cc)
{
  BodyOpT<const Body> op(_body.getBody().nrBody());
  return findCollision(theta,colls,op,true,cc);
}
bool SkinnedMeshUtility::findCollision(const Cold& theta,Collisions& colls,BodyOpT<const Body>& op,bool deepest,const CollisionFilter* cc)
{
  if(_bvhvs.empty())
    buildCollision();
  ArticulatedBody& body=_body.getBody();
  //update
  _body.getBody().transform(body.root(),op,(void*)&theta);
  for(sizeType i=0; i<_body.nrJoint(); i++) {
    Body& b=body.findBodyRef(_body.names()[i]);
    if(b.hasMesh())
      b.meshRef()->setT(op.getTrans(b.id()));
  }
  _geom.update();
  //detect broadphase
  vector<Cache> cache;
  {
    BVHQuery<boost::shared_ptr<StaticGeomCell> > query(_geom.getBVH(),3,boost::shared_ptr<StaticGeomCell>());
    query.broadphaseQuery(query,cache);
    sort(cache.begin(),cache.end());
  }
  //detect narrowphase
  colls.clear();
  for(sizeType i=0; i<(sizeType)cache.size(); i++) {
    if(i > 0 && cache[i] == cache[i-1])
      continue;
    sizeType b1=sizeType(cache[i]._c1->getUserData<void>());
    sizeType b2=sizeType(cache[i]._c2->getUserData<void>());
    if(_exclusive[b1].find(b2) != _exclusive[b1].end())
      continue;
    if(_exclusive[b2].find(b1) != _exclusive[b2].end())
      continue;
    if(cc && !cc->isValid(b1,b2))
      continue;
    //2->1
    PointCallback cb2(cache[i]._c1,cache[i]._c2,colls);
    BVHQuery<sizeType> query2(_bvhvs[b2],3,-1);
    query2.pointQuery(cb2);
    //1->2
    PointCallback cb1(cache[i]._c2,cache[i]._c1,colls);
    BVHQuery<sizeType> query1(_bvhvs[b1],3,-1);
    query1.pointQuery(cb1);
  }
  if(colls.empty())
    return false;
  //deepest
  if(deepest) {
    for(sizeType i=1; i<(sizeType)colls.size(); i++)
      if(colls[0]._nA.squaredNorm() < colls[i]._nA.squaredNorm())
        colls[0]=colls[i];
    colls.resize(1);
  }
  return true;
}
Cold SkinnedMeshUtility::resolveCollision(const Cold& theta,scalar eps)
{
#define CLEAR_JOINT(I)  \
theta0.segment(I*3,3).setZero();
#define ENABLE_JOINT(I) \
var.segment(I*3,3).setOnes(); \
cc.insertJoint(I);
#define OUTPUT_ERROR(STRING)  \
INFO(STRING)  \
_body.getBody().writeFrameVTK("error.vtk",&theta);  \
return resolveCollisionOrder2(theta,eps);

  Coli var;
  Cold theta0;
  CollisionFilter cc;
  Collisions colls;
  eps*=theta.cwiseAbs().maxCoeff();
  //initialize
  theta0=theta;
  //pass 1
  CLEAR_JOINT(1)
  CLEAR_JOINT(2)
  CLEAR_JOINT(4)
  CLEAR_JOINT(5)
  CLEAR_JOINT(7)
  CLEAR_JOINT(8)
  CLEAR_JOINT(10)
  CLEAR_JOINT(11)
  //pass 2
  CLEAR_JOINT(17)
  CLEAR_JOINT(19)
  CLEAR_JOINT(21)
  CLEAR_JOINT(23)
  //pass 3
  CLEAR_JOINT(16)
  CLEAR_JOINT(18)
  CLEAR_JOINT(20)
  CLEAR_JOINT(22)
  //initialize hands to avoid initial collision
  theta0.segment<3>(17*3)=invExpW<scalarD>(rotationZ<scalarD>(-M_PI/4,false));
  theta0.segment<3>(16*3)=invExpW<scalarD>(rotationZ<scalarD>( M_PI/4,false));
  //_body.getBody().writeFrameVTK("theta0.vtk",&theta0);
  //pass 1: search joints (1,2,4,5,7,8,10,11)
  var=Coli::Zero(_body.nrTheta());
  cc.clear();
  cc.setMutual(true);
  ENABLE_JOINT(1)
  ENABLE_JOINT(2)
  ENABLE_JOINT(4)
  ENABLE_JOINT(5)
  ENABLE_JOINT(7)
  ENABLE_JOINT(8)
  ENABLE_JOINT(10)
  ENABLE_JOINT(11)
  if(!findCollision(interpTheta(theta0,theta,var,1),colls,&cc)) {
    theta0=interpTheta(theta0,theta,var,1);
  } else {
    scalarD minV=0,maxV=1,midV;
    if(findCollision(interpTheta(theta0,theta,var,minV),colls,&cc)) {
      OUTPUT_ERROR("Error detected at pass 1!")
    }
    while(true) {
      midV=(minV+maxV)/2;
      if(findCollision(interpTheta(theta0,theta,var,midV),colls,&cc))
        maxV=midV;
      else minV=midV;
      if(abs(maxV-minV) < eps)
        break;
    }
    theta0=interpTheta(theta0,theta,var,maxV);
  }
  //pass 2: search joints (17,19,21,23)
  var=Coli::Zero(_body.nrTheta());
  cc.clear();
  cc.setMutual(false);
  ENABLE_JOINT(17)
  ENABLE_JOINT(19)
  ENABLE_JOINT(21)
  ENABLE_JOINT(23)
  if(!findCollision(interpTheta(theta0,theta,var,1),colls,&cc)) {
    theta0=interpTheta(theta0,theta,var,1);
  } else {
    scalarD minV=0,maxV=1,midV;
    if(findCollision(interpTheta(theta0,theta,var,minV),colls,&cc)) {
      OUTPUT_ERROR("Error detected at pass 2!")
    }
    while(true) {
      midV=(minV+maxV)/2;
      if(findCollision(interpTheta(theta0,theta,var,midV),colls,&cc))
        maxV=midV;
      else minV=midV;
      if(abs(maxV-minV) < eps)
        break;
    }
    theta0=interpTheta(theta0,theta,var,maxV);
  }
  //pass 3: search joints (16,18,20,22)
  var=Coli::Zero(_body.nrTheta());
  cc.clear();
  cc.setMutual(false);
  ENABLE_JOINT(16)
  ENABLE_JOINT(18)
  ENABLE_JOINT(20)
  ENABLE_JOINT(22)
  if(!findCollision(interpTheta(theta0,theta,var,1),colls,&cc)) {
    theta0=interpTheta(theta0,theta,var,1);
  } else {
    scalarD minV=0,maxV=1,midV;
    if(findCollision(interpTheta(theta0,theta,var,minV),colls,&cc)) {
      OUTPUT_ERROR("Error detected at pass 3!")
    }
    while(true) {
      midV=(minV+maxV)/2;
      if(findCollision(interpTheta(theta0,theta,var,midV),colls,&cc))
        maxV=midV;
      else minV=midV;
      if(abs(maxV-minV) < eps)
        break;
    }
    theta0=interpTheta(theta0,theta,var,maxV);
  }
  return theta0;

#undef CLEAR_JOINT
#undef ENABLE_JOINT
#undef OUTPUT_ERROR
}
Cold SkinnedMeshUtility::resolveCollisionOrder2(const Cold& theta,scalar eps)
{
#define CLEAR_JOINT(I)  \
theta0.segment(I*3,3).setZero();
#define ENABLE_JOINT(I) \
var.segment(I*3,3).setOnes(); \
cc.insertJoint(I);
#define OUTPUT_ERROR(STRING)  \
INFO(STRING)  \
_body.getBody().writeFrameVTK("error.vtk",&theta);  \
//exit(EXIT_FAILURE);
  return Cold::Zero(0);

  Coli var;
  Cold theta0;
  CollisionFilter cc;
  Collisions colls;
  eps*=theta.cwiseAbs().maxCoeff();
  //initialize
  theta0=theta;
  //pass 1
  CLEAR_JOINT(1)
  CLEAR_JOINT(2)
  CLEAR_JOINT(4)
  CLEAR_JOINT(5)
  CLEAR_JOINT(7)
  CLEAR_JOINT(8)
  CLEAR_JOINT(10)
  CLEAR_JOINT(11)
  //pass 2
  CLEAR_JOINT(17)
  CLEAR_JOINT(19)
  CLEAR_JOINT(21)
  CLEAR_JOINT(23)
  //pass 3
  CLEAR_JOINT(16)
  CLEAR_JOINT(18)
  CLEAR_JOINT(20)
  CLEAR_JOINT(22)
  //initialize hands to avoid initial collision
  theta0.segment<3>(17*3)=invExpW<scalarD>(rotationZ<scalarD>(-M_PI/4,false));
  theta0.segment<3>(16*3)=invExpW<scalarD>(rotationZ<scalarD>( M_PI/4,false));
  //_body.getBody().writeFrameVTK("theta0.vtk",&theta0);

  //pass 1: search joints (1,2,4,5,7,8,10,11)
  var=Coli::Zero(_body.nrTheta());
  cc.clear();
  cc.setMutual(true);
  ENABLE_JOINT(1)
  ENABLE_JOINT(2)
  ENABLE_JOINT(4)
  ENABLE_JOINT(5)
  ENABLE_JOINT(7)
  ENABLE_JOINT(8)
  ENABLE_JOINT(10)
  ENABLE_JOINT(11)
  if(!findCollision(interpTheta(theta0,theta,var,1),colls,&cc)) {
    theta0=interpTheta(theta0,theta,var,1);
  } else {
    scalarD minV=0,maxV=1,midV;
    if(findCollision(interpTheta(theta0,theta,var,minV),colls,&cc)) {
      OUTPUT_ERROR("Error detected at pass 1!")
    }
    while(true) {
      midV=(minV+maxV)/2;
      if(findCollision(interpTheta(theta0,theta,var,midV),colls,&cc))
        maxV=midV;
      else minV=midV;
      if(abs(maxV-minV) < eps)
        break;
    }
    theta0=interpTheta(theta0,theta,var,maxV);
  }
  //pass 2: search joints (16,18,20,22)
  var=Coli::Zero(_body.nrTheta());
  cc.clear();
  cc.setMutual(false);
  ENABLE_JOINT(16)
  ENABLE_JOINT(18)
  ENABLE_JOINT(20)
  ENABLE_JOINT(22)
  if(!findCollision(interpTheta(theta0,theta,var,1),colls,&cc)) {
    theta0=interpTheta(theta0,theta,var,1);
  } else {
    scalarD minV=0,maxV=1,midV;
    if(findCollision(interpTheta(theta0,theta,var,minV),colls,&cc)) {
      OUTPUT_ERROR("Error detected at pass 2!")
    }
    while(true) {
      midV=(minV+maxV)/2;
      if(findCollision(interpTheta(theta0,theta,var,midV),colls,&cc))
        maxV=midV;
      else minV=midV;
      if(abs(maxV-minV) < eps)
        break;
    }
    theta0=interpTheta(theta0,theta,var,maxV);
  }
  //pass 3: search joints (17,19,21,23)
  var=Coli::Zero(_body.nrTheta());
  cc.clear();
  cc.setMutual(false);
  ENABLE_JOINT(17)
  ENABLE_JOINT(19)
  ENABLE_JOINT(21)
  ENABLE_JOINT(23)
  if(!findCollision(interpTheta(theta0,theta,var,1),colls,&cc)) {
    theta0=interpTheta(theta0,theta,var,1);
  } else {
    scalarD minV=0,maxV=1,midV;
    if(findCollision(interpTheta(theta0,theta,var,minV),colls,&cc)) {
      OUTPUT_ERROR("Error detected at pass 3!")
    }
    while(true) {
      midV=(minV+maxV)/2;
      if(findCollision(interpTheta(theta0,theta,var,midV),colls,&cc))
        maxV=midV;
      else minV=midV;
      if(abs(maxV-minV) < eps)
        break;
    }
    theta0=interpTheta(theta0,theta,var,maxV);
  }
  return theta0;

#undef CLEAR_JOINT
#undef ENABLE_JOINT
#undef OUTPUT_ERROR
}
void SkinnedMeshUtility::writeCollision(const string& path,const Collisions& colls) const
{
  VTKWriter<scalar> os("collisions",path,true);
  vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
  for(sizeType i=0; i<(sizeType)colls.size(); i++) {
    vss.push_back(colls[i]._posA);
    vss.push_back(colls[i]._posA+colls[i]._nA);
  }
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,2,0),
                 VTKWriter<scalar>::IteratorIndex<Vec3i>(vss.size()/2,2,0),
                 VTKWriter<scalar>::LINE);
}
//sample
void SkinnedMeshUtility::clearDataset()
{
  _time.clear();
  _trajOff.clear();
  _pathOff.clear();
  _frms.clear();
  _feats.clear();
}
scalarD SkinnedMeshUtility::getDist(const Cold& thetaA,const Cold& thetaB)
{
  MatX3d jointsA,jointsB;
  scalarD num=0,denom=0;
  _body.getMesh(thetaA,&jointsA,NULL,NULL,NULL,NULL,true);
  _body.getMesh(thetaB,&jointsB,NULL,NULL,NULL,NULL,true);
  for(sizeType i=1,parent; i<_body.nrJoint(); i++) {
    parent=_body.kintree()(0,i);
    Vec3d dx0=jointsA.row(i)-jointsB.row(i);
    Vec3d dd=(jointsA.row(i)-jointsA.row(parent))-(jointsB.row(i)-jointsB.row(parent));
    scalarD dist=(jointsA.row(i)-jointsA.row(parent)).norm();
    num+=(dd.squaredNorm()+3*dd.dot(dx0)+3*dx0.squaredNorm())/3*dist;
    denom+=dist;
  }
  return sqrt(num/denom);
}
void SkinnedMeshUtility::resampleDataset(const string& path,scalar thresDist,bool hasTranslation,bool uniformScale,scalar checkCollision,FEATURE_TYPE feat,sizeType maxFrm)
{
  {
    INFOV("Reading frames from: %s!",path.c_str())
    boost::filesystem::ifstream is(path,ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    _body.getBody().clearFrm();
    _body.getBody().readFrm(is,dat.get());
  }
  saveState();
  //initial test
  Matd frms=_body.getBody().frm();
  if(!hasTranslation)
    frms.block(_body.nrTheta()-4,0,3,frms.cols()).setZero();
  if(!_frms.empty()) {
    ASSERT(_feat == feat)
    if(uniformScale)
      frms.row(_body.nrTheta()-1).setConstant(_frms[0][_body.nrTheta()-1]);
  } else _feat=feat;
  //initial frame
  if(maxFrm > 0)
    maxFrm+=_frms.size();
  _time.push_back(0);
  _trajOff.push_back(_frms.size());
  _pathOff.push_back(path);
  if(checkCollision > 0) {
    //frms.col(0)=resolveCollision(frms.col(0),checkCollision);
    Cold resolved=resolveCollision(frms.col(0),checkCollision);
    if(resolved.size() == 0) {
      WARNING("Skipped dataset due to collision error!")
      loadState();
      return;
    } else frms.col(0)=resolved;
  }
  _frms.push_back(frms.col(0));
  _feats.push_back(getFeature(_frms.back(),(FEATURE_TYPE)_feat));
  //INFOV("Adding frame %ld/%ld!",0,frms.cols())
  //return;
  //add more frames
  for(sizeType i=1; i<frms.cols(); i++) {
    //INFOV("Checking frame %ld/%ld!",i,frms.cols())
    if(checkCollision > 0) {
      //frms.col(i)=resolveCollision(frms.col(i),checkCollision);
      Cold resolved=resolveCollision(frms.col(i),checkCollision);
      if(resolved.size() == 0) {
        WARNING("Skipped dataset due to collision error!")
        loadState();
        return;
      } else frms.col(i)=resolved;
    }
    if(getDist(frms.col(i),_frms.back()) > thresDist) {
      //INFO("Inserting!")
      _time.push_back(i);
      _frms.push_back(frms.col(i));
      _feats.push_back(getFeature(_frms.back(),(FEATURE_TYPE)_feat));
      if(maxFrm > 0 && (sizeType)_frms.size() > maxFrm)
        return;
    }
  }
}
void SkinnedMeshUtility::uniformScale(scalar scale,FEATURE_TYPE feat)
{
  if(feat != SAME_FEATURE)
    _feat=feat;
  for(sizeType i=0; i<(sizeType)_frms.size(); i++) {
    _frms[i][_frms[i].size()-1]=scale;
    _feats[i]=getFeature(_frms[i],(FEATURE_TYPE)_feat);
  }
}
void SkinnedMeshUtility::writeDatasetVTK(const string& path)
{
  recreate(path);
  for(sizeType i=0; i<(sizeType)_frms.size(); i++) {
    writeBodyVTK(path+"/frm"+boost::lexical_cast<string>(i)+".vtk",&(_frms[i]));
    writeFeatureVTK(_feats[i],path+"/feat"+boost::lexical_cast<string>(i)+".vtk",(FEATURE_TYPE)_feat);
  }
}
Cold SkinnedMeshUtility::restPose() const
{
  Cold theta=Cold::Zero(_body.nrTheta());
  theta[theta.size()-1]=_frms[0][theta.size()-1];
  return theta;
}
//helper
void SkinnedMeshUtility::saveState()
{
  _savedState.reset(new SkinnedMeshUtility);
  _savedState->_time=_time;
  _savedState->_trajOff=_trajOff;
  _savedState->_pathOff=_pathOff;
  _savedState->_frms=_frms;
  _savedState->_feats=_feats;
}
void SkinnedMeshUtility::loadState()
{
  _time=_savedState->_time;
  _trajOff=_savedState->_trajOff;
  _pathOff=_savedState->_pathOff;
  _frms=_savedState->_frms;
  _feats=_savedState->_feats;
}
