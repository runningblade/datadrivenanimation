#ifndef SKINNED_MESH_UTILITY_H
#define SKINNED_MESH_UTILITY_H

#include "SkinnedMesh.h"
#ifndef CUDA_SUPPORT
#define CPU_ONLY
#endif
#include <caffe/util/db.hpp>
using namespace caffe;

PRJ_BEGIN

class SkinnedMeshUtility : public Serializable
{
public:
  enum FEATURE_TYPE {
    LIMB_DIRECTION,
    NORMALIZED_LIMB_DIRECTION,
    LIMB_ROTATION,
    THETA,
    SAME_FEATURE,
  };
  struct Collision {
    sizeType _bodyA,_bodyB;
    Vec3 _posA,_nA,_normalA;
    Vec3 _posAL,_posBL;
  };
  struct CollisionFilter {
    void clear();
    void setMutual(bool mutual);
    void insertJoint(sizeType j);
    bool isValid(sizeType j1,sizeType j2) const;
    set<sizeType> _joints;
    bool _isMutual;
  };
  typedef vector<Node<sizeType> > BVHV;
  typedef vector<Collision> Collisions;
public:
  SkinnedMeshUtility();
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  const SkinnedMeshUsingArticulatedBody& getBody() const;
  SkinnedMeshUsingArticulatedBody& getBody();
  bool readSkinnedMesh(const string& path);
  bool writeSkinnedMesh(const string& path) const;
  void writeBodyVTK(const string& path,const Cold* theta=NULL);
  sizeType nrTraj() const;
  sizeType nrFrm() const;
  Matd getFrm() const;
  Cold getFrm(sizeType i) const;
  bool isBodyUpright(sizeType i) const;
  bool isTrajBodyUpright(sizeType i) const;
  const vector<sizeType>& getTrajOff() const;
  //feature
  sizeType getFeatureType() const;
  static void pruneFootAndHand(set<sizeType>& filter);
  static SkinnedMeshUtility::FEATURE_TYPE parseFeatureFromString(string name,set<sizeType>* filter=NULL);
  static string parseFeatureToString(FEATURE_TYPE feat,const set<sizeType>* filter=NULL);
  Cold getFeature(const Cold& theta,FEATURE_TYPE feat,const set<sizeType>* filter=NULL);
  void writeFeatureVTK(const Cold& f,const string& path,FEATURE_TYPE feat,const set<sizeType>* filter=NULL);
  void writeFeatureObj(const Cold& f,ObjMesh& mesh,FEATURE_TYPE feat,const set<sizeType>* filter=NULL);
  void writeFeatureVTK(const Datum& f,const string& path,FEATURE_TYPE feat,const set<sizeType>* filter=NULL);
  void writeFeatureObj(const Datum& f,ObjMesh& mesh,FEATURE_TYPE feat,const set<sizeType>* filter=NULL);
  static Cold datumToFeature(const Datum& f);
  //collision
  void excludeCollision(const vector<sizeType>& parts);
  void buildCollision();
  void debugCollision(const string& path,scalar eps);
  bool findCollision(const Cold& theta,Collisions& colls,const CollisionFilter* cc);
  bool findCollision(const Cold& theta,Collisions& colls,BodyOpT<const Body>& op,bool deepest,const CollisionFilter* cc);
  Cold resolveCollision(const Cold& theta,scalar eps);
  Cold resolveCollisionOrder2(const Cold& theta,scalar eps);
  void writeCollision(const string& path,const Collisions& colls) const;
  //sample
  void clearDataset();
  scalarD getDist(const Cold& thetaA,const Cold& thetaB);
  void resampleDataset(const string& path,scalar thresDist,bool hasTranslation,bool uniformScale,scalar checkCollision,FEATURE_TYPE feat,sizeType maxFrm);
  void uniformScale(scalar scale,FEATURE_TYPE feat=SAME_FEATURE);
  virtual void writeDatasetVTK(const string& path);
  Cold restPose() const;
  //dataset parameter
  static const int TRAJ_LENGTH_THRES=10;
  static const int IGNORE_TRAJ_FRM=10;
protected:
  void saveState();
  void loadState();
  SkinnedMeshUsingArticulatedBody _body;
  //dataset
  sizeType _feat;
  vector<sizeType> _time;
  vector<sizeType> _trajOff;
  vector<string> _pathOff;
  vector<Cold,Eigen::aligned_allocator<Cold> > _frms;
  vector<Cold,Eigen::aligned_allocator<Cold> > _feats;
  //collision
  StaticGeom _geom;
  vector<BVHV> _bvhvs;
  vector<set<sizeType> > _exclusive;
  boost::shared_ptr<SkinnedMeshUtility> _savedState;
};

PRJ_END

#endif
