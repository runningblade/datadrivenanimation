#include "DTEntry.h"
#include <CommonFile/RotationUtil.h>

PRJ_BEGIN

//sparse matrix
void check(const vector<DTEntry>& I)
{
  for(sizeType i=0; i<(sizeType)I.size()-1; i++)
    ASSERT(I[i] < I[i+1])
  }
void check(const vector<pair<sizeType,Mat4> >& I)
{
  for(sizeType i=0; i<(sizeType)I.size()-1; i++)
    ASSERT(I[i].first < I[i+1].first)
  }
DTEntry::DTEntry():_Dq(Mat4::Zero()),_DqDt(Mat4::Zero()),_col(-1) {}
DTEntry::DTEntry(const Mat4& Dq,const Mat4& DqDt,sizeType col):_Dq(Dq),_DqDt(DqDt),_col(col) {}
bool DTEntry::operator<(const DTEntry& other) const
{
  return _col < other._col;
}
bool DTEntry::operator==(const DTEntry& other) const
{
  return _col == other._col;
}
void DTEntry::merge(const vector<DTEntry>& I,const vector<DTEntry>& J,vector<DTEntry>& out)
{
  check(I);
  check(J);

  out.clear();
  sizeType i=0,j=0;
  while(i<(sizeType)I.size() && j<(sizeType)J.size()) {
    if(I[i] < J[j])
      out.push_back(I[i++]);
    else if(J[j] < I[i])
      out.push_back(J[j++]);
    else ASSERT_MSG(false,"Parent/child cannot share same degree of freedom!")
    }
  while(i<(sizeType)I.size())
    out.push_back(I[i++]);
  while(j<(sizeType)J.size())
    out.push_back(J[j++]);

  check(out);
}
void DTEntry::merge(vector<pair<sizeType,Mat4> >& J,vector<pair<sizeType,Mat4> >& out)
{
  vector<pair<sizeType,Mat4> > I=out;
  check(I);
  check(J);

  out.clear();
  sizeType i=0,j=0;
  while(i<(sizeType)I.size() && j<(sizeType)J.size()) {
    if(I[i].first < J[j].first)
      out.push_back(I[i++]);
    else if(J[j].first < I[i].first)
      out.push_back(J[j++]);
    else {
      out.push_back(make_pair(I[i].first,I[i].second+J[j].second));
      i++;
      j++;
    }
  }
  while(i<(sizeType)I.size())
    out.push_back(I[i++]);
  while(j<(sizeType)J.size())
    out.push_back(J[j++]);

  check(out);
  vector<pair<sizeType,Mat4> > other;
  J.swap(other);
}
void DTTransfer::computeJ(const Mat6& M)
{
  //omega generalized V
  Mat3 cOmega=_DTDt.block<3,3>(0,0)*_T.block<3,3>(0,0).transpose();
  _vel.block<3,1>(0,0)=_DTDt.block<3,1>(0,3);
  _vel.block<3,1>(3,0)=invCross<scalar>(cOmega);

  //compute Mg
  Mat6 TR=Mat6::Zero(),DTR=Mat6::Zero();
  TR.block<3,3>(0,0)=_T.block<3,3>(0,0);
  TR.block<3,3>(3,3)=_T.block<3,3>(0,0);
  _Mg=TR*(M*TR.transpose());

  //compute [w]Mg+Mg\dot{T}^c
  DTR.block<3,3>(0,0)=_DTDt.block<3,3>(0,0);
  DTR.block<3,3>(3,3)=_DTDt.block<3,3>(0,0);
  _cwMg.setZero();
  _cwMg.block<6,3>(0,3)=(DTR*M*TR.transpose()).block<6,3>(0,3);

  //compute J/DJDt
  Mat3 DwDq;
  for(sizeType i=0; i<(sizeType)_entry.size(); i++) {
    DTEntry& e=_entry[i];
    e._J.block<3,1>(0,0)=e._Dq.block<3,1>(0,3);
    DwDq=e._Dq.block<3,3>(0,0)*_T.block<3,3>(0,0).transpose();
    e._J.block<3,1>(3,0)=invCross<scalar>(DwDq);

    e._DJDt.block<3,1>(0,0)=e._DqDt.block<3,1>(0,3);
    DwDq=e._DqDt.block<3,3>(0,0)*_T.block<3,3>(0,0).transpose()+
         e._Dq.block<3,3>(0,0)*_DTDt.block<3,3>(0,0).transpose();
    e._DJDt.block<3,1>(3,0)=invCross<scalar>(DwDq);
  }
}
void DTTransfer::computeDMgDq(const Mat6& M)
{
  Mat6 TR=Mat6::Zero(),DTRDq=Mat6::Zero();
  TR.block<3,3>(0,0)=_T.block<3,3>(0,0);
  TR.block<3,3>(3,3)=_T.block<3,3>(0,0);
  for(sizeType i=0; i<(sizeType)_entry.size(); i++) {
    DTEntry& e=_entry[i];
    DTRDq.block<3,3>(0,0)=e._Dq.block<3,3>(0,0);
    DTRDq.block<3,3>(3,3)=e._Dq.block<3,3>(0,0);

    e._DMgDq=DTRDq*M*TR.transpose();
    e._DMgDq=(e._DMgDq+e._DMgDq.transpose()).eval();
  }
}

PRJ_END
