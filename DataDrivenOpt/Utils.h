#ifndef UTILS_H
#define UTILS_H

#include <CommonFile/IO.h>
#include <CommonFile/solvers/ParallelVector.h>
#include <CommonFile/ImplicitFuncInterface.h>
#include <boost/property_tree/ptree_fwd.hpp>

PRJ_BEGIN

//variables
template <typename TT,typename TF>
void assignVariable(TT& to,const TF& from)
{
  to.setZero();
  sizeType sz=min(to.size(),from.size());
  to.segment(0,sz)=from.segment(0,sz).template cast<typename TT::Scalar>();
}
template <typename TT,typename TF>
TT assignVariableTo(const TF& from)
{
  TT to=TT::Zero();
  sizeType sz=min(to.size(),from.size());
  to.segment(0,sz)=from.segment(0,sz).template cast<typename TT::Scalar>();
  return to;
}
template <typename TT,typename TF>
void assignVariableOnes(TT& to,const TF& from)
{
  to.setOnes();
  sizeType sz=min(to.size(),from.size());
  to.segment(0,sz)=from.segment(0,sz).template cast<typename TT::Scalar>();
}
template <typename TT,typename TF>
TT assignVariableToOnes(const TF& from)
{
  TT to=TT::Ones();
  sizeType sz=min(to.size(),from.size());
  to.segment(0,sz)=from.segment(0,sz).template cast<typename TT::Scalar>();
  return to;
}
template <typename TT1,int DIM1,typename TT2,int DIM2>
void assignVariable(BBox<TT1,DIM1>& to,const BBox<TT2,DIM2>& from)
{
  assignVariable(to._minC,from._minC);
  assignVariable(to._maxC,from._maxC);
}
template <typename TT1,int DIM1,typename TT2,int DIM2>
BBox<TT1,DIM1> assignVariableTo(const BBox<TT2,DIM2>& from)
{
  BBox<TT1,DIM1> to;
  assignVariable(to._minC,from._minC);
  assignVariable(to._maxC,from._maxC);
  return to;
}
//filesystem
bool notDigit(char c);
bool lessDirByNumber(boost::filesystem::path A,boost::filesystem::path B);
bool exists(const boost::filesystem::path& path);
void removeDir(const boost::filesystem::path& path);
void create(const boost::filesystem::path& path);
void recreate(const boost::filesystem::path& path);
vector<boost::filesystem::path> files(const boost::filesystem::path& path);
vector<boost::filesystem::path> directories(const boost::filesystem::path& path);
void sortFilesByNumber(vector<boost::filesystem::path>& files);
bool isDir(const boost::filesystem::path& path);
size_t fileSize(const boost::filesystem::path& path);
string parseProps(sizeType argc,char** argv,boost::property_tree::ptree& pt);
std::string getSourcePath();
//property tree
void readPtreeBinary(boost::property_tree::ptree& pt,istream& is);
void writePtreeBinary(const boost::property_tree::ptree& pt,ostream& os);
void readPtreeAscii(boost::property_tree::ptree& pt,const boost::filesystem::path& path);
void writePtreeAscii(const boost::property_tree::ptree& pt,const boost::filesystem::path& path);
//implicit functions
class ImplicitFuncCircleSmooth : public ImplicitFunc<scalar>
{
public:
  ImplicitFuncCircleSmooth(scalar rad,const Vec3& ctr,scalar alpha);
  template <typename VCTR>
  ImplicitFuncCircleSmooth(scalar rad,const VCTR& ctr,scalar alpha) {
    _rad=rad;
    _alpha=alpha;
    assignVariable(_ctr,ctr);
  }
  scalar operator()(const Vec3& pos) const override;
  virtual BBox<scalar> getBB() const override;
private:
  scalar _rad,_alpha;
  Vec3 _ctr;
};
class ImplicitFuncCircle : public ImplicitFunc<scalar>
{
public:
  ImplicitFuncCircle(scalar rad,const Vec3& ctr);
  template <typename VCTR>
  ImplicitFuncCircle(scalar rad,const VCTR& ctr) {
    _rad=rad;
    assignVariable(_ctr,ctr);
  }
  scalar operator()(const Vec3& pos) const override;
  virtual BBox<scalar> getBB() const override;
private:
  scalar _rad;
  Vec3 _ctr;
};
class ImplicitFuncBox : public ImplicitFunc<scalar>
{
public:
  template <typename VEC>
  ImplicitFuncBox(const VEC& minC,const VEC& maxC,sizeType DIM) {
    _DIM=DIM;
    assignVariable(_bb._minC,minC);
    assignVariable(_bb._maxC,maxC);
  }
  scalar operator()(const Vec3& pos) const override;
  virtual BBox<scalar> getBB() const override;
private:
  sizeType _DIM;
  BBox<scalar> _bb;
};
class VelFuncRotate : public VelFunc<scalar>
{
public:
  VelFuncRotate();
  VelFuncRotate(const Vec3& ctr,const Vec3& dir);
  template <typename VCTR,typename VDIR>
  VelFuncRotate(const VCTR& ctr,const VDIR& dir) {
    assignVariable(_ctr,ctr);
    assignVariable(_dir,dir);
  }
  Vec3 operator()(const Vec3& pos) const override;
private:
  Vec3 _ctr,_dir;
};
class VelFuncConstant : public VelFunc<scalar>
{
public:
  VelFuncConstant();
  VelFuncConstant(const Vec3& dir);
  template <typename VDIR>
  VelFuncConstant(const VDIR& dir) {
    assignVariable(_dir,dir);
  }
  Vec3 operator()(const Vec3& pos) const override;
private:
  Vec3 _dir;
};
//matrix
template <typename T>
static void addIK(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,sizeType r,sizeType c,T coef,int K)
{
  for(int k=0; k<K; k++)
    H.push_back(Eigen::Triplet<scalarD,sizeType>(r+k,c+k,coef));
}
template <typename MT>
static void addI(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,sizeType r,sizeType c,const MT& coef)
{
  sizeType nr=coef.size();
  for(sizeType i=0; i<nr; i++)
    H.push_back(Eigen::Triplet<scalarD,sizeType>(r+i,c+i,coef[i]));
}
template <typename MT>
static void addBlock(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,sizeType r,sizeType c,const MT& coef)
{
  sizeType nrR=coef.rows();
  sizeType nrC=coef.cols();
  for(sizeType i=0; i<nrR; i++)
    for(sizeType j=0; j<nrC; j++)
      H.push_back(Eigen::Triplet<scalarD,sizeType>(r+i,c+j,coef(i,j)));
}
template <typename IT,typename MT>
static void addBlockIndexed(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,const IT& index,const MT& coef)
{
  sizeType nrR=coef.rows();
  sizeType nrC=coef.cols();
  ASSERT(nrR == nrC && nrR == index.size())
  for(sizeType i=0; i<nrR; i++)
    for(sizeType j=0; j<nrR; j++)
      H.push_back(Eigen::Triplet<scalarD,sizeType>(index[i],index[j],coef(i,j)));
}
template <typename SMAT=Eigen::SparseMatrix<scalarD,0,sizeType> >
static void addSparseBlock(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,sizeType r,sizeType c,const SMAT& mat,typename SMAT::Scalar coef)
{
  for(sizeType k=0; k<mat.outerSize(); ++k)
    for(typename SMAT::InnerIterator it(mat,k); it; ++it)
      H.push_back(Eigen::Triplet<scalarD,sizeType>(it.row()+r,it.col()+c,it.value()*coef));
}
template <typename SMAT=Eigen::SparseMatrix<scalarD,0,sizeType> >
static void addSparseBlockKKT(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,sizeType r,sizeType c,const SMAT& mat,typename SMAT::Scalar coef)
{
  for(sizeType k=0; k<mat.outerSize(); ++k)
    for(typename SMAT::InnerIterator it(mat,k); it; ++it) {
      H.push_back(Eigen::Triplet<scalarD,sizeType>(it.row()+r,it.col()+c,it.value()*coef));
      H.push_back(Eigen::Triplet<scalarD,sizeType>(it.col()+c,it.row()+r,it.value()*coef));
    }
}
template <typename VEC>
static void multiplyAdd(const ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,const VEC& b,VEC& out,sizeType off)
{
  typedef ParallelVector<Eigen::Triplet<scalarD,sizeType> >::const_iterator iterator;
  for(iterator beg=H.begin(),end=H.end(); beg!=end; beg++)
    out[beg->row()+off]+=b[beg->col()]*beg->value();
}
template <typename T>
static void extend(T& m,sizeType r,sizeType c)
{
  T tmp=m;
  m.setZero(m.rows()+r,m.cols()+c);
  m.block(0,0,tmp.rows(),tmp.cols())=tmp;
}
//diagonal shift for sparse matrix
template <typename SMAT=Eigen::SparseMatrix<scalarD,0,sizeType> >
typename SMAT::Scalar maxAbsSparseDiag(const SMAT& mat)
{
  typename SMAT::Scalar ret=0;
  for(sizeType k=0; k<mat.outerSize(); ++k)
    for(typename SMAT::InnerIterator it(mat,k); it; ++it)
      if(it.row() == it.col())
        ret=max(ret,abs(it.value()));
  return ret;
}
template <typename SMAT=Eigen::SparseMatrix<scalarD,0,sizeType> >
void addSparseDiag(SMAT& mat,typename SMAT::Scalar shift)
{
  for(sizeType k=0; k<mat.outerSize(); ++k)
    for(typename SMAT::InnerIterator it(mat,k); it; ++it)
      if(it.row() == it.col())
        it.valueRef()+=shift;
}

//instance
#define INSTANCE_ALLDIM(TYPE,SPARSITY)  \
INSTANCE(2,TYPE,SPARSITY) INSTANCE(3,TYPE,SPARSITY)
#define INSTANCE_ALLDIM_ALLTYPE(SPARSITY)  \
INSTANCE_ALLDIM(Octree,SPARSITY) INSTANCE_ALLDIM(Constant,SPARSITY)
#define INSTANCE_ALL  \
INSTANCE_ALLDIM_ALLTYPE(1)  \
INSTANCE_ALLDIM_ALLTYPE(2)  \
INSTANCE_ALLDIM_ALLTYPE(3)  \
INSTANCE_ALLDIM_ALLTYPE(4)  \

#define INSTANCE2_ALLDIM(TYPE,SPARSITY) \
INSTANCE2(2,TYPE,SPARSITY,2) INSTANCE2(2,TYPE,SPARSITY,1) INSTANCE2(2,TYPE,SPARSITY,0)  \
INSTANCE2(3,TYPE,SPARSITY,2) INSTANCE2(3,TYPE,SPARSITY,1) INSTANCE2(3,TYPE,SPARSITY,0)
#define INSTANCE2_ALLDIM_ALLTYPE(SPARSITY)  \
INSTANCE2_ALLDIM(Octree,SPARSITY) INSTANCE2_ALLDIM(Constant,SPARSITY)
#define INSTANCE2_ALL  \
INSTANCE2_ALLDIM_ALLTYPE(1)  \
INSTANCE2_ALLDIM_ALLTYPE(2)  \
INSTANCE2_ALLDIM_ALLTYPE(3)  \
INSTANCE2_ALLDIM_ALLTYPE(4)  \

PRJ_END

#endif
