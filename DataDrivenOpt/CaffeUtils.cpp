#include "CaffeUtils.h"
#include "SkinnedMeshClothUtility.h"
#include <fpzip/fpzip.h>
#include <stdio.h>
#include <queue>
#include <caffe/util/db.hpp>
#include <boost/lockfree/spsc_queue.hpp>
#include <boost/atomic.hpp>
#include <boost/thread.hpp>

PRJ_BEGIN

struct DatumFPZ {
  FPZ _header;
  vector<float> _val;
};
Datum genRandomDatum(int nc,int nh,int nw)
{
  Datum d;
  d.set_channels(nc);
  d.set_height(nh);
  d.set_width(nw);
  google::protobuf::RepeatedField<float>* ptr=d.mutable_float_data();
  for(sizeType i=0; i<d.channels()*d.height()*d.width(); i++)
    ptr->Add(RandEngine::randR01());
  return d;
}
string genRandomString()
{
  string ret;
  ret.resize(RandEngine::randI(10,1000));
  for(sizeType i=0; i<(sizeType)ret.size(); i++)
    ret[i]=(char)RandEngine::randI(0,255);
  return ret;
}
void checkSameDatum(const Datum& A,const Datum& B)
{
  ASSERT(A.channels() == B.channels())
  ASSERT(A.height() == B.height())
  ASSERT(A.width() == B.width())
  for(int i=0; i<A.float_data_size(); i++) {
    ASSERT(A.float_data(i) == B.float_data(i))
  }
}
void reopenDB(db::DB& leveldb,const string& path)
{
  if(boost::filesystem::exists(path))
    boost::filesystem::remove_all(path);
  leveldb.Open(path,db::NEW);
}
//DatasetKey
DatasetKey::DatasetKey():_idDataset(""),_featStr(""),_idTraj(-1),_idFrm(-1) {}
DatasetKey::DatasetKey(string key)
{
  size_t pos;
  //idDataset
  pos=key.find_first_of('_');
  _idDataset=key.substr(0,pos);
  key=key.substr(pos+1);
  //idTraj
  pos=key.find_first_of('_');
  _idTraj=boost::lexical_cast<int>(key.substr(0,pos));
  key=key.substr(pos+1);
  //idFrm
  pos=key.find_first_of('_');
  _idFrm=boost::lexical_cast<int>(key.substr(0,pos));
  key=key.substr(pos+1);
  //featStr
  set<sizeType> filter;
  SkinnedMeshUtility::FEATURE_TYPE type=SkinnedMeshUtility::parseFeatureFromString(key,&filter);
  _featStr=SkinnedMeshUtility::parseFeatureToString(type,&filter);
  key=key.substr(_featStr.length()+1);
  //datumSpecs
  int d0,d1,d2;
  _datumSpecs.clear();
  typedef boost::tokenizer<boost::char_separator<char> > Token;
  Token tok(key,boost::char_separator<char>("_"));
  Token::iterator it=tok.begin();
  for(; it!=tok.end(); ++it)
    if(sscanf(it->c_str(),"%d,%d,%d",&d0,&d1,&d2) == 3)
      _datumSpecs.push_back(Vec3i(d0,d1,d2));
    else break;
  _imageSpecs.clear();
  for(; it!=tok.end(); ++it)
    if(sscanf(it->c_str(),"JPEG,%d",&d0) == 1)
      _imageSpecs.push_back(make_pair("JPEG",d0));
    else break;
}
DatasetKey::~DatasetKey() {}
string DatasetKey::str() const
{
  string ret=strPrefix();
  for(sizeType i=0; i<(sizeType)_datumSpecs.size(); i++)
    ret+="_"+boost::lexical_cast<string>(_datumSpecs[i][0])+","+boost::lexical_cast<string>(_datumSpecs[i][1])+","+boost::lexical_cast<string>(_datumSpecs[i][2]);
  for(sizeType i=0; i<(sizeType)_imageSpecs.size(); i++)
    ret+="_"+_imageSpecs[i].first+","+boost::lexical_cast<string>(_imageSpecs[i].second);
  return ret;
}
string DatasetKey::strPrefix() const
{
  return _idDataset+"_"+boost::lexical_cast<string>(_idTraj)+"_"+boost::lexical_cast<string>(_idFrm)+"_"+_featStr;
}
bool DatasetKey::operator<(const DatasetKey& other) const
{
  if(_idDataset < other._idDataset)
    return true;
  else if(_idDataset > other._idDataset)
    return false;

  if(_featStr < other._featStr)
    return true;
  else if(_featStr > other._featStr)
    return false;

  if(_idTraj < other._idTraj)
    return true;
  else if(_idTraj > other._idTraj)
    return false;

  if(_idFrm < other._idFrm)
    return true;
  else if(_idFrm > other._idFrm)
    return false;

  return false;
}
bool DatasetKey::compressDatums(const vector<Datum>& datums,const vector<string>& images,string& key,string& value)
{
  value="";
  for(sizeType i=0; i<(sizeType)datums.size(); i++) {
    const Datum& dat=datums[i];
    Vec3i spec(dat.channels(),dat.height(),dat.width());
    key+="_"+boost::lexical_cast<string>(spec[0])+","+boost::lexical_cast<string>(spec[1])+","+boost::lexical_cast<string>(spec[2]);
    if(compressDatum(dat,value) == 0)
      return false;
  }
  for(sizeType i=0; i<(sizeType)images.size(); i++) {
    key+="_JPEG,"+boost::lexical_cast<string>(images[i].size());
    value.append(images[i].begin(),images[i].end());
  }
  return true;
}
bool DatasetKey::decompressDatumsFPZ(vector<DatumFPZ>& datums,vector<string>& images,string value)
{
  datums.resize(_datumSpecs.size());
  for(sizeType i=0; i<(sizeType)datums.size(); i++) {
    DatumFPZ& dat=datums[i];
    dat._header.nx=_datumSpecs[i][0];
    dat._header.ny=_datumSpecs[i][1];
    dat._header.nz=_datumSpecs[i][2];
    size_t ret=decompressDatumFPZ(dat,value);
    if(ret == 0)
      return false;
  }
  images.resize(_imageSpecs.size());
  for(sizeType i=0; i<(sizeType)images.size(); i++) {
    images[i].clear();
    images[i].append(value.begin(),value.begin()+_imageSpecs[i].second);
    value.erase(value.begin(),value.begin()+_imageSpecs[i].second);
  }
  return true;
}
bool DatasetKey::decompressDatums(vector<Datum>& datums,vector<string>& images,string value)
{
  datums.resize(_datumSpecs.size());
  for(sizeType i=0; i<(sizeType)datums.size(); i++) {
    Datum& dat=datums[i];
    dat.set_channels(_datumSpecs[i][0]);
    dat.set_height(_datumSpecs[i][1]);
    dat.set_width(_datumSpecs[i][2]);
    size_t ret=decompressDatum(dat,value);
    if(ret == 0)
      return false;
  }
  images.resize(_imageSpecs.size());
  for(sizeType i=0; i<(sizeType)images.size(); i++) {
    images[i].clear();
    images[i].append(value.begin(),value.begin()+_imageSpecs[i].second);
    value.erase(value.begin(),value.begin()+_imageSpecs[i].second);
  }
  return true;
}
void DatasetKey::debugCompression(sizeType N)
{
  string key="1_1_1_LIMB_ROTATION",value;
  vector<Datum> datumsC,datumsD;
  vector<string> imagesC,imagesD;
  for(sizeType i=0; i<N; i++)
    datumsC.push_back(genRandomDatum(RandEngine::randR(1,10),RandEngine::randR(1,10),RandEngine::randR(1,10)));
  for(sizeType i=0; i<N; i++)
    imagesC.push_back(genRandomString());
  ASSERT(compressDatums(datumsC,imagesC,key,value))
  ASSERT(DatasetKey(key).decompressDatums(datumsD,imagesD,value))
  for(sizeType i=0; i<(sizeType)datumsC.size(); i++)
    checkSameDatum(datumsC[i],datumsD[i]);
  for(sizeType i=0; i<(sizeType)imagesC.size(); i++) {
    ASSERT(imagesC[i] == imagesD[i])
  }
  ASSERT(datumsC.size() == datumsD.size())
}
void DatasetKey::debugFindRemove(const string& dbType)
{
  INFOV("Begin checking %s",dbType.c_str())
  boost::shared_ptr<db::DB> db(db::GetDB(dbType));
  reopenDB(*db,"testDB");
  string val;

  //check find
  ASSERT(!db->Find("datum",val))
  INFO("A")
  boost::shared_ptr<db::Transaction> txn(db->NewTransaction());
  txn->Put("datum","value");
  txn->Commit();
  ASSERT(db->Find("datum",val) && val == "value")

  //check remove
  ASSERT(db->Remove("datum"))
  val.empty();
  ASSERT(!db->Find("datum",val))
  INFOV("End checking %s",dbType.c_str())
}
//helper
size_t DatasetKey::compressDatum(const Datum& dat,string& str)
{
  size_t sz=1024+sizeof(float)*dat.float_data_size();
  char* buffer=(char*)malloc(sz);
  FPZ* fpz=fpzip_write_to_buffer(buffer,sz);
  fpz->type=FPZIP_TYPE_FLOAT;
  fpz->prec=8*sizeof(float);
  fpz->nx=dat.channels();
  fpz->ny=dat.height();
  fpz->nz=dat.width();
  fpz->nf=1;

  vector<float> data(fpz->nx*fpz->ny*fpz->nz);
  for(int i=0; i<(int)data.size(); i++)
    data[i]=dat.float_data(i);
  size_t ret=fpzip_write(fpz,(const void*)&data[0]);
  str.insert(str.end(),buffer,buffer+ret);
  return ret;
}
size_t DatasetKey::decompressDatumFPZ(DatumFPZ& dat,string& str)
{
  FPZ* fpz=fpzip_read_from_buffer(&str[0]);
  fpz->type=FPZIP_TYPE_FLOAT;
  fpz->prec=8*sizeof(float);
  fpz->nx=dat._header.nx;
  fpz->ny=dat._header.ny;
  fpz->nz=dat._header.nz;
  fpz->nf=1;
  dat._header=*fpz;
  dat._val.resize(fpz->nx*fpz->ny*fpz->nz);
  size_t ret=fpzip_read(fpz,&(dat._val[0]));
  if(ret > 0)
    str.erase(str.begin(),str.begin()+ret);
  fpzip_read_close(fpz);
  return ret;
}
size_t DatasetKey::decompressDatum(Datum& dat,string& str)
{
  DatumFPZ datFPZ;
  datFPZ._header.nx=dat.channels();
  datFPZ._header.ny=dat.height();
  datFPZ._header.nz=dat.width();
  size_t ret=decompressDatumFPZ(datFPZ,str);
  google::protobuf::RepeatedField<float>* ptr=dat.mutable_float_data();
  for(int i=0; i<(int)datFPZ._val.size(); i++)
    ptr->Add(datFPZ._val[i]);
  return ret;
}

PRJ_END

//python leveldb interface: tensorflow
extern "C"
{
  //tensorflow data structure
  struct Datums {
    string _key;
    vector<COMMON::DatumFPZ> _datums;
    vector<string> _images;
    int _epoch;
  };
  struct DatasetHandle {
    Datums _datumsTF;
    boost::shared_ptr<db::DB> _dbTF;
    boost::shared_ptr<db::Cursor> _cursorTF;
  };
  //python api
  void closeDB(void* handle)
  {
    DatasetHandle* hPtr=(DatasetHandle*)handle;
    if(hPtr->_cursorTF)
      hPtr->_cursorTF=NULL;
    if(hPtr->_dbTF)
      hPtr->_dbTF=NULL;
    delete hPtr;
  }
  void* openDB(const char* path)
  {
    DatasetHandle* hPtr=new DatasetHandle;
    if(!boost::filesystem::exists(path) || !boost::filesystem::is_directory(path)) {
      INFOV("Cannot file leveldb: %s!",path)
      exit(EXIT_FAILURE);
    }
    hPtr->_datumsTF._epoch=0;
    hPtr->_dbTF.reset(db::GetDB("leveldb"));
    hPtr->_dbTF->Open(path,db::READ);
    hPtr->_cursorTF.reset(hPtr->_dbTF->NewCursor());
    ASSERT_MSG(hPtr->_cursorTF->valid(),"Found empty dataset!")
    hPtr->_datumsTF._key=hPtr->_cursorTF->key();
    COMMON::DatasetKey(hPtr->_cursorTF->key()).decompressDatumsFPZ(hPtr->_datumsTF._datums,hPtr->_datumsTF._images,hPtr->_cursorTF->value());
    return hPtr;
  }
  void resetEpoch(void* handle)
  {
    DatasetHandle* hPtr=(DatasetHandle*)handle;
    hPtr->_datumsTF._epoch=0;
  }
  int getEpoch(void* handle)
  {
    DatasetHandle* hPtr=(DatasetHandle*)handle;
    return hPtr->_datumsTF._epoch;
  }
  void getKey(void* handle,char* buf,int nrByte)
  {
    DatasetHandle* hPtr=(DatasetHandle*)handle;
    memset(buf,0,nrByte);
    strcpy(buf,hPtr->_datumsTF._key.c_str());
  }
  bool nextDB(void* handle)
  {
    DatasetHandle* hPtr=(DatasetHandle*)handle;
    if(!hPtr->_cursorTF || !hPtr->_cursorTF->valid())
      return false;
    hPtr->_cursorTF->Next();
    if(hPtr->_cursorTF->valid()) {
      hPtr->_datumsTF._key=hPtr->_cursorTF->key();
      COMMON::DatasetKey(hPtr->_cursorTF->key()).decompressDatumsFPZ(hPtr->_datumsTF._datums,hPtr->_datumsTF._images,hPtr->_cursorTF->value());
    } else {
      //new epoch
      hPtr->_cursorTF.reset(hPtr->_dbTF->NewCursor());
      ASSERT_MSG(hPtr->_cursorTF->valid(),"Found empty dataset!")
      hPtr->_datumsTF._key=hPtr->_cursorTF->key();
      COMMON::DatasetKey(hPtr->_cursorTF->key()).decompressDatumsFPZ(hPtr->_datumsTF._datums,hPtr->_datumsTF._images,hPtr->_cursorTF->value());
      hPtr->_datumsTF._epoch++;
    }
    return true;
  }
  bool readMeanBinary(const char* path,float* dat,int* shape)
  {
    Blob<float> blob;
    {
      BlobProto proto;
      bool ret=ReadProtoFromBinaryFile(path,&proto);
      if(!ret)
        return false;
      blob.FromProto(proto);
    }
    shape[0]=blob.channels();
    shape[1]=blob.height();
    shape[2]=blob.width();
    if(dat) {
      int n=shape[0]*shape[1]*shape[2];
      for(int i=0; i<n; i++)
        dat[i]=(float)blob.cpu_data()[i];
    }
    return true;
  }
  //debug
  void createTestDB()
  {
    DatasetHandle* hPtr=new DatasetHandle;
    hPtr->_dbTF.reset(db::GetDB("leveldb"));
    COMMON::reopenDB(*(hPtr->_dbTF),"leveldb_test");
    boost::shared_ptr<db::Transaction> txn(hPtr->_dbTF->NewTransaction());
    for(sizeType i=0; i<1500; i++) {
      vector<Datum> datums;
      vector<string> images;
      datums.push_back(COMMON::genRandomDatum(4,3,2));
      datums.push_back(COMMON::genRandomDatum(2,3,4));
      images.push_back(COMMON::genRandomString());
      images.push_back(COMMON::genRandomString());
      string key=boost::lexical_cast<string>(i)+"_0_0_LIMB_ROTATION",value;
      COMMON::DatasetKey::compressDatums(datums,images,key,value);
      txn->Put(key,value);
    }
    txn->Commit();
    txn=NULL;
    closeDB(hPtr);
  }
  void debugPythonAPI()
  {
    createTestDB();
    //read pass1
    {
      DatasetHandle* hPtr=(DatasetHandle*)openDB("leveldb_test");
      boost::filesystem::ofstream os("leveldb_test_read1.txt");
      for(sizeType i=0; i<10000; i++) {
        os << hPtr->_datumsTF._key << " " << hPtr->_datumsTF._epoch << std::endl;
        nextDB(hPtr);
      }
      closeDB(hPtr);
    }
    //read pass1
    {
      DatasetHandle* hPtr=(DatasetHandle*)openDB("leveldb_test");
      boost::filesystem::ofstream os("leveldb_test_read2.txt");
      for(sizeType i=0; i<10000; i++) {
        os << hPtr->_datumsTF._key << " " << hPtr->_datumsTF._epoch << std::endl;
        nextDB(hPtr);
      }
      closeDB(hPtr);
    }
  }
}

//prefetched python leveldb interface: tensorflow
extern "C"
{
#define BUFFER_SZ_PREFETCH_TF 1000
  struct DatasetHandlePrefetch {
    DatasetHandlePrefetch(DatasetHandle* handle)
      :_handle(handle),_done(false),_datumsTFPrefetch(handle->_datumsTF),_threadTFPrefetch(boost::bind(&DatasetHandlePrefetch::fetch,this)) {}
    //prefetched python api
    void fetch() {
      while(!_done) {
        boost::mutex::scoped_lock lock(_m);
        if(_queueTFPrefetch.size() <= BUFFER_SZ_PREFETCH_TF) {
          if(nextDB(_handle)) {
            _queueTFPrefetch.push(_handle->_datumsTF);
            _qEmpty.notify_one();
          }
        } else _qFull.wait(lock);
      }
    }
    DatasetHandle* _handle;
    //private
    boost::condition_variable _qFull,_qEmpty;
    boost::atomic<bool> _done;
    boost::mutex _m;
    //tensorflow data structure
    Datums _datumsTFPrefetch;
    std::queue<Datums> _queueTFPrefetch;
    boost::thread _threadTFPrefetch;
  };
  void closeDBPrefetch(void* handle)
  {
    DatasetHandlePrefetch* hPtr=(DatasetHandlePrefetch*)handle;
    if(hPtr->_handle->_dbTF) {
      hPtr->_done=true;
      while(hPtr->_threadTFPrefetch.joinable()) {
        hPtr->_qFull.notify_one();
        if(hPtr->_threadTFPrefetch.try_join_for(boost::chrono::milliseconds(1000)))
          break;
      }
      while(!hPtr->_queueTFPrefetch.empty())
        hPtr->_queueTFPrefetch.pop();
    }
    closeDB(hPtr->_handle);
    delete hPtr;
  }
  void* openDBPrefetch(const char* path)
  {
    DatasetHandle* handle=(DatasetHandle*)openDB(path);
    if(!handle)
      return NULL;
    return new DatasetHandlePrefetch(handle);
  }
  int getEpochPrefetch(void* handle)
  {
    DatasetHandlePrefetch* hPtr=(DatasetHandlePrefetch*)handle;
    return hPtr->_datumsTFPrefetch._epoch;
  }
  void getKeyPrefetch(void* handle,char* buf,int nrByte)
  {
    DatasetHandlePrefetch* hPtr=(DatasetHandlePrefetch*)handle;
    memset(buf,0,nrByte);
    strcpy(buf,hPtr->_datumsTFPrefetch._key.c_str());
  }
  bool nextDBPrefetch(void* handle)
  {
    DatasetHandlePrefetch* hPtr=(DatasetHandlePrefetch*)handle;
    while(!hPtr->_done) {
      boost::mutex::scoped_lock lock(hPtr->_m);
      if(!hPtr->_queueTFPrefetch.empty()) {
        hPtr->_datumsTFPrefetch=hPtr->_queueTFPrefetch.front();
        hPtr->_queueTFPrefetch.pop();
        hPtr->_qFull.notify_one();
        break;
      } else {
        hPtr->_qEmpty.wait(lock);
      }
    }
    return !hPtr->_done;
  }
  int nrDatumsPrefetch(void* handle)
  {
    DatasetHandlePrefetch* hPtr=(DatasetHandlePrefetch*)handle;
    return (int)hPtr->_datumsTFPrefetch._datums.size();
  }
  const float* getDatumPrefetch(void* handle,int id,int* shape)
  {
    DatasetHandlePrefetch* hPtr=(DatasetHandlePrefetch*)handle;
    ASSERT_MSGV(id >= 0 && id < (int)hPtr->_datumsTFPrefetch._datums.size(),"0<%d<%d",id,(int)hPtr->_datumsTFPrefetch._datums.size())
    ASSERT_MSG(shape,"Must provide shape pointer!")
    shape[0]=hPtr->_datumsTFPrefetch._datums[id]._header.nx;
    shape[1]=hPtr->_datumsTFPrefetch._datums[id]._header.ny;
    shape[2]=hPtr->_datumsTFPrefetch._datums[id]._header.nz;
    return &(hPtr->_datumsTFPrefetch._datums[id]._val[0]);
  }
  int nrImagesPrefetch(void* handle)
  {
    DatasetHandlePrefetch* hPtr=(DatasetHandlePrefetch*)handle;
    return (int)hPtr->_datumsTFPrefetch._images.size();
  }
  const char* getImagePrefetch(void* handle,int id,int* shape)
  {
    DatasetHandlePrefetch* hPtr=(DatasetHandlePrefetch*)handle;
    ASSERT_MSGV(id >= 0 && id < (int)hPtr->_datumsTFPrefetch._images.size(),"0<%d<%d",id,(int)hPtr->_datumsTFPrefetch._images.size())
    ASSERT_MSG(shape,"Must provide shape pointer!")
    shape[0]=hPtr->_datumsTFPrefetch._images[id].size();
    return &(hPtr->_datumsTFPrefetch._images[id][0]);
  }
  //debug
  void debugPythonPrefetchAPI()
  {
    debugPythonAPI();
    //read
    DatasetHandlePrefetch* hPtr=(DatasetHandlePrefetch*)openDBPrefetch("leveldb_test");
    boost::filesystem::ofstream os("leveldb_test_read_prefetch.txt");
    for(sizeType i=0; i<10000; i++) {
      os << hPtr->_datumsTFPrefetch._key << " " << hPtr->_datumsTFPrefetch._epoch << std::endl;
      nextDBPrefetch(hPtr);
    }
    closeDBPrefetch(hPtr);
  }
}

//visualize python interface: tensorflow
extern "C"
{
  boost::shared_ptr<COMMON::SkinnedMeshUtility> _meshTF;
  bool readSkinnedMeshUtility(const char* path)
  {
    if(!boost::filesystem::exists(path)) {
      WARNINGV("File does not exist: %s!",path)
      return false;
    }
    _meshTF.reset(new COMMON::SkinnedMeshUtility);
    return _meshTF->readSkinnedMesh(path);
  }
  void writeBodyVTK(const char* path,const char* key,const float* dat,const int* shape)
  {
    if(!_meshTF) {
      WARNING("SkinnedMeshUtility not initialized!")
      exit(EXIT_FAILURE);
    }
    set<sizeType> filter;
    COMMON::DatasetKey keyS(key);
    COMMON::SkinnedMeshClothUtility::FEATURE_TYPE ftype=COMMON::SkinnedMeshClothUtility::parseFeatureFromString(keyS._featStr,&filter);
    _meshTF->writeFeatureVTK(Eigen::Map<const COMMON::Colf>(dat,shape[0]).cast<scalarD>(),path,ftype,&filter);
  }
  void writeClothVTK(const char* path,const float* dat,const int* shape)
  {
    Datum datCaffe;
    //tensorflow's shape order is different from that of caffe
    datCaffe.set_channels(shape[0]);
    datCaffe.set_height(shape[1]);
    datCaffe.set_width(shape[2]);
    google::protobuf::RepeatedField<float>* ptr=datCaffe.mutable_float_data();
    for(int i=0,nr=shape[0]*shape[1]*shape[2]; i<nr; i++)
      ptr->Add(dat[i]);
    COMMON::SkinnedMeshClothUtility::drawClothVTK(datCaffe,path);
  }
}
