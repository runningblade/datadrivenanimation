#ifndef CLOTH_MESH_H
#define CLOTH_MESH_H

#include "Utils.h"
#include <CommonFile/ObjMesh.h>
#include <CommonFile/solvers/Objective.h>
#include <Eigen/Sparse>

PRJ_BEGIN

class ClothMesh : public Serializable
{
public:
  typedef Objective<scalarD>::Vec Vec;
  typedef Objective<scalarD>::STrip STrip;
  typedef Objective<scalarD>::STrips STrips;
  enum MASS_MODE {
    BARYCENTER=1,
    CIRCUMCENTER=2,
  };
  enum MESH_TYPE {
    CLOTH_MESH=0,
    RIGID_MESH=1,
  };
  struct ClothTriangle;
  struct ClothVertex : public Serializable {
    ClothVertex();
    ClothVertex(const Vec3d& pos,MESH_TYPE type);
    bool write(ostream& os,IOData* dat) const;
    bool read(istream& is,IOData* dat);
    boost::shared_ptr<Serializable> copy() const {
      return boost::shared_ptr<Serializable>(new ClothVertex);
    }
    //data
    Vec3d _pos,_lastPos,_vel,_pos0;
    scalarD _mass,_weight;
    sizeType _index;
    //for collision detect
    sizeType _type;
    vector<boost::shared_ptr<ClothTriangle> > _oneRing;
  };
  struct ClothEdge : public Serializable {
    ClothEdge();
    ClothEdge(boost::shared_ptr<ClothVertex> v0,boost::shared_ptr<ClothVertex> v1,MESH_TYPE type);
    bool write(ostream& os,IOData* dat) const;
    bool read(istream& is,IOData* dat);
    boost::shared_ptr<Serializable> copy() const {
      return boost::shared_ptr<Serializable>(new ClothEdge);
    }
    //data
    boost::shared_ptr<ClothVertex> _v[2];
    boost::shared_ptr<ClothTriangle> _t[2];
    Vec3d _pos,_vel,_pos0;
    scalarD _mass;
    sizeType _index;
    //for collision detection
    sizeType _type;
  };
  struct ClothTriangle : public Serializable {
    ClothTriangle();
    ClothTriangle(boost::shared_ptr<ClothEdge> e0,bool p0,boost::shared_ptr<ClothEdge> e1,bool p1,boost::shared_ptr<ClothEdge> e2,bool p2,MESH_TYPE type);
    boost::shared_ptr<ClothVertex> getV0() const;
    boost::shared_ptr<ClothVertex> getV1() const;
    boost::shared_ptr<ClothVertex> getV2() const;
    bool write(ostream& os,IOData* dat) const;
    bool read(istream& is,IOData* dat);
    boost::shared_ptr<Serializable> copy() const {
      return boost::shared_ptr<Serializable>(new ClothTriangle);
    }
    template <typename T>
    boost::shared_ptr<T> getElem(sizeType i) const;
    //data
    boost::shared_ptr<ClothEdge> _e[3];
    Vec3c _edgeDir;
    sizeType _index;
    //for collision detection
    sizeType _type;
    vector<bool>* _activeTag;
  };
public:
  ClothMesh();
  ClothMesh(const ObjMeshF& mesh,MESH_TYPE type);
  ClothMesh(const ObjMeshD& mesh,MESH_TYPE type);
  ClothMesh(const ClothMesh& other);
  ClothMesh& operator=(const ClothMesh& other);
  void reset(const ObjMeshF& mesh,MESH_TYPE type);
  void reset(const ObjMeshD& mesh,MESH_TYPE type);
  bool write(ostream& os,IOData* dat) const override;
  bool read(istream& is,IOData* dat) override;
  bool write(ostream& os) const override;
  bool read(istream& is) override;
  boost::shared_ptr<Serializable> copy() const override;
  void writeVTKC(const string& str,char defColor=-1,const vector<char>* color=NULL,bool last=false) const;
  void writeVTKN(const string& str) const;
  void assignMass(MASS_MODE mode);
  void saveLast();
  void findBoundary(vector<vector<boost::shared_ptr<ClothTriangle> > >* boundaryT,vector<vector<boost::shared_ptr<ClothVertex> > >* boundaryVL);
  void findGridIndex(vector<vector<boost::shared_ptr<ClothVertex> > >& indices,const vector<boost::shared_ptr<ClothVertex> >& boundaryVL);
  void assembleIndex();
  void assembleA();
  void assembleN(Vec* N=NULL,Vec* NV=NULL,Vec* M=NULL,bool pos0=false) const;
  void assembleC(Vec* C=NULL,Vec* CV=NULL,Vec* M=NULL,bool pos0=false) const;
  void assignN(const Vec* N=NULL,const Vec* NV=NULL);
  void assignC(const Vec* C=NULL,const Vec* CV=NULL);
  void convertN2C();
  void convertC2N();
  void convertC2Obj(ObjMeshD& mesh);
  static Vec3d perp(const Vec3d& in);
  void parityCheck();
  void parityCheckVss();
  //for rigid body
  Mat4d getTransform() const;
  void transform(const Mat3d& R,const Vec3d& T);
  //data
  vector<boost::shared_ptr<ClothVertex> > _vss;
  vector<boost::shared_ptr<ClothEdge> > _ess;
  vector<boost::shared_ptr<ClothTriangle> > _tss;
private:
  //fixed sparse matrix
  Eigen::SparseMatrix<scalarD,0,sizeType> _A,_AAT;
  Eigen::SimplicialCholesky<Eigen::SparseMatrix<scalarD,0,sizeType> > _AATSol;
};

PRJ_END

#endif
