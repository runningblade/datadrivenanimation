#ifndef CLOTH_ENERGIES_H
#define CLOTH_ENERGIES_H

#include "ClothMesh.h"
#include "ClothPhysics.h"
#include "LMInterface.h"

PRJ_BEGIN

//energy solver
enum STEP_MODE {
  NEWTON_FIRST_ORDER,
  NEWTON_SECOND_ORDER,
  QUASISTATIC,
};
class ArticulatedBody;
class ClothCollisionSolver;
class ArticulatedBodyEnergy;
class ClothEnergySolver : public ClothConstraintSolver
{
public:
  enum SOLVER_MODE {
    LM,
    LBFGS,
    HYBRID,
  };
  typedef Eigen::Matrix<scalarD,-1,1> Vec;
  typedef ClothEnergy::STrips STrips;
  ClothEnergySolver(boost::shared_ptr<ClothMesh> mesh,bool NMesh);
  void addEnergyKinetic();
  void addEnergyRegularize(scalarD coef);
  void addEnergyHookean(scalarD K,bool linear,bool SPD,bool QUAD=false,bool rest=false);
  void addEnergyStretch(scalarD mu,scalarD lambda);
  void addEnergyQuadraticBend(scalarD K);
  void addEnergyBend(scalarD K);
  void addEnergyARAP(scalarD K);
  void addEnergyMass(const Vec3d& G);
  void addEnergyFixClosestPoint(scalarD K,const Vec3d& pt,bool pull=false,bool pos0=false);
  void addEnergyFixClosestPointLine(scalarD K,const Vec3d& pt,Vec3d& dir,bool line,bool pos0=false);
  boost::shared_ptr<ArticulatedBodyEnergy> addEnergyArticulatedBody(ArticulatedBody& body,const set<sizeType>* collisionSet,sizeType nrSubd=0);
  template <typename T>
  void removeEnergy();
  void removeEnergy(boost::shared_ptr<ClothEnergy> e);
  void addEnergy(boost::shared_ptr<ClothEnergy> e);
  void writeVTK(const string& str) const;
  virtual bool solve(scalarD dt,STEP_MODE mode,bool useCB=false,SOLVER_MODE smode=HYBRID);
  virtual void solve(const string& path,scalarD t,scalarD dt,STEP_MODE mode,bool useCB=false,SOLVER_MODE smode=HYBRID);
  int operator()(const Vec& x,scalarD& FX,Vec& DFDX,scalarD step,bool wantGradient);
  void setIgnoreC(bool ignoreC);
  void setSearchD(bool searchD);
  //data: we only accept linear constraint in the default version using Fast-Projection Solver
  boost::shared_ptr<ClothConstraintSolver> _solC;
  boost::shared_ptr<ClothCollisionSolver> _solColl;
protected:
  virtual bool adjustX(scalarD dt);
  vector<boost::shared_ptr<ClothEnergy> > _energys;
  //temporary info
  Eigen::SparseMatrix<scalarD,0,sizeType> _LHS;
  bool _ignoreC,_searchD;
};
//build-in energy
template <typename T>
class ClothKineticEnergy : public ClothEnergy
{
public:
  ClothKineticEnergy(boost::shared_ptr<T> v);
  void initialize(scalarD dt,STEP_MODE mode);
  void finalize(scalarD dt,const Vec& XNew);
  virtual void getRelatedVertex(vector<sizeType>& vs) const;
  virtual scalarD eval() const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  Vec3d _b,_vel,_pos;
  scalarD _coef;
  boost::shared_ptr<Vec3d> _lastV,_lastX;
  boost::shared_ptr<T> _v;
  STEP_MODE _mode;
};
template <typename T>
class ClothFixedForceEnergy : public ClothEnergy
{
public:
  ClothFixedForceEnergy(boost::shared_ptr<T> v,const Vec3d& force);
  virtual void writeVTK(VTKWriter<scalarD>& os) const;
  virtual void getRelatedVertex(vector<sizeType>& vs) const;
  virtual scalarD eval() const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  boost::shared_ptr<T> _v;
  Vec3d _force;
};
template <typename T>
class ClothFixedNormalEnergy : public ClothEnergy
{
public:
  ClothFixedNormalEnergy(boost::shared_ptr<ClothMesh::ClothTriangle> t,const Vec3d& axis,scalarD stiff);
  void initialize(scalarD dt,STEP_MODE mode);
  virtual void writeVTK(VTKWriter<scalarD>& os) const;
  virtual void getRelatedVertex(vector<sizeType>& vs) const;
  virtual scalarD eval() const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  static void evalN(const Vec3d& x0,const Vec3d& x1,const Vec3d& x2,Vec3d& n,Eigen::Matrix<scalarD,3,9>* G=NULL);
  boost::shared_ptr<T> _v[3];
  Vec3d _axis,_n0;
  scalarD _stiff;
};
template <typename T>
class ClothHookeanEnergy : public ClothEnergy
{
public:
  ClothHookeanEnergy(boost::shared_ptr<T> v0,boost::shared_ptr<T> v1,scalarD rest,scalarD K,bool SPD,bool QUAD);
  virtual void writeVTK(VTKWriter<scalarD>& os) const;
  virtual bool linear() const;
  virtual void getRelatedVertex(vector<sizeType>& vs) const;
  virtual bool contributeToDamping() const;
  virtual scalarD eval() const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  virtual void prepare();
  scalarD strain() const;
  boost::shared_ptr<T> _v0;
  boost::shared_ptr<T> _v1;
  scalarD _rest,_K;
  bool _SPD,_QUAD;
  //prepare
  Vec3d _force;
  Mat3d _JA;
};
template <typename T>
class ClothLinearHookeanEnergy : public ClothHookeanEnergy<T>
{
public:
  using typename ClothHookeanEnergy<T>::Vec;
  using typename ClothHookeanEnergy<T>::STrip;
  using typename ClothHookeanEnergy<T>::STrips;
  using ClothHookeanEnergy<T>::_v0;
  using ClothHookeanEnergy<T>::_v1;
  using ClothHookeanEnergy<T>::_rest;
  using ClothHookeanEnergy<T>::_K;
  using ClothHookeanEnergy<T>::_needHESS;
  ClothLinearHookeanEnergy(boost::shared_ptr<T> v0,boost::shared_ptr<T> v1,scalarD rest,scalarD K);
  virtual bool linear() const;
  virtual scalarD eval() const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
};
template <typename T>
class ClothRestLinearHookeanEnergy : public ClothHookeanEnergy<T>
{
public:
  using typename ClothHookeanEnergy<T>::Vec;
  using typename ClothHookeanEnergy<T>::STrip;
  using typename ClothHookeanEnergy<T>::STrips;
  using ClothHookeanEnergy<T>::_v0;
  using ClothHookeanEnergy<T>::_v1;
  using ClothHookeanEnergy<T>::_rest;
  using ClothHookeanEnergy<T>::_K;
  using ClothHookeanEnergy<T>::_needHESS;
  ClothRestLinearHookeanEnergy(boost::shared_ptr<T> v0,boost::shared_ptr<T> v1,scalarD rest,scalarD K);
  virtual bool linear() const;
  virtual scalarD eval() const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  Vec3d _dist;
};
template <typename T>
class ClothConstraintEnergy : public ClothEnergy
{
public:
  ClothConstraintEnergy(boost::shared_ptr<T> v,const Vec3d& pos,scalarD K);
  ClothConstraintEnergy(boost::shared_ptr<T> v,const Vec3d& pos,const Vec3d& dir,scalarD K,bool line);
  virtual bool linear() const;
  virtual void getRelatedVertex(vector<sizeType>& vs) const;
  virtual scalarD eval() const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  boost::shared_ptr<T> _v;
  Vec3d _pos;
  Mat3d _H;
};
class ClothARAPEnergy : public ClothEnergy
{
public:
  ClothARAPEnergy(boost::shared_ptr<ClothMesh::ClothVertex> vertex,set<boost::shared_ptr<ClothMesh::ClothEdge> > oneRing,scalarD K);
  virtual bool linear() const {
    return true;
  }
  virtual void getRelatedVertex(vector<sizeType>& vs) const;
  virtual bool contributeToDamping() const {
    return false;
  }
  virtual scalarD eval() const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  Mat3d rotation() const;
  boost::shared_ptr<ClothMesh::ClothVertex> _vertex;
  vector<boost::shared_ptr<ClothMesh::ClothVertex> > _oneRing;
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > _v0;
  vector<scalarD> _weight;
};
class ClothStretchEnergy : public ClothEnergy
{
public:
  ClothStretchEnergy(boost::shared_ptr<ClothMesh::ClothTriangle> t,scalarD mu,scalarD lambda);
  virtual bool linear() const {
    return false;
  }
  virtual void getRelatedVertex(vector<sizeType>& vs) const;
  virtual bool contributeToDamping() const {
    return false;
  }
  scalarD evalStretchEnergy(const Vec3d& v0,const Vec3d& v1,const Vec3d& v2,const Mat3d& m0,const Mat3d& m1,const Mat3d& m2,const Mat3d& m) const;
  void evalStretchForce(const Vec3d& v0,const Vec3d& v1,const Vec3d& v2,const Mat3d& m0,const Mat3d& m1,const Mat3d& m2,const Mat3d& m,Vec3d& f0,Vec3d& f1,Vec3d& f2,Matd* HESS) const;
  virtual scalarD eval() const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  boost::shared_ptr<ClothMesh::ClothVertex> _v[3];
  scalarD _mu,_lambda;
  scalarD _A;
  Mat3d _m0,_m1,_m2,_m;
};
class ClothQuadraticBendEnergy : public ClothEnergy
{
public:
  ClothQuadraticBendEnergy(boost::shared_ptr<ClothMesh::ClothEdge> e,scalarD K,bool NMesh);
  void writeVTK(VTKWriter<scalarD>& os) const;
  static
  void findOtherEdge(boost::shared_ptr<ClothMesh::ClothTriangle> t,
                     boost::shared_ptr<ClothMesh::ClothVertex> v,
                     boost::shared_ptr<ClothMesh::ClothEdge> e,
                     boost::shared_ptr<ClothMesh::ClothEdge>& eOut,
                     boost::shared_ptr<ClothMesh::ClothVertex>& vOut);
  scalarD coef(boost::shared_ptr<ClothMesh::ClothVertex> v0,
               boost::shared_ptr<ClothMesh::ClothVertex> v1,
               boost::shared_ptr<ClothMesh::ClothVertex> vb) const;
  virtual void getRelatedVertex(vector<sizeType>& vs) const;
  virtual scalarD eval() const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  boost::shared_ptr<ClothMesh::ClothEdge> _e[5];
  boost::shared_ptr<ClothMesh::ClothVertex> _v[4];
  Eigen::Matrix<scalarD,4,4> _QV;
  Eigen::Matrix<scalarD,5,5> _QE;
  bool _NMesh;
};
class ClothBendEnergy : public ClothQuadraticBendEnergy
{
public:
  ClothBendEnergy(boost::shared_ptr<ClothMesh::ClothEdge> e,scalarD K);
  scalarD evalBendEnergy(const Vec3d& v0,const Vec3d& v1,const Vec3d& v2,const Vec3d& v3) const;
  void evalBendForce(const Vec3d& v0,const Vec3d& v1,const Vec3d& v2,const Vec3d& v3,Vec3d& f0,Vec3d& f1,Vec3d& f2,Vec3d& f3,scalarD& theta) const;
  virtual scalarD eval() const;
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  virtual void prepare();
  scalarD _A;
  Vec3d _f[4];
  scalarD _theta;
};

PRJ_END

#endif
