#include "ClothArticulatedEnergies.h"
#include <CommonFile/geom/BVHBuilder.h>
#include <CommonFile/geom/StaticGeomCell.h>
#include <CommonFile/Timing.h>

USE_PRJ_NAMESPACE

void debugGradient(ObjMeshGeomCell& cell)
{
#define NR_TEST 100
#define DELTA 1E-12f
  for(sizeType i=0; i<NR_TEST; i++) {
    Mat3 hess;
    Vec3 n,n2,grad,delta,p=Vec3::Random();
    cell.closestHess(p,n,NULL,&hess);
    scalar E=n.dot(hess*n)/2;
    grad=-hess*n;

    delta=Vec3::Random();
    cell.closestHess(p+delta*DELTA,n2,NULL,NULL);
    scalar E2=n2.dot(hess*n2)/2;

    INFOV("Gradient: %f Err: %f",grad.dot(delta),(grad.dot(delta)-(E2-E)/DELTA))
  }
#undef DELTA
#undef NR_TEST
}
//callback
struct CallbackWriteVTK {
  typedef ArticulatedBodyEnergy::BBOX BBOX;
  CallbackWriteVTK(boost::shared_ptr<ClothMesh> mesh,const string& path)
    :_mesh(mesh),_os("coll",path,true) {}
  virtual ~CallbackWriteVTK() {
    _os.appendPoints(_vss.begin(),_vss.end());
    _os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,2,0),
                    VTKWriter<scalar>::IteratorIndex<Vec3i>((sizeType)_vss.size()/2,2,0),
                    VTKWriter<scalar>::LINE);
  }
  void onCell(const Node<sizeType,BBOX>& A,const Node<BodyRef,BBOX>& B) {
    Vec3 n,p=_mesh->_vss[A._cell]->_pos.cast<scalar>();
    if(B._cell._mesh->dist(p,n)) {
      _vss.push_back(p);
      _vss.push_back(p+n);
    }
  }
  vector<Vec3,Eigen::aligned_allocator<Vec3> > _vss;
  boost::shared_ptr<ClothMesh> _mesh;
  VTKWriter<scalar> _os;
};
struct CallbackCache {
  typedef ArticulatedBodyEnergy::BBOX BBOX;
  typedef ArticulatedBodyEnergy::Cache Cache;
  CallbackCache(vector<Cache>& cache)
    :_cache(cache) {
    _cache.clear();
  }
  void onCell(const Node<sizeType,BBOX>& A,const Node<BodyRef,BBOX>& B) {
    Cache c;
    c._vid=A._cell;
    c._mesh=B._cell._mesh;
    _cache.push_back(c);
  }
  vector<Cache>& _cache;
};
//BodyRef
BodyRef::BodyRef():Serializable(typeid(BodyRef).name()) {}
bool BodyRef::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_mesh,os,dat);
  writeBinaryData(_bid,os,dat);
  return os.good();
}
bool BodyRef::read(istream& is,IOData* dat)
{
  readBinaryData(_mesh,is,dat);
  readBinaryData(_bid,is,dat);
  return is.good();
}
boost::shared_ptr<Serializable> BodyRef::copy() const
{
  return boost::shared_ptr<Serializable>(new BodyRef);
}
bool BodyRef::operator<(const BodyRef& other) const
{
  return _mesh<other._mesh || (_mesh==other._mesh && _bid<other._bid);
}
bool BodyRef::operator!=(const BodyRef& other) const
{
  return !operator==(other);
}
bool BodyRef::operator==(const BodyRef& other) const
{
  return _mesh==other._mesh && _bid==other._bid;
}
//ArticulatedBodyEnergy
ArticulatedBodyEnergy::ArticulatedBodyEnergy(const ArticulatedBody& body):_body(body) {}
ArticulatedBodyEnergy::ArticulatedBodyEnergy(boost::shared_ptr<ClothMesh> mesh,const ArticulatedBody& body,const set<sizeType>* collisionSet)
  :_mesh(mesh),_body(body),_K(1E4f)
{
  buildBVHV();
  buildBVHB(collisionSet);
}
void ArticulatedBodyEnergy::setConfiguration(const BodyOpT<const Body>& op)
{
  Mat4 T;
  for(sizeType b=0; b<(sizeType)(_bvhb.size()+1)/2; b++) {
    BodyRef& ref=_bvhb[b]._cell;
    T=op.getTrans(ref._bid);
    _bvhb[b]._bb.reset();
    ref._mesh->setT(op.getTrans(ref._bid));
    dynamic_cast<ObjMeshGeomCell&>(*(ref._mesh)).depth()=ScalarUtil<scalar>::scalar_max;
    //debugGradient(dynamic_cast<ObjMeshGeomCell&>(*(ref._mesh)));
    for(sizeType v=0; v<(sizeType)ref._mesh->vss().size(); v++)
      _bvhb[b]._bb.setUnion(transformHomo<scalar>(T,ref._mesh->vss()[v]));
  }
  BodyRef ref;
  ref._bid=-1;
  ref._mesh=NULL;
  BVHQuery<BodyRef,BBOX>(_bvhb,3,ref).updateBVH();
}
bool ArticulatedBodyEnergy::linear() const
{
  return false;
}
void ArticulatedBodyEnergy::getRelatedVertex(vector<sizeType>& vs) const
{
  vs.clear();
  for(sizeType v=0; v<(sizeType)_mesh->_vss.size(); v++)
    vs.push_back(v);
}
scalarD ArticulatedBodyEnergy::eval() const
{
  scalarD E=0;
  OMP_PARALLEL_FOR_I(OMP_ADD(E))
  for(sizeType i=0; i<(sizeType)_cache.size(); i++)
    if(_cache[i]._vid >= 0)
      E+=_cache[i]._n.squaredNorm();
  return -E*_K/2;
}
void ArticulatedBodyEnergy::eval(ParallelMatrix<Vec>& c,STrips& grad,sizeType off)
{
  BodyRef ref;
  ref._bid=-1;
  ref._mesh=NULL;
  updateBVHV();
  BVHQuery<sizeType,BBOX> qv(_bvhv,3,-1);
  BVHQuery<BodyRef,BBOX> qb(_bvhb,3,ref);
  //broadphase detection
  CallbackCache cbE(_cache);
  qv.interBodyQuery(qb,cbE);
  //narrowphase detection
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)_cache.size(); i++) {
    Cache& C=_cache[i];
    C._p=_mesh->_vss[C._vid]->_pos.cast<scalar>();
    if(C._mesh->closestHess(C._p,C._n,&C._normal,&C._hess)) {
      C._normal.normalize();
      if(!isFinite(C._normal))
        C._vid=-1;
    } else C._vid=-1;
    if(C._vid >= 0) {
      c.getMatrixI().segment<3>(C._vid*3)+=C._hess.cast<scalarD>()*C._n.cast<scalarD>()*_K;
      if(_needHESS)
        addBlock(grad,C._vid*3,C._vid*3,-C._hess*_K);
    }
  }
  disableTiming();
//#define DEBUG_WRITE_COLL
#ifdef DEBUG_WRITE_COLL
  {
    CallbackWriteVTK cbW(_mesh,"coll.vtk");
    qv.interBodyQuery(qb,cbW);
  }
  exit(EXIT_SUCCESS);
#endif
}
void ArticulatedBodyEnergy::eval(Vec& c,STrips& grad,sizeType off) const
{
  FUNCTION_NOT_IMPLEMENTED
}
scalarD ArticulatedBodyEnergy::penetrationDepth()
{
  Vec C;
  STrips hess;
  _needHESS=false;
  _mesh->assembleC(&C);
  ParallelMatrix<Vec> grad(C);
  eval(grad,hess,0);

  scalar maxDepth=0;
  for(sizeType i=0; i<(sizeType)_cache.size(); i++)
    if(_cache[i]._vid >= 0)
      maxDepth=max(maxDepth,_cache[i]._n.norm());
  return maxDepth;
}
void ArticulatedBodyEnergy::setK(scalarD K)
{
  _K=K;
}
void ArticulatedBodyEnergy::updateBVHV()
{
  sizeType nrV=(sizeType)_mesh->_vss.size();
  OMP_PARALLEL_FOR_
  for(sizeType v=0; v<nrV; v++) {
    _bvhv[v]._bb.reset();
    _bvhv[v]._bb.setUnion(_mesh->_vss[v]->_pos.cast<scalar>());
  }
  BVHQuery<sizeType,BBOX>(_bvhv,3,-1).updateBVH();
}
void ArticulatedBodyEnergy::buildBVHV()
{
  _bvhv.resize(_mesh->_vss.size());
  for(sizeType v=0; v<(sizeType)_mesh->_vss.size(); v++) {
    _bvhv[v]._cell=v;
    _bvhv[v]._nrCell=1;
    _bvhv[v]._bb.setUnion(_mesh->_vss[v]->_pos.cast<scalar>());
  }
  BVHBuilder<Node<sizeType,BBOX>,3> builder;
  builder.buildBVH(_bvhv);
  for(sizeType v=(sizeType)_mesh->_vss.size(); v<(sizeType)_bvhv.size(); v++)
    _bvhv[v]._cell=-1;
}
void ArticulatedBodyEnergy::buildBVHB(const set<sizeType>* collisionSet)
{
  Mat4 T;
  BodyRef ref;
  _bvhb.clear();
  BodyOpT<const Body> op(_body.nrBody());
  _body.transform(_body.root(),op,NULL);
  for(sizeType b=0; b<_body.nrBody(); b++)
    if(_body.findBody(b).hasMesh()) {
      ref._bid=_body.findBody(b).id();
      ref._mesh=_body.findBody(b).meshRef();
      T=op.getTrans(ref._bid);
      if(collisionSet && collisionSet->find(ref._bid) == collisionSet->end())
        continue;
      _bvhb.push_back(Node<BodyRef,BBOX>());
      _bvhb.back()._cell=ref;
      _bvhb.back()._nrCell=1;
      for(sizeType v=0; v<(sizeType)ref._mesh->vss().size(); v++)
        _bvhb.back()._bb.setUnion(transformHomo<scalar>(T,ref._mesh->vss()[v]));
    }
  ref._bid=-1;
  ref._mesh=NULL;
  sizeType nrB=_bvhb.size();
  BVHBuilder<Node<BodyRef,BBOX>,3> builder;
  builder.buildBVH(_bvhb);
  for(sizeType b=nrB; b<(sizeType)_bvhb.size(); b++)
    _bvhb[b]._cell=ref;
}
//ArticulatedBodyEnergySubd
ArticulatedBodyEnergySubd::ArticulatedBodyEnergySubd(boost::shared_ptr<ClothMesh> mesh,const ArticulatedBody& body,const set<sizeType>* collisionSet,sizeType nrSubd)
  :ArticulatedBodyEnergy(body)
{
  _mesh=mesh;
  _K=1E4f;
  buildBVHVSubd(nrSubd);
  buildBVHB(collisionSet);
}
void ArticulatedBodyEnergySubd::eval(ParallelMatrix<Vec>& c,STrips& grad,sizeType off)
{
  //enableTiming();
  //TBEG("Broadphase");
  BodyRef ref;
  ref._bid=-1;
  ref._mesh=NULL;
  updateBVHV();
  BVHQuery<sizeType,BBOX> qv(_bvhv,3,-1);
  BVHQuery<BodyRef,BBOX> qb(_bvhb,3,ref);
  //broadphase detection
  CallbackCache cbE(_cache);
  qv.interBodyQuery(qb,cbE);
  //TEND();
  //TBEG("Narrowphase");
  //narrowphase detection
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)_cache.size(); i++) {
    Cache& C=_cache[i];
    C._p=_IC.segment<3>(C._vid*3).cast<scalar>();
    if(C._mesh->closestHess(C._p,C._n,&C._normal,&C._hess)) {
      C._normal.normalize();
      if(!isFinite(C._normal))
        C._vid=-1;
    } else C._vid=-1;
    if(C._vid >= 0) {
      for(ConstSMIterator<scalarD> begR=_interp.begin(C._vid),endR=_interp.end(C._vid); begR!=endR; ++begR) {
        c.getMatrixI().segment<3>(begR.col()*3)+=C._hess.cast<scalarD>()*C._n.cast<scalarD>()*(_K**begR);
        if(_needHESS)
          for(ConstSMIterator<scalarD> begC=_interp.begin(C._vid),endC=_interp.end(C._vid); begC!=endC; ++begC)
            addBlock(grad,begR.col()*3,begC.col()*3,-C._hess*(_K**begR**begC));
      }
    }
  }
  //TEND();
  //disableTiming();
}
void ArticulatedBodyEnergySubd::updateBVHV()
{
  _mesh->assembleC(&_C);
  ASSERT(_C.size() == _interp.cols()*3)
  _IC.resize(_interp.rows()*3);
  _interp.multiplyB<3,Vec,Vec>(_C,_IC);
  sizeType nrV=_IC.size()/3;
  OMP_PARALLEL_FOR_
  for(sizeType v=0; v<nrV; v++) {
    _bvhv[v]._bb.reset();
    _bvhv[v]._bb.setUnion(_IC.segment<3>(v*3).cast<scalar>());
  }
  BVHQuery<sizeType,BBOX>(_bvhv,3,-1).updateBVH();
}
void ArticulatedBodyEnergySubd::buildBVHVSubd(sizeType nrSubd)
{
  //build base mesh
  ObjMesh mesh;
  sizeType nrV=(sizeType)_mesh->_vss.size();
  for(sizeType i=0; i<nrV; i++)
    mesh.getV().push_back(_mesh->_vss[i]->_pos.cast<scalar>());
  for(sizeType i=0; i<(sizeType)_mesh->_tss.size(); i++) {
    const ClothMesh::ClothTriangle& t=*(_mesh->_tss[i]);
    mesh.getI().push_back(Vec3i(t.getV0()->_index,t.getV1()->_index,t.getV2()->_index));
  }
  mesh.smooth();
  //build subdivison stencil
  Objective<scalarD>::SMat m;
  m.resize(nrV,nrV);
  m.setIdentity();
  _interp.fromEigen(m);
  for(sizeType i=0; i<nrSubd; i++) {
    nrV=(sizeType)mesh.getV().size();
    vector<std::pair<int,int> > vssInfo;
    mesh.subdivide(1,&vssInfo);
    //build subdivide matrix
    STrips trips;
    FixedSparseMatrix<scalarD,Kernel<scalarD> > transfer,tmp;
    transfer.resize((sizeType)mesh.getV().size(),nrV);
    for(sizeType v=0; v<(sizeType)vssInfo.size(); v++)
      if(vssInfo[v].first == -1)
        trips.push_back(STrip(v,v,1));
      else {
        trips.push_back(STrip(v,vssInfo[v].first,0.5f));
        trips.push_back(STrip(v,vssInfo[v].second,0.5f));
      }
    transfer.buildFromTriplets(trips.begin(),trips.end());
    //multiply
    transfer.multiplyMatrix(_interp,tmp);
    _interp=tmp;
  }
  //assemble
  _mesh->assembleC(&_C);
  ASSERT(_C.size() == _interp.cols()*3)
  _IC.resize(_interp.rows()*3);
  _interp.multiplyB<3,Vec,Vec>(_C,_IC);
  _bvhv.resize(_interp.rows());
  for(sizeType v=0; v<_interp.rows(); v++) {
    _bvhv[v]._cell=v;
    _bvhv[v]._nrCell=1;
    _bvhv[v]._bb.setUnion(_IC.segment<3>(v*3).cast<scalar>());
  }
  BVHBuilder<Node<sizeType,BBOX>,3> builder;
  builder.buildBVH(_bvhv);
  for(sizeType v=_interp.rows(); v<(sizeType)_bvhv.size(); v++)
    _bvhv[v]._cell=-1;
}
