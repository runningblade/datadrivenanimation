#include "SkinnedMesh.h"
#include "LMInterface.h"
#include <CommonFile/CollisionDetection.h>
#include <CommonFile/RotationUtil.h>
#include <stack>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

USE_PRJ_NAMESPACE

//helper
string readString(istream& is)
{
  string str;
  getline(is,str);
  return str;
}
template <typename T>
T readMatrix(istream& is)
{
  string str;
  getline(is,str);
  istringstream iss(str);

  T ret;
  int R,C;
  iss >> R >> C;
  ret.resize(R,C);
  for(int r=0; r<R; r++)
    for(int c=0; c<C; c++)
      iss >> ret(r,c);
  return ret;
}
template <typename T>
vector<T,Eigen::aligned_allocator<T> > readMatrices(istream& is)
{
  string str;
  getline(is,str);
  istringstream iss(str);

  int nr;
  iss >> nr;
  vector<T,Eigen::aligned_allocator<T> > ret(nr);
  for(int i=0; i<nr; i++)
    ret[i]=readMatrix<T>(is);
  return ret;
}
class ICPObjective : public Objective<scalarD>
{
public:
  ICPObjective(const BodyOpT<const Body>& opOther,const Coli& jointMap,
               SkinnedMeshUsingArticulatedBody& skinnedMesh,scalarD reg=1E-6f)
    :_skinnedMesh(skinnedMesh) {
    _jointMap=jointMap;
    _reg=reg;
    //assign joint poses
    _jointsOther.resize(_jointMap.size());
    for(sizeType i=0; i<_jointMap.size(); i++)
      if(_jointMap[i] >= 0)
        _jointsOther[i]=opOther.getTransD(_jointMap[i]).block<3,1>(0,3);
  }
  virtual int operator()(const Vec& x,Vec& fvec,DMat* fjac,bool modifiable) override {
    //joints
    MatX3d joints;
    Matd djoints[3];
    _skinnedMesh.getMesh(x,&joints,fjac ? djoints : NULL,NULL,NULL,NULL,true);
    //assemble
    fvec.setZero(values());
    if(fjac)
      fjac->setZero(values(),inputs());
    for(sizeType i=0; i<_jointMap.size(); i++)
      if(_jointMap[i] >= 0) {
        fvec.segment<3>(i*3)=joints.row(i).transpose()-_jointsOther[i];
        if(fjac)
          for(unsigned char d=0; d<3; d++)
            fjac->row(i*3+d)=djoints[d].row(i);
      }
    return 0;
  }
  virtual scalarD operator()(const Vec& x,Vec* fgrad,DMat* fhess) override {
    if(fgrad) {
      fgrad->setZero(inputs());
      fgrad->segment(0,inputs()-4)=x.segment(0,inputs()-4)*_reg;
    }
    if(fhess) {
      fhess->setZero(inputs(),inputs());
      fhess->diagonal().segment(0,inputs()-4).setConstant(_reg);
    }
    return x.segment(0,inputs()-4).squaredNorm()*_reg/2;
  }
  void makeSymmetry(const ArticulatedBody& body,sizeType axis) {
    _symJointMap.setConstant(_jointMap.size(),-1);
    vector<Vec3d,Eigen::aligned_allocator<Vec3d> > tmpJointsOther=_jointsOther;
    for(sizeType i=0,isym; i<_jointMap.size(); i++)
      if(_jointMap[i] >= 0) {
        //type of joint
        string name=body.findBody(_jointMap[i]).name();
        string nameSym="";
        if(name[0] == 'l') {
          nameSym=name;
          nameSym[0]='r';
        } else if(name[0] == 'r') {
          nameSym=name;
          nameSym[0]='l';
        } else {
          _jointsOther[i][axis]=0;
        }
        //average left/right
        if(!nameSym.empty()) {
          isym=-1;
          for(sizeType j=0; j<_jointMap.size(); j++)
            if(_jointMap[j] >= 0 && body.findBody(_jointMap[j]).name() == nameSym)
              isym=j;
          if(isym >= 0) {
            Vec3d pos=tmpJointsOther[i];
            Vec3d posSym=tmpJointsOther[isym];
            for(sizeType d=0; d<3; d++)
              if(d != axis)
                pos[d]=(pos[d]+posSym[d])/2;
              else pos[d]=(pos[d]-posSym[d])/2;
            _jointsOther[i]=pos;
            _symJointMap[i]=isym;
          } else {
            _jointsOther[i][axis]=0;
          }
        }
      }
  }
  int inputs() const override {
    return _skinnedMesh.nrTheta();
  }
  int values() const override {
    return _skinnedMesh.nrJoint()*3;
  }
  //data
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > _jointsOther;
  SkinnedMeshUsingArticulatedBody& _skinnedMesh;
  Coli _jointMap,_symJointMap;
  scalarD _reg;
};
//SkinnedMesh
SkinnedMesh::SkinnedMesh():Serializable(typeid(SkinnedMesh).name()) {}
bool SkinnedMesh::read(istream& is,IOData* dat)
{
  readBinaryData(_bsStyle,is);
  readBinaryData(_bsType,is);
  readBinaryData(_kintree,is);
  readBinaryData(_vTemplate,is);
  readBinaryData(_f,is);
  readBinaryData(_weights,is);
  readBinaryData(_J,is);
  readBinaryData(_JRegressor,is);
  readVector(_shapeDirs,is);
  readVector(_poseDirs,is);
  //beta
  readBinaryData(_beta,is);
  return is.good();
}
bool SkinnedMesh::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_bsStyle,os);
  writeBinaryData(_bsType,os);
  writeBinaryData(_kintree,os);
  writeBinaryData(_vTemplate,os);
  writeBinaryData(_f,os);
  writeBinaryData(_weights,os);
  writeBinaryData(_J,os);
  writeBinaryData(_JRegressor,os);
  writeVector(_shapeDirs,os);
  writeVector(_poseDirs,os);
  //beta
  writeBinaryData(_beta,os);
  return os.good();
}
void SkinnedMesh::getMesh
(const Cold& theta,MatX3d* joints,
 Matd djoints[3],MatX3d* meshv,
 vector<MatX3d,Eigen::aligned_allocator<MatX3d> >* dmeshv,
 ObjMesh* mesh,bool adjustPose)
{
  //initialize
  Vec3d v;
  MatX3d J;
  Mat4d T,DT;
  Mat3d rot,drot[3];
  MatX3d vTemplate=_vTemplate;
  vector<Mat4d,Eigen::aligned_allocator<Mat4d> > tss;
  vector<vector<Mat4d,Eigen::aligned_allocator<Mat4d> > > dtss;
  ASSERT(theta.size() == nrTheta())
  //compute shape
  for(sizeType i=0; i<(sizeType)_shapeDirs.size(); i++)
    vTemplate+=_shapeDirs[i]*_beta[i];
  //J_regressor
  J=_JRegressor*vTemplate;
  //compute pose
  getJoint(theta,J,tss,dmeshv ? &dtss : NULL,joints,djoints);
  //compute mesh
  if(dmeshv)
    dmeshv->assign(nrTheta(),Matd::Zero(vTemplate.rows(),vTemplate.cols()));
  if(mesh || meshv) {
    for(sizeType i=0; i<_kintree.cols(); i++) {
      tss[i].block<3,1>(0,3)-=tss[i].block<3,3>(0,0)*J.row(i).transpose();
      if(dmeshv)
        for(sizeType t=0; t<nrTheta(); t++)
          dtss[i][t].block<3,1>(0,3)-=dtss[i][t].block<3,3>(0,0)*J.row(i).transpose();
    }
    //pose
    if(adjustPose)
      for(sizeType i=1,k=0; i<_kintree.cols(); i++) {
        rot=expWGradV<scalarD>(theta.segment<3>(i*3),drot,NULL,NULL,NULL)-Mat3d::Identity();
        for(sizeType r=0; r<3; r++)
          for(sizeType c=0; c<3; c++,k++) {
            vTemplate+=rot(r,c)*_poseDirs[k];
            if(dmeshv)
              for(sizeType d=0; d<3; d++)
                dmeshv->at(i*3+d)+=drot[d](r,c)*_poseDirs[k];
          }
      }
    //weighting
    OMP_PARALLEL_FOR_I(OMP_PRI(T,DT,v))
    for(sizeType i=0; i<vTemplate.rows(); i++) {
      T.setZero();
      for(sizeType c=0; c<_weights.cols(); c++)
        T+=tss[c]*_weights(i,c);
      if(dmeshv)
        for(sizeType t=0; t<nrTheta(); t++) {
          if(adjustPose) {
            v=dmeshv->at(t).row(i);
            v=T.block<3,3>(0,0)*v;
            dmeshv->at(t).row(i)=v.transpose();
          }
          DT.setZero();
          for(sizeType c=0; c<_weights.cols(); c++)
            DT+=dtss[c][t]*_weights(i,c);
          v=vTemplate.row(i);
          v=DT.block<3,3>(0,0)*v+DT.block<3,1>(0,3);
          dmeshv->at(t).row(i)+=v;
        }
      v=vTemplate.row(i);
      v=T.block<3,3>(0,0)*v+T.block<3,1>(0,3);
      vTemplate.row(i)=v;
    }
    //build meshv
    if(meshv)
      *meshv=vTemplate;
    //build mesh
    if(mesh) {
      mesh->getV().resize(_vTemplate.rows());
      for(sizeType i=0; i<_vTemplate.rows(); i++)
        mesh->getV()[i]=vTemplate.row(i).cast<scalar>();
      mesh->getI().resize(_f.rows());
      for(sizeType i=0; i<_f.rows(); i++)
        mesh->getI()[i]=_f.row(i);
      //mesh->insideOut();
      mesh->smooth();
    }
  }
}
void SkinnedMesh::writeJointVTK(const MatX3d& J,const string& path) const
{
  VTKWriter<scalar> os("joint",path,true);
  vector<scalar> css;
  vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
  vector<Vec3i,Eigen::aligned_allocator<Vec3i> > iss;
  for(sizeType i=0; i<J.rows(); i++) {
    vss.push_back(J.row(i).cast<scalar>());
    css.push_back(i);
  }
  for(sizeType i=1,parent; i<J.rows(); i++) {
    parent=_kintree(0,i);
    iss.push_back(Vec3i(i,parent,0));
    css.push_back(i);
  }
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,0,0),
                 VTKWriter<scalar>::IteratorIndex<Vec3i>(J.rows(),0,0),
                 VTKWriter<scalar>::POINT);
  os.appendCells(iss.begin(),iss.end(),VTKWriter<scalar>::LINE);
  os.appendCustomData("color",css.begin(),css.end());
}
void SkinnedMesh::readPkl(const string& path)
{
  boost::filesystem::ifstream is(path);
  ASSERT(readString(is)=="bs_style")
  _bsStyle=readString(is);
  ASSERT(readString(is)=="bs_type")
  _bsType=readString(is);
  ASSERT(readString(is)=="kintree_table")
  _kintree=readMatrix<Mat2Xi>(is);
  ASSERT(readString(is)=="v_template")
  _vTemplate=readMatrix<MatX3d>(is);
  ASSERT(readString(is)=="f")
  _f=readMatrix<MatX3i>(is);
  ASSERT(readString(is)=="weights")
  _weights=readMatrix<Matd>(is);
  ASSERT(readString(is)=="J")
  _J=readMatrix<Matd>(is);
  ASSERT(readString(is)=="J_regressor")
  _JRegressor=readMatrix<Matd>(is);
  ASSERT(readString(is)=="shapedirs")
  _shapeDirs=readMatrices<MatX3d>(is);
  ASSERT(readString(is)=="posedirs")
  _poseDirs=readMatrices<MatX3d>(is);
  //beta
  _beta.setZero(nrBeta());
}
void SkinnedMesh::readBeta(const string& path)
{
  sizeType i=0;
  boost::property_tree::ptree root;
  boost::property_tree::read_json(path,root);
  BOOST_FOREACH(boost::property_tree::ptree::value_type& v, root.get_child("betas")) {
    assert(v.first.empty());  //array elements have no names
    scalarD beta=boost::lexical_cast<scalarD>(v.second.data());
    INFOV("Beta %ld: %f",i,beta)
    _beta[i]=beta;
    i++;
  }
}
sizeType SkinnedMesh::nrTheta() const
{
  return _kintree.cols()*3+3;
}
sizeType SkinnedMesh::nrBeta() const
{
  return (sizeType)_shapeDirs.size();
}
sizeType SkinnedMesh::nrJoint() const
{
  return _J.rows();
}
const Mat2Xi& SkinnedMesh::kintree() const
{
  return _kintree;
}
//debug
void SkinnedMesh::debugJoint(const string& path,scalar delta,sizeType nr)
{
  ObjMesh obj;
  ObjMesh obj_NoAdjust;
  MatX3d joints;
  string pathJ;
  recreate(path);
  for(sizeType i=0; i<_kintree.cols()*3; i+=3) {
    pathJ=path+"/joint"+boost::lexical_cast<string>(i/3);
    recreate(pathJ);
    Cold theta=Cold::Zero(nrTheta());
    for(sizeType j=0; j<nr; j++) {
      theta.segment<3>(i)=Vec3d::Random()*delta;
      getMesh(theta,&joints,NULL,NULL,NULL,&obj,true);
      getMesh(theta,NULL,NULL,NULL,NULL,&obj_NoAdjust,false);
      writeJointVTK(joints,pathJ+"/jointFrm"+boost::lexical_cast<string>(j)+".vtk");
      obj.writeVTK(pathJ+"/frm"+boost::lexical_cast<string>(j)+".vtk",true);
      obj_NoAdjust.writeVTK(pathJ+"/frm_NoAdjust"+boost::lexical_cast<string>(j)+".vtk",true);
    }
  }
}
void SkinnedMesh::debugDJoint(bool adjustPose)
{
#define NR_TEST 100
#define DELTA 1E-7f
  vector<MatX3d,Eigen::aligned_allocator<MatX3d> > dmeshv;
  MatX3d joints,joints2,meshv,meshv2,dmeshvDelta;
  Cold theta,delta;
  Matd djoints[3];
  for(sizeType i=0; i<NR_TEST; i++) {
    theta=Cold::Random(nrTheta());
    delta=Cold::Random(nrTheta());
    getMesh(theta,&joints,djoints,NULL,NULL,NULL,adjustPose);
    getMesh(theta+delta*DELTA,&joints2,NULL,NULL,NULL,NULL,adjustPose);
    for(unsigned char d=0; d<3; d++) {
      INFOV("GradientJ: %f Err: %f",(djoints[d]*delta).norm(),(djoints[d]*delta-(joints2.col(d)-joints.col(d))/DELTA).norm())
    }
  }
  for(sizeType i=0; i<NR_TEST; i++) {
    theta=Cold::Random(nrTheta());
    delta=Cold::Random(nrTheta());
    getMesh(theta,NULL,NULL,&meshv,&dmeshv,NULL,adjustPose);
    getMesh(theta+delta*DELTA,NULL,NULL,&meshv2,NULL,NULL,adjustPose);
    dmeshvDelta.setZero(meshv.rows(),meshv.cols());
    for(sizeType t=0; t<nrTheta(); t++)
      dmeshvDelta+=dmeshv[t]*delta[t];
    INFOV("GradientV: %f Err: %f",dmeshvDelta.norm(),(dmeshvDelta-(meshv2-meshv)/DELTA).norm())
  }
  exit(-1);
#undef DELTA
#undef NR_TEST
}
//SkinnedMeshUsingArticulatedBody
SkinnedMeshUsingArticulatedBody::SkinnedMeshUsingArticulatedBody()
{
  setType(typeid(SkinnedMeshUsingArticulatedBody).name());
}
bool SkinnedMeshUsingArticulatedBody::read(istream& is,IOData* dat)
{
  SkinnedMesh::read(is,dat);
  readVector(_names,is);
  _body.read(is,dat);
  return is.good();
}
bool SkinnedMeshUsingArticulatedBody::write(ostream& os,IOData* dat) const
{
  SkinnedMesh::write(os,dat);
  writeVector(_names,os);
  _body.write(os,dat);
  return os.good();
}
boost::shared_ptr<Serializable> SkinnedMeshUsingArticulatedBody::copy() const
{
  return boost::shared_ptr<Serializable>(new SkinnedMeshUsingArticulatedBody);
}
void SkinnedMeshUsingArticulatedBody::getJoint
(const Cold& theta,const MatX3d& J,
 vector<Mat4d,Eigen::aligned_allocator<Mat4d> >& tss,
 vector<vector<Mat4d,Eigen::aligned_allocator<Mat4d> > >* dtss,
 MatX3d* joints,Matd djoints[3])
{
  _body._bodyMap[_names[0]]->_toParent.block<3,1>(0,3)=J.row(0).transpose().cast<scalar>();
  for(sizeType i=1,parent; i<_kintree.cols(); i++) {
    parent=_kintree(0,i);
    _body._bodyMap[_names[i]]->_toParent.block<3,1>(0,3)=(J.row(i)-J.row(parent)).transpose().cast<scalar>();
  }
  BodyOpT<const Body> OP(_body.nrBody(),(djoints||dtss) ? theta.size() : 0);
  _body.transform(_body.root(),OP,(void*)&theta);
  tss.resize(_kintree.cols());
  if(joints)
    joints->setZero(J.rows(),3);
  for(sizeType i=0; i<_kintree.cols(); i++) {
    tss[i]=OP.getTransD(i);
    if(joints)
      joints->row(i)=tss[i].block<3,1>(0,3).transpose();
  }
  if(djoints)
    for(unsigned char d=0; d<3; d++) {
      djoints[d].setZero(J.rows(),nrTheta());
      for(sizeType r=0; r<J.rows(); r++)
        for(sizeType c=0; c<nrTheta(); c++)
          djoints[d](r,c)=OP.getDTrans()(r*4+d,c*4+3);
    }
  if(dtss) {
    dtss->assign(nrJoint(),vector<Mat4d,Eigen::aligned_allocator<Mat4d> >(nrTheta()));
    for(sizeType j=0; j<nrJoint(); j++)
      for(sizeType t=0; t<nrTheta(); t++)
        dtss->at(j).at(t)=OP.getDTransD(j,t);
  }
}
void SkinnedMeshUsingArticulatedBody::readPkl(const string& path)
{
  SkinnedMesh::readPkl(path);
  ArticulatedBody::BodyMap bodies;
  _names.resize(_kintree.cols());
  {
    Body b(true);
    b._id=_kintree.cols();
    b._name="root";
    b._toParent.setIdentity();
    b._dir.setZero();
    b._dof.push_back(DOF(S,_kintree.cols()*3+3));
    for(unsigned char d=0; d<3; d++)
      b._dof.push_back(DOF((DOF_TYPE)(TX+d),_kintree.cols()*3+d));
    bodies["root"]=boost::dynamic_pointer_cast<Body>(b.copy());
  }
  for(sizeType i=0,parent; i<_kintree.cols(); i++) {
    _names[i]=boost::lexical_cast<string>(i);
    Body b(true);
    b._id=i;
    b._name=_names[i];
    b._toParent.setIdentity();
    b._dir.setZero();
    for(unsigned char d=0; d<3; d++)
      b._dof.push_back(DOF((DOF_TYPE)(RX+d),i*3+d));
    bodies[_names[i]]=boost::dynamic_pointer_cast<Body>(b.copy());
    parent=_kintree(0,i);
    if(parent >= 0 && parent < _kintree.cols())
      Body::addChild(bodies[_names[parent]],bodies[_names[i]]);
    else Body::addChild(bodies["root"],bodies[_names[i]]);
  }
  _body=ArticulatedBody(bodies);
  //_body.buildBodyM(joint);
  //_body.debugDJoint();
}
sizeType SkinnedMeshUsingArticulatedBody::nrTheta() const
{
  return _kintree.cols()*3+4;
}
const vector<string>& SkinnedMeshUsingArticulatedBody::names() const
{
  return _names;
}
//debug
void SkinnedMeshUsingArticulatedBody::debugJoint(const string& path,scalar delta,sizeType nr)
{
  SkinnedMesh::debugJoint(path,delta,nr);
  string pathJ=path+"/transScale";
  ObjMesh obj;
  MatX3d joints;
  recreate(pathJ);
  Cold theta=Cold::Zero(nrTheta());
  for(sizeType j=0; j<nr; j++) {
    theta.segment<4>(nrTheta()-4)=Vec4d::Random()*delta;
    getMesh(theta,&joints,NULL,NULL,NULL,&obj,true);
    writeJointVTK(joints,pathJ+"/jointFrm"+boost::lexical_cast<string>(j)+".vtk");
    obj.writeVTK(pathJ+"/frm"+boost::lexical_cast<string>(j)+".vtk",true);
  }
}
//articulated body utility
void SkinnedMeshUsingArticulatedBody::writeFrameSeqVTK(const string& path,sizeType begId,sizeType step)
{
  recreate(path);
  for(sizeType i=begId; i<_body.nrFrm(); i+=step) {
    Cold dof=_body.frm().col(i);
    writeFrameVTK(path+"/frm"+boost::lexical_cast<string>(i)+".vtk",&dof);
  }
}
void SkinnedMeshUsingArticulatedBody::writeFrameVTK(const string& path,const Cold* dof)
{
  ObjMesh obj;
  if(dof)
    getMesh(*dof,NULL,NULL,NULL,NULL,&obj,true);
  else getMesh(Cold::Zero(nrTheta()),NULL,NULL,NULL,NULL,&obj,true);
  obj.writeVTK(path,true);
}
void SkinnedMeshUsingArticulatedBody::registerASF(const ArticulatedBody& other,Coli& jointMap,Cold& theta,Coli* symJointMap,sizeType symAxis,const string& checkWrite)
{
  //write body
  if(!checkWrite.empty()) {
    MatX3d J;
    recreate(checkWrite);
    getMesh(Cold::Zero(nrTheta()),&J,NULL,NULL,NULL,NULL,true);
    writeJointVTK(J,checkWrite+"/joints.vtk");
    other.writeJointsVTK(checkWrite+"/mocapJoints.vtk");
    other.writeBodyConfiguration();
  }
#define COMPUTE_BB  \
getMesh(theta,&joints,NULL,NULL,NULL,NULL,true); \
bb.reset(); \
for(sizeType i=0;i<joints.rows();i++) \
  bb.setUnion(joints.row(i));
  //initialize
  MatX3d joints;
  ObjMesh objMesh;
  BBox<scalarD> bb;
  theta=Cold::Zero(nrTheta());
  //other
  BodyOpT<const Body> opOther(other.nrBody());
  other.transform(other.root(),opOther,NULL);
  BBox<scalarD> bbOther=opOther.getBBD();
  COMPUTE_BB
  //scale
  theta[nrTheta()-1]=bbOther.getExtent().norm()/bb.getExtent().norm()-1;
  COMPUTE_BB
  if(!checkWrite.empty()) {
    other.writeFrameVTK(checkWrite+"/other.vtk");
    writeJointVTK(joints,checkWrite+"/thisScale.vtk");
  }
  //translation
  theta.segment<3>(nrTheta()-4)=(bbOther._minC+bbOther._maxC)/2;
  theta.segment<3>(nrTheta()-4)-=(bb._minC+bb._maxC)/2;
  COMPUTE_BB
  if(!checkWrite.empty())
    writeJointVTK(joints,checkWrite+"/thisTrans.vtk");
#undef COMPUTE_BB
  //joint map
  jointMap.setConstant(nrJoint(),-1);
  //foot
  jointMap[11]=other.findBody("rtoes").id();
  jointMap[10]=other.findBody("ltoes").id();
  jointMap[8]=other.findBody("rfoot").id();
  jointMap[7]=other.findBody("lfoot").id();
  jointMap[5]=other.findBody("rtibia").id();
  jointMap[4]=other.findBody("ltibia").id();
  jointMap[2]=other.findBody("rfemur").id();
  jointMap[1]=other.findBody("lfemur").id();
  //back
  jointMap[0]=other.findBody("lowerback").id();
  jointMap[3]=other.findBody("upperback").id();
  jointMap[6]=other.findBody("thorax").id();
  jointMap[9]=other.findBody("lowerneck").id();
  jointMap[12]=other.findBody("upperneck").id();
  jointMap[15]=other.findBody("head").id();
  //hand
  jointMap[17]=other.findBody("rhumerus").id();
  jointMap[16]=other.findBody("lhumerus").id();
  jointMap[19]=other.findBody("rradius").id();
  jointMap[18]=other.findBody("lradius").id();
  jointMap[21]=other.findBody("rwrist").id();
  jointMap[20]=other.findBody("lwrist").id();
  jointMap[23]=other.findBody("rfingers").id();
  jointMap[22]=other.findBody("lfingers").id();
  //ICP registration
  Matd CI;
  Cold CI0;
  LMInterface sol;
  ICPObjective obj(opOther,jointMap,*this);
  if(symAxis >= 0) {
    obj.makeSymmetry(other,symAxis);
    if(symJointMap)
      *symJointMap=obj._symJointMap;
  }
  //fix torso joints
  vector<sizeType> consJoint;
  Callback<scalarD,Kernel<scalarD> > cb;
  NoCallback<scalarD,Kernel<scalarD> > noCb;
  sol.setTolF(0);
  sol.setTolG(1E-5f);
  consJoint.push_back(0);
  consJoint.push_back(3);
  consJoint.push_back(6);
  consJoint.push_back(9);
  consJoint.push_back(13);
  consJoint.push_back(14);
  consJoint.push_back(12);
  consJoint.push_back(15);
  CI.setZero((sizeType)consJoint.size()*3,obj.inputs());
  CI0.setZero((sizeType)consJoint.size()*3);
  for(sizeType i=0; i<(sizeType)consJoint.size(); i++)
    for(sizeType d=0;d<3;d++)
      CI(i*3+d,consJoint[i]*3+d)=1;
  sol.setCI(CI,CI0,Coli::Zero(CI0.size()));
  //sol.debugGradient(obj,theta+Cold::Random(obj.inputs())*0.1f);
  sol.solveDense(theta,obj,!checkWrite.empty() ? cb : noCb);
  if(!checkWrite.empty()) {
    getMesh(theta,&joints,NULL,NULL,NULL,&objMesh,true);
    writeJointVTK(joints,checkWrite+"/bodyRegistered.vtk");
    objMesh.writeVTK(checkWrite+"/bodyRegisteredMesh.vtk",true);
    //write jointMap
    VTKWriter<scalar> os("joint",checkWrite+"/jointMap.vtk",true);
    vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
    for(sizeType i=0; i<nrJoint(); i++)
      if(jointMap[i] >= 0) {
        vss.push_back(joints.row(i).cast<scalar>());
        vss.push_back(opOther.getTrans(jointMap[i]).block<3,1>(0,3));
      }
    os.appendPoints(vss.begin(),vss.end());
    os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,2,0),
                   VTKWriter<scalar>::IteratorIndex<Vec3i>(vss.size()/2,2,0),
                   VTKWriter<scalar>::LINE);
  }
}
void SkinnedMeshUsingArticulatedBody::registerAMC(const ArticulatedBody& other,const Coli& jointMap,const Cold& theta,const string& checkWrite)
{
  //find position variable
  sizeType idT[3]= {-1,-1,-1};
  const Body& root=other.root();
  for(sizeType i=0; i<(sizeType)root.dof().size(); i++) {
    const DOF& d=root.dof()[i];
    if(d._type >= TX && d._type < RX)
      idT[(int)d._type]=d._off;
  }
  if(!checkWrite.empty())
    recreate(checkWrite+"/transformedJoints");
  //covert
  MatX3d joints;
  Matd& frm=_body._frm;
  frm.setZero(nrTheta(),other.nrFrm());
  frm.block(nrTheta()-4,0,4,frm.cols())=theta.segment<4>(nrTheta()-4)*Cold::Ones(frm.cols()).transpose();
  vector<Mat3d,Eigen::aligned_allocator<Mat3d> > rss(nrJoint(),Mat3d::Identity());
  //compute _body's rotation
  BodyOpT<const Body> op(_body.nrBody());
  {
    Cold thetaNoS=theta;
    thetaNoS[nrTheta()-1]=0;
    _body.transform(_body.root(),op,(void*)&thetaNoS);
  }
  //compute other's initial rotation
  BodyOpT<const Body> opOther0(other.nrBody());
  other.transform(other.root(),opOther0,NULL);
  for(sizeType f=0; f<other.nrFrm(); f++) {
    //compute other's current rotation
    Cold dof=other.frm().col(f);
    BodyOpT<const Body> opOther(other.nrBody());
    other.transform(other.root(),opOther,&dof);
    //match position
    for(unsigned char d=0; d<3; d++)
      frm(nrTheta()-4+d,f)+=other.frm()(idT[d],f);
    //compute transformed joint coordinates
    for(sizeType i=0,parent; i<nrJoint(); i++) {
      parent=_kintree(0,i);
      //compute rotation
      Mat3d R=Mat3d::Identity();
      if(jointMap[i] >= 0) {
        R=opOther.getTransD(jointMap[i]).block<3,3>(0,0);
        R*=opOther0.getTransD(jointMap[i]).block<3,3>(0,0).transpose();
      } else {
        R=opOther.getTransD(jointMap[parent]).block<3,3>(0,0);
        R*=opOther0.getTransD(jointMap[parent]).block<3,3>(0,0).transpose();
      }
      R*=op.getTransD(i).block<3,3>(0,0);
      //assign
      rss[i]=R;
      ASSERT(isFinite(R))
      if(parent >= 0 && parent < nrJoint())
        R=(rss[parent].transpose()*R).eval();
      frm.block<3,1>(i*3,f)=invExpW<scalarD>(R);
    }
    if(!checkWrite.empty()) {
      getMesh(frm.col(f),&joints,NULL,NULL,NULL,NULL,true);
      writeJointVTK(joints,checkWrite+"/transformedJoints/frm"+boost::lexical_cast<string>(f)+".vtk");
    }
  }
}
const ArticulatedBody& SkinnedMeshUsingArticulatedBody::getBody() const
{
  return _body;
}
ArticulatedBody& SkinnedMeshUsingArticulatedBody::getBody()
{
  return _body;
}
