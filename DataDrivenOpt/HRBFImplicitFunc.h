#ifndef HRBF_IMPLICIT_FUNC_H
#define HRBF_IMPLICIT_FUNC_H

#include <CommonFile/ImplicitFuncInterface.h>
#include <CommonFile/ParticleSet.h>
#include <CommonFile/ObjMesh.h>

PRJ_BEGIN

class HRBFImplicitFunc : public ImplicitFunc<scalar>, public Serializable
{
public:
  typedef ScalarUtil<scalar>::ScalarCol Vec;
  typedef ScalarUtil<scalar>::ScalarMat3X Mat3X;
  typedef ScalarUtil<scalar>::ScalarMat Mat;
  HRBFImplicitFunc();
  HRBFImplicitFunc(const ParticleSetN& pset,bool randomize=false);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void writePSetVTK(const string& path) const;
  virtual scalar operator()(const Vec3& pos) const;
  virtual Vec3 grad(const Vec3& pos) const override;
  virtual Vec3 gradN(const Vec3& pos,scalar delta) const override;
  virtual BBox<scalar> getBB() const;
  const scalar& r() const;
  scalar& r();
  const Mat3X& x() const;
  Mat3X& x();
  //debug
  static void debugMesh();
  //helper
  static Vec solveDense(const Mat& LHS,const Vec& RHS);
  Eigen::DiagonalMatrix<scalar,Eigen::Dynamic> toDiag(const Vec& d) const;
  scalar computeFunc(const Vec3& pos,Vec3* grad) const;
  scalar dclamp(scalar val) const;
  scalar clamp(scalar val) const;
protected:
  Vec _lambda;
  Mat3X _beta;
  Mat3X _x;
  scalar _r;
};

PRJ_END

#endif
