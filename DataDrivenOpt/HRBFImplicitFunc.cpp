#include "HRBFImplicitFunc.h"
#include <CommonFile/MakeMesh.h>

USE_PRJ_NAMESPACE

//HRBFImplicitFunc
HRBFImplicitFunc::HRBFImplicitFunc():Serializable(typeid(HRBFImplicitFunc).name()) {}
HRBFImplicitFunc::HRBFImplicitFunc(const ParticleSetN& pset,bool randomize):Serializable(typeid(HRBFImplicitFunc).name())
{
  sizeType np=pset.size();
  Mat LHS=Mat::Zero(np*4,np*4);
  Vec RHS=Vec::Zero(np*4);
  Vec3 pos;
  Mat3X dir;
  Vec normDir;
  Mat3X betaCoef;
  Vec lambdaCoef;
  //build
  _x.resize(3,np);
  for(sizeType i=0; i<np; i++)
    _x.col(i)=pset[i]._pos;
  if(randomize) {
    _lambda.setRandom(np);
    _beta.setRandom(3,np);
    return;
  }
  for(sizeType i=0; i<np; i++) {
    pos=pset[i]._pos;
    dir=pos*Vec::Ones(_x.cols()).transpose()-_x;
    normDir=(dir.transpose()*dir).diagonal().cwiseSqrt();
    betaCoef=dir*3*toDiag(normDir);
    lambdaCoef=normDir.array().pow(3).matrix();
    //build value equation
    LHS.block(i*4+0,0,1,np)=lambdaCoef.transpose();
    for(sizeType d=0; d<3; d++)
      LHS.block(i*4,np*(d+1),1,np)=betaCoef.row(d);
    RHS[i*4+0]=0;
    //build normal equation
    LHS.block(i*4+1,0,3,np)=betaCoef;
    for(sizeType d=0; d<3; d++) {
      betaCoef=dir*3*toDiag((dir.row(d).transpose().array()/normDir.cwiseMax(EPS).array()).matrix());
      betaCoef.row(d)+=3*normDir;
      LHS.block(i*4+1,np*(d+1),3,np)=betaCoef;
    }
    RHS.segment<3>(i*4+1)=pset[i]._normal;
  }
  //solve
  Vec sol=solveDense(LHS,RHS);
  _lambda=sol.segment(0,np);
  _beta.resize(3,np);
  for(sizeType d=0; d<3; d++)
    _beta.row(d)=sol.segment(np*(d+1),np);
  //set r
  _r=getBB().getExtent().norm()/2;
}
bool HRBFImplicitFunc::read(istream& is,IOData* dat)
{
  readBinaryData(_lambda,is);
  readBinaryData(_beta,is);
  readBinaryData(_x,is);
  readBinaryData(_r,is);
  return is.good();
}
bool HRBFImplicitFunc::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_lambda,os);
  writeBinaryData(_beta,os);
  writeBinaryData(_x,os);
  writeBinaryData(_r,os);
  return os.good();
}
boost::shared_ptr<Serializable> HRBFImplicitFunc::copy() const
{
  return boost::shared_ptr<Serializable>(new HRBFImplicitFunc);
}
void HRBFImplicitFunc::writePSetVTK(const string& path) const
{
  ParticleSetN pset;
  for(sizeType i=0; i<_x.cols(); i++) {
    ParticleN<scalar> p;
    p._pos=_x.col(i).cast<scalar>();
    pset.addParticle(p);
  }
  pset.writeVTK(path);
}
scalar HRBFImplicitFunc::operator()(const Vec3& pos) const
{
  return clamp(computeFunc(pos,NULL));
}
Vec3 HRBFImplicitFunc::grad(const Vec3& pos) const
{
  Vec3 g;
  scalar val=computeFunc(pos,&g);
  return g*dclamp(val);
}
Vec3 HRBFImplicitFunc::gradN(const Vec3& pos,scalar delta) const
{
  Vec3 val;
  for(sizeType d=0; d<3; d++)
    val[d]=(operator()(pos+Vec3::Unit(d)*delta)-operator()(pos-Vec3::Unit(d)*delta))/delta/2;
  return val;
}
BBox<scalar> HRBFImplicitFunc::getBB() const
{
  BBox<scalar> bb;
  for(sizeType i=0; i<_x.cols(); i++)
    bb.setUnion(_x.col(i));
  bb.enlargedEps(1);
  return bb;
}
const scalar& HRBFImplicitFunc::r() const
{
  return _r;
}
scalar& HRBFImplicitFunc::r()
{
  return _r;
}
const HRBFImplicitFunc::Mat3X& HRBFImplicitFunc::x() const
{
  return _x;
}
HRBFImplicitFunc::Mat3X& HRBFImplicitFunc::x()
{
  return _x;
}
//debug
void HRBFImplicitFunc::debugMesh()
{
#define DELTA 1E-7f
  ObjMesh obj;
  ParticleSetN pset;
  ParticleN<scalar> p;
  MakeMesh::makeSphere3D(obj,1,8);
  for(sizeType i=0; i<(sizeType)obj.getV().size(); i++) {
    p._pos=obj.getV()[i];
    p._normal=obj.getN()[i];
    pset.addParticle(p);
  }
  //gradient
  {
    HRBFImplicitFunc f(pset,true);
    for(sizeType i=0; i<(sizeType)pset.size(); i++) {
      Vec grad=f.grad(pset[i]._pos);
      Vec gradN=f.gradN(pset[i]._pos,DELTA);
      INFOV("Gradient: %f Err: %f",grad.norm(),(grad-gradN).norm())
    }
  }
  //solver
  {
    Vec3 grad;
    scalar val;
    HRBFImplicitFunc f(pset,false);
    for(sizeType i=0; i<(sizeType)pset.size(); i++) {
      val=f.computeFunc(pset[i]._pos,&grad);
      ASSERT_MSG(abs(val) < EPS,"Value Error!")
      ASSERT_MSG((grad-pset[i]._normal).norm() < EPS,"Gradient Error!")
    }
  }
  exit(-1);
#undef DELTA
}
//helper
HRBFImplicitFunc::Vec HRBFImplicitFunc::solveDense(const Mat& LHS,const Vec& RHS)
{
  return LHS.cast<scalarD>().partialPivLu().solve(RHS.cast<scalarD>()).cast<scalar>();
}
Eigen::DiagonalMatrix<scalar,Eigen::Dynamic> HRBFImplicitFunc::toDiag(const Vec& d) const
{
  Eigen::DiagonalMatrix<scalar,Eigen::Dynamic> ret(d.size());
  ret.diagonal()=d;
  return ret;
}
scalar HRBFImplicitFunc::computeFunc(const Vec3& pos,Vec3* grad) const
{
  Mat3X dir=pos*Vec::Ones(_x.cols()).transpose()-_x;
  Vec normDir=(dir.transpose()*dir).diagonal().cwiseSqrt();
  Mat3X betaCoef=dir*3*toDiag(normDir);
  Vec lambdaCoef=normDir.array().pow(3).matrix();
  scalar ret=lambdaCoef.dot(_lambda)+(betaCoef.transpose()*_beta).diagonal().sum();
  //lambda component
  if(grad) {
    *grad=betaCoef*_lambda;
    for(sizeType d=0; d<3; d++) {
      betaCoef=dir*3*toDiag((dir.row(d).transpose().array()/normDir.cwiseMax(EPS).array()).matrix());
      betaCoef.row(d)+=3*normDir;
      *grad+=betaCoef*_beta.row(d).transpose();
    }
  }
  return ret;
}
scalar HRBFImplicitFunc::dclamp(scalar val) const
{
  if(val < -_r)
    return 0;
  else if(val > _r)
    return 0;
  else {
    val/=_r;
    scalar val2=val*val;
    scalar val4=val2*val2;
    return (-15.0f*val4/16.0f+15.0f*val2/8.0f-15.0f/16.0f)/_r;
  }
}
scalar HRBFImplicitFunc::clamp(scalar val) const
{
  if(val < -_r)
    return 1;
  else if(val > _r)
    return 0;
  else {
    val/=_r;
    scalar val2=val*val;
    scalar val3=val*val2;
    scalar val5=val3*val2;
    return -3.0f*val5/16.0f+5.0f*val3/8.0f-15.0f*val/16.0f+0.5f;
  }
}
