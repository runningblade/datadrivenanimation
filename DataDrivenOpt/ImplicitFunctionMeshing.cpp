#include "ImplicitFunctionMeshing.h"
#include "LMInterface.h"
#include "LMCholmod.h"
#include "Utils.h"
#include <CommonFile/MarchingCube3D.h>
#include <CommonFile/solvers/Objective.h>
#include <CommonFile/GridOp.h>

PRJ_BEGIN

//normal mesher
class FairObjective : public Objective<scalarD>
{
public:
  FairObjective(ObjMesh& mesh,const ImplicitFunc<scalar>& func,scalar level)
    :_mesh(mesh),_func(func),_coef(10),_level(level) {
    vector<set<sizeType> > neigh(mesh.getV().size());
    for(sizeType i=0; i<(sizeType)mesh.getI().size(); i++)
      for(sizeType d=0; d<3; d++) {
        neigh[mesh.getI()[i][d]].insert(mesh.getI()[i][(d+1)%3]);
        neigh[mesh.getI()[i][d]].insert(mesh.getI()[i][(d+2)%3]);
      }
    //fill laplacian
    STrips trips;
    for(sizeType i=0; i<(sizeType)mesh.getV().size(); i++) {
      addIK(trips,i*3,i*3,1,3);
      for(set<sizeType>::const_iterator beg=neigh[i].begin(),end=neigh[i].end(); beg!=end; beg++)
        addIK(trips,i*3,*beg*3,-1/(scalar)neigh[i].size(),3);
    }
    _laplacian.resize(mesh.getV().size()*3,mesh.getV().size()*3);
    _laplacian.setFromTriplets(trips.begin(),trips.end());
  }
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable) override {
    fvec.resize(values());
    fvec.segment(0,_laplacian.rows())=_laplacian*x;
    if(fjac)
      addSparseBlock(*fjac,0,0,_laplacian,1);
    for(sizeType i=0; i<(sizeType)_mesh.getI().size(); i++) {
      Vec3 grad,ctr=Vec3::Zero();
      ctr+=x.segment<3>(_mesh.getI()[i][0]*3).cast<scalar>();
      ctr+=x.segment<3>(_mesh.getI()[i][1]*3).cast<scalar>();
      ctr+=x.segment<3>(_mesh.getI()[i][2]*3).cast<scalar>();
      ctr/=3;
      //distance to surface
      fvec[_laplacian.rows()+i]=(_func(ctr)-_level)*_coef;
      if(fjac) {
        grad=_func.grad(ctr);
        Matd gradV=grad.transpose().cast<scalarD>()*_coef/3;
        addBlock(*fjac,_laplacian.rows()+i,_mesh.getI()[i][0]*3,gradV);
        addBlock(*fjac,_laplacian.rows()+i,_mesh.getI()[i][1]*3,gradV);
        addBlock(*fjac,_laplacian.rows()+i,_mesh.getI()[i][2]*3,gradV);
      }
    }
    return 0;
  }
  virtual Cold assemble() const {
    Cold vss=Cold::Zero(inputs());
    for(sizeType i=0; i<(sizeType)_mesh.getV().size(); i++)
      vss.segment<3>(i*3)=_mesh.getV()[i].cast<scalarD>();
    return vss;
  }
  virtual void assign(const Cold& vss) {
    for(sizeType i=0; i<(sizeType)_mesh.getV().size(); i++)
      _mesh.getV()[i]=vss.segment<3>(i*3).cast<scalar>();
    _mesh.smooth();
  }
  int inputs() const override {
    return (int)(_mesh.getV().size()*3);
  }
  int values() const override {
    return (int)(_mesh.getV().size()*3+_mesh.getI().size());
  }
  //data
  ObjMesh& _mesh;
  const ImplicitFunc<scalar>& _func;
  SMat _laplacian;
  scalar _coef;
  scalar _level;
};
ObjMesh implicitFuncToObj(const ImplicitFunc<scalar>& f,scalar radius,scalar level)
{
  ScalarField field;
  BBox<scalar> bb=f.getBB();
  field.reset(ceilV(Vec3(bb.getExtent()/radius)),bb,0,false);
  GridOp<scalar,scalar>::copyFromImplictFunc(field,f);
  //GridOp<scalar,scalar>::write3DScalarGridVTK(path,f);

  ObjMesh mesh;
  MarchingCube3D<scalar> mc(mesh);
  mc.solve(field,level);
  return mesh;
}
void implicitFuncFairObj(const ImplicitFunc<scalar>& f,ObjMesh& mesh,scalar eps,scalar level,scalar coef,bool debug)
{
  LMInterface sol;
  FairObjective obj(mesh,f,level);
  Callback<scalarD,Kernel<scalarD> > cb;
  NoCallback<scalarD,Kernel<scalarD> > noCb;
  obj._coef=coef;
  sol.setTolF(eps);
  sol.setTolG(eps);
#ifdef CHOLMOD_SUPPORT
  boost::shared_ptr<CholmodWrapper> cholmod(new CholmodWrapper);
  sol.useInterface(cholmod);
#endif
  Cold vss=obj.assemble();
  //sol.debugGradient(obj,vss,Cold::Ones(vss.size()));
  sol.solveSparse(vss,obj,debug ? cb : noCb);
  obj.assign(vss);
}
void implicitFuncToVTK(const ImplicitFunc<scalar>& f,const string& path,scalar radius,scalar level)
{
  ObjMesh mesh=implicitFuncToObj(f,radius,level);
  mesh.writeVTK(path,true);
}

PRJ_END
