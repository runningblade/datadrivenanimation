#ifndef CLOTH_ARTICULATED_ENERGIES_H
#define CLOTH_ARTICULATED_ENERGIES_H

#include "ClothEnergies.h"
#include "ArticulatedBody.h"
#include <CommonFile/geom/StaticGeom.h>
#include <CommonFile/CollisionDetection.h>

PRJ_BEGIN

struct BodyRef : public Serializable {
  BodyRef();
  bool write(ostream& os,IOData* dat) const;
  bool read(istream& is,IOData* dat);
  boost::shared_ptr<Serializable> copy() const;
  bool operator<(const BodyRef& other) const;
  bool operator!=(const BodyRef& other) const;
  bool operator==(const BodyRef& other) const;
  boost::shared_ptr<StaticGeomCell> _mesh;
  sizeType _bid;
};
class ArticulatedBodyEnergy : public ClothEnergy
{
public:
  struct Cache {
    sizeType _vid;
    Vec3 _p,_n,_normal;
    Mat3 _hess;
    boost::shared_ptr<StaticGeomCell> _mesh;
  };
  //typedef BBox<scalar> BBOX;
  typedef KDOP18<scalar> BBOX;
  ArticulatedBodyEnergy(const ArticulatedBody& body);
  ArticulatedBodyEnergy(boost::shared_ptr<ClothMesh> mesh,const ArticulatedBody& body,const set<sizeType>* collisionSet);
  void setConfiguration(const BodyOpT<const Body>& op);
  virtual bool linear() const;
  virtual void getRelatedVertex(vector<sizeType>& vs) const;
  virtual scalarD eval() const;
  virtual void eval(ParallelMatrix<Vec>& c,STrips& grad,sizeType off);
  virtual void eval(Vec& c,STrips& grad,sizeType off) const;
  virtual scalarD penetrationDepth();
  virtual void setK(scalarD K);
protected:
  virtual void updateBVHV();
  virtual void buildBVHV();
  virtual void buildBVHB(const set<sizeType>* collisionSet);
  boost::shared_ptr<ClothMesh> _mesh;
  const ArticulatedBody& _body;
  //collision
  vector<Node<sizeType,BBOX> > _bvhv;
  vector<Node<BodyRef,BBOX> > _bvhb;
  vector<Cache> _cache;
  scalarD _K;
};
class ArticulatedBodyEnergySubd : public ArticulatedBodyEnergy
{
public:
  ArticulatedBodyEnergySubd(boost::shared_ptr<ClothMesh> mesh,const ArticulatedBody& body,const set<sizeType>* collisionSet,sizeType nrSubd);
  virtual void eval(ParallelMatrix<Vec>& c,STrips& grad,sizeType off);
protected:
  virtual void updateBVHV();
  virtual void buildBVHVSubd(sizeType nrSubd);
  FixedSparseMatrix<scalarD,Kernel<scalarD> > _interp;
  Cold _C,_IC;
};

PRJ_END

#endif
