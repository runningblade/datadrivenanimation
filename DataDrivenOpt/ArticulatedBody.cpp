#include "ArticulatedBody.h"
#include "RigidBodyMass.h"
#include <CommonFile/ObjMesh.h>
#include <CommonFile/RotationUtil.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

bool check(string& buf,const char* name)
{
  string bufTmp=buf;
  transform(bufTmp.begin(),bufTmp.end(),bufTmp.begin(),::tolower);

  if(bufTmp == name) {
    buf.clear();
    return true;
  } else return false;
}
bool check(string& buf,sizeType& num)
{
  try {
    num=boost::lexical_cast<sizeType>(buf);
    return true;
  } catch(...) {
    return false;
  }
}

DOF::DOF():Serializable(typeid(DOF).name()),_lmt(-numeric_limits<scalar>::infinity(),numeric_limits<scalar>::infinity()) {}
DOF::DOF(DOF_TYPE type,sizeType off):Serializable(typeid(DOF).name()),_type(type),_off(off),_lmt(-numeric_limits<scalar>::infinity(),numeric_limits<scalar>::infinity()) {}
bool DOF::read(istream& is, IOData* dat)
{
  readBinaryData(_type,is);
  readBinaryData(_off,is);
  readBinaryData(_lmt,is);
  return is.good();
}
bool DOF::write(ostream& os, IOData* dat) const
{
  writeBinaryData(_type,os);
  writeBinaryData(_off,os);
  writeBinaryData(_lmt,os);
  return os.good();
}
boost::shared_ptr<Serializable> DOF::copy() const
{
  return boost::shared_ptr<Serializable>(new DOF);
}
void DOF::setUnit(scalar length, scalar angle)
{
  if(_type < RX)
    _lmt*=length;
  else _lmt*=angle;
}
string DOF::typeStr() const
{
  static string types[7]= {"TX","TY","TZ","RX","RY","RZ","S"};
  return types[(int)_type];
}

Body::ForceContact::ForceContact() {}
Body::ForceContact::ForceContact(const Vec3& r,const Vec3& f)
  :_r(r),_f(f),_isForce(true) {}
Body::ForceContact::ForceContact(const Vec3& r,const Vec3& x0,const Vec3& n)
  :_r(r),_x0(x0),_n(n),_isForce(false) {}

Body::Body(bool rodriguez) :Serializable(typeid(Body).name())
{
  _M.setZero();
  _dir.setZero();
  _COM.setZero();
  _toParent.setIdentity();
  _rodriguez=rodriguez;
}
Body::~Body() {}
bool Body::read(istream& is, IOData* dat)
{
  readBinaryData(_M,is);
  readBinaryData(_dir, is);
  readBinaryData(_COM, is);
  readBinaryData(_toParent, is);
  readVector(_dof, is, dat);
  readBinaryData(_geom, is, dat);

  readBinaryData(_id, is);
  readBinaryData(_name, is);
  readBinaryData(_child, is, dat);
  readBinaryData(_next, is, dat);
  readBinaryData(_parent, is, dat);
  readBinaryData(_rodriguez, is, dat);
  return is.good();
}
bool Body::write(ostream& os, IOData* dat) const
{
  writeBinaryData(_M,os);
  writeBinaryData(_dir, os);
  writeBinaryData(_COM, os);
  writeBinaryData(_toParent, os);
  writeVector(_dof, os, dat);
  writeBinaryData(_geom, os, dat);

  writeBinaryData(_id, os);
  writeBinaryData(_name, os);
  writeBinaryData(_child, os, dat);
  writeBinaryData(_next, os, dat);
  writeBinaryData(_parent, os, dat);
  writeBinaryData(_rodriguez, os, dat);
  return os.good();
}
boost::shared_ptr<Serializable> Body::copy() const
{
  return boost::shared_ptr<Serializable>(new Body(*this));
}
bool Body::readASF(string& buf,istream& is,scalar length,scalar angle,sizeType& dof)
{
  scalar len;
  Vec3 axis;
  char bracket;

  //main loop
  bool begun = false;
  while (is.good() && !is.eof())
    if (!begun && check(buf,"begin"))
      begun = true;
    else if (check(buf,"end")) {
      _dir=(rotationEulerDeriv<scalar>(axis,NULL,NULL).transpose()*_dir*len).eval();
      _toParent.setIdentity();
      _toParent.block<3,3>(0,0)=rotationEulerDeriv<scalar>(axis,NULL,NULL);
      _toParent.block<3,1>(0,3)=_dir;
      return begun;
    } else if (check(buf,"id"))
      is >> _id;
    else if (check(buf,"name"))
      is >> _name;
    else if (check(buf,"direction")) {
      is >> _dir[0] >> _dir[1] >> _dir[2];
      _dir /= max<scalar>(_dir.norm(), EPS);
    } else if (check(buf,"length")) {
      is >> len;
      len *= length;
    } else if (check(buf,"axis")) {
      is >> axis[0] >> axis[1] >> axis[2] >> buf;
      axis *= angle;
      ASSERT_MSG(check(buf,"xyz"),"We can only handle axis order=XYZ!")
    } else if (check(buf,"dof")) {
      is >> buf;
      while (is.good() && !is.eof())
        if (check(buf,"tx"))
          _dof.push_back(DOF(TX,dof++));
        else if (check(buf,"ty"))
          _dof.push_back(DOF(TY,dof++));
        else if (check(buf,"tz"))
          _dof.push_back(DOF(TZ,dof++));
        else if (check(buf,"rx"))
          _dof.push_back(DOF(RX,dof++));
        else if (check(buf,"ry"))
          _dof.push_back(DOF(RY,dof++));
        else if (check(buf,"rz"))
          _dof.push_back(DOF(RZ,dof++));
        else if (buf == "limits")
          break;
        else is >> buf;
    } else if (check(buf,"limits")) {
      for (sizeType i=0; i<(sizeType)_dof.size(); i++) {
        is >> bracket >> _dof[i]._lmt[0] >> _dof[i]._lmt[1] >> bracket;
        _dof[i].setUnit(length,angle);
      }
    } else if(beginsWith(buf,":"))
      return false;
    else is >> buf;
  return false;
}
Mat4d Body::buildMat(const Cold& dat,Matd* ddat,const Mat4d& T0) const
{
  Mat3d drot[3];
  scalarD scale=1;
  Vec3d rot=Vec3d::Zero();
  Vec3d trans=Vec3d::Zero();
  for(sizeType d=0; d<(char)_dof.size(); d++) {
    const DOF& dof=_dof[d];
    if(dof._type == S)
      scale+=dat[dof._off];
    else if(dof._type >= RX)
      rot[dof._type-RX]=dat[dof._off];
    else trans[dof._type]=dat[dof._off];
  }
  Mat4d T=Mat4d::Identity();
  if(_rodriguez)
    T.block<3,3>(0,0)=expWGradV<scalarD>(rot,ddat ? drot : NULL,NULL,NULL,NULL,NULL);
  else T.block<3,3>(0,0)=rotationEulerDeriv<scalarD>(rot,ddat ? drot : NULL,NULL);
  T.block<3,3>(0,0)*=scale;
  T.block<3,1>(0,3)=trans;
  if(ddat)
    for(sizeType d=0; d<(char)_dof.size(); d++) {
      const DOF& dof=_dof[d];
      if(dof._type == S)
        ddat->block<3,3>(_id*4,dof._off*4)=T0.block<3,3>(0,0);
      else if(dof._type >= RX)
        ddat->block<3,3>(_id*4,dof._off*4)=T0.block<3,3>(0,0)*drot[dof._type-RX];
      else ddat->block<3,1>(_id*4,dof._off*4+3)=T0.block<3,1>(0,dof._type);
    }
  return T;
}
const Mat6& Body::M() const
{
  return _M;
}
const Vec3& Body::dir() const
{
  return _dir;
}
const Vec3& Body::COM() const
{
  return _COM;
}
const Mat4& Body::toParent() const
{
  return _toParent;
}
const vector<DOF>& Body::dof() const
{
  return _dof;
}
sizeType Body::id() const
{
  return _id;
}
const string& Body::name() const
{
  return _name;
}
bool Body::hasParent() const
{
  return !_parent.expired();
}
const Body& Body::parent() const
{
  return _parent.lock().operator*();
}
bool Body::hasMesh() const
{
  return (bool)_geom;
}
const StaticGeomCell& Body::mesh() const
{
  return *_geom;
}
boost::shared_ptr<StaticGeomCell> Body::meshRef() const
{
  return _geom;
}
void Body::setMesh(boost::shared_ptr<StaticGeomCell> geom)
{
  _geom=geom;
}
void Body::addChild(boost::shared_ptr<Body> parent,boost::shared_ptr<Body> child)
{
  //set children
  boost::shared_ptr<Body>* curr = &(parent->_child);
  while (*curr) {
    if(*curr == child)
      return;
    curr = &((*curr)->_next);
  }
  *curr = child;
  //set parent
  child->_parent=parent;
}
void Body::buildMap(boost::unordered_map<string, boost::shared_ptr<Body> >& BodyMap,boost::shared_ptr<Body> body)
{
  ASSERT(BodyMap.find(body->name()) == BodyMap.end())
  BodyMap[body->name()]=body;
  for(boost::shared_ptr<Body> curr=body->_child; curr; curr=curr->_next)
    buildMap(BodyMap,curr);
}

PRJ_BEGIN
class BodyOpVTK : public BodyOpT<const Body>
{
public:
  BodyOpVTK(sizeType nrBody,const string& path,const string& refName)
    :BodyOpT(nrBody),_refName(refName),_T0(Mat4::Identity()) {
    if(boost::filesystem::path(path).extension().string() == ".vtk" ||
       boost::filesystem::path(path).extension().string() == "vtk")
      _writer.reset(new VTKWriter<scalar>("articulatedFrm",path,true));
  }
  virtual void operator()(const Body& b,void* data) {
    BodyOpT::operator()(b,data);
    if(b.name() == _refName)
      _T0=getTrans(b.id());

    Mat4 T=getTrans(b.id());
    if(!b.hasMesh()) {
      scalar len=b.dir().norm();
      if(len == 0)
        return;
      Vec3 pts[2]= {T.block<3,1>(0,3),transformHomo<scalar>(T,b.dir())};
      if(_writer) {
        _writer->setRelativeIndex();
        _writer->appendPoints(pts,pts+2);
        _writer->appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,1,0),VTKWriter<scalar>::IteratorIndex<Vec3i>(1,1,0),VTKWriter<scalar>::LINE,true);
      }
    } else {
      ObjMesh tmp;
      b.mesh().getMesh(tmp,true);
      Mat4 TT=_T0.inverse()*T;
      tmp.getPos()=TT.block<3,1>(0,3);
      tmp.getT()=TT.block<3,3>(0,0);
      tmp.applyTrans(Vec3::Zero());
      tmp.smooth();
      if(_writer)
        tmp.writeVTK(*_writer,false,false);
      _meshAssembly.addMesh(tmp,b.name());
    }
  }
  boost::shared_ptr<VTKWriter<scalar> > _writer;
  ObjMesh _meshAssembly;
  const string& _refName;
  Mat4 _T0;
};
class BodyOpBuildM : public BodyOpT<Body>
{
  class BodyTransVTK
  {
  public:
    typedef Vec3 value_type;
    typedef ScalarUtil<scalar>::ScalarQuat Quat;
    typedef vector<Vec3,Eigen::aligned_allocator<Vec3> >::const_iterator CIter;
    BodyTransVTK(CIter it,const Vec3& dir=Vec3::Zero(),const Mat4& T=Mat4::Identity()):_it(it) {
      _len=dir.norm();
      Mat4 R=Mat4::Identity();
      R.block<3,3>(0,0)=Quat::FromTwoVectors(Vec3::Unit(1)*_len,dir).toRotationMatrix();
      _TT=T*R;
    }
    bool operator!=(const BodyTransVTK& other) const {
      return _it != other._it;
    }
    virtual Vec3 operator*() const {
      Vec3 pt=*_it;
      if(pt[1] > 0)pt[1]+=_len;
      return transformHomo<scalar>(_TT,pt);
    }
    void operator++() {
      _it++;
    }
    CIter _it;
    scalar _len;
    Mat4 _TT;
  };
public:
  BodyOpBuildM(sizeType nrBody,const ObjMesh& joint)
    :BodyOpT(nrBody),_joint(joint) {}
  virtual void operator()(Body& b,void* data) {
    BodyOpT::operator()(b,data);
    scalar len=b.dir().norm();
    if(len == 0 || _joint.getV().empty())
      return;

    ObjMesh mesh=_joint;
    mesh.getV().clear();
    for(BodyTransVTK beg(_joint.getV().begin(),b.dir(),Mat4::Identity()),end(_joint.getV().end()); beg!=end; ++beg)
      mesh.getV().push_back(*beg);
    mesh.smooth();

    StaticGeom geom(mesh.getDim());
    geom.addGeomMesh(Mat4::Identity(),mesh);

    RigidBodyMass mass(mesh);
    b._M=mass.getMass();
    b._COM=mass.getCtr();
    b._geom=geom.getGPtr(geom.nrG()-1);
  }
  const ObjMesh& _joint;
};
PRJ_END
ArticulatedBody::ArticulatedBody():Serializable(typeid(ArticulatedBody).name())
{
  _bodyMap["root"]=boost::shared_ptr<Body>(new Body);
}
ArticulatedBody::ArticulatedBody(const BodyMap& bodies):Serializable(typeid(ArticulatedBody).name())
{
  _bodyMap=bodies;
}
bool ArticulatedBody::read(istream& is,IOData* dat)
{
  StaticGeom::registerType(dat);
  registerType<DOF>(dat);
  registerType<Body>(dat);

  boost::shared_ptr<Body> root;
  readBinaryData(root,is,dat);
  readBinaryData(_frm,is);

  _bodyMap.clear();
  Body::buildMap(_bodyMap,root);
  return is.good();
}
bool ArticulatedBody::write(ostream& os,IOData* dat) const
{
  StaticGeom::registerType(dat);
  registerType<DOF>(dat);
  registerType<Body>(dat);

  writeBinaryData(_bodyMap.find("root")->second,os,dat);
  writeBinaryData(_frm,os);
  return os.good();
}
boost::shared_ptr<Serializable> ArticulatedBody::copy() const
{
  return boost::shared_ptr<Serializable>(new ArticulatedBody);
}
bool ArticulatedBody::readFrm(istream& is,IOData* dat)
{
  readBinaryData(_frm,is);
  return is.good();
}
bool ArticulatedBody::writeFrm(ostream& os,IOData* dat) const
{
  writeBinaryData(_frm,os);
  return os.good();
}
bool ArticulatedBody::readASF(istream& is,const ObjMesh& joint)
{
  string buf;
  sizeType dof=0;
  scalar mass=1, length=1, angle=1;
  boost::shared_ptr<Body> root;
  //reset
  *this=ArticulatedBody();

  //main loop
  is >> buf;
  while (is.good() && !is.eof())
    if (check(buf,":units")) {
      is >> buf;
      while (is.good() && !is.eof())
        if (check(buf,"mass"))
          is >> mass;
        else if (check(buf,"length"))
          is >> length;
        else if (check(buf,"angle")) {
          is >> buf;
          angle = check(buf,"deg") ? M_PI / 180.0f : 1.0f;
        } else if(beginsWith(buf,":"))
          break;
        else is >> buf;
    } else if (check(buf,":root")) {
      ostringstream oss;
      oss << "begin" << endl;
      oss << "id 0" << endl;
      oss << "name root" << endl;
      oss << "length 0.0" << endl;
      oss << "direction 0.0 0.0 0.0" << endl;
      oss << "axis 0.0 0.0 0.0 xyz" << endl;
      oss << "dof " << endl;

      is >> buf;
      while (is.good() && !is.eof())
        if(check(buf,"order")) {
          is >> buf;
          oss << buf << " ";
          is >> buf;
          oss << buf << " ";
          is >> buf;
          oss << buf << " ";
          is >> buf;
          oss << buf << " ";
          is >> buf;
          oss << buf << " ";
          is >> buf;
          oss << buf << endl;
          break;
        } else is >> buf;

      //oss << "limits (0 0) (0 0) (0 0) (0 0) (0 0) (0 0)" << endl;
      oss << "end" << endl;
      root.reset(new Body);
      istringstream iss(oss.str());
      root->readASF(buf,iss,length,angle,dof);
      _bodyMap["root"] = root;
    } else if (check(buf,":bonedata"))
      while (is.good() && !is.eof()) {
        boost::shared_ptr<Body> b(new Body);
        if (b->readASF(buf,is,length,angle,dof))
          _bodyMap[b->_name] = b;
        else break;
      }
    else if (check(buf,":hierarchy")) {
      is >> buf;
      bool begun = false;
      while (is.good() && !is.eof())
        if (check(buf,"begin"))
          begun = true;
        else if (check(buf,"end")) {
          //valid return state
          buildToParent(*root);
          BodyOpBuildM op(nrBody(),joint);
          transform(*root,op,NULL);
          return begun && is.good();
        } else if(_bodyMap.find(buf) != _bodyMap.end()) {
          string child;
          getline(is, child);
          istringstream iss(child);
          while (!iss.eof()) {
            iss >> child;
            if (_bodyMap.find(child) == _bodyMap.end()) {
              WARNINGV("Cannot find Body %s!", child.c_str())
              return false;
            }
            Body::addChild(_bodyMap.find(buf)->second,_bodyMap.find(child)->second);
          }
          buf.clear();
        } else is >> buf;
    } else is >> buf;
  return false;
}
bool ArticulatedBody::readAMC(istream& is)
{
  scalar angle=1;
  sizeType nrFrm,idFrm,nrBody;
  string buf,name;

  //count nrFrm
  while(getline(is,buf).good() && !is.eof()) {
    istringstream(buf) >> name;
    if(check(name,":degrees"))
      angle=M_PI / 180.0f;
    else check(name,nrFrm);
  }
  INFOV("Found %ld frames!",nrFrm)

  //read frm
  Matd frm(nrDof(),nrFrm);
  is.clear(ios::eofbit);
  is.seekg(0,ios::beg);
  getline(is,buf);
  while(is.good() && !is.eof()) {
    istringstream(buf) >> name;
    if(!check(name,idFrm)) {
      getline(is,buf);
      continue;
    }
    for(nrBody=0; getline(is,buf).good() && !is.eof() && nrBody < nrValidBody(); nrBody++) {
      istringstream iss(buf);
      iss >> name;
      if(_bodyMap.find(name) == _bodyMap.end()) {
        WARNINGV("Cannot find Body %s!", name.c_str())
        return false;
      }
      Body& b=*(_bodyMap.find(name)->second);
      for(sizeType d=0; d<(sizeType)b._dof.size(); d++) {
        const DOF& dof=b._dof[d];
        iss >> frm(dof._off,idFrm-1);
        if(dof._type > TZ)	//it is an angle
          frm(dof._off,idFrm-1)*=angle;
      }
    }
    if(nrBody != nrValidBody()) {
      WARNINGV("nrBody %ld != nrValidBody %ld!",nrBody,nrValidBody())
      return false;
    }
  }
  if(idFrm == nrFrm) {
    Matd tmp=_frm;
    _frm.resize(frm.rows(),tmp.cols()+frm.cols());
    _frm.block(0,0,tmp.rows(),tmp.cols())=tmp;
    _frm.block(0,tmp.cols(),frm.rows(),frm.cols())=frm;
    INFOV("Read %ld frames in all!",_frm.cols())
    return true;
  } else return false;
}
void ArticulatedBody::setFrm(const Matd& frm)
{
  _frm=frm;
}
void ArticulatedBody::clearFrm()
{
  _frm.resize(nrDof(),0);
}
void ArticulatedBody::randomShuffle()
{
  vector<sizeType> id(nrBody());
  for(sizeType i=0; i<nrBody(); i++)id[i]=i;
  random_shuffle(id.begin(),id.end());

  sizeType i=0;
  for(BodyMap::const_iterator beg=_bodyMap.begin(),end=_bodyMap.end(); beg!=end; beg++,i++)
    beg->second->_id=id[i];
}
Body& ArticulatedBody::root()
{
  return *(_bodyMap.find("root")->second);
}
const Body& ArticulatedBody::root() const
{
  return *(_bodyMap.find("root")->second);
}
const Matd& ArticulatedBody::frm() const
{
  return _frm;
}
sizeType ArticulatedBody::nrFrm() const
{
  return _frm.cols();
}
sizeType ArticulatedBody::nrDof() const
{
  sizeType nrDof=0;
  for(BodyMap::const_iterator beg=_bodyMap.begin(),end=_bodyMap.end(); beg!=end; beg++)
    nrDof+=(sizeType)beg->second->_dof.size();
  return nrDof;
}
sizeType ArticulatedBody::nrBody() const
{
  return (sizeType)_bodyMap.size();
}
sizeType ArticulatedBody::nrValidBody() const
{
  sizeType nrValidBody=0;
  for(BodyMap::const_iterator beg=_bodyMap.begin(),end=_bodyMap.end(); beg!=end; beg++)
    nrValidBody+=beg->second->_dof.empty() ? 0 : 1;
  return nrValidBody;
}
const Body& ArticulatedBody::findBody(sizeType id) const
{
  for(BodyMap::const_iterator beg=_bodyMap.begin(),end=_bodyMap.end(); beg!=end; beg++)
    if(beg->second->_id == id)return *(beg->second);
  return *(_bodyMap.find("root")->second);
}
const Body& ArticulatedBody::findBody(const string& name) const
{
  return *(_bodyMap.find(name)->second);
}
Body& ArticulatedBody::findBodyRef(sizeType id)
{
  return const_cast<Body&>(findBody(id));
}
Body& ArticulatedBody::findBodyRef(const string& name)
{
  return const_cast<Body&>(findBody(name));
}
void ArticulatedBody::writeFrameSeqVTK(const string& path,sizeType begId,sizeType step,const string& refName) const
{
  recreate(path);
  sizeType nrF=nrFrm();
  for(sizeType i=begId; i<nrF; i+=step) {
    ostringstream oss;
    oss << path << "/frm" << i << ".vtk";
    Cold v(_frm.col(i));
    writeFrameVTK(oss.str(),&v,refName);
  }
}
void ArticulatedBody::writeFrameVTK(const string& path,const Cold* dof,const string& refName) const
{
  BodyOpVTK op(nrBody(),path,refName);
  transform(root(),op,(void*)dof);
  if(boost::filesystem::path(path).extension().string() == ".obj" ||
     boost::filesystem::path(path).extension().string() == "obj") {
    boost::filesystem::ofstream os(path);
    op._meshAssembly.smooth();
    op._meshAssembly.write(os);
  }
}
void ArticulatedBody::writeJointsSeqVTK(const string& path,sizeType begId,sizeType step) const
{
  recreate(path);
  sizeType nrF=nrFrm();
  for(sizeType i=begId; i<nrF; i+=step) {
    ostringstream oss;
    oss << path << "/frm" << i << ".vtk";
    Cold v(_frm.col(i));
    writeJointsVTK(oss.str(),&v);
  }
}
void ArticulatedBody::writeJointsVTK(const string& path,const Cold* dof) const
{
  BodyOpT<const Body> op(nrBody());
  transform(root(),op,(void*)dof);
  VTKWriter<scalar> os("joint",path,true);
  vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
  for(sizeType i=0; i<nrBody(); i++)
    vss.push_back(op.getTrans(i).block<3,1>(0,3));
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,0,0),
                 VTKWriter<scalar>::IteratorIndex<Vec3i>(nrBody(),0,0),
                 VTKWriter<scalar>::POINT);
}
void ArticulatedBody::writeBodyConfiguration() const
{
  INFO("Begin Body Configuration!!!")
  for(sizeType i=0; i<nrBody(); i++) {
    const Body& b=findBody(i);
    cout << "Body" << i << ": " << b.name().c_str() << " [";
    for(sizeType d=0,nDof=(sizeType)b.dof().size(); d<nDof; d++)
      cout << b.dof()[d].typeStr() << (d==nDof-1 ? "" : ",");
    cout << "] ";
    if(b.hasParent())
      cout << "Parent: " << b.parent().name();
    cout << endl;
  }
  INFO("End Body Configuration!!!")
}
void ArticulatedBody::transform(Body& b,BodyOp<Body>& op,void* data)
{
  //draw current Body
  op(b,data);
  //draw children
  boost::shared_ptr<Body> curr=b._child;
  while(curr) {
    transform(*curr,op,data);
    curr=curr->_next;
  }
}
void ArticulatedBody::transform(const Body& b,BodyOp<const Body>& op,void* data) const
{
  //draw current Body
  op(b,data);
  //draw children
  boost::shared_ptr<Body> curr=b._child;
  while(curr) {
    transform(*curr,op,data);
    curr=curr->_next;
  }
}
void ArticulatedBody::debugDJoint()
{
#define NR_TEST 100
#define DELTA 1E-7f
  Cold theta,delta,theta2;
  Mat4Xd dtransN,dtrans;
  for(sizeType i=0; i<NR_TEST; i++) {
    theta=Cold::Random(nrDof());
    delta=Cold::Random(nrDof());
    theta2=theta+delta*DELTA;
    BodyOpT<Body> OP(nrBody(),nrDof());
    BodyOpT<Body> OP2(nrBody());
    transform(root(),OP,&theta);
    transform(root(),OP2,&theta2);

    dtrans=OP.getDTransDir(delta);
    dtransN=(OP2.getTrans()-OP.getTrans())/DELTA;
    INFOV("Gradient: %f Err: %f",dtrans.norm(),(dtrans-dtransN).norm())
  }
  exit(-1);
#undef DELTA
#undef NR_TEST
}
void ArticulatedBody::buildBodyM(const ObjMesh& joint)
{
  BodyOpBuildM op(nrBody(),joint);
  transform(root(),op,NULL);
}
void ArticulatedBody::buildToParent(Body& b)
{
  //build children first
  boost::shared_ptr<Body> curr=b._child;
  while(curr) {
    buildToParent(*curr);
    curr=curr->_next;
  }

  //build toParent
  Mat4 childF=b._toParent;
  Mat4 parentF=b.hasParent() ? b.parent()._toParent : Mat4::Identity();
  childF.block<3,1>(0,3)=parentF.block<3,3>(0,0)*parentF.block<3,1>(0,3);
  parentF.block<3,1>(0,3).setZero();
  b._toParent=parentF.transpose()*childF;
}
