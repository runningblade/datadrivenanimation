#include "SkinnedMeshClothUtility.h"
#include "ClothArticulatedEnergies.h"
#include "ClothCollision.h"
#include "ClothMesh.h"
#include <boost/lexical_cast.hpp>
#include <CommonFile/solvers/alglib/dataanalysis.h>

USE_PRJ_NAMESPACE

//parameter: solver stop
#define STEP_TOO_SMALL 1E-3f
//helper
//#define CLOTHSOLVER_DEBUG_MODE
SkinnedMeshClothUtility::Iterator::Iterator(SkinnedMeshClothUtility& util):_util(util),_index(0)
{
  if(_util._cloths.size() < _util._frms.size())
    _util._cloths.resize(_util._frms.size(),Cold::Zero(0));
  while(_index < (sizeType)_util._cloths.size() && _util._cloths[_index].size() == 0)
    _index++;
}
sizeType SkinnedMeshClothUtility::Iterator::index() const
{
  return _index;
}
const Cold& SkinnedMeshClothUtility::Iterator::cloth() const
{
  ASSERT(!end())
  return _util._cloths[_index];
}
const Cold& SkinnedMeshClothUtility::Iterator::feature() const
{
  ASSERT(!end())
  return _util._feats[_index];
}
const Cold& SkinnedMeshClothUtility::Iterator::theta() const
{
  ASSERT(!end())
  return _util._frms[_index];
}
void SkinnedMeshClothUtility::Iterator::operator++()
{
  _index++;
  while(_index < (sizeType)_util._cloths.size() && _util._cloths[_index].size() == 0)
    _index++;
}
bool SkinnedMeshClothUtility::Iterator::end() const
{
  for(sizeType i=_index; i<(sizeType)_util._cloths.size(); i++)
    if(_util._cloths[_index].size() > 0)
      return false;
  return true;
}
//SkinnedMeshClothUtility
SkinnedMeshClothUtility::SkinnedMeshClothUtility()
{
  setType(typeid(SkinnedMeshClothUtility).name());
}
bool SkinnedMeshClothUtility::read(istream& is,IOData* dat)
{
  SkinnedMeshUtility::read(is,dat);
  readVector(_cloths,is);
  return is.good();
}
bool SkinnedMeshClothUtility::write(ostream& os,IOData* dat) const
{
  SkinnedMeshUtility::write(os,dat);
  writeVector(_cloths,os);
  return os.good();
}
boost::shared_ptr<Serializable> SkinnedMeshClothUtility::copy() const
{
  return boost::shared_ptr<Serializable>(new SkinnedMeshClothUtility);
}
bool SkinnedMeshClothUtility::readClothOnly(const string& path)
{
  vector<Cold,Eigen::aligned_allocator<Cold> > cloths;
  boost::filesystem::ifstream is(path,ios::binary);
  readVector(cloths,is);
  if(_cloths.size() < cloths.size())
    _cloths.resize(cloths.size(),Cold::Zero(0));
  for(sizeType i=0; i<(sizeType)cloths.size(); i++)
    if(_cloths[i].size() == 0 && cloths[i].size() > 0)
      _cloths[i]=cloths[i];
  return is.good();
}
bool SkinnedMeshClothUtility::writeClothOnly(const string& path)
{
  boost::filesystem::ofstream os(path,ios::binary);
  writeVector(_cloths,os);
  return os.good();
}
bool SkinnedMeshClothUtility::readSkinnedMeshCloth(const string& path)
{
  boost::filesystem::ifstream is(path,ios::binary);
  boost::shared_ptr<IOData> dat=getIOData();
  return SkinnedMeshClothUtility::read(is,dat.get());
}
bool SkinnedMeshClothUtility::writeSkinnedMeshCloth(const string& path) const
{
  boost::filesystem::ofstream os(path,ios::binary);
  boost::shared_ptr<IOData> dat=getIOData();
  return SkinnedMeshClothUtility::write(os,dat.get());
}
void SkinnedMeshClothUtility::writeClothVTK(const string& path,const Cold* X) const
{
  if(X)
    _mesh->assignC(X);
  _mesh->writeVTKC(path);
}
//collision filter (call this before calling generate***)
void SkinnedMeshClothUtility::enableCollisionSet()
{
  _collisionSet.reset(new set<sizeType>());
}
void SkinnedMeshClothUtility::disableCollisionSet()
{
  _collisionSet=NULL;
}
void SkinnedMeshClothUtility::addCollisionJoint(sizeType id)
{
  _collisionSet->insert(id);
}
//generate cloth template
void SkinnedMeshClothUtility::generateSolver(const ObjMesh& obj,const map<sizeType,Vec3>& cons,sizeType jid,sizeType collSubd,scalar mu,scalar lambda,scalar bend,const Vec3& initExpandXZ)
{
  //rest pose
  Cold theta0=restPose();
  BodyOpT<const Body> op(_body.getBody().nrBody());
  _body.getBody().transform(_body.getBody().root(),op,&theta0);
  //setup mesh
  _mesh.reset(new ClothMesh(obj,ClothMesh::CLOTH_MESH));
  //setup solver
  _sol.reset(new ClothEnergySolver(_mesh,false));
  _sol->addEnergyKinetic();
  _sol->addEnergyMass(Vec3d(0,-9.81f,0));
  _sol->addEnergyStretch(mu,lambda);
  _sol->addEnergyBend(bend);
  //setup body constraints
  JointConstraint c;
  _jointCons.clear();
  for(map<sizeType,Vec3>::const_iterator beg=cons.begin(),end=cons.end(); beg!=end; beg++) {
    const Body& b=_body.getBody().findBody(_body.names()[jid]);
    Vec3d& pos=_mesh->_vss[beg->first]->_pos;
    pos=beg->second.cast<scalarD>();
    c._cons=_sol->fixClosestPoint(pos);
    c._rel=transformHomoInv<scalarD>(op.getTransD(b.id()),pos);
    c._jid=jid;
    _jointCons.push_back(c);
  }
  //expand
  BBox<scalar> bb=obj.getBB();
  Vec3 ctr=(bb._minC+bb._maxC)/2;
  for(sizeType v=0; v<(sizeType)_mesh->_vss.size(); v++) {
    ClothMesh::ClothVertex& V=*(_mesh->_vss[v]);
    for(sizeType d=0; d<3; d++)
      V._pos[d]=(V._pos[d]-ctr[d])*initExpandXZ[d]+ctr[d];
  }
  //setup collision
  _collE=_sol->addEnergyArticulatedBody(_body.getBody(),_collisionSet.get(),collSubd);
  INFOV("Created cloth with %ld vertices, %ld triangles!",_mesh->_vss.size(),_mesh->_tss.size())
}
bool SkinnedMeshClothUtility::solveClothForPose()
{
  _last.resize(0);
  return solveClothForPose(restPose());
}
bool SkinnedMeshClothUtility::solveClothForPose(sizeType i)
{
  //save
  Cold last=_last,lastC;
  _mesh->assembleC(&lastC);
  //solve
  if((sizeType)_cloths.size() <= i)
    _cloths.resize(i+1,Cold::Zero(0));
  if(find(_trajOff.begin(),_trajOff.end(),i) != _trajOff.end()) {
    Cold pose;
    Mat4d T0,T;
    //compute initial pose
    if(!solveClothForPose()) {
      //load
      _last=last;
      _mesh->assignC(&lastC);
      _cloths[i].resize(0);
      return false;
    }
    BodyOpT<const Body> op(_body.getBody().nrBody());
    const Body& b=_body.getBody().findBody(_body.names()[0]);
    //transform0
    pose=restPose();
    _body.getBody().transform(_body.getBody().root(),op,(void*)&pose);
    T0=op.getTransD(b.id());
    //transform
    pose=_frms[i];
    _body.getBody().transform(_body.getBody().root(),op,(void*)&pose);
    T=op.getTransD(b.id())*T0.inverse();
    for(sizeType v=0; v<(sizeType)_mesh->_vss.size(); v++)
      _mesh->_vss[v]->_pos=transformHomo<scalarD>(T,_mesh->_vss[v]->_pos);
    //set last pose
    _last=pose;
    _last.segment(3,_last.size()-7).setZero();
  }
  if(solveClothForPose(_frms[i])) {
    _mesh->assembleC(&_cloths[i]);
    return true;
  } else {
    //load
    _last=last;
    _mesh->assignC(&lastC);
    _cloths[i].resize(0);
    return false;
  }
}
bool SkinnedMeshClothUtility::solveClothForPose(const Cold& theta)
{
  scalar thres=_avgElemSz*2;
  scalar alpha=0,step=1,testAlpha,depth;
  while(true) {
    //rest pose
    BodyOpT<const Body> op(_body.getBody().nrBody());
    if(_last.size() == 0) {
      alpha=1;
      _body.getBody().transform(_body.getBody().root(),op,(void*)&theta);
      _collE->setConfiguration(op);
    } else {
      while(true) {
        testAlpha=min<scalar>(alpha+step,1);
        Cold currTheta=interp1D(_last,theta,testAlpha);
        _body.getBody().transform(_body.getBody().root(),op,(void*)&currTheta);
        _collE->setConfiguration(op);
        depth=_collE->penetrationDepth();
        INFOV("Depth: %f at alpha: %f, step: %f",depth,alpha,step)
        if(depth < thres) {
          alpha=testAlpha;
          step*=2;
          break;
        } else step/=2;
        if(step < STEP_TOO_SMALL)
          return false;
      }
    }
    //update constraints
    for(sizeType i=0; i<(sizeType)_jointCons.size(); i++) {
      const JointConstraint& c=_jointCons[i];
      Vec3d p=transformHomo<scalarD>(op.getTransD(c._jid),c._rel);
      boost::dynamic_pointer_cast<ClothFixedConstraint<ClothMesh::ClothVertex> >(c._cons)->_pos=p;
    }
    //solve
#ifdef CLOTHSOLVER_DEBUG_MODE
    _sol->eps()=0.001f;
    _sol->epsF()=0;
    //_collE->setK(1E4f);
    _sol->solve(0,STEP_MODE::QUASISTATIC,true,ClothEnergySolver::LBFGS);
#else
    _sol->eps()=0.01f;
    _sol->epsF()=0;
    //_collE->setK(1E4f);
    _sol->solve(0,STEP_MODE::QUASISTATIC,false,ClothEnergySolver::LBFGS);
#endif
    depth=_collE->penetrationDepth();
    INFOV("Optimized, depth: %f",depth)
    //_body.getBody().writeFrameVTK("body.vtk",&theta);
    //_mesh->writeVTKC("cloth.vtk");
    if(alpha == 1)
      break;
    if(step < STEP_TOO_SMALL)
      return false;
  }
  _last=theta;
  return true;
}
void SkinnedMeshClothUtility::enableCollision()
{
  _sol->addEnergy(_collE);
}
void SkinnedMeshClothUtility::disableCollision()
{
  _sol->removeEnergy(_collE);
}
void SkinnedMeshClothUtility::simulateCloth(sizeType startTraj,sizeType nrTraj,sizeType nrFrm)
{
  ASSERT_MSG(startTraj < (sizeType)_trajOff.size(),"Incorrect StartTraj!")
  sizeType start=_trajOff[startTraj];
  sizeType end=(sizeType)_frms.size();
  if(nrTraj > 0) {
    sizeType trajId=startTraj+nrTraj;
    if(trajId < (sizeType)_trajOff.size())
      end=min<sizeType>(end,_trajOff[trajId]);
  }
  if(nrFrm > 0) {
    end=min<sizeType>(end,start+nrFrm);
  }
  for(sizeType i=start; i<end; i++) {
    INFOV("Generating cloth for frame: %ld (%ld-%ld,%ld)!",i,start,end,_frms.size())
    solveClothForPose(i);
  }
}
void SkinnedMeshClothUtility::writeDatasetVTK(const string& path)
{
  recreate(path);
  for(sizeType i=0; i<(sizeType)_frms.size(); i++)
    if(i < (sizeType)_cloths.size() && (sizeType)_cloths[i].size() == (sizeType)_mesh->_vss.size()*3) {
      writeBodyVTK(path+"/frm"+boost::lexical_cast<string>(i)+".vtk",&(_frms[i]));
      writeFeatureVTK(_feats[i],path+"/feat"+boost::lexical_cast<string>(i)+".vtk",(FEATURE_TYPE)_feat);
      _mesh->assignC(&_cloths[i]);
      _mesh->writeVTKC(path+"/cloth"+boost::lexical_cast<string>(i)+".vtk");
    }
}
sizeType SkinnedMeshClothUtility::nrClothFrm() const
{
  sizeType nrFrm=0;
  for(sizeType i=0; i<(sizeType)_cloths.size(); i++)
    if(_cloths[i].size() > 0)
      nrFrm++;
  return nrFrm;
}
const ClothMesh& SkinnedMeshClothUtility::mesh() const
{
  return *_mesh;
}
ClothMesh& SkinnedMeshClothUtility::mesh()
{
  if(!_mesh)
    _mesh.reset(new ClothMesh);
  return *_mesh;
}
//hierarchical clustering
sizeType SkinnedMeshClothUtility::findTrajOff(sizeType fid) const
{
  for(sizeType i=0; i<(sizeType)_trajOff.size()-1; i++)
    if(_trajOff[i+1] > fid)
      return i;
  return (sizeType)_trajOff.size()-1;
}
SkinnedMeshClothUtility::Cluster SkinnedMeshClothUtility::cluster(const string& clothName,FEATURE_TYPE feat,sizeType MAX_CLUSTER_CAPACITY)
{
  //find feature size
  vector<sizeType> ids;
  alglib::real_2d_array xy;
  OMP_CRITICAL_ {
    Datum datum;
    SkinnedMeshClothUtility::PATTERN_TYPE pattern=parsePatternFromString(clothName);
    convertBodyToCaffe(_frms[0],datum,feat,pattern,NULL);
    //reduce ds size: ignore first IGNORE_TRAJ_FRM frames
    for(sizeType i=0; i<(sizeType)_frms.size(); i++) {
      sizeType tid=findTrajOff(i);
      if(i-_trajOff[tid] < SkinnedMeshUtility::IGNORE_TRAJ_FRM)
        continue;
      else ids.push_back(i);
    }
    //reduce ds size: resample
    {
      sizeType interval=1;
      vector<sizeType> idsFilter;
      while((sizeType)ids.size()/interval > MAX_CLUSTER_CAPACITY)
        interval++;
      for(sizeType i=0; i<(sizeType)ids.size(); i+=interval)
        idsFilter.push_back(ids[i]);
      ids.swap(idsFilter);
      INFOV("Using interval: %d!",interval)
    }
    //create dataset
    xy.setlength((int)ids.size(),datum.float_data_size());
    for(sizeType i=0; i<(sizeType)ids.size(); i++) {
      Datum datum;
      convertBodyToCaffe(_frms[ids[i]],datum,feat,pattern,NULL);
      for(int j=0; j<xy.cols(); j++)
        xy(i,j)=datum.float_data(j);
    }
    INFOV("Clustering %d points!",xy.rows())
  }
  //run ahc
  alglib::ahcreport rep;
  alglib::clusterizerstate s;
  alglib::clusterizercreate(s);
  alglib::clusterizersetpoints(s,xy,2);
  alglib::clusterizerrunahc(s,rep);
  //fillin
  Cluster ret;
  for(sizeType i=0; i<xy.rows(); i++)
    ret._nodes.push_back(ClusterNode(ids[i],-1,-1,0));
  for(sizeType i=0; i<xy.rows()-1; i++)
    ret._nodes.push_back(ClusterNode(-1,rep.z(i,0),rep.z(i,1),rep.mergedist(i)));
  return ret;
}
