#include <DataDrivenOpt/SkinnedMeshUtility.h>
#include <boost/filesystem/operations.hpp>
#include <CommonFile/Timing.h>

USE_PRJ_NAMESPACE

void generateImplicitMesh(const string& pathASF,
                          const string& pathIn,
                          const string& pathOut,
                          const string& pathSegment)
{
  disableTiming();
  ArticulatedBody body;
  {
    boost::filesystem::ifstream is(getSourcePath()+pathASF,ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    body.read(is,dat.get());
  }
  Cold theta;
  Coli jointMap;
  Coli symJointMap;
  set<sizeType> exclude;
  SkinnedMeshUsingArticulatedBody mesh;
  exclude.insert(6);
  mesh.readPkl(getSourcePath()+pathIn);
  //mesh.debugDJoint(true);
  mesh.registerASF(body,jointMap,theta,&symJointMap,0,"registerASF");
  Coli symJointMapV=mesh.detectSymmetry(make_pair(3497,3498));
  mesh.optimizeSymmetry(symJointMapV,theta,0,"symmetry");
  mesh.generateImplicitRigidBody(jointMap,theta,&symJointMap,0,&exclude,0.5f,0,getSourcePath()+"/"+pathSegment);
  {
    boost::filesystem::ofstream os(getSourcePath()+pathOut,ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    mesh.write(os,dat.get());
  }
}
void generateImplicitMesh()
{
  generateImplicitMesh("/all_asfamc/subjects/01/01.asfb",
                       "/models/basicModel_f_lbs_10_207_0_v1.0.0.pkl_ascii",
                       "/models/basicModel_f.data",
                       "/models/segment_f");
  generateImplicitMesh("/all_asfamc/subjects/01/01.asfb",
                       "/models/basicmodel_m_lbs_10_207_0_v1.0.0.pkl_ascii",
                       "/models/basicmodel_m.data",
                       "/models/segment_m");
  exit(EXIT_SUCCESS);
}
void readDataset(const string& pathModel,
                 const string& pathSubject,
                 const string& pathOut,
                 bool hasTranslation)
{
  if(boost::filesystem::exists(pathOut)) {
    INFOV("Dataset %s exists!",pathOut.c_str())
    return;
  }
  //read model
  INFOV("Resampling %s!",pathOut.c_str())
  SkinnedMeshUtility utility;
  boost::filesystem::ifstream is(pathModel,ios::binary);
  boost::shared_ptr<IOData> dat=getIOData();
  utility.getBody().read(is,dat.get());
  //read frames
  for(boost::filesystem::directory_iterator beg(pathSubject),end; beg!=end; beg++)
    if(beg->path().extension().string() == ".samcb" || beg->path().extension().string() == "samcb") {
      utility.resampleDataset(beg->path().string(),0.1f,hasTranslation,true,1E-3f,SkinnedMeshUtility::LIMB_DIRECTION,-1);
      //utility.writeDatasetVTK("dataset");
      //exit(EXIT_FAILURE);
    }
  //output
  utility.writeSkinnedMesh(pathOut);
}
void resampleDataset()
{
  boost::filesystem::create_directory(getSourcePath()+"/datasetStaticFemale");
  for(boost::filesystem::directory_iterator beg(getSourcePath()+"/all_asfamc/subjectsskinnedFemale/"),end; beg!=end; beg++) {
    if(!boost::filesystem::is_directory(beg->path()))
      continue;
    readDataset(getSourcePath()+"/models/basicModel_f.data",
                beg->path().string(),
                getSourcePath()+"/datasetStaticFemale/"+beg->path().filename().string()+".data",false);
  }
  boost::filesystem::create_directory(getSourcePath()+"/datasetStaticMale");
  for(boost::filesystem::directory_iterator beg(getSourcePath()+"/all_asfamc/subjectsskinnedMale/"),end; beg!=end; beg++) {
    if(!boost::filesystem::is_directory(beg->path()))
      continue;
    readDataset(getSourcePath()+"/models/basicmodel_m.data",
                beg->path().string(),
                getSourcePath()+"/datasetStaticMale/"+beg->path().filename().string()+".data",false);
  }
}
void rescaleDataset()
{
  boost::filesystem::create_directory(getSourcePath()+"/datasetStaticFemaleRescaled");
  for(boost::filesystem::directory_iterator beg(getSourcePath()+"/all_asfamc/subjectsskinnedFemale/"),end; beg!=end; beg++) {
    if(!boost::filesystem::is_directory(beg->path()))
      continue;
    string pathOut=getSourcePath()+"/datasetStaticFemale/"+beg->path().filename().string()+".data";
    string pathOutRescaled=getSourcePath()+"/datasetStaticFemaleRescaled/"+beg->path().filename().string()+".data";
    if(!boost::filesystem::exists(pathOut))
      continue;
    INFOV("Rescaling %s->%s!",pathOut.c_str(),pathOutRescaled.c_str())
    SkinnedMeshUtility utility;
    utility.readSkinnedMesh(pathOut);
    utility.uniformScale(1);
    utility.writeSkinnedMesh(pathOutRescaled);
  }
  boost::filesystem::create_directory(getSourcePath()+"/datasetStaticMaleRescaled");
  for(boost::filesystem::directory_iterator beg(getSourcePath()+"/all_asfamc/subjectsskinnedMale/"),end; beg!=end; beg++) {
    if(!boost::filesystem::is_directory(beg->path()))
      continue;
    string pathOut=getSourcePath()+"/datasetStaticMale/"+beg->path().filename().string()+".data";
    string pathOutRescaled=getSourcePath()+"/datasetStaticMaleRescaled/"+beg->path().filename().string()+".data";
    if(!boost::filesystem::exists(pathOut))
      continue;
    INFOV("Rescaling %s->%s!",pathOut.c_str(),pathOutRescaled.c_str())
    SkinnedMeshUtility utility;
    utility.readSkinnedMesh(pathOut);
    utility.uniformScale(1);
    utility.writeSkinnedMesh(pathOutRescaled);
  }
}
int main(int argc, char *argv[])
{
  //STEP 1: generate implicit mesh
  //generateImplicitMesh();
  //STEP 2: avoid collision
  //resampleDataset();
  //STEP 3: uniform scale/recompute feature
  rescaleDataset();
}
