#include <DataDrivenOpt/Utils.h>
#include <DataDrivenOpt/CaffeUtils.h>

USE_PRJ_NAMESPACE

int main(int argc, char *argv[])
{
  if(argc < 4) {
    WARNING("Usage: mainVisualizeImageDataCaffe clothMesh folder prefix [mode] [notifyInterval]!")
    return 0;
  }
  bool addMode=false;
  bool checkMode=false;
  bool visualizeMode=true;
  sizeType notifyInterval=1000;
  map<string,sizeType> keys;
  if(argc > 4) {
    if(boost::filesystem::exists(argv[4])) {
      string line;
      sizeType k=0;
      boost::filesystem::ifstream is(argv[4]);
      while(getline(is,line)) {
        while(line.back() == '\n')
          line.pop_back();
        keys[line]=k++;
      }
    } else if(string(argv[4]) == "addMode") {
      if(argc > 5)
        notifyInterval=boost::lexical_cast<sizeType>(argv[5]);
      visualizeMode=false;
      addMode=true;
    } else if(string(argv[4]) == "checkMode") {
      visualizeMode=false;
      checkMode=true;
    }
  }
  //analyze datasetName
  string clothName,datasetName=string(argv[2]);
  string prefix=string(argv[3]);
  sizeType pos=datasetName.find("_");
  if(pos < 0) {
    INFO("Cannot find datasetName!")
    return 0;
  }
  clothName=datasetName.substr(0,pos);
  datasetName=datasetName.substr(pos+1);
  INFOV("Found datasetName: %s!",datasetName.c_str())
  //read property
  SkinnedMeshClothUtility::RenderProperty property("Media/renderProperty.xml");
  //read db
  google::InitGoogleLogging(argv[0]);
  boost::shared_ptr<db::DB> db(db::GetDB("leveldb"));
  db->Open(getSourcePath()+"/"+prefix+"_"+string(argv[2]),db::READ);
  boost::shared_ptr<db::Cursor> c(db->NewCursor());
  //create db if addMode
  boost::shared_ptr<db::DB> dbImg(db::GetDB("leveldb"));
  boost::shared_ptr<db::Transaction> txnImg;
  if(addMode)
    reopenDB(*dbImg,getSourcePath()+"/image_"+prefix+"_"+string(argv[2]));
  //read db
  recreate("visualizationImage");
  SkinnedMeshClothUtility body;
  {
    boost::filesystem::ifstream is(argv[1],ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    string bodyPath=getSourcePath()+"/"+datasetName+"/01.data";
    INFOV("Reading skinned mesh: %s!",bodyPath.c_str())
    body.readSkinnedMesh(bodyPath);
    INFOV("Reading cloth mesh template: %s!",argv[1])
    body.mesh().read(is,dat.get());
  }
  //evaluate
  sizeType k=0,kg=0,txnTerms=0;
  SkinnedMeshClothUtility::PATTERN_TYPE pattern=body.parsePatternFromString(clothName);
  boost::filesystem::ofstream os("visualizationImage.dat");
  for(; c->valid(); c->Next()) {
    DatasetKey keyS(c->key());
    if(!keys.empty() && keys.find(c->key()) == keys.end())
      continue;
    //process
    vector<Datum> datums;
    vector<string> images;
    set<sizeType> filter;
    keyS.decompressDatums(datums,images,c->value());
    //draw image
    k=keys.empty() ? (kg++) : keys[c->key()];
    SkinnedMeshUtility::FEATURE_TYPE feat=SkinnedMeshUtility::parseFeatureFromString(keyS._featStr,&filter);
    SkinnedMeshUtility::pruneFootAndHand(filter);
    string fname="visualizationImage/color"+boost::lexical_cast<string>(k)+".jpg";
    if(visualizeMode) {
      body.drawClothImageFile(property,datums,feat,&filter,pattern,fname);
      cout << k << ": " << c->key().c_str() << endl;
      os << k << ": " << c->key().c_str() << endl;
    } else if(addMode) {
      images.resize(1);
      string key=keyS.strPrefix(),value;
      body.drawClothImage(property,datums,feat,&filter,pattern,images[0]);
      DatasetKey::compressDatums(datums,images,key,value);
      if(!txnImg) {
        txnImg.reset(dbImg->NewTransaction());
        txnTerms=0;
      }
      txnImg->Put(key,value);
      txnTerms++;
      if(txnTerms >= notifyInterval) {
        txnImg->Commit();
        txnImg.reset(dbImg->NewTransaction());
        INFOV("Put %d items",txnTerms)
        txnTerms=0;
      }
    } else if(checkMode) {
      if(images.size() != 1) {
        WARNINGV("Cannot find image for datum: %s!",c->key().c_str())
      } else {
        boost::filesystem::ofstream os(fname,ios::binary);
        os.write(&images[0][0],images[0].size());
      }
    }
  }
  //finish db
  c=NULL;
  db=NULL;
  if(txnImg) {
    txnImg->Commit();
    txnImg=NULL;
  }
}
