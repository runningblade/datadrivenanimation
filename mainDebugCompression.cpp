#include "DataDrivenOpt/CaffeUtils.h"

USE_PRJ_NAMESPACE

extern "C"
{
  void debugPythonAPI();

  void closeDB(void* handle);
  void* openDB(const char* path);
  bool nextDB(void* handle);

  void closeDBPrefetch(void* handle);
  void* openDBPrefetch(const char* path);
  bool nextDBPrefetch(void* handle);
  void debugPythonPrefetchAPI();
}

int main(int argc, char *argv[])
{
  //DatasetKey::debugCompression(5);
  //debugPythonAPI();
  //debugPythonPrefetchAPI();

  DatasetKey::debugFindRemove("leveldb");
  DatasetKey::debugFindRemove("lmdb");

  /*for(int i=0;i<1000;i++) {
    INFO("Start")
    void* handle=openDBPrefetch("Media/leveldb2_mainClothDressTightScripts_datasetStaticFemaleRescaled");
    for(int i=0;i<10000;i++)
      nextDBPrefetch(handle);
    closeDBPrefetch(handle);
    INFO("End")
  }*/
}
