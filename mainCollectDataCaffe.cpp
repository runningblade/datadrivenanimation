#include <DataDrivenOpt/Utils.h>
#include <DataDrivenOpt/CaffeUtils.h>

USE_PRJ_NAMESPACE

int main(int argc, char *argv[])
{
  if(argc < 4) {
    WARNING("Usage: mainCollectDataCaffe clothMesh folder feature_type [keys] [prefix]!")
    return 0;
  }
  set<string> keys;
  if(argc > 4) {
    std::string line;
    boost::filesystem::ifstream is(argv[4]);
    while(std::getline(is,line))
      keys.insert(line);
  }
  string prefix="leveldb";
  if(argc > 5) {
    prefix=argv[5];
  }
  //analyze datasetName
  string clothName,datasetName=string(argv[2]);
  sizeType pos=datasetName.find("_");
  if(pos < 0) {
    INFO("Cannot find datasetName!")
    return 0;
  }
  clothName=datasetName.substr(0,pos);
  datasetName=datasetName.substr(pos+1);
  sizeType feat=SkinnedMeshUtility::parseFeatureFromString(argv[3]);
  INFOV("Found datasetName: %s, feat: %s!",datasetName.c_str(),argv[3])
  //create db
  google::InitGoogleLogging(argv[0]);
  boost::shared_ptr<db::DB> db(db::GetDB("leveldb"));
  reopenDB(*db,getSourcePath()+"/"+prefix+boost::lexical_cast<string>(feat)+"_"+string(argv[2]));
  //read files
  for(boost::filesystem::directory_iterator beg(argv[2]),end; beg!=end; beg++)
    if(boost::filesystem::is_directory(beg->path())) {
      string name=beg->path().filename().string();
      sizeType pos=name.find_first_of("_");
      if(pos > 0) {
        INFOV("Found subject: %s!",name.c_str())
        string idDataset=name.substr(0,pos);
        int idTraj=boost::lexical_cast<int>(name.substr(pos+1));
        string bodyPath=getSourcePath()+"/"+datasetName+"/"+idDataset+".data";
        //read clothData (frames)
        if(!boost::filesystem::exists(beg->path().string()+"/clothData.data"))
          continue;
        //read body/clothMesh topology
        SkinnedMeshClothUtility body;
        {
          boost::filesystem::ifstream is(argv[1],ios::binary);
          boost::shared_ptr<IOData> dat=getIOData();
          INFOV("Reading skinned mesh: %s!",bodyPath.c_str())
          body.readSkinnedMesh(bodyPath);
          INFOV("Reading cloth mesh template: %s!",argv[1])
          body.mesh().read(is,dat.get());
          INFOV("Reading cloth data: %s!",(beg->path().string()+"/clothData.data").c_str())
          body.readClothOnly(beg->path().string()+"/clothData.data");
          if(body.nrClothFrm() < SkinnedMeshUtility::TRAJ_LENGTH_THRES+SkinnedMeshUtility::IGNORE_TRAJ_FRM) {
            WARNINGV("Only %ld frames found, ignoring short trajectory!",body.nrClothFrm())
            continue;
          }
          if(!body.isTrajBodyUpright(idTraj)) {
            WARNING("Body no upright, ignoring misleading trajectory!")
            continue;
          }
        }
        //insert the dataset
        sizeType fid=0;
        boost::shared_ptr<db::Transaction> txn(db->NewTransaction());
        SkinnedMeshClothUtility::PATTERN_TYPE pattern=body.parsePatternFromString(clothName);
        for(SkinnedMeshClothUtility::Iterator it(body); !it.end(); ++it,fid++) {
          if(fid < SkinnedMeshUtility::IGNORE_TRAJ_FRM)
            continue;
          string featStr,keyStr=name+"_"+boost::lexical_cast<string>(it.index());
          string clusterStr=idDataset+"_"+boost::lexical_cast<string>(it.index()),out;
          if(!keys.empty()) {
            if(keys.find(clusterStr) == keys.end())
              continue;
            else {
              INFOV("Inserting clustered key: %s!",clusterStr.c_str())
            }
          }
          //collect cloth/body features
          vector<Datum> datums;
          vector<string> images;
          body.convertClothToCaffe(it.cloth(),it.theta(),datums,pattern);
          datums.push_back(Datum());
          featStr=body.convertBodyToCaffe(it.theta(),datums.back(),(SkinnedMeshUtility::FEATURE_TYPE)feat,pattern,NULL);
          //combine/insert
          keyStr+="_"+featStr;
          //INFOV("Inserting: %s!",keyStr.c_str())
          ASSERT_MSG(COMMON::DatasetKey::compressDatums(datums,images,keyStr,out),"Compression failed!")
          txn->Put(keyStr,out);
        }
        txn->Commit();
        txn=NULL;
      }
    }
  //finish db
  db=NULL;
}
