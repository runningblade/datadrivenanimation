#include <DataDrivenOpt/Utils.h>
#include <DataDrivenOpt/CaffeUtils.h>

USE_PRJ_NAMESPACE

int main(int argc, char *argv[])
{
  if(argc < 4) {
    WARNING("Usage: mainVisualizeDataCaffe clothMesh folder prefix [keys]!")
    return 0;
  }
  map<string,sizeType> keys;
  if(argc > 4) {
    string line;
    sizeType k=0;
    boost::filesystem::ifstream is(argv[4]);
    while(getline(is,line)) {
      while(line.back() == '\n')
        line.pop_back();
      keys[line]=k++;
    }
  }
  //analyze datasetName
  string clothName,datasetName=string(argv[2]);
  string prefix=string(argv[3]);
  sizeType pos=datasetName.find("_");
  if(pos < 0) {
    INFO("Cannot find datasetName!")
    return 0;
  }
  clothName=datasetName.substr(0,pos);
  datasetName=datasetName.substr(pos+1);
  INFOV("Found datasetName: %s!",datasetName.c_str())
  //create db
  google::InitGoogleLogging(argv[0]);
  boost::shared_ptr<db::DB> db(db::GetDB("leveldb"));
  db->Open(getSourcePath()+"/"+prefix+"_"+string(argv[2]),db::READ);
  boost::shared_ptr<db::Cursor> c(db->NewCursor());
  //read db
  recreate("visualization");
  SkinnedMeshClothUtility body;
  {
    boost::filesystem::ifstream is(argv[1],ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    string bodyPath=getSourcePath()+"/"+datasetName+"/01.data";
    INFOV("Reading skinned mesh: %s!",bodyPath.c_str())
    body.readSkinnedMesh(bodyPath);
    INFOV("Reading cloth mesh template: %s!",argv[1])
    body.mesh().read(is,dat.get());
  }
  //evaluate
  sizeType k=0,kg=0;
  boost::filesystem::ofstream os("visualization.dat");
  for(; c->valid(); c->Next()) {
    DatasetKey keyS(c->key());
    if(!keys.empty() && keys.find(c->key()) == keys.end())
      continue;
    //process
    vector<Datum> datums;
    vector<string> images;
    set<sizeType> filter;
    keyS.decompressDatums(datums,images,c->value());
    //draw
    k=keys.empty() ? (kg++) : keys[c->key()];
    for(sizeType i=0; i<(sizeType)datums.size()-1; i++)
      SkinnedMeshClothUtility::drawClothVTK(datums[i],"visualization/cloth"+boost::lexical_cast<string>(i)+"_"+boost::lexical_cast<string>(k)+".vtk");
    SkinnedMeshUtility::FEATURE_TYPE feat=SkinnedMeshUtility::parseFeatureFromString(keyS._featStr,&filter);
    body.writeFeatureVTK(datums.back(),"visualization/feature"+boost::lexical_cast<string>(k)+".vtk",feat,&filter);
    cout << k << ": " << c->key().c_str() << endl;
    os << k << ": " << c->key().c_str() << endl;
    os.flush();
  }
  //finish db
  c=NULL;
  db=NULL;
}
