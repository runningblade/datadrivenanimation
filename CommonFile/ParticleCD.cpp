#include "ParticleCD.h"

PRJ_BEGIN

template <int DIM>
void debugParticleCollisionInterface(sizeType N,const Vec3i& warpMode)
{
  BBox<scalar> bb;
  CollisionInterfaceDirect cd;
  std::vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
  for(sizeType i=0; i<N; i++) {
    vss.push_back(Vec3::Zero());
    vss.back().segment<DIM>(0).setRandom();
    bb.setUnion(vss.back());
  }
  bb.enlarged(0.01f,DIM);
  scalar rad=0.1f;
  cd.reset(bb,(Vec3)Vec3::Constant(rad));
  cd.prepare(vss);
  for(sizeType d=0; d<DIM; d++)
    if(warpMode[d] > 0)
      cd.setWarp((unsigned char)d);
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<N; i++) {
    set<sizeType> neigh=cd.getNeigh(vss,vss[i],rad*rad);
    set<sizeType> neighBF=cd.getNeighBF(vss,vss[i],rad*rad);
    ASSERT(neigh == neighBF && cd.hasNeigh(vss,vss[i],rad*rad) == cd.hasNeighBF(vss,vss[i],rad*rad))
  }
}
template <int DIM>
void debugParticleCollisionInterfaceAllWarp(sizeType N)
{
  INFO("Debugging ParticleCD all warp!")
  debugParticleCollisionInterface<DIM>(N,Vec3i::Zero());
  for(sizeType d=0; d<DIM; d++)
    debugParticleCollisionInterface<DIM>(N,Vec3i::Unit(d));
}
void debugParticleCollisionInterfaceAllDIM(sizeType N)
{
  INFO("Debugging ParticleCD all DIM!")
  debugParticleCollisionInterfaceAllWarp<2>(N);
  debugParticleCollisionInterfaceAllWarp<3>(N);
}

PRJ_END
