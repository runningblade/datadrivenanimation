#include "Timing.h"
#include <vector>
#include <boost/timer/timer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/ptree.hpp>

PRJ_BEGIN

bool __timing=true;
std::vector<char> __timingSetting;
std::string __name="";
boost::timer::cpu_timer __timer;

void saveTimingSetting()
{
  __timingSetting.push_back(__timing);
}
void loadTimingSetting()
{
  ASSERT_MSG(!__timingSetting.empty(),"loading timing setting with no setting available!")
  __timing=(bool)__timingSetting.back();
}
void enableTiming()
{
  __timing=true;
}
void disableTiming()
{
  __timing=false;
}
std::string TGETNAME(const std::string& path,const std::string& pathFrm,boost::property_tree::ptree& pt)
{
  std::string pathReal=path;
  sizeType fid=pt.get<sizeType>(pathFrm,0);
  boost::replace_all(pathReal,"#",boost::lexical_cast<std::string>(fid));
  return pathReal;
}
void TFRMRESET(const std::string& pathFrm,boost::property_tree::ptree& pt)
{
  //if(!__timing)
  //  return;
  pt.put<sizeType>(pathFrm,0);
}
void TFRMADVANCE(const std::string& pathFrm,boost::property_tree::ptree& pt)
{
  //if(!__timing)
  //  return;
  sizeType fid=pt.get<sizeType>(pathFrm,0);
  pt.put<sizeType>(pathFrm,fid+1);
}

void TBEG()
{
  if(!__timing)
    return;
  __timer.start();
}
void TBEG(const std::string& name)
{
  if(!__timing)
    return;
  __name=name;
  __timer.start();
}
void TENDT(const std::string& path,boost::property_tree::ptree& pt)
{
  if(!__timing)
    return;
  __timer.stop();
  pt.put<double>(path,__timer.elapsed().wall/1000000000.0L);
}
void TENDT(const std::string& path,const std::string& pathFrm,boost::property_tree::ptree& pt)
{
  if(!__timing)
    return;
  TENDT(TGETNAME(path,pathFrm,pt),pt);
}
void TEND(std::ostream& os)
{
  if(!__timing)
    return;
  __timer.stop();
  os << __name << ": " << __timer.format() << std::endl;
}
void TEND()
{
  TEND(std::cout);
}

PRJ_END
