#ifndef COLLISION_DETECTION_H
#define COLLISION_DETECTION_H

#include "MathBasic.h"
#include "IO.h"

PRJ_BEGIN

template <typename T>
class LineSegTpl
{
public:
  typedef typename Eigen::Matrix<T,3,1> PT;
  typedef typename Eigen::Matrix<T,2,1> PT2;
  typedef typename Eigen::Matrix<T,2,2> MAT2;
public:
  LineSegTpl(const PT& x,const PT& y);
  T length() const;
  PT circumcenter() const;
  PT masscenter() const;
  PT normal() const;
  PT gradDir(sizeType id) const;
  T signedArea() const;
  bool intersect(const LineSegTpl<T>& l,T& t,bool infinite=false) const;
  void calcPointDist(const PT& pt,T& sqrDistance,PT& cp,PT& b) const;
  void writeVTK(VTKWriter<T>& os) const;
public:
  //data
  PT _x;
  PT _y;
};
template <typename T>
class PlaneTpl
{
public:
  typedef typename Eigen::Matrix<T,3,1> PT;
  typedef typename Eigen::Matrix<T,2,1> PT2;
  PlaneTpl();
  PlaneTpl(const PT& x0,const PT& n);
  PlaneTpl(const PT& a,const PT& b,const PT& c);
  T side(const PT& p) const;
  bool intersect(const BBox<T>& bb) const;
  PT2 project(const PT& d) const;
  void writeVTK(VTKWriter<T>& os) const;
public:
  //data
  PT _x0;
  PT _n;
};
template <typename T>
class TriangleTpl
{
public:
  typedef typename Eigen::Matrix<T,3,1> PT;
  typedef typename Eigen::Matrix<T,2,1> PT2;
  typedef typename Eigen::Matrix<T,2,2> MAT2;
  typedef typename Eigen::Matrix<T,3,3> MAT3;
public:
  TriangleTpl();
  TriangleTpl(const PT& a,const PT& b,const PT& c);
  PT circumcenter() const;
  PT masscenter() const;
  PT bary(const PT& pt) const;
  T area() const;
  T signedVolume() const;
  PT normal() const;
  PT gradDir(sizeType id) const;
  PT height(const PT& a,const PT& b,const PT& c) const;
  //intersect
  bool isInside(const PT& pt) const;
  bool isPlanePointInside(const PT& pt) const;
  bool updateIntr(T& s,T& t,T num,T denom) const;
  bool intersect(const LineSegTpl<T>& l,T& s,T& t) const;
  bool intersect(const LineSegTpl<T>& l,T& t,bool infinite=false) const;
  bool intersect(const TriangleTpl<T>& t) const;
  bool intersect(const BBox<T>& bb) const;
  bool calcLineDist(const LineSegTpl<T>& l,PT& bt,PT2& bl) const;
  template <typename PT_BARY>
  void calcPointDist(const PT& pt,T& sqrDistance,PT& cp,PT_BARY& b) const;
  PT2 project(const PT& d) const;
  void writeVTK(VTKWriter<T>& os) const;
public:
  //data
  PT _a;
  PT _b;
  PT _c;
};
template <typename T>
class TetrahedronTpl
{
public:
  typedef typename Eigen::Matrix<T,2,1> PT2;
  typedef typename Eigen::Matrix<T,3,1> PT;
  typedef typename Eigen::Matrix<T,4,1> PT4;
  typedef typename Eigen::Matrix<T,6,1> PT6;
  typedef typename Eigen::Matrix<T,3,3> MAT3;
public:
  TetrahedronTpl();
  TetrahedronTpl(const PT& a,const PT& b,const PT& c,const PT& d);
  PT circumcenter() const;
  PT masscenter() const;
  PT4 bary(const PT& pt) const;
  bool isInside(const PT& pt) const;
  T volume() const;
  bool dualCellVolume(PT4& dv);
  static bool dualFaceVolume(T& va,T& vb,T& vc,const PT& a,const PT& b,const PT& c,const PT& cc);
  static T dihedralAngle(const PT& a,const PT& b,const PT& c,const PT& d);
  PT6 dihedralAngleTet();
  //intersection
  bool calcLineDist(const LineSegTpl<T>& l,PT4& bt,PT2& bl) const;
  void calcPointDist(const PT& pt,T& sqrDistance,PT& cp,PT4& bc) const;
  void getPlane(const sizeType& i,PlaneTpl<T>& p,Vec3i& ind) const;
  const PT& getNode(const sizeType& i) const;
  void writeVTK(VTKWriter<T>& os) const;
public:
  //data
  PT _a;
  PT _b;
  PT _c;
  PT _d;
  bool _swap;
};
template <typename T,int dim>
class OBBTpl;
template <typename T>
class OBBTpl<T,2>
{
public:
  typedef typename Eigen::Matrix<T,2,1> PT;
  typedef typename Eigen::Matrix<T,3,1> PT3;
  typedef typename Eigen::Matrix<T,2,2> MAT;
  typedef typename Eigen::Matrix<T,3,3> MAT3;
public:
  OBBTpl();
  OBBTpl(const BBox<T,2>& bb);
  OBBTpl(const BBox<T,3>& bb);
  OBBTpl(const MAT& rot,const PT& trans,const BBox<T,2>& bb);
  OBBTpl(const MAT3& rot,const PT3& trans,const BBox<T,3>& bb);
  OBBTpl(const MAT& rot,const PT& trans,const PT& ext);
  OBBTpl(const MAT3& rot,const PT3& trans,const PT3& ext);
  bool intersect(const OBBTpl<T,2>& other) const;
  bool intersect(const BBox<T,2>& other) const;
  void writeVTK(VTKWriter<T>& os) const;
public:
  MAT _rot;
  PT _trans;
  PT _ext;
};
template <typename T>
class OBBTpl<T,3>
{
public:
  typedef typename Eigen::Matrix<T,3,1> PT;
  typedef typename Eigen::Matrix<T,2,1> PT2;
  typedef typename Eigen::Matrix<T,3,3> MAT;
public:
  OBBTpl();
  OBBTpl(const BBox<T,3>& bb);
  OBBTpl(const MAT& rot,const PT& trans,const BBox<T,3>& bb);
  OBBTpl(const MAT& rot,const PT& trans,const PT& ext);
  bool intersect(const OBBTpl<T,3>& other) const;
  bool intersect(const BBox<T,3>& other) const;
  void writeVTK(VTKWriter<T>& os) const;
public:
  MAT _rot;
  PT _trans;
  PT _ext;
};
template <typename T>
class KDOP18 : public Serializable
{
public:
  typedef typename Eigen::Matrix<T,3,1> PT;
public:
  KDOP18();
  KDOP18(const PT& v);
  KDOP18(const PT& a,const PT& b);
  bool write(std::ostream& os,IOData* dat) const;
  bool read(std::istream& is,IOData* dat);
  boost::shared_ptr<Serializable> copy() const;
  void reset();
  void empty();
  void enlarged(T len);
  void enlarged(T len,sizeType dim);
  void setPoints(const PT& a,const PT& b,const PT& c);
  void setUnion(const PT& p);
  void setUnion(const KDOP18& b);
  PT minCorner() const;
  PT maxCorner() const;
  bool intersect(const KDOP18& b,sizeType DIM=3) const;
  bool intersect(const BBox<T>& b,sizeType DIM=3) const;
  bool contain(const PT& p) const;
protected:
  static void getDistances(const PT& p,T &d3, T &d4, T &d5, T &d6, T &d7, T &d8);
  static void getDistances(const PT& p, T d[]);
  static T getDistances(const PT &p, int i);
  T _dist[18];
};
template <typename T>
class Sphere
{
public:
  typedef typename Eigen::Matrix<T,3,1> PT;
  Sphere();
  Sphere(const PT& ctr,const T& rad);
  bool intersect(const PT& a,const PT& b) const;
  //data
  PT _ctr;
  T _rad;
};

typedef LineSegTpl<scalar> LineSeg;
typedef PlaneTpl<scalar> Plane;
typedef TriangleTpl<scalar> Triangle;
typedef TetrahedronTpl<scalar> Tetrahedron;
typedef OBBTpl<scalar,2> OBB2D;
typedef OBBTpl<scalar,3> OBB3D;

PRJ_END

#endif
