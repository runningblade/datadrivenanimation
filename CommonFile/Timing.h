#ifndef TIMING_H
#define TIMING_H

#include "Config.h"
#include <iostream>
#include <boost/property_tree/ptree_fwd.hpp>

PRJ_BEGIN

//timing
void saveTimingSetting();
void loadTimingSetting();
void enableTiming();
void disableTiming();
std::string TGETNAME(const std::string& path,const std::string& pathFrm,boost::property_tree::ptree& pt);
void TFRMRESET(const std::string& pathFrm,boost::property_tree::ptree& pt);
void TFRMADVANCE(const std::string& pathFrm,boost::property_tree::ptree& pt);

void TBEG();
void TBEG(const std::string& name);
void TENDT(const std::string& path,boost::property_tree::ptree& pt);
void TENDT(const std::string& path,const std::string& pathFrm,boost::property_tree::ptree& pt);
void TEND(std::ostream& os);
void TEND();

PRJ_END

#endif
