#include <DataDrivenOpt/SkinnedMesh.h>
#include <CommonFile/MakeMesh.h>
#include <boost/filesystem/operations.hpp>

USE_PRJ_NAMESPACE

void convertMocap(SkinnedMeshUsingArticulatedBody& mesh,const string& folder,bool startOver,bool noAMC)
{
  //convert
  Cold theta;
  Coli jointMap;
  ObjMesh joint;
  MakeMesh::makeSphere3D(joint,0.2f,32);

  boost::filesystem::path path=getSourcePath()+"/all_asfamc/subjects";
  boost::filesystem::path pathSkinned=path.string()+folder;
  if(startOver)
    recreate(pathSkinned);
  else create(pathSkinned);
  for(boost::filesystem::directory_iterator beg(path),end; beg!=end; beg++) {
    if(!boost::filesystem::is_directory(beg->path()))
      continue;
    boost::filesystem::path subjectDir=pathSkinned/beg->path().filename();
    create(subjectDir);
    //find asf
    bool hasASF=false;
    ArticulatedBody body;
    for(boost::filesystem::directory_iterator begS(beg->path()),endS; begS!=endS; begS++) {
      if(!boost::filesystem::is_regular_file(begS->path()))
        continue;
      if(begS->path().extension().string() == ".asf" || begS->path().extension().string() == "asf") {
        boost::filesystem::path pathASF=begS->path();
        boost::filesystem::path pathASFTo=(subjectDir/pathASF.filename()).replace_extension(".sasfb");
        INFOV("Registering: %s -> %s!",pathASF.string().c_str(),pathASFTo.string().c_str())
        //readBody
        boost::filesystem::path pathASFB=pathASF;
        pathASFB.replace_extension(".asfb");
        if(!boost::filesystem::exists(pathASFB)) {
          boost::filesystem::ifstream is(pathASF);
          body.readASF(is,joint);
          //save for fast later access
          boost::filesystem::ofstream os(pathASFB,ios::binary);
          boost::shared_ptr<IOData> dat=getIOData();
          body.write(os,dat.get());
        } else {
          boost::filesystem::ifstream is(pathASFB,ios::binary);
          boost::shared_ptr<IOData> dat=getIOData();
          body.read(is,dat.get());
        }
        //convert
        if(!boost::filesystem::exists(pathASFTo)) {
          mesh.registerASF(body,jointMap,theta,NULL,-1,pathASFTo.parent_path().string());
          boost::filesystem::ofstream os(pathASFTo,ios::binary);
          boost::shared_ptr<IOData> dat=getIOData();
          mesh.write(os,dat.get());
        } else {
          boost::filesystem::ifstream is(pathASFTo,ios::binary);
          boost::shared_ptr<IOData> dat=getIOData();
          mesh.read(is,dat.get());
        }
        hasASF=true;
        break;
      }
    }
    if(!hasASF || noAMC)
      continue;
    //find amc
    for(boost::filesystem::directory_iterator begS(beg->path()),endS; begS!=endS; begS++) {
      if(!boost::filesystem::is_regular_file(begS->path()))
        continue;
      if(begS->path().extension().string() == ".amc" || begS->path().extension().string() == "amc") {
        boost::filesystem::path pathAMC=begS->path();
        boost::filesystem::path pathAMCTo=(subjectDir/pathAMC.filename()).replace_extension(".samcb");
        INFOV("\tRegistering: %s -> %s!",pathAMC.string().c_str(),pathAMCTo.string().c_str())
        //convert
        if(!boost::filesystem::exists(pathAMCTo)) {
          {
            boost::filesystem::ifstream is(pathAMC);
            body.clearFrm();
            body.readAMC(is);
          }
          mesh.registerAMC(body,jointMap,theta,"");
          boost::filesystem::ofstream os(pathAMCTo,ios::binary);
          boost::shared_ptr<IOData> dat=getIOData();
          mesh.getBody().writeFrm(os,dat.get());
        }
      }
    }
  }
}
int main(int argc, char *argv[])
{
//#define DEBUG_REGISTER
#ifdef DEBUG_REGISTER
  { //this is for test or debug
    ArticulatedBody body;
    {
      boost::filesystem::ifstream is(getSourcePath()+"/all_asfamc/subjects/01/01.asfb",ios::binary);
      boost::shared_ptr<IOData> dat=getIOData();
      body.read(is,dat.get());
    }
    {
      boost::filesystem::ifstream is(getSourcePath()+"/all_asfamc/subjects/01/01_01.amc");
      body.readAMC(is);
    }
    Coli jointMap;
    Cold theta;
    SkinnedMeshUsingArticulatedBody mesh;
    mesh.readPkl(getSourcePath()+"/models/basicModel_f_lbs_10_207_0_v1.0.0.pkl_ascii");
    mesh.registerASF(body,jointMap,theta,"");
    mesh.registerAMC(body,jointMap,theta,"");
    mesh.getBody().writeJointsSeqVTK("jointSeq");
    exit(-1);
  }
#endif
  {
    SkinnedMeshUsingArticulatedBody mesh;
    mesh.readPkl(getSourcePath()+"/models/basicModel_f_lbs_10_207_0_v1.0.0.pkl_ascii");
    convertMocap(mesh,"skinnedFemale",false,false);
  }
  {
    SkinnedMeshUsingArticulatedBody mesh;
    mesh.readPkl(getSourcePath()+"/models/basicmodel_m_lbs_10_207_0_v1.0.0.pkl_ascii");
    convertMocap(mesh,"skinnedMale",false,false);
  }
  return 0;
}
