from matplotlib import pyplot as pp
from PIL import Image
import tensorflow as tf
import ctypes as ct
import numpy as np
import argparse,os,shutil,StringIO

class Dataset:
    def __init__(self, leveldb_path, useMean, useMem=True):
        self.recreate(leveldb_path,useMean,useMem)
    def recreate(self, leveldb_path, useMean, useMem=True):
        #find library
        self.leveldb_path=leveldb_path
        self.media_path=os.path.dirname(os.path.abspath(leveldb_path))
        self.lib_name='libDataDrivenOpt.so'
        #search in current path
        try:
            self.leveldb
        except AttributeError:
            self.lib_path=self.media_path+'/'+self.lib_name
            if os.path.exists(self.lib_path):
                self.leveldb = ct.cdll.LoadLibrary(self.lib_path)
        #search in source path
        try:
            self.leveldb
        except AttributeError:
            self.source_path=os.path.dirname(self.media_path)
            self.lib_path=self.source_path+'/'+self.lib_name
            if os.path.exists(self.lib_path):
                self.leveldb = ct.cdll.LoadLibrary(self.lib_path)
        #search in build path
        try:
            self.leveldb
        except AttributeError:
            self.build_name=os.path.basename(self.source_path)+'-build'
            self.lib_path=os.path.dirname(self.source_path)+'/'+self.build_name+'/'+self.lib_name
            if os.path.exists(self.lib_path):
                self.leveldb = ct.cdll.LoadLibrary(self.lib_path)
        #finalize
        try: 
            self.leveldb
        except AttributeError:
            print('Cannot find %s'%self.lib_name)
            exit()
        print('lib_path: %s!'%self.lib_path)
        #db read
        self.leveldb.readMeanBinary.argtypes=[ct.c_char_p,ct.POINTER(ct.c_float),ct.POINTER(ct.c_int)]
        self.leveldb.closeDBPrefetch.argtypes=[ct.c_void_p]
        #self.leveldb.closeDBPrefetch.restype
        self.leveldb.openDBPrefetch.argtypes=[ct.c_char_p]
        self.leveldb.openDBPrefetch.restype=ct.c_void_p
        self.leveldb.getEpochPrefetch.argtypes=[ct.c_void_p]
        self.leveldb.getEpochPrefetch.restype=ct.c_int
        self.leveldb.getKeyPrefetch.argtypes=[ct.c_void_p,ct.c_char_p,ct.c_int]
        #self.leveldb.getKeyPrefetch.restype
        self.leveldb.nextDBPrefetch.argtypes=[ct.c_void_p]
        self.leveldb.nextDBPrefetch.restype=ct.c_bool
        self.leveldb.nrDatumsPrefetch.argtypes=[ct.c_void_p]
        self.leveldb.nrDatumsPrefetch.restype=ct.c_int
        self.leveldb.getDatumPrefetch.argtypes=[ct.c_void_p,ct.c_int,ct.POINTER(ct.c_int)]
        self.leveldb.getDatumPrefetch.restype=ct.POINTER(ct.c_float)
        self.leveldb.nrImagesPrefetch.argtypes=[ct.c_void_p]
        self.leveldb.nrImagesPrefetch.restype=ct.c_int
        self.leveldb.getImagePrefetch.argtypes=[ct.c_void_p,ct.c_int,ct.POINTER(ct.c_int)]
        self.leveldb.getImagePrefetch.restype=ct.POINTER(ct.c_ubyte)
        #visualize
        self.leveldb.readSkinnedMeshUtility.argtypes=[ct.c_char_p]
        self.leveldb.writeBodyVTK.argtypes=[ct.c_char_p,ct.c_char_p,ct.POINTER(ct.c_float),ct.POINTER(ct.c_int)]
        self.leveldb.writeClothVTK.argtypes=[ct.c_char_p,ct.POINTER(ct.c_float),ct.POINTER(ct.c_int)]
        #open dataset
        self.leveldb_path=leveldb_path
        names=leveldb_path.split('_')
        for name in names:
            self.skinnedMesh_path=self.media_path+'/'+name+'/01.data'
            if os.path.exists(self.skinnedMesh_path):
                if not self.leveldb.readSkinnedMeshUtility(self.skinnedMesh_path):
                    print('cannot open dataset: %s'%self.skinnedMesh_path)
                    exit()
        assert os.path.exists(self.skinnedMesh_path)
        print('skinnedMesh_path: %s!'%self.skinnedMesh_path)
        #open db
        self.handle=self.leveldb.openDBPrefetch(leveldb_path.encode())
        if self.handle == (ct.c_void_p)():
            print('failed opening leveldb!')
            exit()
        #check for mean file
        if useMean:
            self.mean=[]
            print('Using mean files!')
            for f in os.listdir(self.media_path):
                fpath=self.media_path+'/'+f
                if(f.startswith('mean') and not f.startswith('meanText')):
                    components=f.split('_')
                    id=int(components[0][4:])
                    while len(self.mean) <= id:
                        self.mean.append(None)
                        shape_ptr=(ct.c_int*3)()
                    if not self.leveldb.readMeanBinary(fpath,ct.POINTER(ct.c_float)(),shape_ptr):
                        del self.mean
                        break
                    data_ptr=(ct.c_float*(shape_ptr[0]*shape_ptr[1]*shape_ptr[2]))()
                    if not self.leveldb.readMeanBinary(fpath,data_ptr,shape_ptr):
                        del self.mean
                        break
                    self.mean[id]=self.convertNumpy(data_ptr,shape_ptr)
        else: print('Not using mean files!')
        #fill datums
        self.fillDatums()
        #mem dataset
        if useMem:
            mem_handle=[]
            while True:
                mem_handle.append([self.datums,self.images,self.key])
                self.next()
                if self.leveldb.getEpochPrefetch(self.handle) > 0:
                    break
            self.close()
            self.mem_epoch=0
            self.mem_index=0
            self.mem_handle=mem_handle
    def close(self):
        if self.isFile():
            print('Closing: %s!'%self.leveldb_path)
            self.leveldb.closeDBPrefetch(self.handle)
            del self.handle
    def convertNumpy(self, data_ptr, shape_ptr, mean=None):
        ret=np.ctypeslib.as_array(data_ptr,(shape_ptr[0]*shape_ptr[1]*shape_ptr[2],))
        if not isinstance(mean,type(None)):
            ret=np.subtract(ret,mean)
        if shape_ptr[0] != 1 or shape_ptr[1] != 1:
            ret=np.reshape(ret,(shape_ptr[0],shape_ptr[1],shape_ptr[2]))
            ret=np.rollaxis(ret,1,0)
            ret=np.rollaxis(ret,2,1)
        return ret
    def addMean(self, val, mean):
        return np.add(val,np.reshape(mean,val.shape))
    def fillDatums(self):
        #datums
        self.datums=[]
        for i in xrange(self.leveldb.nrDatumsPrefetch(self.handle)):
            shape_ptr=(ct.c_int*3)()
            data_ptr=self.leveldb.getDatumPrefetch(self.handle,i,shape_ptr)
            self.datums.append(self.convertNumpy(data_ptr,shape_ptr,self.mean[i] if hasattr(self,'mean') else None))
        #images
        self.images=[]
        for i in xrange(self.leveldb.nrImagesPrefetch(self.handle)):
            size_ptr=(ct.c_int*1)()
            data_ptr=self.leveldb.getImagePrefetch(self.handle,i,size_ptr)
            data_array=np.ctypeslib.as_array(data_ptr,(size_ptr[0],))
            im=Image.open(StringIO.StringIO(data_array.tobytes()))
            self.images.append(np.array(im))
        #key
        key_ptr=(ct.c_char*1024)()
        self.leveldb.getKeyPrefetch(self.handle,key_ptr,1024)
        self.key=key_ptr.value
        return self.datums,self.images,self.key
    def epoch(self):
        if self.isFile():
            return self.leveldb.getEpochPrefetch(self.handle)
        elif self.isMem():
            return self.mem_epoch
        else: ValueError('No handle or mem_handle found!')
    def next(self):
        if self.isFile():
            self.leveldb.nextDBPrefetch(self.handle)
            return self.fillDatums()
        elif self.isMem():
            self.mem_index=self.mem_index+1
            if self.mem_index == len(self.mem_handle):
                self.mem_index=0
                self.mem_epoch=self.mem_epoch+1
            ret=self.mem_handle[self.mem_index]
            self.datums,self.images,self.key=ret[0],ret[1],ret[2]
            return self.datums,self.images,self.key
        else: raise ValueError('No handle or mem_handle found!')
    def nextBatch(self, batchSize):
        nrDatums=len(self.datums)
        nrImages=len(self.images)
        self.batchDatums=[[None for i in range(batchSize)] for i in range(nrDatums)]
        self.batchImages=[[None for i in range(batchSize)] for i in range(nrImages)]
        for i in xrange(batchSize):
            for j in xrange(nrDatums):
                self.batchDatums[j][i]=self.datums[j]
            for j in xrange(nrImages):
                self.batchImages[j][i]=self.images[j]
            self.next()
        return self.batchDatums, self.batchImages
    def readToEpochEnd(self):
        nrDatums=len(self.datums)
        nrImages=len(self.images)
        self.batchDatums=[[] for i in range(nrDatums)]
        self.batchImages=[[] for i in range(nrImages)]
        epoch=self.epoch()
        while True:
            for j in xrange(nrDatums):
                self.batchDatums[j].append(self.datums[j])
            for j in xrange(nrImages):
                self.batchImages[j].append(self.images[j])
            self.next()
            if self.epoch() != epoch:
                break
        return self.batchDatums, self.batchImages
    def writeClothVTK(self, path, id, datums=None):
        if isinstance(datums,type(None)):
            datums=self.datums
        for i in xrange(len(self.datums)-1):
            if isinstance(datums[i],list):
                tmp=datums[i][0]
            else: tmp=datums[i]
            tmp=np.rollaxis(tmp,2)
            if hasattr(self,'mean'):
                tmp=self.addMean(tmp,self.mean[i])
            shape_ptr=(ct.c_int*3)(*tmp.shape)
            name=path+str(i)+'_'+str(id)+'.vtk'
            self.leveldb.writeClothVTK(name.encode(),tmp.ctypes.data_as(ct.POINTER(ct.c_float)),shape_ptr)
    def writeBodyVTK(self, path, id, datum=None):
        if isinstance(datum,type(None)):
            datum=self.datums[-1]
        if isinstance(datum,list):
            datum=datum[0]
        if hasattr(self,'mean'):
            datum=self.addMean(datum,self.mean[len(self.datums)-1])
        shape_ptr=(ct.c_int)(*datum.shape)
        name=path+'_'+str(id)+'.vtk'
        self.leveldb.writeBodyVTK(name.encode(),self.key.encode(),datum.ctypes.data_as(ct.POINTER(ct.c_float)),shape_ptr)
    def writeImage(self, path, id, images=None):
        if isinstance(images,type(None)):
            images=self.images
        for i in xrange(len(images)):
            name=path+str(i)+'_'+str(id)+'.jpg'
            if isinstance(images[i],list):
                pp.imsave(name,images[i][0])
            else: pp.imsave(name,images[i])
    def nrDatums(self):
        if self.isMem():
            return len(self.mem_handle)
        else: raise ValueError('Cannot count dataset size if not inMem()')
    def reset(self):
        if self.isMem():
            self.mem_index=len(self.mem_handle)-1
            self.mem_epoch=-1
            self.next()
        elif self.isFile():
            self.close()
            self.recreate(self.leveldb_path,True if hasattr(self,'mean') else False,False)
        else: raise ValueError('No handle or mem_handle found!')
    def isFile(self):
        return hasattr(self,'handle')
    def isMem(self):
        return hasattr(self,'mem_handle') and hasattr(self,'mem_epoch') and hasattr(self,'mem_index')
#db
def open_trainDB(results):
    if not hasattr(results,'trainDB'):
        results.trainDB=Dataset(results.leveldb_path,results.mean_file,results.use_mem)
    else: results.trainDB.reset()
def close_trainDB(results):
    if hasattr(results,'trainDB'):
        results.trainDB.close()
        delattr(results,'trainDB')
def open_testDB(results):
    if not hasattr(results,'testDB'):
        results.testDB=Dataset(results.test_leveldb_path,results.mean_file,results.use_mem)
    else: results.testDB.reset()
def close_testDB(results):
    if hasattr(results,'testDB'):
        results.testDB.close()
        delattr(results, 'testDB')

if __name__ == '__main__':
    parser=argparse.ArgumentParser(description='Visualizing Dataset')
    parser.add_argument('--leveldb_path',metavar='[path to leveldb dataset]',action='store',type=str,   \
                        default='../Media/image_test_leveldb2_mainClothDressTightScripts_datasetStaticFemaleRescaled')
    parser.add_argument('--max_iter',metavar='[max iterations]',action='store',type=int,choices=xrange(0,1000000),default=10000)
    parser.add_argument('--batch_size',metavar='[batch size]',action='store',type=int,choices=xrange(1,1000),default=64)
    parser.add_argument('--mean_file', dest='mean_file', action='store_true')
    parser.add_argument('--no_mean_file', dest='mean_file', action='store_false')
    parser.set_defaults(mean_file=True)
    results=parser.parse_args()
    ds=Dataset(results.leveldb_path,results.mean_file,True)
    dsFile=Dataset(results.leveldb_path,results.mean_file,False)
    #compare and test visualize
    if os.path.exists('visualize'):
        shutil.rmtree('visualize')
    os.mkdir('visualize')
    for i in xrange(results.max_iter):
        print('Retrieving %dth batch, batch_size=%d, epoch=%d!'%(i,results.batch_size,ds.epoch()))
        if ds.epoch() > 0:
            break
        batchDatums,batchImages=ds.nextBatch(results.batch_size)
        batchDatumsFile,batchImagesFile=dsFile.nextBatch(results.batch_size)
        #write
        ds.writeClothVTK('visualize/cloth',i,batchDatums)
        ds.writeBodyVTK('visualize/body',i,batchDatums[-1])
        ds.writeImage('visualize/img',i,batchImages)
        #compare file/mem
        assert ds.epoch() == dsFile.epoch()
        for bid in xrange(results.batch_size):
            for lid in xrange(len(ds.datums)):
                assert np.array_equal(batchDatums[lid][bid],batchDatumsFile[lid][bid])
            for lid in xrange(len(ds.images)):
                assert np.array_equal(batchImages[lid][bid],batchImagesFile[lid][bid])
    #test reset            
    ds.reset()
    dsFile.reset()
    assert ds.epoch() == dsFile.epoch()
    for lid in xrange(len(ds.datums)):
        assert np.array_equal(ds.datums[lid],dsFile.datums[lid])
    for lid in xrange(len(ds.images)):
        assert np.array_equal(ds.images[lid],dsFile.images[lid])
    #close
    ds.close()
    dsFile.close()
    print('Done!')