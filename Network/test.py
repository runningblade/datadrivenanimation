import tensorflow as tf
import numpy as np
from dataset import *
from nn_utils import *
from bodyToCloth import *
import argparse,os,sys,shutil

if __name__ == '__main__':
    with tf.Session() as sess:
        initial = tf.truncated_normal([10000],stddev=0.1)
        tst=tf.get_variable(initializer=initial,name='test')
        sess.run(tf.global_variables_initializer())
        print tf.reduce_mean(tf.cast(tf.greater(tst.eval(),tf.constant(0.1,shape=tst.shape)),tf.float32)).eval()