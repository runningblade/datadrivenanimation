import tensorflow as tf
from dataset import *
import os,sys,datetime,math

#nn util
def norm_loss(results,t):
    if results.norm_loss == 'l2_loss':
        return tf.nn.l2_loss(t)
    elif results.norm_loss == 'l1_loss':
        return tf.reduce_sum(tf.abs(t))
    else:
        print('unknown gan_loss: %s'%results.gan_loss)
        exit()
def gan_loss(results,real,fake):
    if results.gan_loss == 'log_loss':
        eucLossDis=tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=real,labels=tf.ones_like(real)))+   \
                   tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=fake,labels=tf.zeros_like(fake)))
        eucLossGen=tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=fake,labels=tf.ones_like(fake)))
    elif results.gan_loss == 'ls_loss':
        eucLossDis=tf.nn.l2_loss(real-tf.ones_like(real))+tf.nn.l2_loss(fake-tf.zeros_like(fake))
        eucLossGen=tf.nn.l2_loss(fake-tf.ones_like(fake))
    else:
        print('unknown gan_loss: %s'%results.gan_loss)
        exit()
    return eucLossDis,eucLossGen
def weight_variable(shape, n='W', std=0.001):
    initial = tf.truncated_normal(shape,stddev=std)
    return tf.get_variable(initializer=initial,name=n)
def bias_variable(shape, n='b', std=0.001):
    initial = tf.truncated_normal(shape,stddev=std)
    return tf.get_variable(initializer=initial,name=n)
def conv2d(x, W, stride=1, pd='SAME'):
    return tf.nn.conv2d(x,W,strides=[1, stride, stride, 1],padding=pd)
def max_pool(x, ksize=2, stride=2, pd='SAME'):
    return tf.nn.max_pool(x,ksize=[1, ksize, ksize, 1],strides=[1, stride, stride, 1],padding=pd)
def get_shape(x):
    ret=1
    shape=x.shape.as_list()
    for i in range(1,len(shape)):
        ret=ret*shape[i]
    return ret
def reshape_like(x, xRef):
    shape=[-1]+xRef.shape.as_list()[1:]
    return tf.reshape(x,shape)
def flatten(x):
    return tf.reshape(x,[-1,get_shape(x)])
def create_trainer(mode, results, loss, vars=None):
    vlist=[]
    if isinstance(vars,type(None)):
        vlist+=tf.trainable_variables()
    else:
        for name in vars:
            vlist+=tf.trainable_variables(name)
    #create minimizer
    with tf.variable_scope('optimizer',reuse=tf.AUTO_REUSE):
        if not hasattr(results,'global_step'):
            results.global_step=tf.placeholder(tf.float32,name='global_step')
            alpha=pow(results.to_stepsize/results.from_stepsize,1.0/float(results.max_epoch))
            results.learning_rate=tf.train.exponential_decay(results.from_stepsize,results.global_step,1.0,alpha,staircase=True)
    with tf.variable_scope('optimizer_'+mode,reuse=tf.AUTO_REUSE):
        trainStep=tf.train.AdamOptimizer(results.learning_rate).minimize(loss,var_list=vlist)
    print('Trainer info:')
    for v in vlist:
        print('\tVariable: %s!'%v.name)
    print('From stepsize %f to stepsize %f over %d epoch!'% \
          (results.learning_rate.eval(feed_dict={results.global_step:0}),   \
           results.learning_rate.eval(feed_dict={results.global_step:results.max_epoch}),results.max_epoch))
    return trainStep
#checkpoint
def load_latest(results, sess):
    latest_i=0
    saver=tf.train.Saver()
    for i in range(results.max_epoch):
        if i%results.save_interval == 0 and os.path.exists('%s/iter%d.ckpt.meta'%(results.model_path,i)):
            print('Restoring %s/iter%d.ckpt!'%(results.model_path,i))
            saver.restore(sess,'%s/iter%d.ckpt'%(results.model_path,i))
            latest_i=i+1
    return latest_i
#parser
def get_parser(model_default, withImage=False):
    imagePrefix='image_' if withImage else ''
    source_dir=os.path.dirname(os.path.abspath(__file__))
    parser=argparse.ArgumentParser(description='Train Network')
    parser.add_argument('--op',metavar='[type of operation]',action='store',type=str,default='train',choices=['train','test'])
    parser.add_argument('--mode',metavar='[mode of training]',action='store',type=str,default='gan',choices=['normal','gan'])
    parser.add_argument('--leveldb_path',metavar='[path to leveldb dataset]',action='store',type=str,default=source_dir+'/../Media/'+imagePrefix+'leveldb2_mainClothDressTightScripts_datasetStaticFemaleRescaled')
    parser.add_argument('--test_leveldb_path',metavar='[path to leveldb test dataset]',action='store',type=str,default=source_dir+'/../Media/'+imagePrefix+'test_leveldb2_mainClothDressTightScripts_datasetStaticFemaleRescaled')
    parser.add_argument('--vgg_ckpt_path',metavar='[path to pretrained vgg model]',action='store',type=str,default='../Media/vgg_16.ckpt')
    parser.add_argument('--model_path',metavar='[path to saved model]',action='store',type=str,default=model_default)
    parser.add_argument('--graph_path',metavar='[path to saved model]',action='store',type=str,default='./graph')
    parser.add_argument('--max_epoch',metavar='[max epochs]',action='store',type=int,choices=xrange(0,1000),default=1000)
    parser.add_argument('--save_interval',metavar='[save interval in #epoch]',action='store',type=int,choices=xrange(0,10),default=1)
    parser.add_argument('--batch_size',metavar='[batch size]',action='store',type=int,choices=xrange(1,1000),default=64)
    parser.add_argument('--iter_per_patch',metavar='[how many iterations to run per batch]',action='store',type=int,choices=xrange(1,100),default=1)
    parser.add_argument('--GPUID',metavar='[which GPU to use]',action='store',type=int,choices=xrange(0,10))
    parser.add_argument('--keep_prob',metavar='[dropout rate]',action='store',type=float,default=0.9)
    parser.add_argument('--from_stepsize',metavar='[starting stepsize]',action='store',type=float,default=1e-4)
    parser.add_argument('--to_stepsize',metavar='[ending stepsize]',action='store',type=float,default=1e-4)
    parser.add_argument('--visualize', dest='visualize', action='store_true')
    parser.add_argument('--no-visualize', dest='visualize', action='store_false')
    parser.set_defaults(visualize=True)
    parser.add_argument('--mean_file', dest='mean_file', action='store_true')
    parser.add_argument('--no_mean_file', dest='mean_file', action='store_false')
    parser.set_defaults(mean_file=True)
    parser.add_argument('--use_mem', dest='use_mem', action='store_true')
    parser.add_argument('--no_use_mem', dest='use_mem', action='store_false')
    parser.set_defaults(use_mem=not withImage)
    parser.add_argument('--gan_loss',metavar='[type of loss used by gan]',action='store',type=str,default='ls_loss',choices=['log_loss','ls_loss'])
    parser.add_argument('--norm_loss',metavar='[type of loss used by gan]',action='store',type=str,default='l1_loss',choices=['l2_loss','l1_loss'])
    results=parser.parse_args()
    if results.mode == 'gan':
        results.model_path+='_gan'
    results.source_dir=source_dir
    #limit GPU usage
    assert results.GPUID is not None
    os.environ['CUDA_DEVICE_ORDER']='PCI_BUS_ID'
    os.environ['CUDA_VISIBLE_DEVICES']=str(results.GPUID)
    #write results
    print('Configuration:')
    attrs=vars(results)
    f=open('configuration.txt','w')
    for k,v in attrs.iteritems():
        print('%s: %s'%(k,v))
        f.write('%s: %s\n'%(k,v))
    f.close()
    return results
#debug
def print_all_variables(scope=None,justGet=False):
    var_list=tf.trainable_variables(scope)
    if not justGet:
        print('all variables:')
        for v in var_list:
            print(v.name)
    return var_list
#train
def build_dict(results, batchDatums, batchImages, **args):
    dict={args['keep']:results.keep_prob,results.global_step:results.global_step_val}
    if 'cloth' in args:
        dict[args['cloth']]=batchDatums[0]
    if 'body' in args:
        dict[args['body']]=batchDatums[-1]
    if 'image' in args:
        dict[args['image']]=batchImages[0]
    return dict
def test_discriminator(results, prefix, sess, **args):
    if str(prefix)+'Real' in args and str(prefix)+'Fake' in args:
        real=args[str(prefix)+'Real']
        fake=args[str(prefix)+'Fake']
    else: return
    accReal=tf.greater(real,tf.ones_like(real)*0.5)
    accFake=tf.less(fake,tf.ones_like(fake)*0.5)
    acc=tf.reduce_sum(tf.cast(tf.concat([accReal,accFake],0),tf.float32))/2
    accF=(tf.nn.l2_loss(real-tf.ones_like(real))+tf.nn.l2_loss(fake))/2
    #calculate
    nrD=results.trainDB.nrDatums()
    results.trainDB.reset()
    i=0
    accAvg=0
    accAvgF=0.0
    while i<nrD:
        batchSz=min(results.batch_size,nrD-i)
        batchDatums,batchImages=results.trainDB.nextBatch(batchSz)
        dict=build_dict(results,batchDatums, batchImages,**args)
        dict[args['keep']]=1.0
        accAvg+=sess.run(acc,feed_dict=dict)
        accAvgF+=sess.run(accF,feed_dict=dict)
        i+=batchSz
    print('%s Discriminator accuracy: %f, accuracy-float: %f!'%(str(prefix),accAvg/float(nrD),math.sqrt(accAvgF/float(nrD))))
def test_generator(results, sess, **args):
    if not hasattr(results, 'test_generator_func'):
        return
    fn=results.test_generator_func
    #calculate
    nrD=results.trainDB.nrDatums()
    results.trainDB.reset()
    i=0
    fnAvg=0
    while i<nrD:
        batchSz=min(results.batch_size,nrD-i)
        batchDatums,batchImages=results.trainDB.nextBatch(batchSz)
        dict=build_dict(results,batchDatums, batchImages,**args)
        dict[args['keep']]=1.0
        fnAvg+=sess.run(fn,feed_dict=dict)
        i+=batchSz
    print('test_generator_func loss: %f!'%(fnAvg/float(nrD)))
def train_gan_tpl(results, sess, **args):
    #train discriminator
    print('Training Discriminator')
    open_trainDB(results)
    while results.trainDB.epoch() == 0:
        batchDatums,batchImages=results.trainDB.nextBatch(results.batch_size)
        for i_per_batch in xrange(results.iter_per_patch):
            args['trainStep_dis'].run(feed_dict=build_dict(results,batchDatums,batchImages,**args))
    #test discriminator
    test_discriminator(results,'body',sess,**args)
    test_discriminator(results,'cloth',sess,**args)
    #train generator
    print('Training Generator')
    open_trainDB(results)
    while results.trainDB.epoch() == 0:
        batchDatums,batchImages=results.trainDB.nextBatch(results.batch_size)
        for i_per_batch in xrange(results.iter_per_patch):
            args['trainStep_gen'].run(feed_dict=build_dict(results,batchDatums,batchImages,**args))
    #test user func
    test_generator(results,sess,**args)
def train_normal_tpl(results, sess, **args):
    print('Training Normal')
    open_trainDB(results)
    while results.trainDB.epoch() == 0:
        batchDatums,batchImages=results.trainDB.nextBatch(results.batch_size)
        for i_per_batch in xrange(results.iter_per_patch):
            args['trainStep'].run(feed_dict=build_dict(results,batchDatums,batchImages,**args))
    #test user func
    test_generator(results,sess,**args)
def train_tpl(results, sess, test_fcn, **args):
    #train
    saver=tf.train.Saver()
    if not os.path.exists(results.model_path):
        os.mkdir(results.model_path)
    #visualize graph
    if os.path.exists(results.graph_path):
        shutil.rmtree(results.graph_path)
    writer=tf.summary.FileWriter(results.graph_path,sess.graph)
    avg_loss_cloth=tf.placeholder(tf.float32,name='avg_loss_cloth')
    avg_loss_body=tf.placeholder(tf.float32,name='avg_loss_body')
    tf.summary.scalar('loss_cloth',avg_loss_cloth)
    tf.summary.scalar('loss_body',avg_loss_body)
    merged_summary_op=tf.summary.merge_all()
    #train
    sess.run(tf.global_variables_initializer())
    i=load_latest(results,sess)
    while i<results.max_epoch:
        #run one epoch
        results.global_step_val=i
        if results.mode == 'normal':
            train_normal_tpl(results,sess,**args)
        elif results.mode == 'gan':
            train_gan_tpl(results,sess,**args)
        print(datetime.datetime.now().time().strftime('%H:%M:%S.%f'))
        #test result
        print('Iterated %dth epoch learning_rate=%.10f!'%(i,sess.run(results.learning_rate,feed_dict={results.global_step:i})))
        dict={'keep':args['keep']}
        if 'cloth' in args:
            dict['cloth']=args['cloth']
        if 'clothPred' in args:
            dict['clothPred']=args['clothPred']
        if 'body' in args:
            dict['body']=args['body']
        if 'bodyPred' in args:
            dict['bodyPred']=args['bodyPred']
        lossCloth,lossBody,_=test_fcn(results,sess,dict)
        print('Average loss in dataset: cloth=%f, body=%f!'%(lossCloth,lossBody))
        writer.add_summary(sess.run(merged_summary_op,feed_dict={avg_loss_cloth:lossCloth,avg_loss_body:lossBody}))
        #write/profile
        if i%results.save_interval == 0:
            print('Saving %d epoch, batchSize=%d'%(i,results.batch_size))
            saver.save(sess,'%s/iter%d.ckpt'%(results.model_path,i))
            sys.stdout.flush()
        i=i+1
    writer.close()
    close_trainDB(results)