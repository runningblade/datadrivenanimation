import tensorflow as tf
import numpy as np
from dataset import *
from nn_utils import *
from bodyToCloth import *
from clothToBody import *
import argparse,os,sys,shutil

def clothBodyCyclic_create():
    cloth=tf.placeholder(tf.float32,[None,35,74,3],name='cloth')
    body=tf.placeholder(tf.float32,[None,21],name='body')
    keep=tf.placeholder(tf.float32,name='keepProb')
    with tf.variable_scope('bodyToCloth',reuse=tf.AUTO_REUSE):
        clothPred=bodyToCloth(body,cloth,keep)
    with tf.variable_scope('clothToBody',reuse=tf.AUTO_REUSE):
        bodyPred=clothToBody(cloth,body,keep)
    #cyclic
    with tf.variable_scope('bodyToCloth',reuse=tf.AUTO_REUSE):
        bodyPredCloth=bodyToCloth(bodyPred,cloth,keep)
    with tf.variable_scope('clothToBody',reuse=tf.AUTO_REUSE):
        clothPredBody=clothToBody(clothPred,body,keep)
    return cloth,clothPred,bodyPredCloth,   \
           body,bodyPred,clothPredBody,keep
def clothBodyCyclicGan_create():
    cloth,clothPred,bodyPredCloth,  \
    body,bodyPred,clothPredBody,keep=clothBodyCyclic_create()
    #discriminator
    with tf.variable_scope('bodyToClothDis',reuse=tf.AUTO_REUSE):
        clothFake=bodyToCloth(bodyPred,cloth,keep,hasGan=True)
        clothReal=bodyToCloth(body,cloth,keep,hasGan=True)
    with tf.variable_scope('clothToBodyDis',reuse=tf.AUTO_REUSE):
        bodyFake=clothToBody(clothPred,body,keep,hasGan=True)
        bodyReal=clothToBody(cloth,body,keep,hasGan=True)
    return cloth,clothPred,bodyPredCloth,       \
           body,bodyPred,clothPredBody,keep,    \
           clothFake,clothReal,bodyFake,bodyReal
def clothBodyCyclic_create_trainer(results, cloth, clothPred, bodyPredCloth, body, bodyPred, clothPredBody):
    with tf.variable_scope('loss',reuse=tf.AUTO_REUSE):
        eucLoss=norm_loss(results,cloth-clothPred)+norm_loss(results,body-bodyPred)+    \
                norm_loss(results,cloth-bodyPredCloth)+norm_loss(results,body-clothPredBody)
    print_all_variables()
    return create_trainer('',results,eucLoss)
def clothBodyCyclicGan_create_trainer(results, cloth, clothPred, bodyPredCloth, body, bodyPred, clothPredBody, clothFake, clothReal, bodyFake, bodyReal):
    with tf.variable_scope('loss',reuse=tf.AUTO_REUSE):
        eucLossDisC,eucLossGenC=gan_loss(results,clothReal,clothFake)
        eucLossDisB,eucLossGenB=gan_loss(results,bodyReal,bodyFake)
        eucLossDis=eucLossDisC+eucLossDisB
        eucLossGen=eucLossGenC+eucLossGenB+(norm_loss(results,cloth-bodyPredCloth)+norm_loss(results,body-clothPredBody))*10
    print_all_variables()
    trainStep_dis=create_trainer('dis',results,eucLossDis,['bodyToClothDis/','clothToBodyDis/'])
    trainStep_gen=create_trainer('gen',results,eucLossGen,['bodyToCloth/','clothToBody/'])
    return  trainStep_dis,trainStep_gen
def clothBodyCyclic_test(results, sess, bundle=None):
    #create net
    if isinstance(bundle,type(None)):
        cloth,clothPred,bodyPredCloth,body,bodyPred,clothPredBody,keep=clothBodyCyclic_create()
        createNet=True
    else:
        cloth,clothPred,body,bodyPred,keep=bundle['cloth'],bundle['clothPred'],bundle['body'],bundle['bodyPred'],bundle['keep']
        createNet=False
    #load latest model
    if createNet:
        load_latest(results,sess)
    #check visualization folder
    if results.visualize:
        if os.path.exists('visualize'):
            shutil.rmtree('visualize')
        os.mkdir('visualize')
    #test
    i=0
    lossCloth=0.0
    lossBody=0.0
    open_testDB(results)
    while results.testDB.epoch() == 0:
        batchDatums,batchImages=results.testDB.nextBatch(1)
        clothFeat=sess.run(clothPred,feed_dict={body:batchDatums[-1],keep:1})
        clothFeat=clothFeat.reshape((clothFeat.shape[1],clothFeat.shape[2],clothFeat.shape[3]))
        bodyFeat=sess.run(bodyPred,feed_dict={cloth:batchDatums[0],keep:1})
        bodyFeat=bodyFeat.reshape((bodyFeat.shape[1],))
        if results.visualize:
            #print('Visualizing %dth datum!'%i)
            results.testDB.writeClothVTK('visualize/cloth',i,batchDatums)
            results.testDB.writeClothVTK('visualize/clothPred',i,datums=[clothFeat])
            results.testDB.writeBodyVTK('visualize/body',i,datum=batchDatums[-1])
            results.testDB.writeBodyVTK('visualize/bodyPred',i,datum=bodyFeat)
        lossCloth+=np.linalg.norm(np.subtract(clothFeat,batchDatums[0]))
        lossBody+=np.linalg.norm(np.subtract(bodyFeat,batchDatums[-1]))
        i=i+1
    close_testDB(results)
    #profile loss
    lossCloth/=float(i)
    lossBody/=float(i)
    return lossCloth,lossBody,0
def clothBodyCyclic_train(results, sess):
    #create net/trainer
    if results.mode != 'normal':
        cloth,clothPred,bodyPredCloth,  \
        body,bodyPred,clothPredBody,keep,   \
        clothFake,clothReal,bodyFake,bodyReal=clothBodyCyclicGan_create()
        trainStep_dis,trainStep_gen=clothBodyCyclicGan_create_trainer(results,cloth,clothPred,bodyPredCloth,body,bodyPred,clothPredBody,clothFake,clothReal,bodyFake,bodyReal)
        train_tpl(results,sess,clothBodyCyclic_test,cloth=cloth,clothPred=clothPred,body=body,bodyPred=bodyPred,keep=keep,    \
                  bodyReal=bodyReal,bodyFake=bodyFake,clothReal=clothReal,clothFake=clothFake,  \
                  trainStep_dis=trainStep_dis,trainStep_gen=trainStep_gen)
    else:
        cloth,clothPred,bodyPredCloth,  \
        body,bodyPred,clothPredBody,keep=clothBodyCyclic_create()
        trainStep=clothBodyCyclic_create_trainer(results,cloth,clothPred,bodyPredCloth,body,bodyPred,clothPredBody)
        train_tpl(results,sess,clothBodyCyclic_test,cloth=cloth,clothPred=clothPred,body=body,bodyPred=bodyPred,keep=keep,  \
                  trainStep=trainStep)

if __name__ == '__main__':
    print(tf.__version__)
    results=get_parser('model_clothBodyCyclic')
    with tf.Session() as sess:
        if results.op == 'train':
            results.visualize=False
            clothBodyCyclic_train(results,sess)
        elif results.op == 'test':
            clothBodyCyclic_test(results,sess)
