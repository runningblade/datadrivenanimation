import tensorflow as tf
import numpy as np
from dataset import *
from nn_utils import *
from bodyToCloth import *
import argparse,os,sys,shutil

def vgg_16(inputs,
           num_classes=1000,
           is_training=True,
           dropout_keep_prob=0.5,
           spatial_squeeze=True,
           scope='vgg_16',
           fc_conv_padding='VALID',
           global_pool=False):
    slim=tf.contrib.slim
    with tf.variable_scope(scope,'vgg_16',[inputs],reuse=tf.AUTO_REUSE) as sc:
        end_points_collection=sc.original_name_scope+'_end_points'
        # Collect outputs for conv2d, fully_connected and max_pool2d.
        with slim.arg_scope([slim.conv2d, slim.fully_connected, slim.max_pool2d],
                            outputs_collections=end_points_collection):
            net=slim.repeat(inputs,2,slim.conv2d,64,[3,3],scope='conv1')
            net=slim.max_pool2d(net,[2,2],scope='pool1')
            net=slim.repeat(net,2,slim.conv2d,128,[3,3],scope='conv2')
            net=slim.max_pool2d(net,[2,2],scope='pool2')
            net=slim.repeat(net,3,slim.conv2d,256,[3,3],scope='conv3')
            net=slim.max_pool2d(net,[2,2],scope='pool3')
            net=slim.repeat(net,3,slim.conv2d,512,[3,3],scope='conv4')
            net=slim.max_pool2d(net,[2,2],scope='pool4')
            net=slim.repeat(net,3,slim.conv2d,512,[3,3],scope='conv5')
            net=slim.max_pool2d(net,[2,2],scope='pool5')
    return flatten(net)
def imageToClothBody_create(results, sess, s={'fc':[1024,1024]}):
    image=tf.placeholder(tf.float32,[None,256,256*3,3],name='image')
    cloth=tf.placeholder(tf.float32,[None,35,74,3],name='cloth')
    body=tf.placeholder(tf.float32,[None,21],name='body')
    keep=tf.placeholder(tf.float32,name='keepProb')
    #segment image
    view1,view2,view3=tf.split(image,[256,256,256],2)
    view1=tf.image.resize_images(view1,[224,224])
    view2=tf.image.resize_images(view2,[224,224])
    view3=tf.image.resize_images(view3,[224,224])
    #bring through vgg16
    last=tf.concat([vgg_16(view1),vgg_16(view2),vgg_16(view3)],1)
    #full connected layers
    with tf.variable_scope('imageToClothBody',reuse=tf.AUTO_REUSE):
        for iFc in xrange(len(s['fc'])):
            with tf.variable_scope('fc'+str(iFc+1),reuse=tf.AUTO_REUSE):
                fcVar=s['fc'][iFc]
                W=weight_variable([get_shape(last),fcVar])
                b=bias_variable([fcVar])
                last=tf.nn.relu(tf.matmul(last,W)+b)
        #dropout
        with tf.variable_scope('dropout',reuse=tf.AUTO_REUSE):
            last=tf.nn.dropout(last,keep)
        #output body
        with tf.variable_scope('output',reuse=tf.AUTO_REUSE):
            W=weight_variable([get_shape(last),get_shape(body)])
            b=bias_variable([get_shape(body)])
            bodyPred=tf.matmul(last,W)+b
    #output cloth but reuse bodyToCloth network
    with tf.variable_scope('bodyToCloth/output',reuse=tf.AUTO_REUSE):
        W=weight_variable([get_shape(last),get_shape(cloth)])
        b=bias_variable([get_shape(cloth)])
        clothPred=tf.matmul(last,W)+b
        clothPred=reshape_like(clothPred,cloth)
    #load vgg
    s=tf.train.Saver(var_list=print_all_variables(scope='vgg_16',justGet=True))
    s.restore(sess,results.vgg_ckpt_path)
    return image,cloth,body,clothPred,bodyPred,keep
def imageToClothBodyGan_create(results, sess, s={'fc':[1024,1024]}):
    image,cloth,body,clothPred,bodyPred,keep=imageToClothBody_create(results,sess,s)
    #discriminator
    with tf.variable_scope('bodyToClothDis',reuse=tf.AUTO_REUSE):
        clothFake=bodyToCloth(bodyPred,cloth,keep,hasGan=True)
        clothReal=bodyToCloth(body,cloth,keep,hasGan=True)
    with tf.variable_scope('clothToBodyDis',reuse=tf.AUTO_REUSE):
        bodyFake=clothToBody(clothPred,body,keep,hasGan=True)
        bodyReal=clothToBody(cloth,body,keep,hasGan=True)
    return image,cloth,clothPred,body,bodyPred,keep,    \
           clothFake,clothReal,bodyFake,bodyReal
def imageToClothBody_create_trainer(results, cloth, clothPred, body, bodyPred):
    with tf.variable_scope('loss',reuse=tf.AUTO_REUSE):
        eucLoss=norm_loss(results,body-bodyPred)+norm_loss(results,cloth-clothPred)
    print_all_variables()
    return create_trainer('',results,eucLoss)
def imageToClothBodyGan_create_trainer(results, cloth, clothPred, body, bodyPred, clothFake, clothReal, bodyFake, bodyReal):
    with tf.variable_scope('loss',reuse=tf.AUTO_REUSE):
        eucLossDisC,eucLossGenC=gan_loss(results,clothReal,clothFake)
        eucLossDisB,eucLossGenB=gan_loss(results,bodyReal,bodyFake)
        eucLossDis=eucLossDisC+eucLossDisB
        eucLossGen=eucLossGenC+eucLossGenB+(norm_loss(results,body-bodyPred)+norm_loss(results,cloth-clothPred))*10
    print_all_variables()
    trainStep_dis=create_trainer('dis',results,eucLossDis,['bodyToClothDis/','clothToBodyDis/'])
    trainStep_gen=create_trainer('gen',results,eucLossGen,['bodyToCloth/','clothToBody/','vgg_16/','imageToClothBody/'])
    return  trainStep_dis,trainStep_gen
def imageToClothBody_test(results, sess, bundle=None):
    #create net
    if isinstance(bundle,type(None)):
        image,cloth,body,bodyPred,keep=imageToClothBody_create()
        createNet=True
    else:
        image,cloth,body,clothPred,bodyPred,keep=bundle['image'],bundle['cloth'],bundle['body'],bundle['clothPred'],bundle['bodyPred'],bundle['keep']
        createNet=False
    #load latest model
    if createNet:
        load_latest(results,sess)
    #check visualization folder
    if results.visualize:
        if os.path.exists('visualize'):
            shutil.rmtree('visualize')
        os.mkdir('visualize')
    #test
    i=0
    lossCloth=0.0
    lossBody=0.0
    open_testDB(results)
    while results.testDB.epoch() == 0:
        batchDatums,batchImages=results.testDB.nextBatch(1)
        clothFeat=sess.run(clothPred,feed_dict={image:batchImages[0],keep:1})
        clothFeat=bodyFeat.reshape((clothFeat.shape[1],))
        bodyFeat=sess.run(bodyPred,feed_dict={image:batchImages[0],keep:1})
        bodyFeat=bodyFeat.reshape((bodyFeat.shape[1],))
        if results.visualize:
            #print('Visualizing %dth datum!'%i)
            results.testDB.writeImage('visualize/image',i,batchImages)
            results.testDB.writeClothVTK('visualize/cloth',i,datum=batchDatums[0])
            results.testDB.writeClothVTK('visualize/clothPred',i,datum=clothFeat)
            results.testDB.writeBodyVTK('visualize/body',i,datum=batchDatums[-1])
            results.testDB.writeBodyVTK('visualize/bodyPred',i,datum=bodyFeat)
        lossCloth+=np.linalg.norm(np.subtract(clothFeat,batchDatums[0]))
        lossBody+=np.linalg.norm(np.subtract(bodyFeat,batchDatums[-1]))
        i=i+1
    close_testDB(results)
    #profile loss
    lossCloth/=float(i)
    lossBody/=float(i)
    return lossCloth,lossBody,0
def imageToClothBody_train(results, sess):
    #create net/train
    if results.mode != 'normal':
        image,cloth,clothPred,body,bodyPred,keep, \
        ClothFake,ClothReal,bodyFake,bodyReal=imageToClothBodyGan_create(results,sess)
        trainStep_dis,trainStep_gen=imageToClothBodyGan_create_trainer(results,cloth,clothPred,body,bodyPred,ClothFake,ClothReal,bodyFake,bodyReal)
        train_tpl(results,sess,imageToClothBody_test,image=image,cloth=cloth,body=body,clothPred=clothPred,bodyPred=bodyPred,keep=keep,  \
                  trainStep_dis=trainStep_dis,trainStep_gen=trainStep_gen)
    else:
        image,cloth,body,clothPred,bodyPred,keep=imageToClothBody_create(results,sess)
        trainStep=imageToClothBody_create_trainer(results,cloth,clothPred,body,bodyPred)
        train_tpl(results,sess,imageToClothBody_test,image=image,cloth=cloth,body=body,clothPred=clothPred,bodyPred=bodyPred,keep=keep,  \
                  trainStep=trainStep)

if __name__ == '__main__':
    print(tf.__version__)
    results=get_parser('model_imageToClothBody',True)
    with tf.Session() as sess:
        if results.op == 'train':
            results.visualize=False
            imageToClothBody_train(results,sess)
        elif results.op == 'test':
            imageToClothBody_test(results,sess)