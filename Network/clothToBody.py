import tensorflow as tf
import numpy as np
from dataset import *
from nn_utils import *
from bodyToCloth import *
import argparse,os,sys,shutil

def clothToBody(cloth, body, keep, s={'conv':[[3,64],[3,128],[3,256]],'fc':[1024,1024]}, hasGan=False):
    #convolutional layers
    last=cloth
    channel=cloth.shape.as_list()[3]
    for iConv in xrange(len(s['conv'])):
        with tf.variable_scope('conv'+str(iConv+1),reuse=tf.AUTO_REUSE):
            convVar=s['conv'][iConv]
            W=weight_variable([convVar[0],convVar[0],channel,convVar[1]])
            b=bias_variable([convVar[1]])
            last=max_pool(conv2d(last,W)+b)
            channel=convVar[1]
    #full connected layers
    last=flatten(last)
    for iFc in xrange(len(s['fc'])):
        with tf.variable_scope('fc'+str(iFc+1),reuse=tf.AUTO_REUSE):
            fcVar=s['fc'][iFc]
            W=weight_variable([get_shape(last),fcVar])
            b=bias_variable([fcVar])
            last=tf.nn.relu(tf.matmul(last,W)+b)
    #dropout
    with tf.variable_scope('dropout',reuse=tf.AUTO_REUSE):
        last=tf.nn.dropout(last,keep)
    #output head
    if hasGan:
        with tf.variable_scope('discriminator',reuse=tf.AUTO_REUSE):
            W=weight_variable([get_shape(last),1])
            b=bias_variable([1])
            dis=tf.sigmoid(tf.matmul(last,W)+b)
        return dis
    else: 
        with tf.variable_scope('output',reuse=tf.AUTO_REUSE):
            W=weight_variable([get_shape(last),get_shape(body)])
            b=bias_variable([get_shape(body)])
            output=tf.matmul(last,W)+b
        return output
def clothToBody_create():
    cloth=tf.placeholder(tf.float32,[None,35,74,3],name='cloth')
    body=tf.placeholder(tf.float32,[None,21],name='body')
    keep=tf.placeholder(tf.float32,name='keepProb')
    with tf.variable_scope('clothToBody',reuse=tf.AUTO_REUSE):
        bodyPred=clothToBody(cloth,body,keep)
    return cloth,body,bodyPred,keep
def clothToBodyGan_create():
    cloth,body,bodyPred,keep=clothToBody_create()
    #discriminator
    with tf.variable_scope('clothToBodyDis',reuse=tf.AUTO_REUSE):
        bodyFake=bodyToCloth(bodyPred,cloth,keep,hasGan=True)
        bodyReal=bodyToCloth(body,cloth,keep,hasGan=True)
    return cloth,body,bodyPred,keep,bodyFake,bodyReal
def clothToBody_create_trainer(results, body, bodyPred):
    with tf.variable_scope('loss',reuse=tf.AUTO_REUSE):
        eucLoss=norm_loss(results,body-bodyPred)
    print_all_variables()
    return create_trainer('',results,eucLoss)
def clothToBodyGan_create_trainer(results, body, bodyPred, bodyFake, bodyReal):
    with tf.variable_scope('loss',reuse=tf.AUTO_REUSE):
        eucLossDis,eucLossGen=gan_loss(results,bodyReal,bodyFake)
        eucLossGen+=norm_loss(results,body-bodyPred)*10
    print_all_variables()
    trainStep_dis=create_trainer('dis',results,eucLossDis,['clothToBodyDis/'])
    trainStep_gen=create_trainer('gen',results,eucLossGen,['clothToBody/'])
    return trainStep_dis,trainStep_gen
def clothToBody_test(results, sess, bundle=None):
    #create net
    if isinstance(bundle,type(None)):
        cloth,body,bodyPred,keep=clothToBody_create()
        createNet=True
    else:
        cloth,body,bodyPred,keep=bundle['cloth'],bundle['body'],bundle['bodyPred'],bundle['keep']
        createNet=False
    #load latest model
    if createNet:
        load_latest(results,sess)
    #check visualization folder
    if results.visualize:
        if os.path.exists('visualize'):
            shutil.rmtree('visualize')
        os.mkdir('visualize')
    #test
    i=0
    loss=0.0
    open_testDB(results)
    while results.testDB.epoch() == 0:
        batchDatums,batchImages=results.testDB.nextBatch(1)
        bodyFeat=sess.run(bodyPred,feed_dict={cloth:batchDatums[0],keep:1})
        bodyFeat=bodyFeat.reshape((bodyFeat.shape[1],))
        if results.visualize:
            #print('Visualizing %dth datum!'%i)
            results.testDB.writeClothVTK('visualize/cloth',i,batchDatums)
            results.testDB.writeBodyVTK('visualize/body',i,datum=batchDatums[-1])
            results.testDB.writeBodyVTK('visualize/bodyPred',i,datum=bodyFeat)
        loss+=np.linalg.norm(np.subtract(bodyFeat,batchDatums[-1]))
        i=i+1
    close_testDB(results)
    #profile loss
    loss/=float(i)
    return 0,loss,0
def clothToBody_train(results, sess):
    #create net/train
    if results.mode != 'normal':
        cloth,body,bodyPred,keep,bodyFake,bodyReal=clothToBodyGan_create()
        trainStep_dis,trainStep_gen=clothToBodyGan_create_trainer(results,body,bodyPred,bodyFake,bodyReal)
        train_tpl(results,sess,clothToBody_test,cloth=cloth,body=body,bodyPred=bodyPred,keep=keep,bodyReal=bodyReal,bodyFake=bodyFake,  \
                  trainStep_dis=trainStep_dis,trainStep_gen=trainStep_gen)
    else:
        cloth,body,bodyPred,keep=clothToBody_create()
        trainStep=clothToBody_create_trainer(results,body,bodyPred)
        train_tpl(results,sess,clothToBody_test,cloth=cloth,body=body,bodyPred=bodyPred,keep=keep,  \
                  trainStep=trainStep)

if __name__ == '__main__':
    print(tf.__version__)
    results=get_parser('model_clothToBody')
    with tf.Session() as sess:
        if results.op == 'train':
            results.visualize=False
            clothToBody_train(results,sess)
        elif results.op == 'test':
            clothToBody_test(results,sess)
