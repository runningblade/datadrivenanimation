import tensorflow as tf
import numpy as np
from dataset import *
from nn_utils import *
from bodyToCloth import *
import argparse,os,sys,shutil

def imageToBody_AlexNet(image, body, keep, nrk=[96,256,384,384,256,4096,4096], ss=4):
    with tf.variable_scope('conv1',reuse=tf.AUTO_REUSE):
        W=weight_variable([11,11,3,nrk[0]])
        b=bias_variable([nrk[0]])
        c1=tf.nn.relu(conv2d(image,W,stride=ss,pd='VALID')+b)
        nc1=tf.nn.local_response_normalization(c1,depth_radius=2,alpha=2e-05,beta=0.75,bias=1.0)
        pnc1=max_pool(nc1,ksize=3,stride=2,pd='VALID')
    with tf.variable_scope('conv2',reuse=tf.AUTO_REUSE):
        W=weight_variable([5,5,nrk[0],nrk[1]])
        b=bias_variable([nrk[1]])
        c2=tf.nn.relu(conv2d(pnc1,W,stride=ss,pd='VALID'))
        nc2=tf.nn.local_response_normalization(c2,depth_radius=2,alpha=2e-05,beta=0.75,bias=1.0)
        pnc2=max_pool(nc2,ksize=3,stride=2,pd='VALID')
    with tf.variable_scope('conv3',reuse=tf.AUTO_REUSE):
        W=weight_variable([3,3,nrk[1],nrk[2]])
        b=bias_variable([nrk[2]])
        c3=tf.nn.relu(conv2d(pnc2,W)+b)
    with tf.variable_scope('conv4',reuse=tf.AUTO_REUSE):
        W=weight_variable([3,3,nrk[2],nrk[3]])
        b=bias_variable([nrk[3]])
        c4=tf.nn.relu(conv2d(c3,W)+b)
    with tf.variable_scope('conv5',reuse=tf.AUTO_REUSE):
        W=weight_variable([3,3,nrk[3],nrk[4]])
        b=bias_variable([nrk[4]])
        c5=tf.nn.relu(conv2d(c4,W)+b)
        pc5=max_pool(c5,ksize=3,stride=2,pd='VALID')
    with tf.variable_scope('fc1',reuse=tf.AUTO_REUSE):
        s=pc5.shape
        sprod=s[1].value*s[2].value*s[3].value
        W=weight_variable([sprod,nrk[5]])
        b=bias_variable([nrk[5]])
        pc5Flat=tf.reshape(pc5,[-1,sprod])
        f1=tf.nn.relu(tf.matmul(pc5Flat,W)+b)
    with tf.variable_scope('dropout1',reuse=tf.AUTO_REUSE):
        df1=tf.nn.dropout(f1,keep)
    with tf.variable_scope('fc2',reuse=tf.AUTO_REUSE):
        W=weight_variable([nrk[5],nrk[6]])
        b=bias_variable([nrk[6]])
        f2=tf.nn.relu(tf.matmul(df1,W)+b)
    with tf.variable_scope('dropout2',reuse=tf.AUTO_REUSE):
        df2=tf.nn.dropout(f2,keep)
    with tf.variable_scope('fc3',reuse=tf.AUTO_REUSE):
        W=weight_variable([nrk[6],body.shape[1].value])
        b=bias_variable([body.shape[1].value])
        f3=tf.matmul(df2,W)+b
    return f3
def imageToBody_AlexNet_create():
    image=tf.placeholder(tf.float32,[None,256,256*3,3],name='image')
    body=tf.placeholder(tf.float32,[None,21],name='body')
    keep=tf.placeholder(tf.float32,name='keepProb')
    with tf.variable_scope('imageToBody',reuse=tf.AUTO_REUSE):
        bodyPred=imageToBody_AlexNet(image,body,keep, nrk=[64,128,256,256,128,1024,1024], ss=2)
    return image,body,bodyPred,keep
def imageToBodyGan_AlexNet_create():
    cloth=tf.placeholder(tf.float32,[None,35,74,3],name='cloth')
    image,body,bodyPred,keep=imageToBody_AlexNet_create()
    #discriminator
    with tf.variable_scope('imageToBodyDis',reuse=tf.AUTO_REUSE):
        bodyFake=bodyToCloth_MLP(bodyPred,cloth,keep,hasGan=True)
        bodyReal=bodyToCloth_MLP(body,cloth,keep,hasGan=True)
    return image,body,bodyPred,keep,bodyFake,bodyReal
def imageToBody_AlexNet_create_trainer(results, body, bodyPred):
    with tf.variable_scope('loss',reuse=tf.AUTO_REUSE):
        eucLoss=norm_loss(results,body-bodyPred)
    print_all_variables()
    return create_trainer('',results,eucLoss)
def imageToBodyGan_AlexNet_create_trainer(results, body, bodyPred, bodyFake, bodyReal):
    with tf.variable_scope('loss',reuse=tf.AUTO_REUSE):
        eucLossDis,eucLossGen=gan_loss(results,bodyReal,bodyFake)
        eucLossGen+=norm_loss(results,body-bodyPred)*10
    print_all_variables()
    trainStep_dis=create_trainer('dis',results,eucLossDis,['imageToBodyDis/'])
    trainStep_gen=create_trainer('gen',results,eucLossGen,['imageToBody/'])
    return trainStep_dis,trainStep_gen
def imageToBody_test(results, sess, bundle=None):
    #create net
    if isinstance(bundle,type(None)):
        image,body,bodyPred,keep=imageToBody_AlexNet_create()
        createNet=True
    else:
        image,body,bodyPred,keep=bundle['image'],bundle['body'],bundle['bodyPred'],bundle['keep']
        createNet=False
    #load latest model
    if createNet:
        load_latest(results,sess)
    #check visualization folder
    if results.visualize:
        if os.path.exists('visualize'):
            shutil.rmtree('visualize')
        os.mkdir('visualize')
    #test
    i=0
    loss=0.0
    open_testDB(results)
    while results.testDB.epoch() == 0:
        batchDatums,batchImages=results.testDB.nextBatch(1)
        bodyFeat=sess.run(bodyPred,feed_dict={image:batchImages[0],keep:1})
        bodyFeat=bodyFeat.reshape((bodyFeat.shape[1],))
        if results.visualize:
            #print('Visualizing %dth datum!'%i)
            results.testDB.writeImage('visualize/image',i,batchImages)
            results.testDB.writeBodyVTK('visualize/body',i,datum=batchDatums[-1])
            results.testDB.writeBodyVTK('visualize/bodyPred',i,datum=bodyFeat)
        loss+=np.linalg.norm(np.subtract(bodyFeat,batchDatums[-1]))
        i=i+1
    close_testDB(results)
    #profile loss
    loss/=float(i)
    return 0,0,loss
def imageToBody_train(results, sess):
    #create net/trainer
    if results.mode != 'normal':
        image,body,bodyPred,keep,bodyFake,bodyReal=imageToBodyGan_AlexNet_create()
        trainStep_dis,trainStep_gen=imageToBodyGan_AlexNet_create_trainer(results,body,bodyPred,bodyFake,bodyReal)
        train_tpl(results,sess,imageToBody_test,image=image,body=body,bodyPred=bodyPred,keep=keep,bodyReal=bodyReal,bodyFake=bodyFake,    \
                  trainStep_dis=trainStep_dis,trainStep_gen=trainStep_gen)
    else:
        image,body,bodyPred,keep=imageToBody_AlexNet_create()
        trainStep=imageToBody_AlexNet_create_trainer(results,body,bodyPred)
        train_tpl(results,sess,imageToBody_test,image=image,body=body,bodyPred=bodyPred,keep=keep,    \
                  trainStep=trainStep)

if __name__ == '__main__':
    print(tf.__version__)
    results=get_parser('model_imageToBody',True)
    with tf.Session() as sess:
        if results.op == 'train':
            results.visualize=False
            imageToBody_train(results,sess)
        elif results.op == 'test':
            imageToBody_test(results,sess)