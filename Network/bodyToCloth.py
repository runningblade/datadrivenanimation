import tensorflow as tf
import numpy as np
from dataset import *
from nn_utils import *
from clothToBody import *
import argparse,os,sys,shutil

def bodyToCloth(body, cloth, keep, s={'fc':[1024,1024]}, hasGan=False):
    #full connected layers
    last=body
    for iFc in xrange(len(s['fc'])):
        with tf.variable_scope('fc'+str(iFc+1),reuse=tf.AUTO_REUSE):
            fcVar=s['fc'][iFc]
            W=weight_variable([get_shape(last),fcVar])
            b=bias_variable([fcVar])
            last=tf.nn.relu(tf.matmul(last,W)+b)
    #dropout
    with tf.variable_scope('dropout',reuse=tf.AUTO_REUSE):
        last=tf.nn.dropout(last,keep)
    #output head
    if hasGan:
        with tf.variable_scope('discriminator',reuse=tf.AUTO_REUSE):
            W=weight_variable([get_shape(last),1])
            b=bias_variable([1])
            dis=tf.sigmoid(tf.matmul(last,W)+b)
        return dis
    else: 
        with tf.variable_scope('output',reuse=tf.AUTO_REUSE):
            W=weight_variable([get_shape(last),get_shape(cloth)])
            b=bias_variable([get_shape(cloth)])
            last=tf.matmul(last,W)+b
            output=reshape_like(last,cloth)
        return output
def bodyToCloth_create():
    cloth=tf.placeholder(tf.float32,[None,35,74,3],name='cloth')
    body=tf.placeholder(tf.float32,[None,21],name='body')
    keep=tf.placeholder(tf.float32,name='keepProb')
    with tf.variable_scope('bodyToCloth',reuse=tf.AUTO_REUSE):
        clothPred=bodyToCloth(body,cloth,keep)
    return cloth,clothPred,body,keep
def bodyToClothGan_create():
    cloth,clothPred,body,keep=bodyToCloth_create()
    #discriminator
    with tf.variable_scope('bodyToClothDis',reuse=tf.AUTO_REUSE):
        clothFake=clothToBody(clothPred,body,keep,hasGan=True)
        clothReal=clothToBody(cloth,body,keep,hasGan=True)
    return cloth,clothPred,body,keep,clothFake,clothReal
def bodyToCloth_create_trainer(results, cloth, clothPred):
    with tf.variable_scope('loss',reuse=tf.AUTO_REUSE):
        eucLoss=norm_loss(results,cloth-clothPred)
    print_all_variables()
    return create_trainer('',results,eucLoss)
def bodyToClothGan_create_trainer(results, cloth, clothPred, clothFake, clothReal):
    with tf.variable_scope('loss',reuse=tf.AUTO_REUSE):
        eucLossDis,eucLossGen=gan_loss(results,clothReal,clothFake)
        eucLossGen+=norm_loss(results,cloth-clothPred)*10
    print_all_variables()
    trainStep_dis=create_trainer('dis',results,eucLossDis,['bodyToClothDis/'])
    trainStep_gen=create_trainer('gen',results,eucLossGen,['bodyToCloth/'])
    return trainStep_dis,trainStep_gen
def bodyToCloth_test(results, sess, bundle=None):
    #create net
    if isinstance(bundle,type(None)):
        cloth,clothPred,body,keep=bodyToCloth_create()
        createNet=True
    else:
        cloth,clothPred,body,keep=bundle['cloth'],bundle['clothPred'],bundle['body'],bundle['keep']
        createNet=False
    #load latest model
    if createNet:
        load_latest(results,sess)
    #check visualization folder
    if results.visualize:
        if os.path.exists('visualize'):
            shutil.rmtree('visualize')
        os.mkdir('visualize')
    #test
    i=0
    loss=0.0
    open_testDB(results)
    while results.testDB.epoch() == 0:
        batchDatums,batchImages=results.testDB.nextBatch(1)
        clothFeat=sess.run(clothPred,feed_dict={body:batchDatums[-1],keep:1})
        clothFeat=clothFeat.reshape((clothFeat.shape[1],clothFeat.shape[2],clothFeat.shape[3]))
        if results.visualize:
            #print('Visualizing %dth datum!'%i)
            results.testDB.writeClothVTK('visualize/cloth',i,batchDatums)
            results.testDB.writeClothVTK('visualize/clothPred',i,datums=[clothFeat])
            results.testDB.writeBodyVTK('visualize/body',i,datum=batchDatums[-1])
        loss+=np.linalg.norm(np.subtract(clothFeat,batchDatums[0]))
        i=i+1
    close_testDB(results)
    #profile loss
    loss/=float(i)
    return loss,0,0
def bodyToCloth_train(results, sess):
    #create net/trainer
    if results.mode != 'normal':
        cloth,clothPred,body,keep,clothFake,clothReal=bodyToClothGan_create()
        trainStep_dis,trainStep_gen=bodyToClothGan_create_trainer(results,cloth,clothPred,clothFake,clothReal)
        train_tpl(results,sess,bodyToCloth_test,cloth=cloth,clothPred=clothPred,body=body,keep=keep,clothReal=clothReal,clothFake=clothFake,    \
                  trainStep_dis=trainStep_dis,trainStep_gen=trainStep_gen)
    else:
        cloth,clothPred,body,keep=bodyToCloth_create()
        trainStep=bodyToCloth_create_trainer(results,cloth,clothPred)
        train_tpl(results,sess,bodyToCloth_test,cloth=cloth,clothPred=clothPred,body=body,keep=keep,    \
                  trainStep=trainStep)

if __name__ == '__main__':
    print(tf.__version__)
    results=get_parser('model_bodyToCloth')
    with tf.Session() as sess:
        if results.op == 'train':
            results.visualize=False
            bodyToCloth_train(results,sess)
        elif results.op == 'test':
            bodyToCloth_test(results,sess)