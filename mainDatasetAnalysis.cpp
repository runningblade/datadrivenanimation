#include "DataDrivenOpt/SkinnedMeshClothUtility.h"
#include <boost/filesystem/operations.hpp>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

typedef map<string,SkinnedMeshClothUtility::Cluster> CLUSTERS;
Vec2 distRange(const CLUSTERS& clusters)
{
  Vec2 minMax(ScalarUtil<scalar>::scalar_max,-ScalarUtil<scalar>::scalar_max);
  for(CLUSTERS::const_iterator beg=clusters.begin(),end=clusters.end(); beg!=end; beg++) {
    const SkinnedMeshClothUtility::Cluster& c=beg->second;
    for(sizeType i=0; i<(sizeType)c._nodes.size(); i++) {
      if(c._nodes[i]._idFrm == -1) {
        minMax[0]=min(minMax[0],c._nodes[i]._dist);
        minMax[1]=max(minMax[1],c._nodes[i]._dist);
      }
    }
  }
  return minMax;
}
sizeType countLeaves(const CLUSTERS& clusters)
{
  sizeType nrLeaves=0;
  for(CLUSTERS::const_iterator beg=clusters.begin(),end=clusters.end(); beg!=end; beg++) {
    const SkinnedMeshClothUtility::Cluster& c=beg->second;
    nrLeaves+=((sizeType)c._nodes.size()+1)/2;
  }
  return nrLeaves;
}
sizeType countLeaves(const SkinnedMeshClothUtility::Cluster& cluster,sizeType id,const string& subjectIdStr,const set<string>& excludedKeys)
{
  sizeType fid=cluster._nodes[id]._idFrm;
  if(fid >= 0) {
    string label=subjectIdStr+"_"+boost::lexical_cast<string>(fid);
    return excludedKeys.find(label) == excludedKeys.end() ? 1 : 0;
  }
  sizeType l=countLeaves(cluster,cluster._nodes[id]._l,subjectIdStr,excludedKeys);
  sizeType r=countLeaves(cluster,cluster._nodes[id]._r,subjectIdStr,excludedKeys);
  return l+r;
}
sizeType randomLeave(const SkinnedMeshClothUtility::Cluster& c,sizeType id)
{
  while(true) {
    if(c._nodes[id]._idFrm >= 0)
      return c._nodes[id]._idFrm;
    else if(RandEngine::randR01() > 0.5f)
      id=c._nodes[id]._l;
    else id=c._nodes[id]._r;
  }
  return -1;
}
vector<string> filterCluster(const CLUSTERS& clusters,scalar distThres,const set<string>& excludedKeys)
{
  vector<string> ret;
  for(CLUSTERS::const_iterator beg=clusters.begin(),end=clusters.end(); beg!=end; beg++) {
    boost::filesystem::path p(beg->first);
    const SkinnedMeshClothUtility::Cluster& c=beg->second;
    string subjectIdStr=p.replace_extension("").filename().string();
    sizeType nrLeaves=((sizeType)c._nodes.size()+1)/2;
    //find roots
    stack<sizeType> treeS;
    vector<sizeType> roots;
    treeS.push(nrLeaves*2-2);
    while(!treeS.empty()) {
      sizeType id=treeS.top();
      treeS.pop();
      if(c._nodes[id]._dist < distThres) {
        if(excludedKeys.empty())
          roots.push_back(randomLeave(c,id));
        else if(countLeaves(c,id,subjectIdStr,excludedKeys) > 0)
          while(true) {
            sizeType lid=randomLeave(c,id);
            string label=subjectIdStr+"_"+boost::lexical_cast<string>(lid);
            if(excludedKeys.find(label) == excludedKeys.end()) {
              roots.push_back(lid);
              break;
            }
          }
      } else if(c._nodes[id]._l >= 0) {
        treeS.push(c._nodes[id]._l);
        treeS.push(c._nodes[id]._r);
      }
    }
    //push to ret
    for(sizeType i=0; i<(sizeType)roots.size(); i++)
      ret.push_back(subjectIdStr+"_"+boost::lexical_cast<string>(roots[i]));
  }
  return ret;
}
int main(int argc, char *argv[])
{
  if(argc < 5) {
    WARNING("Usage: mainDatasetAnalysis clothMesh folder feature_type max_capacity [target_size] [excluded_keys]!")
    return 0;
  }
  set<string> excludedKeys; //these keys are not included
  sizeType targetSize=0;
  if(argc > 5)
    targetSize=boost::lexical_cast<sizeType>(argv[5]);
  if(argc > 6) {
    string line;
    boost::filesystem::ifstream is(argv[6]);
    while (std::getline(is,line))
      excludedKeys.insert(line);
  }
  //analyze datasetName
  string clothName,datasetName=string(argv[2]);
  sizeType pos=datasetName.find("_");
  if(pos < 0) {
    INFO("Cannot find datasetName!")
    return 0;
  }
  clothName=datasetName.substr(0,pos);
  datasetName=datasetName.substr(pos+1);
  sizeType feat=SkinnedMeshUtility::parseFeatureFromString(argv[3]);
  sizeType maxCapacity=boost::lexical_cast<sizeType>(argv[4]);
  //find which dataset to cluster
  vector<string> clusterNames;
  for(boost::filesystem::directory_iterator beg(getSourcePath()+"/"+datasetName),end; beg!=end; beg++)
    clusterNames.push_back(beg->path().string());
  sort(clusterNames.begin(),clusterNames.end());
  //cluster
  CLUSTERS clusters;
  create("cluster_"+clothName);
  create("cluster_"+clothName+"/"+datasetName);
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)clusterNames.size(); i++) {
    boost::filesystem::path p(clusterNames[i]);
    string subjectIdStr=p.replace_extension("").filename().string();
    string pathCluster="cluster_"+clothName+"/"+datasetName+"/"+subjectIdStr+".cluster";
    bool exists=false;
    OMP_CRITICAL_
    exists=boost::filesystem::exists(pathCluster);
    if(exists) {
      INFOV("Reading cluster from: %s!",pathCluster.c_str())
      clusters[clusterNames[i]].read(pathCluster);
    } else {
      INFOV("Reading subject %s!",clusterNames[i].c_str())
      SkinnedMeshClothUtility body;
      body.readSkinnedMesh(clusterNames[i]);
      //cluter
      INFOV("Writing cluster to: %s, maxCapacity=%ld!",pathCluster.c_str(),maxCapacity)
      clusters[clusterNames[i]]=body.cluster(clothName,(SkinnedMeshClothUtility::FEATURE_TYPE)feat,maxCapacity);
      clusters[clusterNames[i]].write(pathCluster);
    }
  }
  //sample
  Vec2 minMax=distRange(clusters);
  sizeType totalLeaves=countLeaves(clusters),lastSz=-1;
  INFOV("MinDist: %f, MaxDist: %f, TotalLeaves: %ld!",minMax[0],minMax[1],totalLeaves)
  ASSERT(totalLeaves > targetSize)
  if(targetSize == 0)
    return 0;
  //search
  scalar a=minMax[0],b=minMax[1],mid;
  vector<string> keys;
  while(abs(a-b) > 1E-6f) {
    mid=(a+b)/2;
    keys=filterCluster(clusters,mid,excludedKeys);
    INFOV("Found %ld datums when distThres=%f!",keys.size(),mid)
    if((sizeType)keys.size() > targetSize)
      a=mid;
    else if((sizeType)keys.size() < targetSize)
      b=mid;
    else if((sizeType)keys.size() == targetSize || (sizeType)keys.size() == lastSz)
      break;
    lastSz=(sizeType)keys.size();
  }
  //write
  boost::filesystem::ofstream os("cluster_"+clothName+"/"+datasetName+"/"+(excludedKeys.empty() ? "keys.dat" : "excluded_keys.dat"));
  for(sizeType i=0; i<(sizeType)keys.size(); i++)
    os << keys[i] << endl;
  return 0;
}
