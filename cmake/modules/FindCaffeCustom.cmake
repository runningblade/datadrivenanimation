SET(Caffe_DIR "" CACHE STRING
    "Custom location of the root directory of a Caffe installation")

FIND_PATH(Caffe_INCLUDE_DIR_CUSTOM
          NAMES caffe.hpp
          PATH_SUFFIXES "include/caffe" "caffe"
                        "include/Caffe" "Caffe"
          PATHS ${Caffe_DIR}
          DOC "The directory where caffe.hpp resides.")

FIND_LIBRARY(Caffe_LIBRARY_CUSTOM
             NAMES caffe caffe-d
             PATH_SUFFIXES "lib" "libs"
                           "libs/caffe" "lib/caffe"
                           "libs/Caffe" "lib/Caffe"
             PATHS ${Caffe_DIR}
             DOC "The Caffe library.")

# handle the QUIETLY and REQUIRED arguments and set CAFFE_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Caffe "Could NOT find Caffe. Declare Caffe_DIR (either using -DCaffe_DIR=<path> flag or via CMakeGUI/ccmake) to point to root directory of library."
                                  Caffe_LIBRARY_CUSTOM Caffe_INCLUDE_DIR_CUSTOM)

# test result
IF(Caffe_INCLUDE_DIR_CUSTOM AND Caffe_LIBRARY_CUSTOM)
  SET(Caffe_FOUND TRUE)
ELSE()
  SET(Caffe_FOUND FALSE)
ENDIF()

IF(Caffe_FOUND)
  SET(Caffe_LIBRARIES ${Caffe_LIBRARY_CUSTOM})
  GET_FILENAME_COMPONENT(PARENT_DIR ${Caffe_INCLUDE_DIR_CUSTOM} PATH)
  SET(Caffe_INCLUDE_DIR ${Caffe_INCLUDE_DIR_CUSTOM};${PARENT_DIR})
  SET(Caffe_INCLUDE_DIRS ${Caffe_INCLUDE_DIR_CUSTOM})
ENDIF(Caffe_FOUND)
