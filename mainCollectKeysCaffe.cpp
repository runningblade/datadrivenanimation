#include <DataDrivenOpt/Utils.h>
#include <DataDrivenOpt/CaffeUtils.h>

USE_PRJ_NAMESPACE

int main(int argc, char *argv[])
{
  if(argc < 4) {
    WARNING("Usage: mainCollectKeysCaffe clothMesh folder prefix [mode] [idDataset] [idTraj]!")
    return 0;
  }
  bool randomMode=false;
  bool trajMode=false;
  bool meanMode=false;
  bool printMode=false;
  string idDataset="";
  int idTraj=-1;
  if(argc > 4) {
    if(beginsWith(string(argv[4]),"randomMode")) {
      INFO("randomMode")
      randomMode=true;
    } else if(beginsWith(string(argv[4]),"trajMode")) {
      ASSERT(argc >= 7)
      INFO("trajMode")
      idDataset=string(argv[5]);
      idTraj=boost::lexical_cast<int>(string(argv[6]));
      trajMode=true;
    } else if(beginsWith(string(argv[4]),"meanMode")) {
      meanMode=true;
    } else if(beginsWith(string(argv[4]),"printMode")) {
      printMode=true;
    }
  }
  //analyze datasetName
  string clothName,datasetName=string(argv[2]);
  string prefix=string(argv[3]);
  sizeType pos=datasetName.find("_");
  if(pos < 0) {
    INFO("Cannot find datasetName!")
    return 0;
  }
  clothName=datasetName.substr(0,pos);
  datasetName=datasetName.substr(pos+1);
  INFOV("Found datasetName: %s!",datasetName.c_str())
  //open file in print mode
  boost::shared_ptr<boost::filesystem::ofstream> osPrint;
  if(printMode)
    osPrint.reset(new boost::filesystem::ofstream("keys.dat"));
  //create blobproto
  vector<vector<scalarD> > protoDat;
  //open db
  google::InitGoogleLogging(argv[0]);
  boost::shared_ptr<db::DB> db(db::GetDB("leveldb"));
  db->Open(getSourcePath()+"/"+prefix+"_"+string(argv[2]),db::READ);
  boost::shared_ptr<db::Cursor> c(db->NewCursor());
  //create db if randomMode or trajMode
  boost::shared_ptr<db::DB> dbTest(db::GetDB("leveldb"));
  boost::shared_ptr<db::Transaction> txnTest;
  if(randomMode || trajMode) {
    reopenDB(*dbTest,getSourcePath()+"/test_"+prefix+"_"+string(argv[2]));
    txnTest.reset(dbTest->NewTransaction());
  }
  //main loop
  sizeType k=0;
  map<DatasetKey,string> keys;
  map<DatasetKey,scalar> probs;
  SkinnedMeshClothUtility body;
  for(; c->valid(); k++,c->Next()) {
    DatasetKey keyS(c->key());
    if(k%10000 == 0) {
      INFOV("Parsed %ld datums!",k)
    }
    if(randomMode) {
      keyS._idFrm=-1;
      if(keys.find(keyS) == keys.end()) {
        string bodyPath=getSourcePath()+"/"+datasetName+"/"+keyS._idDataset+".data";
        body.readSkinnedMesh(bodyPath);
        //compute prbability
        keys[keyS]=c->key();
        txnTest->Put(keyS.str(),c->value());
        probs[keyS]=1/(scalar)body.nrFrm();
      } else if(RandEngine::randR01() < probs[keyS]) {
        keys[keyS]=c->key();
        txnTest->Put(keyS.str(),c->value());
      }
    } else if(trajMode) {
      if(keyS._idDataset == idDataset && keyS._idTraj == idTraj) {
        keys[keyS]=c->key();
        txnTest->Put(keyS.str(),c->value());
      }
    } else if(meanMode) {
      vector<Datum> datums;
      vector<string> images;
      scalarD coef=1/(scalarD)(k+1);
      keyS.decompressDatums(datums,images,c->value());
      if(k == 0) {
        protoDat.resize(datums.size());
        for(sizeType i=0; i<(sizeType)datums.size(); i++)
          protoDat[i].assign(datums[i].float_data_size(),0);
      }
      for(sizeType i=0; i<(sizeType)datums.size(); i++) {
        OMP_PARALLEL_FOR_
        for(sizeType j=0; j<(sizeType)protoDat[i].size(); j++)
          protoDat[i][j]=protoDat[i][j]+(datums[i].float_data(j)-protoDat[i][j])*coef;
      }
    } else if(printMode) {
      *osPrint << keyS._idDataset << "_" << keyS._idFrm << endl;
    }
  }
  if(randomMode || trajMode) {
    boost::filesystem::ofstream os("keys.dat");
    for(map<DatasetKey,string>::const_iterator beg=keys.begin(),end=keys.end(); beg!=end; beg++)
      os << beg->second << endl;
  } else if(meanMode) {
    for(sizeType i=0; i<(sizeType)protoDat.size(); i++) {
      BlobProto proto;
      proto.set_num(1);
      proto.set_channels(1);
      proto.set_height(1);
      proto.set_width((int)protoDat[i].size());
      for(sizeType j=0; j<(sizeType)protoDat[i].size(); j++)
        proto.add_data((float)protoDat[i][j]);
      WriteProtoToBinaryFile(proto,"mean"+boost::lexical_cast<string>(i)+"_"+string(argv[2])+".dat");
      WriteProtoToTextFile(proto,"meanText"+boost::lexical_cast<string>(i)+"_"+string(argv[2])+".dat");
    }
  }
  //finish db
  c=NULL;
  db=NULL;
  if(txnTest) {
    txnTest->Commit();
    txnTest=NULL;
    dbTest=NULL;
  }
}
