#include "Gui.h"
#include <DataDrivenOpt/Utils.h>
#include <QApplication>
#include <QSplitter>
#include <QMenuBar>
#include <QStatusBar>
#include <boost/filesystem/path.hpp>
#include <osg/ShapeDrawable>
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/SphericalManipulator>
#include <osgShadow/SoftShadowMap>
#include <osgShadow/ShadowedScene>
#include <osgDB/ReadFile>
#include <osg/Group>

//Gui
Gui::Gui():_model(NULL)
{
#define W1 2
#define W2 1
  QSize sz(800,600);
  setWindowTitle(tr("Data-Driven Cloth Animator"));
  //create layout
  QSplitter* splitter=new QSplitter();
  setCentralWidget(splitter);
  //add file explorer
  {
    _explorer=new QTreeView(this);
    refresh();
    QSizePolicy spLeft(QSizePolicy::Preferred,QSizePolicy::Preferred);
    QItemSelectionModel* selectionModel=_explorer->selectionModel();
    connect(selectionModel,SIGNAL(selectionChanged(const QItemSelection&,const QItemSelection&)),this,SLOT(selectChanged(const QItemSelection&,const QItemSelection&)));
    spLeft.setHorizontalStretch(W1);
    _explorer->setSizePolicy(spLeft);
    splitter->addWidget(_explorer);
  }
  //add renderer
  {
    resetScene();
    _gw=createGraphicsWindow(0,0,sz.width(),sz.height());
    _renderer=addViewWidget(_gw,_scene);
    QSizePolicy spRight(QSizePolicy::Preferred,QSizePolicy::Preferred);
    spRight.setHorizontalStretch(W2);
    _renderer->setSizePolicy(spRight);
    splitter->addWidget(_renderer);
    connect(&_timer,SIGNAL(timeout()),this,SLOT(update()));
    _timer.start(10);
  }
  createFileMenu();
  createViewMenu();
  //status bar
  statusBar()->setSizeGripEnabled(false);
  //size
  setMinimumSize(sz.width(),sz.height());
  setMaximumSize(sz.width(),sz.height());
#undef W1
#undef W2
}
void Gui::createFileMenu()
{
  //create menu
  _file=menuBar()->addMenu("&File");
  //expand button
  {
    QAction* expand=new QAction("&Expand",this);
    _file->addAction(expand);
    connect(expand,SIGNAL(triggered()),this,SLOT(expand()));
  }
  //collapse button
  {
    QAction* collapse=new QAction("&Collapse",this);
    _file->addAction(collapse);
    connect(collapse,SIGNAL(triggered()),this,SLOT(collapse()));
  }
  //refresh button
  {
    QAction* refresh=new QAction("&Refresh",this);
    _file->addAction(refresh);
    connect(refresh,SIGNAL(triggered()),this,SLOT(refresh()));
  }
  _file->addSeparator();
  //exit button
  {
    QAction* quit=new QAction("&Quit",this);
    _file->addAction(quit);
    connect(quit,SIGNAL(triggered()),qApp,SLOT(quit()));
  }
}
void Gui::createViewMenu()
{
  //create menu
  _view=menuBar()->addMenu("&Renderer");
  //reset button
  {
    QAction* reset=new QAction("&Reset Camera",this);
    _view->addAction(reset);
    connect(reset,SIGNAL(triggered()),this,SLOT(resetCamera()));
  }
}
void Gui::paintEvent(QPaintEvent* event)
{
  _viewer->frame();
}
//slots
void Gui::expand()
{
  _explorer->expandAll();
}
void Gui::collapse()
{
  _explorer->collapseAll();
}
void Gui::refresh()
{
  _explorer->setModel(NULL);
  if(_model)
    delete _model;
  //setup
  _model=new TreeModel(QString(COMMON::getSourcePath().c_str()));
  _explorer->setModel(_model);
  _explorer->setColumnHidden(1,true);
  _explorer->setColumnHidden(2,true);
}
void Gui::update()
{
  _viewer->frame();
}
void Gui::clearScene(osg::Group* group,bool addSphere)
{
#define SS 2.5f
  while(group->getNumChildren() > 2)
    group->removeChild(group->getNumChildren()-1);
  //sphere
  if(addSphere) {
    osg::Geode* sphere=new osg::Geode();
    sphere->addDrawable(new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(0,0,SS*2),SS)));
    sphere->getOrCreateStateSet()->setAttribute(createFullMaterial(osg::Vec4(1,0,0,1)));
    sphere->setNodeMask(castShadowMask());
    group->addChild(sphere);
  }
#undef SS
}
void Gui::resetScene()
{
#define LP 50
#define LS 5
  osgShadow::SoftShadowMap* shadow=new osgShadow::SoftShadowMap;
  osgShadow::ShadowedScene* shadowRoot;
  if(_scene)
    shadowRoot=dynamic_cast<osgShadow::ShadowedScene*>(_scene.get());
  else shadowRoot=new osgShadow::ShadowedScene;
  osgShadow::ShadowSettings* settings=shadowRoot->getShadowSettings();
  settings->setTextureSize(osg::Vec2s(2048,2048));
  settings->setReceivesShadowTraversalMask(rcvShadowMask());
  settings->setCastsShadowTraversalMask(castShadowMask());
  settings->setComputeNearFarModeOverride(osg::CullSettings::COMPUTE_NEAR_FAR_USING_BOUNDING_VOLUMES);
  shadowRoot->setShadowTechnique(shadow);
  shadow->setSoftnessWidth(0.01f);
  //floor
  osg::Geode* floor=createFloor();
  floor->setNodeMask(rcvShadowMask());
  shadowRoot->addChild(floor);
  //light
  osg::PositionAttitudeTransform* light=createLightNode(osg::Vec3(0,0,LP),osg::Vec4(LS,LS,LS,1),shadowRoot,0.1f);
  light->setNodeMask(rcvShadowMask());
  shadowRoot->addChild(light);
  //clear everything but add a sphere
  clearScene(shadowRoot,true);
  _scene=shadowRoot;
#undef LP
#undef LS
}
void Gui::resetCamera()
{
  osgGA::SphericalManipulator* mani=dynamic_cast<osgGA::SphericalManipulator*>(_viewer->getCameraManipulator());
  mani->home(0);
}
void Gui::selectChanged(const QItemSelection& sel,const QItemSelection& item)
{
  if(sel.size() == 0)
    return;
  QModelIndex index=sel.begin()->topLeft();
  if(!index.isValid())
    return;
  QString type=_model->itemData(_model->index(index.row(),1,sel.begin()->parent())).value(0).toString();
  QString path=_model->itemData(_model->index(index.row(),2,sel.begin()->parent())).value(0).toString();
  QString msg=tr("Selected ")+type+tr(": ")+path;
  statusBar()->showMessage(msg,1000);
}
//helper
unsigned int Gui::castShadowMask() const
{
  return 0x2;
}
unsigned int Gui::rcvShadowMask() const
{
  return 0x1;
}
osg::Geode* Gui::createFloor() const
{
#define FSZ 250
#define FR 0.8f
#define FG 0.8f
#define FB 0.8f
#define FT 1
  osg::Geode* floor=new osg::Geode();
  osg::Geometry* floorGeom=new osg::Geometry();
  floor->addDrawable(floorGeom);
  //texture
  osg::Texture2D* tex=new osg::Texture2D;
  tex->setDataVariance(osg::Object::DYNAMIC);
  tex->setImage(osgDB::readImageFile(COMMON::getSourcePath()+"/FloorTexture.png"));
  tex->setWrap(osg::Texture2D::WRAP_R,osg::Texture2D::REPEAT);
  tex->setWrap(osg::Texture2D::WRAP_S,osg::Texture2D::REPEAT);
  tex->setWrap(osg::Texture2D::WRAP_T,osg::Texture2D::REPEAT);
  osg::StateSet* state=new osg::StateSet();
  state->setTextureAttributeAndModes(0,tex,osg::StateAttribute::ON);
  state->setAttribute(createFullMaterial(osg::Vec4(FR,FG,FB,1)));
  floor->setStateSet(state);
  //vertices
  osg::Vec3Array* vss=new osg::Vec3Array;
  vss->push_back(osg::Vec3(-FSZ,-FSZ,0));
  vss->push_back(osg::Vec3( FSZ,-FSZ,0));
  vss->push_back(osg::Vec3( FSZ, FSZ,0));
  vss->push_back(osg::Vec3(-FSZ, FSZ,0));
  floorGeom->setVertexArray(vss);
  //indices
  osg::DrawElementsUInt* iss=new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS,0);
  iss->push_back(3);
  iss->push_back(2);
  iss->push_back(1);
  iss->push_back(0);
  floorGeom->addPrimitiveSet(iss);
  //normals
  osg::Vec3Array* nss=new osg::Vec3Array;
  nss->push_back(osg::Vec3(0,0,1));
  nss->push_back(osg::Vec3(0,0,1));
  nss->push_back(osg::Vec3(0,0,1));
  nss->push_back(osg::Vec3(0,0,1));
  floorGeom->setNormalArray(nss);
  floorGeom->setNormalBinding(osg::Geometry::BIND_PER_VERTEX);
  //colors
  osg::Vec4Array* colors=new osg::Vec4Array;
  colors->push_back(osg::Vec4(1,1,1,1));
  colors->push_back(osg::Vec4(1,1,1,1));
  colors->push_back(osg::Vec4(1,1,1,1));
  colors->push_back(osg::Vec4(1,1,1,1));
  floorGeom->setColorArray(colors);
  floorGeom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
  //tcoords
  osg::Vec2Array* tss=new osg::Vec2Array(4);
  (*tss)[0].set( 0, 0);
  (*tss)[1].set(FT, 0);
  (*tss)[2].set(FT,FT);
  (*tss)[3].set( 0,FT);
  floorGeom->setTexCoordArray(0,tss);
  return floor;
#undef FT
#undef FB
#undef FG
#undef FR
#undef FSZ
}
osg::Light* Gui::createLight(osg::Vec4 color) const
{
  static int lightId=0;
  osg::Light* light=new osg::Light();
  light->setLightNum(lightId++);
  light->setPosition(osg::Vec4(0,0,0,1));
  light->setDiffuse(color);
  light->setSpecular(osg::Vec4(1,1,1,1));
  light->setAmbient(osg::Vec4(0,0,0,1));
  return light;
}
osg::Material* Gui::createSimpleMaterial(osg::Vec4 color) const
{
  osg::Material* material=new osg::Material();
  material->setDiffuse(osg::Material::FRONT,osg::Vec4(0,0,0,1));
  material->setEmission(osg::Material::FRONT,color);
  return material;
}
osg::Material* Gui::createFullMaterial(osg::Vec4 color) const
{
  osg::Material* material=new osg::Material();
  material->setDiffuse(osg::Material::FRONT,color);
  material->setSpecular(osg::Material::FRONT,color);
  material->setAmbient(osg::Material::FRONT,osg::Vec4(0.1,0.1,0.1,1));
  material->setEmission(osg::Material::FRONT,osg::Vec4(0,0,0,1));
  material->setShininess(osg::Material::FRONT,25);
  return material;
}
osg::PositionAttitudeTransform* Gui::createLightNode(osg::Vec3 pos,osg::Vec4 color,osg::Node* parent,double radius) const
{
  osg::Geode* marker=new osg::Geode();
  marker->addDrawable(new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(),radius)));
  marker->getOrCreateStateSet()->setAttribute(createSimpleMaterial(color));

  osg::LightSource* src=new osg::LightSource;
  src->setLight(createLight(color));
  src->setLocalStateSetModes(osg::StateAttribute::ON);
  src->setStateSetModes(*(parent->getOrCreateStateSet()),osg::StateAttribute::ON);

  osg::PositionAttitudeTransform* ret=new osg::PositionAttitudeTransform;
  ret->addChild(src);
  ret->addChild(marker);
  ret->setPosition(pos);
  return ret;
}
//helper
QWidget* Gui::addViewWidget(osgQt::GraphicsWindowQt* gw,osg::ref_ptr<osg::Node> scene)
{
  const osg::GraphicsContext::Traits* traits=gw->getTraits();
  _viewer=new osgViewer::Viewer;
  _viewer->setThreadingModel(osgViewer::Viewer::SingleThreaded);
  osg::Camera* camera=_viewer->getCamera();
  camera->setGraphicsContext(gw);
  camera->setClearColor(osg::Vec4(1,1,1,1));
  camera->setViewport(new osg::Viewport(0,0,traits->width,traits->height));
  camera->setProjectionMatrixAsPerspective(30.0f,static_cast<double>(traits->width)/static_cast<double>(traits->height),1.0f,10000.0f);

  _viewer->setSceneData(scene);
  _viewer->addEventHandler(new osgViewer::StatsHandler);
  _viewer->setCameraManipulator(new osgGA::SphericalManipulator);
  return gw->getGLWidget();
}
osgQt::GraphicsWindowQt* Gui::createGraphicsWindow(int x,int y,int w,int h,const std::string& name,bool windowDecoration)
{
  osg::DisplaySettings* ds=osg::DisplaySettings::instance().get();
  osg::ref_ptr<osg::GraphicsContext::Traits> traits=new osg::GraphicsContext::Traits;
  ds->setNumMultiSamples(16);

  traits->windowName=name;
  traits->windowDecoration=windowDecoration;
  traits->x=x;
  traits->y=y;
  traits->width=w;
  traits->height=h;
  traits->doubleBuffer=true;
  traits->alpha=ds->getMinimumNumAlphaBits();
  traits->stencil=ds->getMinimumNumStencilBits();
  traits->sampleBuffers=ds->getMultiSamples();
  traits->samples=ds->getNumMultiSamples();
  return new osgQt::GraphicsWindowQt(traits.get());
}
