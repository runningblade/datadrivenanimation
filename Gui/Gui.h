#ifndef GUI_H
#define GUI_H

#include "TreeModel.h"
#include <osg/PositionAttitudeTransform>
#include <osgQt/GraphicsWindowQt>
#include <osgViewer/Viewer>
#include <osg/Material>
#include <QMainWindow>
#include <QTreeView>
#include <QTimer>

class Gui : public QMainWindow
{
  Q_OBJECT
public:
  Gui();
  virtual void createFileMenu();
  virtual void createViewMenu();
  virtual void paintEvent(QPaintEvent* event);
protected slots:
  virtual void expand();
  virtual void collapse();
  virtual void refresh();
  virtual void update();
  virtual void clearScene(osg::Group* group,bool addSphere);
  virtual void resetScene();
  virtual void resetCamera();
  virtual void selectChanged(const QItemSelection& sel,const QItemSelection& item);
protected:
  unsigned int castShadowMask() const;
  unsigned int rcvShadowMask() const;
  osg::Geode* createFloor() const;
  osg::Light* createLight(osg::Vec4 color) const;
  osg::Material* createSimpleMaterial(osg::Vec4 color) const;
  osg::Material* createFullMaterial(osg::Vec4 color) const;
  osg::PositionAttitudeTransform* createLightNode(osg::Vec3 pos,osg::Vec4 color,osg::Node* parent,double radius) const;
  QWidget* addViewWidget(osgQt::GraphicsWindowQt* gw,osg::ref_ptr<osg::Node> scene);
  static osgQt::GraphicsWindowQt* createGraphicsWindow(int x,int y,int w,int h,const std::string& name="",bool windowDecoration=false);
  //data
  QMenu* _file;
  QMenu* _view;
  QTreeView* _explorer;
  osg::ref_ptr<osg::Node> _scene;
  osg::ref_ptr<osgViewer::Viewer> _viewer;
  osgQt::GraphicsWindowQt* _gw;
  QWidget* _renderer;
  TreeModel* _model;
  QTimer _timer;
};

#endif
