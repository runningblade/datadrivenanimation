#ifndef MOCAP_VIEWER_H
#define MOCAP_VIEWER_H

#include "Gui.h"
#include <osg/MatrixTransform>
#include <DataDrivenOpt/SkinnedMeshUtility.h>

USE_PRJ_NAMESPACE

class MocapViewer : public Gui
{
  Q_OBJECT
public:
  MocapViewer();
  virtual void createMocapMenu();
private slots:
  virtual void setFrm(int frm);
  virtual void nextFrm();
  virtual void lastFrm();
  virtual void fasterFrm();
  virtual void slowerFrm();
  virtual void update();
  virtual void selectChanged(const QItemSelection& sel,const QItemSelection& item);
protected:
  std::string findAsfPath(const std::string& amcPath,const string& extension) const;
  osg::MatrixTransform* addBody(const ObjMesh& mesh) const;
  //mocap
  void showSpecification();
  void showAnimation();
  void assignPose();
  osg::MatrixTransform* addBody(const Body& body);
  //skinned
  void showSkinnedSpecification();
  void showSkinnedAnimation();
  void assignSkinnedPose();
  osg::MatrixTransform* addSkinnedBody();
  //data
  QMenu* _mocap;
  QToolBar* _mocapBar;
  QAction* _recompute;
  QAction* _play;
  QSlider* _slider;
  ArticulatedBody _body;
  SkinnedMeshUsingArticulatedBody _skin;
  std::string _asfPath,_sasfbPath;
  std::string _amcPath,_samcbPath;
  osg::ref_ptr<osg::Group> _bodies;
  vector<osg::ref_ptr<osg::MatrixTransform> > _bodyTrans;
  //frm
  sizeType _currFrm;
  sizeType _step;
};

#endif
