#ifndef TREE_ITEM_H
#define TREE_ITEM_H

#include <QList>
#include <QVariant>

class TreeItem
{
public:
  TreeItem(const QList<QVariant>& data,TreeItem* parent=0);
  ~TreeItem();
  void clearChild();
  void appendChild(TreeItem* child);
  TreeItem* child(int row);
  int childCount() const;
  int columnCount() const;
  QVariant data(int column) const;
  int row() const;
  TreeItem* parentItem();
private:
  QList<TreeItem*> _childItems;
  QList<QVariant> _itemData;
  TreeItem* _parentItem;
};


#endif
