#include "MocapViewer.h"
#include "TreeModel.h"
#include <QAction>
#include <QStatusBar>
#include <QMenuBar>
#include <QToolBar>
#include <QSlider>
#include <CommonFile/MakeMesh.h>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem/operations.hpp>
#include <osg/ComputeBoundsVisitor>
#include <osg/ShapeDrawable>
#include <osgDB/ReadFile>

//MocapViewer
MocapViewer::MocapViewer()
{
  _step=1;
  createMocapMenu();
}
void MocapViewer::createMocapMenu()
{
  //create menu
  _mocap=menuBar()->addMenu("&Mocap");
  //create tool bar
  _mocapBar=addToolBar("&Mocap");
  //reset button
  {
    _recompute=new QAction("&Recompute",this);
    _mocap->addAction(_recompute);
    _mocapBar->addAction(_recompute);
    _recompute->setCheckable(true);
    _recompute->setChecked(false);
  }
  _mocap->addSeparator();
  _mocapBar->addSeparator();
  //last button
  {
    QAction* last=new QAction("&Last",this);
    _mocap->addAction(last);
    _mocapBar->addAction(last);
    connect(last,SIGNAL(triggered()),this,SLOT(lastFrm()));
  }
  //slider
  {
    _slider=new QSlider(Qt::Horizontal);
    _mocapBar->addWidget(_slider);
    _slider->setEnabled(false);
    connect(_slider,SIGNAL(valueChanged(int)),this,SLOT(setFrm(int)));
  }
  //next button
  {
    QAction* next=new QAction("&Next",this);
    _mocap->addAction(next);
    _mocapBar->addAction(next);
    connect(next,SIGNAL(triggered()),this,SLOT(nextFrm()));
  }
  //slower
  {
    QAction* slower=new QAction("&Slower",this);
    _mocap->addAction(slower);
    _mocapBar->addAction(slower);
    connect(slower,SIGNAL(triggered()),this,SLOT(slowerFrm()));
  }
  //faster
  {
    QAction* faster=new QAction("&Faster",this);
    _mocap->addAction(faster);
    _mocapBar->addAction(faster);
    connect(faster,SIGNAL(triggered()),this,SLOT(fasterFrm()));
  }
  //play button
  {
    _play=new QAction("&Play",this);
    _mocap->addAction(_play);
    _mocapBar->addAction(_play);
    _play->setCheckable(true);
    _play->setChecked(false);
  }
}
//slots
void MocapViewer::setFrm(int currFrm)
{
  sizeType nrFrm;
  if(!_asfPath.empty())
    nrFrm=_body.nrFrm();
  else if(!_sasfbPath.empty())
    nrFrm=_skin.getBody().nrFrm();
  else return;

  _currFrm=max<int>(0,min<int>(currFrm,nrFrm-1));
  QString msg=QString("At frame: ")+QString(boost::lexical_cast<string>(_currFrm).c_str());
  statusBar()->showMessage(msg,1000);
  _slider->setValue(_currFrm);
}
void MocapViewer::nextFrm()
{
  sizeType nrFrm;
  if(!_asfPath.empty())
    nrFrm=_body.nrFrm();
  else if(!_sasfbPath.empty())
    nrFrm=_skin.getBody().nrFrm();
  else return;

  if(!_bodies || nrFrm <= 0)
    return;
  while(_currFrm < 0)
    _currFrm++;
  _currFrm=(_currFrm+_step)%nrFrm;
  QString msg=QString("At frame: ")+QString(boost::lexical_cast<string>(_currFrm).c_str());
  statusBar()->showMessage(msg,1000);
  _slider->setValue(_currFrm);
}
void MocapViewer::lastFrm()
{
  sizeType nrFrm;
  if(!_asfPath.empty())
    nrFrm=_body.nrFrm();
  else if(!_sasfbPath.empty())
    nrFrm=_skin.getBody().nrFrm();
  else return;

  if(!_bodies || nrFrm <= 0)
    return;
  while(_currFrm < 0)
    _currFrm++;
  _currFrm=(_currFrm+nrFrm-_step)%nrFrm;
  QString msg=QString("At frame: ")+QString(boost::lexical_cast<string>(_currFrm).c_str());
  statusBar()->showMessage(msg,1000);
  _slider->setValue(_currFrm);
}
void MocapViewer::fasterFrm()
{
  sizeType nrFrm;
  if(!_asfPath.empty())
    nrFrm=_body.nrFrm();
  else if(!_sasfbPath.empty())
    nrFrm=_skin.getBody().nrFrm();
  else return;
  _step=min<sizeType>(_step+1,nrFrm-1);
  QString msg=QString("Speed: ")+QString(boost::lexical_cast<string>(_step).c_str());
  statusBar()->showMessage(msg,1000);
}
void MocapViewer::slowerFrm()
{
  _step=max<sizeType>(_step-1,1);
  QString msg=QString("Speed: ")+QString(boost::lexical_cast<string>(_step).c_str());
  statusBar()->showMessage(msg,1000);
}
void MocapViewer::update()
{
  if(!_asfPath.empty())
    assignPose();
  else if(!_sasfbPath.empty())
    assignSkinnedPose();
  else return;

  if(_play->isChecked())
    nextFrm();
  Gui::update();
}
void MocapViewer::selectChanged(const QItemSelection& sel,const QItemSelection& item)
{
  Gui::selectChanged(sel,item);
  if(sel.size() == 0)
    return;
  QModelIndex index=sel.begin()->topLeft();
  if(!index.isValid())
    return;
  QString type=_model->itemData(_model->index(index.row(),1,sel.begin()->parent())).value(0).toString();
  QString path=_model->itemData(_model->index(index.row(),2,sel.begin()->parent())).value(0).toString();
  clearScene(_scene->asGroup(),false);
  _slider->setEnabled(false);
  if(type == CHARACTER_SPECIFICATION) {
    _amcPath=_sasfbPath=_samcbPath="";
    _asfPath=path.toStdString();
    showSpecification();
    return;
  } else if(type == CHARACTER_ANIMATION) {
    _sasfbPath=_samcbPath="";
    const std::string asfPath=findAsfPath(path.toStdString(),"asf");
    if(asfPath.empty()) {
      clearScene(_scene->asGroup(),true);
      return;
    } else if(asfPath != _asfPath) {
      _asfPath=asfPath;
      showSpecification();
    } else {
      ASSERT(_bodies)
      _scene->asGroup()->addChild(_bodies);
    }
    _amcPath=path.toStdString();
    showAnimation();
    return;
  } else if(type == SKINNED_CHARACTER_SPECIFICATION) {
    _samcbPath=_asfPath=_amcPath="";
    _sasfbPath=path.toStdString();
    showSkinnedSpecification();
    return;
  } else if(type == SKINNED_CHARACTER_ANIMATION) {
    _asfPath=_amcPath="";
    const std::string sasfbPath=findAsfPath(path.toStdString(),"sasfb");
    if(sasfbPath.empty()) {
      clearScene(_scene->asGroup(),true);
      return;
    } else if(sasfbPath != _sasfbPath) {
      _sasfbPath=sasfbPath;
      showSkinnedSpecification();
    } else {
      ASSERT(_bodies)
      _scene->asGroup()->addChild(_bodies);
    }
    _samcbPath=path.toStdString();
    showSkinnedAnimation();
    return;
  } else if(type == PROCESSED_DATASET) {
    _sasfbPath=_samcbPath="";
    if(path.toStdString().empty()) {
      clearScene(_scene->asGroup(),true);
      return;
    } else {
      _asfPath=path.toStdString();
      _amcPath=path.toStdString();
      showSpecification();
      showAnimation();
      return;
    }
  }
  clearScene(_scene->asGroup(),true);
}
std::string MocapViewer::findAsfPath(const std::string& amcPath,const string& extension) const
{
  boost::filesystem::path path(amcPath);
  path=path.parent_path();

  boost::filesystem::directory_iterator beg(path),end;
  while(beg!=end) {
    if(beg->path().extension() == ("."+extension) || beg->path().extension() == extension)
      return beg->path().string();
    ++beg;
  }
  return "";
}
osg::MatrixTransform* MocapViewer::addBody(const ObjMesh& mesh) const
{
#define BR 204.0f/255.0f
#define BG 51.0f/255.0f
#define BB 255.0f/255.0f
  osg::Geode* bodyNode=new osg::Geode;
  osg::Geometry* bodyGeom=new osg::Geometry;
  bodyNode->addDrawable(bodyGeom);
  //texture
  osg::StateSet* state=new osg::StateSet();
  state->setAttribute(createFullMaterial(osg::Vec4(BR,BG,BB,1)));
  bodyNode->setStateSet(state);
  //vertices
  osg::Vec3Array* vss=new osg::Vec3Array;
  for(sizeType i=0; i<(sizeType)mesh.getV().size(); i++)
    vss->push_back(osg::Vec3(mesh.getV()[i][0],mesh.getV()[i][1],mesh.getV()[i][2]));
  bodyGeom->setVertexArray(vss);
  //indices
  osg::DrawElementsUInt* iss=new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES,0);
  for(sizeType i=0; i<(sizeType)mesh.getI().size(); i++) {
    (*iss).push_back(mesh.getI(i)[0]);
    (*iss).push_back(mesh.getI(i)[1]);
    (*iss).push_back(mesh.getI(i)[2]);
  }
  bodyGeom->addPrimitiveSet(iss);
  //normals
  osg::Vec3Array* nss=new osg::Vec3Array;
  for(sizeType i=0; i<(sizeType)mesh.getV().size(); i++)
    nss->push_back(osg::Vec3(mesh.getN()[i][0],mesh.getN()[i][1],mesh.getN()[i][2]));
  bodyGeom->setNormalArray(nss);
  bodyGeom->setNormalBinding(osg::Geometry::BIND_PER_VERTEX);
  //colors
  osg::Vec4Array* colors=new osg::Vec4Array;
  for(sizeType i=0; i<(sizeType)mesh.getV().size(); i++)
    colors->push_back(osg::Vec4(1,1,1,1));
  bodyGeom->setColorArray(colors);
  bodyGeom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
  //transform
  osg::MatrixTransform* trans=new osg::MatrixTransform;
  trans->setNodeMask(castShadowMask()|rcvShadowMask());
  trans->addChild(bodyNode);
  return trans;
#undef BR
#undef BG
#undef BB
}
//helper: body
void MocapViewer::showSpecification()
{
  ObjMesh joint;
  MakeMesh::makeSphere3D(joint,0.2f,32);
  boost::filesystem::path pathBinary(_asfPath);
  pathBinary.replace_extension(".asfb");
  boost::filesystem::path pathBinaryData(_asfPath);
  pathBinaryData.replace_extension(".data");
  if(COMMON::exists(pathBinaryData)) {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ifstream is(pathBinaryData,ios::binary);
    SkinnedMeshUtility util;
    util.read(is,dat.get());
    _body=util.getBody().getBody();
  } else if(COMMON::exists(pathBinary) && !_recompute->isChecked()) {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ifstream is(pathBinary,ios::binary);
    _body.read(is,dat.get());
  } else {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ifstream is(_asfPath);
    boost::filesystem::ofstream os(pathBinary,ios::binary);
    _body.readASF(is,joint);
    _body.write(os,dat.get());
  }
  //_body.writeFrameVTK("mesh.vtk");

  //show
  _bodies=new osg::Group;
  _bodyTrans.assign(_body.nrBody(),NULL);
  _scene->asGroup()->addChild(_bodies);
  for(sizeType i=0; i<_body.nrBody(); i++) {
    const Body& b=_body.findBody(i);
    if(b.hasMesh()) {
      _bodyTrans[i]=addBody(b);
      _bodies->addChild(_bodyTrans[i]);
    }
  }
  _currFrm=-1;
  assignPose();
}
void MocapViewer::showAnimation()
{
  boost::filesystem::path pathBinary(_amcPath);
  pathBinary.replace_extension(".amc");
  boost::filesystem::path pathBinaryData(_amcPath);
  pathBinaryData.replace_extension(".data");
  if(COMMON::exists(pathBinary)) {
    boost::filesystem::ifstream is(pathBinary);
    _body.clearFrm();
    _body.readAMC(is);
  } else if(COMMON::exists(pathBinaryData)) {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ifstream is(pathBinaryData,ios::binary);
    SkinnedMeshUtility util;
    util.read(is,dat.get());
    _body.clearFrm();
    _body.setFrm(util.getFrm());
  }
  //_body.writeFrameSeqVTK("anim");
  _play->setChecked(false);
  _slider->setEnabled(true);
  _slider->setRange(0,_body.nrFrm()-1);
  _slider->setValue(_currFrm=0);
}
void MocapViewer::assignPose()
{
  if(!_bodies)
    return;
  Mat4 mT,T0=Mat4::Identity();
  T0.block<3,3>(0,0)=makeRotation<scalar>(Vec3(M_PI/2,0,0));
  //assign transform
  BodyOpT<Body> trans(_body.nrBody());
  if(_currFrm < 0 || _currFrm >= _body.nrFrm())
    _body.transform(_body.root(),trans,NULL);
  else {
    Cold dof=_body.frm().col(_currFrm);
    _body.transform(_body.root(),trans,&dof);
  }
  osg::Matrix m=osg::Matrix().identity();
  for(sizeType i=0; i<(sizeType)_bodyTrans.size(); i++)
    if(_bodyTrans[i]) {
      mT=T0*trans.getTrans(i);
      for(int r=0; r<4; r++)
        for(int c=0; c<4; c++)
          m(r,c)=mT(c,r);
      _bodyTrans[i]->setMatrix(m);
    }
  //compute bounding box
  osg::ComputeBoundsVisitor cbv;
  _bodies->accept(cbv);
  osg::BoundingBox bb=cbv.getBoundingBox();
  T0(2,3)=-bb._min[2];
  //move whole body
  for(sizeType i=0; i<(sizeType)_bodyTrans.size(); i++)
    if(_bodyTrans[i]) {
      mT=T0*trans.getTrans(i);
      for(int r=0; r<4; r++)
        for(int c=0; c<4; c++)
          m(r,c)=mT(c,r);
      _bodyTrans[i]->setMatrix(m);
    }
}
osg::MatrixTransform* MocapViewer::addBody(const Body& body)
{
  ObjMesh mesh;
  const StaticGeomCell& cell=body.mesh();
  cell.getMesh(mesh,true);
  mesh.smooth();
  return addBody(mesh);
}
//helper: skin
void MocapViewer::showSkinnedSpecification()
{
  boost::shared_ptr<IOData> dat=getIOData();
  boost::filesystem::ifstream is(_sasfbPath,ios::binary);
  _skin.read(is,dat.get());

  //show
  _bodies=addSkinnedBody();
  _bodyTrans.clear();
  _scene->asGroup()->addChild(_bodies);
  _currFrm=-1;
  assignSkinnedPose();
}
void MocapViewer::showSkinnedAnimation()
{
  boost::filesystem::ifstream is(_samcbPath,ios::binary);
  boost::shared_ptr<IOData> dat=getIOData();
  _skin.getBody().clearFrm();
  _skin.getBody().readFrm(is,dat.get());
  //_body.writeFrameSeqVTK("anim");
  _play->setChecked(false);
  _slider->setEnabled(true);
  _slider->setRange(0,_skin.getBody().nrFrm()-1);
  _slider->setValue(_currFrm=0);
}
void MocapViewer::assignSkinnedPose()
{
  //move vertex
  ObjMesh mesh;
  if(_currFrm < 0 || _currFrm >= _skin.getBody().nrFrm())
    _skin.getMesh(Cold::Zero(_skin.nrTheta()),NULL,NULL,NULL,NULL,&mesh,true);
  else _skin.getMesh(_skin.getBody().frm().col(_currFrm),NULL,NULL,NULL,NULL,&mesh,true);
  osg::Drawable* geom=_bodies->asGroup()->getChild(0)->asGeode()->getDrawable(0);
  //compute vertex array
  osg::Vec3Array* vss=new osg::Vec3Array;
  for(sizeType i=0; i<(sizeType)mesh.getV().size(); i++)
    vss->push_back(osg::Vec3(mesh.getV()[i][0],mesh.getV()[i][1],mesh.getV()[i][2]));
  geom->asGeometry()->setVertexArray(vss);
  //compute normal array//normals
  osg::Vec3Array* nss=new osg::Vec3Array;
  for(sizeType i=0; i<(sizeType)mesh.getV().size(); i++)
    nss->push_back(osg::Vec3(mesh.getN()[i][0],mesh.getN()[i][1],mesh.getN()[i][2]));
  geom->asGeometry()->setNormalArray(nss);
  geom->asGeometry()->setNormalBinding(osg::Geometry::BIND_PER_VERTEX);
  //compute bounding box
  Mat4 T0=Mat4::Identity();
  T0.block<3,3>(0,0)=makeRotation<scalar>(Vec3(M_PI/2,0,0));
  T0(2,3)=-mesh.getBB()._minC[1];
  //move whole body
  osg::Matrix m=osg::Matrix().identity();
  for(int r=0; r<4; r++)
    for(int c=0; c<4; c++)
      m(r,c)=T0(c,r);
  _bodies->asTransform()->asMatrixTransform()->setMatrix(m);
}
osg::MatrixTransform* MocapViewer::addSkinnedBody()
{
  ObjMesh mesh;
  _skin.getMesh(Cold::Zero(_skin.nrTheta()),NULL,NULL,NULL,NULL,&mesh,true);
  return addBody(mesh);
}
