#ifndef TREE_MODEL_H
#define TREE_MODEL_H

#include <QAbstractItemModel>
#include <QVariant>

#define CHARACTER_SPECIFICATION tr("Character Specification")
#define CHARACTER_ANIMATION tr("Character Animation")
#define SKINNED_CHARACTER_SPECIFICATION tr("Skinned Character Specification")
#define SKINNED_CHARACTER_ANIMATION tr("Skinned Character Animation")
#define PROCESSED_DATASET tr("Processed Dataset")
#define FOLDER tr("Folder")

class TreeItem;
class TreeModel : public QAbstractItemModel
{
  Q_OBJECT
public:
  TreeModel(const QString& data="",QObject* parent=0);
  ~TreeModel();
  QVariant data(const QModelIndex& index,int role) const override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;
  QVariant headerData(int section,Qt::Orientation orientation,int role=Qt::DisplayRole) const override;
  QModelIndex index(int row,int column,const QModelIndex& parent=QModelIndex()) const override;
  QModelIndex parent(const QModelIndex& index) const override;
  int rowCount(const QModelIndex& parent=QModelIndex()) const override;
  int columnCount(const QModelIndex& parent=QModelIndex()) const override;
  void update(const QString& path=tr("./"),TreeItem* rootItem=NULL);
private:
  QString getDescriptor(const QString& path) const;
  TreeItem* _rootItem;
};

#endif
