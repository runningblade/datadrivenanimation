#include "TreeModel.h"
#include "TreeItem.h"
#include <QStringList>
#include <boost/filesystem/operations.hpp>

TreeModel::TreeModel(const QString& data,QObject* parent)
  :QAbstractItemModel(parent)
{
  QStringList headers;
  headers << tr("Title") << tr("Description") << tr("Path");

  QList<QVariant> rootData;
  foreach(QString header,headers)
  rootData << header;

  _rootItem=new TreeItem(rootData);
  update(data);
}
TreeModel::~TreeModel()
{
  delete _rootItem;
}
QVariant TreeModel::data(const QModelIndex& index,int role) const
{
  if(!index.isValid())
    return QVariant();
  if(role!=Qt::DisplayRole)
    return QVariant();
  TreeItem* item=static_cast<TreeItem*>(index.internalPointer());
  return item->data(index.column());
}
Qt::ItemFlags TreeModel::flags(const QModelIndex& index) const
{
  if (!index.isValid())
    return 0;
  return QAbstractItemModel::flags(index);
}
QVariant TreeModel::headerData(int section,Qt::Orientation orientation,int role) const
{
  if(orientation==Qt::Horizontal && role==Qt::DisplayRole)
    return _rootItem->data(section);
  return QVariant();
}
QModelIndex TreeModel::index(int row,int column,const QModelIndex& parent) const
{
  if(!hasIndex(row,column,parent))
    return QModelIndex();
  TreeItem* parentItem;
  if(!parent.isValid())
    parentItem=_rootItem;
  else parentItem=static_cast<TreeItem*>(parent.internalPointer());
  TreeItem* childItem=parentItem->child(row);
  if(childItem)
    return createIndex(row,column,reinterpret_cast<void*>(childItem));
  else return QModelIndex();
}
QModelIndex TreeModel::parent(const QModelIndex& index) const
{
  if(!index.isValid())
    return QModelIndex();
  TreeItem* childItem=static_cast<TreeItem*>(index.internalPointer());
  TreeItem* parentItem=childItem->parentItem();
  if(parentItem==_rootItem)
    return QModelIndex();
  return createIndex(parentItem->row(),0,parentItem);
}
int TreeModel::rowCount(const QModelIndex& parent) const
{
  TreeItem* parentItem;
  if(parent.column()>0)
    return 0;
  if(!parent.isValid())
    parentItem=_rootItem;
  else parentItem=static_cast<TreeItem*>(parent.internalPointer());
  return parentItem->childCount();
}
int TreeModel::columnCount(const QModelIndex& parent) const
{
  if(parent.isValid())
    return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
  else return _rootItem->columnCount();
}
void TreeModel::update(const QString& path,TreeItem* rootItem)
{
  if(!rootItem) {
    rootItem=_rootItem;
    _rootItem->clearChild();
  }
  boost::filesystem::directory_iterator beg(path.toStdString()),end;
  while(beg!=end) {
    if(boost::filesystem::is_regular_file(*beg)) {
      QList<QVariant> data;
      data << beg->path().filename().string().c_str();
      data << getDescriptor(beg->path().extension().string().c_str());
      data << beg->path().string().c_str();
      TreeItem* item=new TreeItem(data,rootItem);
      rootItem->appendChild(item);
    } else if(boost::filesystem::is_directory(*beg)) {
      QList<QVariant> data;
      data << beg->path().filename().string().c_str();
      data << getDescriptor(beg->path().extension().string().c_str());
      data << beg->path().string().c_str();
      TreeItem* item=new TreeItem(data,rootItem);
      rootItem->appendChild(item);
      update(QString(beg->path().string().c_str()),item);
    }
    ++beg;
  }
}
QString TreeModel::getDescriptor(const QString& path) const
{
  if(path.toStdString() == ".asf" || path.toStdString() == "asf")
    return CHARACTER_SPECIFICATION;
  else if(path.toStdString() == ".amc" || path.toStdString() == "amc")
    return CHARACTER_ANIMATION;
  if(path.toStdString() == ".sasfb" || path.toStdString() == "sasfb")
    return SKINNED_CHARACTER_SPECIFICATION;
  else if(path.toStdString() == ".samcb" || path.toStdString() == "samcb")
    return SKINNED_CHARACTER_ANIMATION;
  else if(path.toStdString() == ".data" || path.toStdString() == "data")
    return PROCESSED_DATASET;
  else if(path.toStdString() == "")
    return FOLDER;
  else return tr("Unknown File Format");
}
