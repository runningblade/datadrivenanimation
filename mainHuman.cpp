#include <Gui/Gui.h>
#include <Gui/MocapViewer.h>
#include <QApplication>

int main(int argc, char *argv[]) {
  QApplication app(argc,argv);
  MocapViewer window;
  window.show();
  return app.exec();
}
